# Infrastructure de Madu

![Stacks](https://miro.medium.com/proxy/1*JIaba979mIQ_A3IBuOncFQ.jpeg)

## What's in the box ?

## Docker

Dans ce projet, nous avons un client et une api.
Chaque partie dispose d'un Dockerfile à la racine qui vont permettent de construire une image Docker, étape par étape. Dans chacun, on ignore les node_modules.

Afin de pouvoir travailler dans un même environnement en local, un docker-compose.yml est poser à la racine de l'api (madu-api). Il va nous permettre de pouvoir gérer plusieurs conteneurs comme un ensemble de services inter-connectés.

Les services :

- mongo, notre base de données.
- mongo-express, une interface qui nous permet d'administrer notre base de données.
- l'API.

> On a un service en plus pour la production, celui du client.

## Terraform

Ici terraform, nous permet de déployer notre infrastructure sur AWS. Plus précisément, déployer nos instances (une pour la preprod et l'autre pour la prod).

Les différentes ressources utilisées:

- Provider aws avec une région eu-west-2.
- AWS key pair pour contrôler l'accès aux instances EC2.
- Data pour demander à l'API AWS de récupérer le dernier AWS AMI ID pour notre instance.
- Modules qui nous permettent d'utiliser une seule ressource aws_instance et de la configurer dans les modules pour avoir plusieurs instances.
- Instance EC2 de type t2.micro - Ubuntu 18.04.
- Elastic Load Balancer qui répartie automatiquement le traffic sur les instances de production.
- Groupe de sécurité avec comme connexions entrantes: le serveur, le client, l'interface d'administration de la base de données et le port ssh. Toutes les connexions sortantes sont autorisées.
- Virtual Private Cloud (vpc).


## Ansible

Nous utilisons ici Ansible pour automatiser les installations, le déploiement et la gestion de nos serveurs.

Notre inventaire est dynamique, il va nous permettre de récupérer via l'AWS APIs nos groupes de machines que l'on a tagger dans notre configuration terraform.

On a deux playbooks, qui décrivent les tâches à éxécuter sur nos serveurs (un pour la preprod et l'autre pour la prod).

Les rôles:

- geerlingguy.docker (installer docker sur notre serveur)
- common (installer python et les pip packages: docker / docker-compose)
- application_deployment (deployer l'application)

Ansible vault est utiliser dans le dossier playbook pour crypter les infos de la base de données.


## How to play with ?

## Deployer nos instances EC2 avec Terraform

Tout d'abord, il faut installer [Terraform](https://www.terraform.io/downloads.html).

Ensuite, génerer une clé ssh, c'est important car lors du terraform apply, on demandera le nom de la clé publique dans le shell qui est variabilisé.

```bash
ssh-keygen -t rsa -b 4096
```

Pour pouvoir déployer nos instances, il nous faudra crée des credentials depuis notre compte AWS. Puis en faire des variables d'environnement, elles seront automatiquement appeler dans le provider:

```bash
export AWS_ACCESS_KEY_ID="..."
export AWS_SECRET_ACCESS_KEY="..."
```

Enfin, dans le dossier terraform :

```bash
terraform init
terraform apply
```

Il nous faudra par la suite indiquer le chemin vers la clé publique que l'on crée juste avant.

Si l'on veut supprimer toutes les ressources que l'on a ajouter avec terraform:

```bash
terraform destroy
```

## Automatisation de la configuration de l'infrastructure avec Ansible

Pareil que Terraform, il faut installer [Ansible](https://www.ansible.com/).

Dans l'inventaire, qui est dynamique on utilise un script communautaire ec2.py afin de récupérer les groupes de machines sur notre AWS. Dans ce script, des modules sont nécéssaires, un fichier requirements.txt est disponible dans:

```bash
infrastructure/ansible/inventory
```

Il faut lancer la commande ci-dessous pour installer les modules:

```bash
pip install -r requirements.txt
```

> Dans le fichier ec2.ini, si la région n'est pas à eu-west-2, pensez à le passer car le script n'est pas éxécutable dans une autre zone.

Ensuite pour déployer une instance automatiquement, il suffit d'éxécuter cette commande: 

```bash
ansible-playbook -i inventory/ec2.py playbooks/application_production.yml --user ubuntu --key ~/.ssh/id_rsa -e "application_docker_tag=cedad5b9dd2aa5a827066a617bc2f4" --ask-vault-pass --become
```

Avec:

- inventory/ec2.py qui va récupérer l'ipv4 de nos instances ainsi que leurs tag name.
- playbooks/application_production qui va éxécuter une liste de tâches qui permettent le deploiment de l'application.
- --key ~/.ssh/id_rsa qui correspond au chemin de notre clé privée.
- -e application_docker_tag qui permet de spécifier une variable d'environmment qui correspond à la version de notre application.
- --ask-vault-pass pour saisir le mot de passe du fichier avec les credentials MySQL.
- --become pour éxécuter les tâches en mode admin.

## GITLAB CI

À la racine du projet, un manifest permet lors de l'ajout d'une nouvelle feature sur la branch master de déployer automiquement sur la production la nouvelle version de l'application.

Ce qu'il fait:
- lance les tests de l'api.
- build/push le client et l'api sur docker hub.
- deploy sur une instance passé en variable d'environnement l'application grâce à Ansible.