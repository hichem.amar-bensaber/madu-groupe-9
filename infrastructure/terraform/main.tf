data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

resource "aws_key_pair" "ssh_key_madu" {
  key_name   = "ssh_key_madu"
  public_key = file(var.ssh_public_key_file)
}

module "production" {
  source = "./application"

  instance_type          = "t2.micro"
  instance_ami           = data.aws_ami.ubuntu.id
  client_instance_count  = 2
  api_instance_count     = 1
  instance_key_name      = aws_key_pair.ssh_key_madu.key_name
  stage                  = "production"
}