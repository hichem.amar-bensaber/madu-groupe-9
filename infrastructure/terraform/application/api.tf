resource "aws_instance" "api" {
  ami           = var.instance_ami
  instance_type = var.instance_type
  count         = var.api_instance_count

  key_name        = var.instance_key_name
  security_groups = [aws_security_group.api.name]

  tags = {
    ansible_machine = "${var.stage}_api"
    component       = "api"
    stage           = var.stage
  }
}

resource "aws_security_group" "api" {
  name        = "${var.stage}-api-security-group"
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port       = 27017
    to_port         = 27017
    protocol        = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_elb" "api_back" {
  name               = "${var.stage}-api-back-elb"
  availability_zones = ["eu-west-2a", "eu-west-2b", "eu-west-2c"]

  listener {
    instance_port     = 8080
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    interval            = 30
    target              = "HTTP:8080/"
    timeout             = 3
    unhealthy_threshold = 2
  }

  instances                   = aws_instance.api.*.id
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "${var.stage}_api_back_elb"
  }
}

resource "aws_elb" "api_database" {
  name               = "${var.stage}-api-database-elb"
  availability_zones = ["eu-west-2a", "eu-west-2b", "eu-west-2c"]

  listener {
    instance_port     = 8081
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  listener {
    instance_port     = 27017
    instance_protocol = "http"
    lb_port           = 27017
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    interval            = 30
    target              = "HTTP:8081/"
    timeout             = 3
    unhealthy_threshold = 2
  }

  instances                   = aws_instance.api.*.id
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "${var.stage}_api_database_elb"
  }
}
