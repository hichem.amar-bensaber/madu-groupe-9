define({ "api": [
  {
    "type": "post",
    "url": "/challenges/create",
    "title": "Create new challenge",
    "name": "CreateChallenge",
    "group": "Challenges",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "create",
            "description": "<p>The challenge create object data.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.name",
            "description": "<p>The challenge name field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "create.description",
            "description": "<p>The challenge description field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.type",
            "description": "<p>The challenge type field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.points",
            "description": "<p>The challenge points field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.duration",
            "description": "<p>The challenge duration field.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Challenge create request",
          "content": "{\n    \"create\": {\n        \"name\": \"Test\",\n        \"description\": \"Test\",\n        \"type\": \"Test\",\n        \"points\": 20,\n        \"duration\": 1589466386\n    }\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>The challenge ID of the challenge who has just been created.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Challenge Created",
          "content": "HTTPS/1.1 201 OK\n{\n    \"success\": true,\n    \"_id\": \"5e3dd2d51d9f190609e8258c\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "errors",
            "description": "<p>Errors object, handleAPIError() manage Mongodb errors.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Required value",
          "content": "HTTPS/1.1 400 Not Found\n{\n    \"success\": false,\n    \"errors\": {\n        \"type\": {\n            \"path\": \"type\",\n            \"kind\": \"required\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Create object null",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Create object is null.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to create a new challenge from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/challenges.js",
    "groupTitle": "Challenges"
  },
  {
    "type": "delete",
    "url": "/challenges/:id/delete",
    "title": "Delete challenge",
    "name": "DeleteChallenge",
    "group": "Challenges",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The challenge ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Challenge deleted",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"Challenge is deleted.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Challenge doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Challenge doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Challenge ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Challenge id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to delete a challenge from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/challenges.js",
    "groupTitle": "Challenges"
  },
  {
    "type": "get",
    "url": "/challenges/:id",
    "title": "Get challenge",
    "name": "GetChallenge",
    "group": "Challenges",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The challenge ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>The challenge data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Challenge Data",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": {\n        \"name\": \"Test\",\n        \"description\": \"Test\",\n        \"type\": \"Test\",\n        \"points\": 20,\n        \"duration\": 1589466386\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Challenge doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n    \"success\": false,\n    \"message\": \"Challenge doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Challenge ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n    \"success\": false,\n    \"message\": \"Challenge id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to retrieve a challenge from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/challenges.js",
    "groupTitle": "Challenges"
  },
  {
    "type": "get",
    "url": "/challenges/list",
    "title": "Get challenges list",
    "name": "GetListChallenges",
    "group": "Challenges",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "selector",
            "description": "<p>The challenge selector object.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "100",
            "description": "<p>The object capture limit.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "defaultValue": "0",
            "description": "<p>The number of objects to ignore.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "sort",
            "defaultValue": "{\"createdAt\":-1}",
            "description": "<p>Sort result by specific field, 1 = ASC, -1 = DESC.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "isDeleted",
            "defaultValue": "false",
            "description": "<p>Returns only objects that have been not deleted.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Challenges list request",
          "content": "{\n    \"selector\": {\n       \"name\": \"Test\"\n    },\n    \"sort\": {\n       \"name\": 1\n    },\n    \"limit\": 1000,\n    \"isDeleted\": false\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>The challenges list data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Challenges list data",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": [\n        {\n            \"name\": \"Test_1\",\n            \"description\": \"Test_1\",\n            \"type\": \"Test_1\",\n            \"points\": 20,\n            \"duration\": 1589466386\n        },\n        {\n            \"name\": \"Test_2\",\n            \"description\": \"Test_2\",\n            \"type\": \"Test_2\",\n            \"points\": 40,\n            \"duration\": 1589466386\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Challenges list empty",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Challenges list is empty.\"\n}",
          "type": "json"
        },
        {
          "title": "Challenges list selector null",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Selector object is null.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to retrieve a challenges list from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/challenges.js",
    "groupTitle": "Challenges"
  },
  {
    "type": "patch",
    "url": "/challenges/:id/restore",
    "title": "Restore challenge",
    "name": "RestoreChallenge",
    "group": "Challenges",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The challenge ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Challenge restored",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"Challenge is restored.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Challenge doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Challenge doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Challenge ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Challenge id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to restore a challenge from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/challenges.js",
    "groupTitle": "Challenges"
  },
  {
    "type": "put",
    "url": "/challenges/:id/update",
    "title": "Update challenge",
    "name": "UpdateChallenge",
    "group": "Challenges",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The challenge ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "update",
            "description": "<p>The challenge update object data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Challenge update request",
          "content": "{\n    \"update\": {\n        \"points\": 200\n    }\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Challenge updated",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"Challenge is updated.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Challenge doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Challenge doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Update object null",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Update object is null.\"\n}",
          "type": "json"
        },
        {
          "title": "Challenge ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Challenge id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to update a challenge from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/challenges.js",
    "groupTitle": "Challenges"
  },
  {
    "type": "post",
    "url": "/clients/check-email-suffix",
    "title": "Check if email suffix exist or not",
    "name": "CheckEmailSuffix",
    "group": "Clients",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email_suffix",
            "description": "<p>The email suffix to check.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Check email suffix request",
          "content": "{\n    \"email_suffix\": \"test.com\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>The client retrieved with the email suffix</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Email suffix exists",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": {\n        \"name\": \"Test\",\n        \"localisation\": {\n            \"city\": \"Test\",\n            \"address\": \"Test\",\n            \"zip_code\": \"00000\"\n        },\n        \"coords\": {\n            \"longitude\": 39.3535,\n            \"latitude\": 4.3535\n        }\n        \"phone\": \"0666666666\",\n        \"email\": \"test@test.com\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Required value",
          "content": "HTTPS/1.1 400 Not Found\n{\n    \"success\": false,\n    \"message\": \"You did not sent the email suffix to verify.\"\n}",
          "type": "json"
        },
        {
          "title": "The email suffix does not exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"The email suffix test.com doesn't exist.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to check if the email suffix already exists in the database or not.</p>",
    "version": "0.0.0",
    "filename": "routes/clients.js",
    "groupTitle": "Clients"
  },
  {
    "type": "post",
    "url": "/clients/create",
    "title": "Create new client",
    "name": "CreateClient",
    "group": "Clients",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "create",
            "description": "<p>The client create object data.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.name",
            "description": "<p>The client name field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "create.localisation",
            "description": "<p>The client localisation field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.localisation.city",
            "description": "<p>The client localisation.city field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.localisation.address",
            "description": "<p>The client localisation.address field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.localisation.zip_code",
            "description": "<p>The client localisation.zip_code field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": true,
            "field": "create.coords",
            "description": "<p>The client coords field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "create.coords.longitude",
            "description": "<p>The client coords.longitude field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "create.coords.latitude",
            "description": "<p>The client coords.latitude field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.phone",
            "description": "<p>The client phone field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.email",
            "description": "<p>The client email field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": true,
            "field": "create.profile_pic",
            "defaultValue": "images/profile-pic/user-default.jpeg",
            "description": "<p>The client profile picture field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.profile_pic.uri",
            "description": "<p>The image data, only base64.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Client create request",
          "content": "{\n    \"create\": {\n        \"name\": \"Test\",\n        \"localisation\": {\n            \"city\": \"Test\",\n            \"address\": \"Test\",\n            \"zip_code\": \"00000\"\n        },\n        \"phone\": \"0666666666\",\n        \"email\": \"test@test.com\",\n        \"profile_pic\": {\n            \"uri\": \"data:image/jpeg;base64,\"\n        }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>The client ID of the client who has just been created.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Client Created",
          "content": "HTTPS/1.1 201 OK\n{\n    \"success\": true,\n    \"_id\": \"5e3dd2d51d9f190609e8258c\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "errors",
            "description": "<p>Errors object, handleAPIError() manage Mongodb errors.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Required value",
          "content": "HTTPS/1.1 400 Not Found\n{\n    \"success\": false,\n    \"errors\": {\n        \"email\": {\n            \"path\": \"email\",\n            \"kind\": \"required\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Create object null",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Create object is null.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to create a new client from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/clients.js",
    "groupTitle": "Clients"
  },
  {
    "type": "delete",
    "url": "/clients/:id/delete",
    "title": "Delete client",
    "name": "DeleteClient",
    "group": "Clients",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The client ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Client deleted",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"Client is deleted.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Client doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Client doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Client ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Client id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to delete a client from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/clients.js",
    "groupTitle": "Clients"
  },
  {
    "type": "get",
    "url": "/clients/:id",
    "title": "Get client",
    "name": "GetClient",
    "group": "Clients",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The client ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>The client data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Client Data",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": {\n        \"name\": \"Test\",\n        \"localisation\": {\n            \"city\": \"Test\",\n            \"address\": \"Test\",\n            \"zip_code\": \"00000\"\n        },\n        \"coords\": {\n            \"longitude\": 39.3535,\n            \"latitude\": 4.3535\n        }\n        \"phone\": \"0666666666\",\n        \"email\": \"test@test.com\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Client doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n    \"success\": false,\n    \"message\": \"Client doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Client ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n    \"success\": false,\n    \"message\": \"Client id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to retrieve a client from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/clients.js",
    "groupTitle": "Clients"
  },
  {
    "type": "get",
    "url": "/clients/list",
    "title": "Get clients list",
    "name": "GetListClients",
    "group": "Clients",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "selector",
            "description": "<p>The client selector object.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "100",
            "description": "<p>The object capture limit.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "defaultValue": "0",
            "description": "<p>The number of objects to ignore.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "sort",
            "defaultValue": "{\"createdAt\":-1}",
            "description": "<p>Sort result by specific field, 1 = ASC, -1 = DESC.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "isDeleted",
            "defaultValue": "false",
            "description": "<p>Returns only objects that have been not deleted.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Clients list request",
          "content": "{\n    \"selector\": {\n       \"name\": \"Test\"\n    },\n    \"sort\": {\n       \"name\": 1\n    },\n    \"limit\": 1000,\n    \"isDeleted\": false\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>The clients list data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Clients list data",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": [\n        {\n            \"name\": \"Test_1\",\n            \"localisation\": {\n                \"city\": \"Test_1\",\n                \"address\": \"Test_1\",\n                \"zip_code\": \"00000\"\n            },\n            \"coords\": {\n                \"longitude\": 39.3535,\n                \"latitude\": 4.3535\n            }\n            \"phone\": \"0666666666\",\n            \"email\": \"test_1@test.com\"\n        },\n        {\n            \"name\": \"Test_2\",\n            \"localisation\": {\n                \"city\": \"Test_2\",\n                \"address\": \"Test_2\",\n                \"zip_code\": \"00000\"\n            },\n            \"coords\": {\n                \"longitude\": 39.3535,\n                \"latitude\": 4.3535\n            }\n            \"phone\": \"0666666666\",\n            \"email\": \"test_2@test.com\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Clients list empty",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Clients list is empty.\"\n}",
          "type": "json"
        },
        {
          "title": "Clients list selector null",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Selector object is null.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to retrieve a clients list from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/clients.js",
    "groupTitle": "Clients"
  },
  {
    "type": "patch",
    "url": "/clients/:id/restore",
    "title": "Restore client",
    "name": "RestoreClient",
    "group": "Clients",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The client ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Client restored",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"Client is restored.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Client doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Client doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Client ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Client id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to restore a client from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/clients.js",
    "groupTitle": "Clients"
  },
  {
    "type": "put",
    "url": "/clients/:id/update",
    "title": "Update client",
    "name": "UpdateClient",
    "group": "Clients",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The client ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "update",
            "description": "<p>The client update object data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Client update request",
          "content": "{\n    \"update\": {\n        \"email\": \"test_3@test.com\"\n        \"profile_pic\": {\n             \"uri\": \"data:image/jpeg;base64,\"\n         }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Client updated",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"Client is updated.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Client doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Client doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Update object null",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Update object is null.\"\n}",
          "type": "json"
        },
        {
          "title": "Client ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Client id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to update a client from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/clients.js",
    "groupTitle": "Clients"
  },
  {
    "type": "post",
    "url": "/login",
    "title": "Login",
    "name": "Login",
    "group": "Login",
    "permission": [
      {
        "name": "SA (SuperAdmin), A (Admin), C (Customer)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>The user e-mail.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>The user password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Login",
          "content": "{\n    \"email\": \"test@test.com\",\n    \"password\": \"azerty\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>The user token, necessary to access the API levels. You must insert the token in the authorization header of your request example: &quot;Authorization&quot;: &quot;Bearer INSERT_YOUR_TOKEN&quot;.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success Login",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlM2RkMmQ1MWQ5ZjE5MDYwOWU4MjU4YyIsImlhdCI6MTU4MTkyOTE3NiwiZXhwIjoxNjEzNDg2MTAyfQ.2i-UFUEdZSl9hH5AcECMMjlYioJ7OBR4fPEx-yFhbWI\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Incorrect email or password",
          "content": "HTTPS/1.1 400 Not Found\n{\n    \"success\": false,\n    \"message\": \"Incorrect email or password.\"\n}",
          "type": "json"
        },
        {
          "title": "Login data null",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Login data is null.\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/auth.js",
    "groupTitle": "Login"
  },
  {
    "type": "post",
    "url": "/quizzes/create",
    "title": "Create new quiz",
    "name": "CreateQuiz",
    "group": "Quizzes",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "create",
            "description": "<p>The quiz create object data.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.name",
            "description": "<p>The quiz name</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "create.list",
            "description": "<p>The quiz questions</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.list.question",
            "description": "<p>The quiz question name</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "create.list.points",
            "defaultValue": "50",
            "description": "<p>The quiz question points</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "create.list.answers",
            "description": "<p>The quiz answers</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.list.answers.answer",
            "description": "<p>The quiz answer name</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "create.list.answers.is_answer",
            "defaultValue": "false",
            "description": "<p>Is this answer the correct one or not</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.list.answers.bad_answer_text",
            "description": "<p>A text for the user if this answer is the wrong one</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Quiz create request",
          "content": "{\n    \"create\": {\n        \"name\": \"La nature\",\n            \"list\": [\n                {\n                    \"points\": 50,\n                    \"answers\": [\n                        {\n                            \"is_answer\": false,\n                            \"answer\": \"1\",\n                            \"bad_answer_text\": \"blablabla\"\n                        },\n                        {\n                            \"is_answer\": false,\n                            \"answer\": \"2\",\n                            \"bad_answer_text\": \"blablabla\"\n                        },\n                        {\n                            \"is_answer\": true,\n                            \"answer\": \"3\"\n                        }\n                    ]\n                }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>The quiz ID of the quiz who has just been created.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Quiz Created",
          "content": "HTTPS/1.1 201 OK\n{\n    \"success\": true,\n    \"_id\": \"5e3dd2d51d9f190609e8258c\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "errors",
            "description": "<p>Errors object, handleAPIError() manage Mongodb errors.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Required value",
          "content": "HTTPS/1.1 400 Not Found\n{\n    \"success\": false,\n    \"errors\": {\n        \"name\": {\n            \"path\": \"name\",\n            \"kind\": \"required\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Create object null",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Create object is null.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to create a new quiz in the database.</p>",
    "version": "0.0.0",
    "filename": "routes/quizzes.js",
    "groupTitle": "Quizzes"
  },
  {
    "type": "delete",
    "url": "/quizzes/:id/delete",
    "title": "Delete quiz",
    "name": "DeleteQuiz",
    "group": "Quizzes",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The quiz ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Quiz deleted",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"Quiz is deleted.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Quiz doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Quiz doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Quiz ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Quiz id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to delete a quiz from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/quizzes.js",
    "groupTitle": "Quizzes"
  },
  {
    "type": "get",
    "url": "/quizzes/list",
    "title": "Get quizzes list",
    "name": "GetListQuizzes",
    "group": "Quizzes",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "selector",
            "description": "<p>The client selector object.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "100",
            "description": "<p>The object capture limit.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "defaultValue": "0",
            "description": "<p>The number of objects to ignore.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "sort",
            "defaultValue": "{\"createdAt\":-1}",
            "description": "<p>Sort result by specific field, 1 = ASC, -1 = DESC.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "isDeleted",
            "defaultValue": "false",
            "description": "<p>Returns only objects that have been not deleted.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Clients list request",
          "content": "{\n    \"selector\": {\n       \"name\": \"Test\"\n    },\n    \"sort\": {\n       \"name\": 1\n    },\n    \"limit\": 1000,\n    \"isDeleted\": false\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>The quizzes list data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Clients list data",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"data\":[\n    {\n        \"_id\": \"5f033bbcc30d07001fd577cb\",\n        \"name\": \"La nature\",\n        \"list\": [\n            {\n                \"points\": 50,\n                \"answers\": [\n                    {\n                        \"is_answer\": false,\n                        \"answer\": \"1\",\n                        \"bad_answer_text\": \"blablabla\"\n                    },\n                    {\n                        \"is_answer\": false,\n                        \"answer\": \"2\",\n                        \"bad_answer_text\": \"blablabla\"\n                    },\n                    {\n                        \"is_answer\": true,\n                        \"answer\": \"3\"\n                    }\n                ],\n                \"_id\": \"5f033bbcc30d07001fd577cc\",\n                \"question\": \"Yolo\"\n            },\n            {\n                \"points\": 50,\n                \"answers\": [\n                    {\n                        \"is_answer\": false,\n                        \"answer\": \"1\",\n                        \"bad_answer_text\": \"blablabla\"\n                    },\n                    {\n                        \"is_answer\": false,\n                        \"answer\": \"2\",\n                        \"bad_answer_text\": \"blablabla\"\n                    },\n                    {\n                        \"is_answer\": true,\n                        \"answer\": \"3\"\n                    }\n                ],\n                \"_id\": \"5f033bbcc30d07001fd577cd\",\n                \"question\": \"Yolo 2\"\n            }\n        ],\n        \"deleted\": false,\n        \"deletedAt\": null,\n        \"createdAt\": \"2020-07-06T14:57:00.998Z\",\n        \"updatedAt\": \"2020-07-06T14:57:00.998Z\",\n        \"__v\": 0\n    }\n]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Quizzes list empty",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Quizzes list is empty.\"\n}",
          "type": "json"
        },
        {
          "title": "Quizzes list selector null",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Selector object is null.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to retrieve a quizzes list from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/quizzes.js",
    "groupTitle": "Quizzes"
  },
  {
    "type": "get",
    "url": "/quizzes/:id",
    "title": "Get Quiz",
    "name": "GetQuiz",
    "group": "Quizzes",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The quiz ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>The quiz data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Quiz Data",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": {\n        \"_id\": \"5f033bbcc30d07001fd577cb\",\n        \"name\": \"La nature\",\n        \"list\": [\n            {\n                \"points\": 50,\n                \"answers\": [\n                    {\n                        \"is_answer\": false,\n                        \"answer\": \"1\",\n                        \"bad_answer_text\": \"blablabla\"\n                    },\n                    {\n                        \"is_answer\": false,\n                        \"answer\": \"2\",\n                        \"bad_answer_text\": \"blablabla\"\n                    },\n                    {\n                        \"is_answer\": true,\n                        \"answer\": \"3\"\n                    }\n                ],\n                \"_id\": \"5f033bbcc30d07001fd577cc\",\n                \"question\": \"Yolo\"\n            },\n            {\n                \"points\": 50,\n                \"answers\": [\n                    {\n                        \"is_answer\": false,\n                        \"answer\": \"1\",\n                        \"bad_answer_text\": \"blablabla\"\n                    },\n                    {\n                        \"is_answer\": false,\n                        \"answer\": \"2\",\n                        \"bad_answer_text\": \"blablabla\"\n                    },\n                    {\n                        \"is_answer\": true,\n                        \"answer\": \"3\"\n                    }\n                ],\n                \"_id\": \"5f033bbcc30d07001fd577cd\",\n                \"question\": \"Yolo 2\"\n            }\n        ],\n        \"deleted\": false,\n        \"deletedAt\": null,\n        \"createdAt\": \"2020-07-06T14:57:00.998Z\",\n        \"updatedAt\": \"2020-07-06T14:57:00.998Z\",\n        \"__v\": 0\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Quiz doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n    \"success\": false,\n    \"message\": \"Quiz doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Quiz ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n    \"success\": false,\n    \"message\": \"Quiz id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to retrieve a quiz from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/quizzes.js",
    "groupTitle": "Quizzes"
  },
  {
    "type": "patch",
    "url": "/quizzes/:id/restore",
    "title": "Restore quiz",
    "name": "RestoreQuiz",
    "group": "Quizzes",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The quiz ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Quiz restored",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"Quiz is restored.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Quiz doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Quiz doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Quiz ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Quiz id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to restore a quiz from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/quizzes.js",
    "groupTitle": "Quizzes"
  },
  {
    "type": "put",
    "url": "/quizzes/:id/update",
    "title": "Update Quiz",
    "name": "UpdateQuiz",
    "group": "Quizzes",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The quiz ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "update",
            "description": "<p>The quiz update object data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Quiz update request",
          "content": "{\n    \"update\": {\n        \"name\": \"La nature\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Quiz updated",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"Quiz is updated.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Quiz doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Quiz doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Update object null",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Update object is null.\"\n}",
          "type": "json"
        },
        {
          "title": "Quiz ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Quiz id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to update a quiz from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/quizzes.js",
    "groupTitle": "Quizzes"
  },
  {
    "type": "post",
    "url": "/rewards/create",
    "title": "Create new reward",
    "name": "CreateReward",
    "group": "Rewards",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "create",
            "description": "<p>The reward create object data.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.novice",
            "description": "<p>The novice reward field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "create.adventurer",
            "description": "<p>The adventurer reward field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.master",
            "description": "<p>The master reward field.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Reward create request",
          "content": "{\n    \"create\": {\n        \"novice\": \"Test\",\n        \"adventurer\": \"Test\",\n        \"master\": \"Test\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>The reward ID of the reward who has just been created.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Reward Created",
          "content": "HTTPS/1.1 201 OK\n{\n    \"success\": true,\n    \"_id\": \"5e3dd2d51d9f190609e8258c\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "errors",
            "description": "<p>Errors object, handleAPIError() manage Mongodb errors.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Required value",
          "content": "HTTPS/1.1 400 Not Found\n{\n    \"success\": false,\n    \"errors\": {\n        \"master\": {\n            \"path\": \"master\",\n            \"kind\": \"required\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Create object null",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Create object is null.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to create a new reward from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/rewards.js",
    "groupTitle": "Rewards"
  },
  {
    "type": "delete",
    "url": "/rewards/:id/delete",
    "title": "Delete reward",
    "name": "DeleteReward",
    "group": "Rewards",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The reward ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Reward deleted",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"Reward is deleted.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Reward doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Reward doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Reward ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Reward id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to delete a reward from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/rewards.js",
    "groupTitle": "Rewards"
  },
  {
    "type": "get",
    "url": "/rewards/list",
    "title": "Get rewards list",
    "name": "GetListRewards",
    "group": "Rewards",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "selector",
            "description": "<p>The reward selector object.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "100",
            "description": "<p>The object capture limit.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "defaultValue": "0",
            "description": "<p>The number of objects to ignore.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "sort",
            "defaultValue": "{\"createdAt\":-1}",
            "description": "<p>Sort result by specific field, 1 = ASC, -1 = DESC.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "isDeleted",
            "defaultValue": "false",
            "description": "<p>Returns only objects that have been not deleted.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Rewards list request",
          "content": "{\n    \"selector\": {\n       \"master\": \"Test\"\n    },\n    \"sort\": {\n       \"master\": 1\n    },\n    \"limit\": 1000,\n    \"isDeleted\": false\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>The rewards list data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Rewards list data",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": [\n        {\n            \"novice\": \"Test_1\",\n            \"adventurer\": \"Test_1\",\n            \"master\": \"Test_1\"\n        },\n        {\n            \"novice\": \"Test_2\",\n            \"adventurer\": \"Test_2\",\n            \"master\": \"Test_2\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Rewards list empty",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Rewards list is empty.\"\n}",
          "type": "json"
        },
        {
          "title": "Rewards list selector null",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Selector object is null.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to retrieve a rewards list from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/rewards.js",
    "groupTitle": "Rewards"
  },
  {
    "type": "get",
    "url": "/rewards/:id",
    "title": "Get reward",
    "name": "GetReward",
    "group": "Rewards",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The reward ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>The reward data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Reward Data",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": {\n        \"novice\": \"Test\",\n        \"adventurer\": \"Test\",\n        \"master\": \"Test\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Reward doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n    \"success\": false,\n    \"message\": \"Reward doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Reward ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n    \"success\": false,\n    \"message\": \"Reward id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to retrieve a reward from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/rewards.js",
    "groupTitle": "Rewards"
  },
  {
    "type": "patch",
    "url": "/rewards/:id/restore",
    "title": "Restore reward",
    "name": "RestoreReward",
    "group": "Rewards",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The reward ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Reward restored",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"Reward is restored.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Reward doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Reward doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Reward ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Reward id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to restore a reward from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/rewards.js",
    "groupTitle": "Rewards"
  },
  {
    "type": "put",
    "url": "/rewards/:id/update",
    "title": "Update reward",
    "name": "UpdateReward",
    "group": "Rewards",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The reward ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "update",
            "description": "<p>The reward update object data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Reward update request",
          "content": "{\n    \"update\": {\n        \"master\": \"Test\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Reward updated",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"Reward is updated.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Reward doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Reward doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Update object null",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Update object is null.\"\n}",
          "type": "json"
        },
        {
          "title": "Reward ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Reward id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to update a reward from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/rewards.js",
    "groupTitle": "Rewards"
  },
  {
    "type": "post",
    "url": "/stores/create",
    "title": "Create new store",
    "name": "CreateStore",
    "group": "Stores",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "create",
            "description": "<p>The store create object data.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.name",
            "description": "<p>The store name field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.description",
            "description": "<p>The store description field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.email",
            "description": "<p>The store email field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.phone",
            "description": "<p>The store phone field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "create.web_site",
            "description": "<p>The store web_site field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.store_type",
            "description": "<p>The store type field, it's a enumeration [&quot;Boutique&quot;, &quot;Restaurant&quot;, &quot;Activité&quot;].</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.store_speciality",
            "description": "<p>The store speciality field ex (House, Fast food, Beauty, Sport, DIY,..).</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "create.store_filters",
            "description": "<p>The store filters object.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.store_filters.price_average",
            "description": "<p>The store store_filters.price average field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "create.store_filters.bio",
            "defaultValue": "false",
            "description": "<p>The store store_filters.bio free field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "create.store_filters.gluten_free",
            "defaultValue": "false",
            "description": "<p>The store store_filters.gluten free field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "create.store_filters.vegan",
            "defaultValue": "false",
            "description": "<p>The store store_filters.vegan field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "create.store_filters.veggie",
            "defaultValue": "false",
            "description": "<p>The store store_filters.veggie field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "create.store_filters.terrace",
            "defaultValue": "false",
            "description": "<p>The store store_filters.terrace field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "create.store_filters.take_away",
            "defaultValue": "false",
            "description": "<p>The store store_filters.take_away field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": true,
            "field": "create.store_filters.handicap",
            "description": "<p>The store store_filters.handicap field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "create.store_filters.handicap.type",
            "description": "<p>The store store_filters.handicap.type field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "create.localisation",
            "description": "<p>The store localisation field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.localisation.city",
            "description": "<p>The store localisation.city field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.localisation.address",
            "description": "<p>The store localisation.address field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.localisation.zip_code",
            "description": "<p>The store localisation.zip_code field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": true,
            "field": "create.coords",
            "description": "<p>The store coords field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "create.coords.latitude",
            "description": "<p>The store coords.latitude field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "create.coords.longitude",
            "description": "<p>The store coords.longitude field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "create.open_hours",
            "description": "<p>The store open_hours field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.open_hours.mon",
            "description": "<p>The store open_hours.mon field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.open_hours.tue",
            "description": "<p>The store open_hours.tue field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.open_hours.wed",
            "description": "<p>The store open_hours.wed field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.open_hours.thu",
            "description": "<p>The store open_hours.thu field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.open_hours.fri",
            "description": "<p>The store open_hours.fri field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.open_hours.sat",
            "description": "<p>The store open_hours.sat field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.open_hours.sun",
            "description": "<p>The store open_hours.sun field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.status",
            "defaultValue": "PENDING",
            "description": "<p>The store status field, it's a enumeration [&quot;PENDING&quot;, &quot;ON&quot;, &quot;OFF&quot;].</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "create.images",
            "description": "<p>The store images field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.images.uri",
            "description": "<p>The store images.uri field, only base64.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "create.greenscore",
            "defaultValue": "0",
            "description": "<p>The store greenscore field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "create.greenscore_description",
            "description": "<p>The store greenscore_description field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create._proposed_by_user",
            "description": "<p>The store _proposed_by_user field.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Store create request",
          "content": "{\n    \"create\": {\n        \"name\": \"Test\",\n        \"description\": \"Test\",\n        \"email\": \"test@test.com\",\n        \"phone\": \"0164546465\",\n        \"web_site\": \"https://google.com\",\n        \"store_type\": \"Boutique\",\n        \"store_speciality\": \"Maison\",\n        \"store_filters\": {\n            \"price_average\": \"15€\",\n            \"bio\": false,\n            \"vegan\": false,\n            \"veggie\": false,\n            \"terrace\": false,\n            \"take_away\": true,\n            \"handicap\": [\n                {\"type\": \"Visuel\"},\n                {\"type\": \"Auditif\"},\n                {\"type\": \"Moteur\"}\n            ]\n        },\n        \"localisation\": {\n            \"city\": \"Test\",\n            \"address\": \"Test\",\n            \"zip_code\": \"00000\"\n        },\n        \"coords\": {\n            \"longitude\": 39.3535,\n            \"latitude\": 4.3535\n        },\n        \"open_hours\": {\n            \"mon\": \"9AM - 7PM\",\n            \"tue\": \"9AM - 7PM\",\n            \"wed\": \"9AM - 7PM\",\n            \"thu\": \"9AM - 7PM\",\n            \"fri\": \"9AM - 7PM\",\n            \"sat\": \"9AM - 7PM\",\n            \"sun\": \"9AM - 7PM\"\n        },\n        \"status\": \"PENDING\",\n        \"images\":  [\n            {\n                \"uri\": \"data:image/jpeg;base64,\"\n            }\n        ],\n        \"greenscore\": 0,\n        \"greenscore_description\": \"Greenscore justification\",\n        \"_proposed_by_user\": \"5e3dd2d51d9f190609e8258c\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>The store ID of the store who has just been created.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Store Created",
          "content": "HTTPS/1.1 201 OK\n{\n    \"success\": true,\n    \"store_id\": \"5e3dd2d51d9f190609e8258c\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "errors",
            "description": "<p>Errors object, handleAPIError() manage Mongodb errors.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Unique value",
          "content": "HTTPS/1.1 400 Not Found\n{\n    \"success\": false,\n    \"errors\": {\n        \"name\": {\n            \"path\": \"name\",\n            \"kind\": \"unique\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Create object null",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Create object is null.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to create a new store from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/stores.js",
    "groupTitle": "Stores"
  },
  {
    "type": "delete",
    "url": "/stores/:id/delete",
    "title": "Delete store",
    "name": "DeleteStore",
    "group": "Stores",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The store ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Store deleted",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"Store is deleted.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Store doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Store doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Store ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Store id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to delete a store from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/stores.js",
    "groupTitle": "Stores"
  },
  {
    "type": "get",
    "url": "/stores/list",
    "title": "Get stores list",
    "name": "GetListStores",
    "group": "Stores",
    "permission": [
      {
        "name": "SA (SuperAdmin), A (Admin), C (Customer)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "selector",
            "description": "<p>The store selector object.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "100",
            "description": "<p>The object capture limit.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "defaultValue": "0",
            "description": "<p>The number of objects to ignore.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "sort",
            "defaultValue": "{\"createdAt\":-1}",
            "description": "<p>Sort result by specific field, 1 = ASC, -1 = DESC.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "isDeleted",
            "defaultValue": "false",
            "description": "<p>Returns only objects that have been not deleted.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Stores list request",
          "content": "{\n    \"selector\": {\n       \"store_type\": \"Boutique\"\n    },\n    \"sort\": {\n       \"name\": 1\n    },\n    \"limit\": 1000,\n    \"isDeleted\": false\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>The stores list data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Stores list data",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": [\n        {\n            \"name\": \"Test\",\n            \"description\": \"Test\",\n            \"email\": \"test@test.com\",\n            \"phone\": \"0164546465\",\n            \"web_site\": \"https://google.com\",\n            \"store_type\": \"Boutique\",\n            \"store_speciality\": \"Maison\",\n            \"store_filters\": {\n                \"price_average\": \"15€\",\n                \"bio\": false,\n                \"vegan\": false,\n                \"veggie\": false,\n                \"terrace\": false,\n                \"take_away\": true,\n                \"handicap\": [\n                    {\"type\": \"Visuel\"},\n                    {\"type\": \"Auditif\"},\n                    {\"type\": \"Moteur\"}\n                ]\n            },\n            \"localisation\": {\n                \"city\": \"Test\",\n                \"address\": \"Test\",\n                \"zip_code\": \"00000\"\n            },\n            \"coords\": {\n                \"longitude\": 39.3535,\n                \"latitude\": 4.3535\n            },\n            \"open_hours\": {\n                \"mon\": \"9AM - 7PM\",\n                \"tue\": \"9AM - 7PM\",\n                \"wed\": \"9AM - 7PM\",\n                \"thu\": \"9AM - 7PM\",\n                \"fri\": \"9AM - 7PM\",\n                \"sat\": \"9AM - 7PM\",\n                \"sun\": \"9AM - 7PM\",\n            },\n            \"status\": \"PENDING\",\n            \"images\":  [\n                {\n                    \"uri\": \"images/stores/a4otdtpohzn3w8k5oi7gsf6rlo751ara/c2edkk1n35.jpeg\"\n                },\n                {\n                    \"uri\": \"images/stores/a4otdtpohzn3w8k5oi7gsf6rlo751ara/fsdfsd34df.jpeg\"\n                }\n            ],\n            \"greenscore\": 0,\n            \"greenscore_description\": \"Greenscore justification\",\n            \"_proposed_by_user\": \"5e3dd2d51d9f190609e8258c\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Stores list empty",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Stores list is empty.\"\n}",
          "type": "json"
        },
        {
          "title": "Stores list selector null",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Selector object is null.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to retrieve a stores list from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/stores.js",
    "groupTitle": "Stores"
  },
  {
    "type": "get",
    "url": "/stores/:id",
    "title": "Get store",
    "name": "GetStore",
    "group": "Stores",
    "permission": [
      {
        "name": "SA (SuperAdmin), A (Admin), C (Customer)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The store ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>The store data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Store Data",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": {\n        \"name\": \"Test\",\n        \"description\": \"Test\",\n        \"email\": \"test@test.com\",\n        \"phone\": \"0164546465\",\n        \"web_site\": \"https://google.com\",\n        \"store_type\": \"Boutique\",\n        \"store_speciality\": \"Maison\",\n        \"store_filters\": {\n            \"price_average\": \"15€\",\n            \"bio\": \"false\",\n            \"vegan\": false,\n            \"veggie\": false,\n            \"terrace\": false,\n            \"take_away\": true,\n            \"handicap\": [\n                {\"type\": \"Visuel\"},\n                {\"type\": \"Auditif\"},\n                {\"type\": \"Moteur\"}\n            ]\n        },\n        \"localisation\": {\n            \"city\": \"Test\",\n            \"address\": \"Test\",\n            \"zip_code\": \"00000\"\n        },\n        \"coords\": {\n            \"longitude\": 39.3535,\n            \"latitude\": 4.3535\n        },\n        \"open_hours\": {\n            \"mon\": \"9AM - 7PM\",\n            \"tue\": \"9AM - 7PM\",\n            \"wed\": \"9AM - 7PM\",\n            \"thu\": \"9AM - 7PM\",\n            \"fri\": \"9AM - 7PM\",\n            \"sat\": \"9AM - 7PM\",\n            \"sun\": \"9AM - 7PM\",\n        },\n        \"status\": \"PENDING\",\n        \"images\":  [\n            {\n                \"uri\": \"images/stores/a4otdtpohzn3w8k5oi7gsf6rlo751ara/c2edkk1n35.jpeg\"\n            },\n            {\n                \"uri\": \"images/stores/a4otdtpohzn3w8k5oi7gsf6rlo751ara/fsdfsd34df.jpeg\"\n            }\n        ],\n        \"greenscore\": 0,\n        \"greenscore_description\": \"Greenscore justification\",\n        \"_proposed_by_user\": \"5e3dd2d51d9f190609e8258c\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Store doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n    \"success\": false,\n    \"message\": \"Store doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Store ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n    \"success\": false,\n    \"message\": \"Store id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to retrieve a store from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/stores.js",
    "groupTitle": "Stores"
  },
  {
    "type": "patch",
    "url": "/stores/:id/restore",
    "title": "Restore store",
    "name": "RestoreStore",
    "group": "Stores",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The store ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Store restored",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"Store is restored.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Store doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Store doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Store ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Store id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to restore a store from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/stores.js",
    "groupTitle": "Stores"
  },
  {
    "type": "put",
    "url": "/stores/:id/update",
    "title": "Update store",
    "name": "UpdateStore",
    "group": "Stores",
    "permission": [
      {
        "name": "SA (SuperAdmin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The store ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "update",
            "description": "<p>The store update object data.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "update.email",
            "description": "<p>The store update email field.</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "update.images",
            "description": "<p>The store update images, all the old photos are replaced by the new ones.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "update.images.uri",
            "description": "<p>The store update images.uri field, only base64.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Store update request",
          "content": "{\n    \"update\": {\n        \"email\": \"test_3@test.com\",\n        \"images\": [\n            {\n                \"uri\": \"data:image/jpeg;base64,\"\n            },\n            {\n                \"uri\": \"data:image/jpeg;base64,\"\n            }\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Store updated",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"Store is updated.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Store doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Store doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Update object null",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Update object is null.\"\n}",
          "type": "json"
        },
        {
          "title": "Store ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Store id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to update a store from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/stores.js",
    "groupTitle": "Stores"
  },
  {
    "type": "post",
    "url": "/token/verify",
    "title": "Verify if the token is expired",
    "name": "VerifyToken",
    "group": "Token",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>The token to verify.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Token verify request",
          "content": "{\n   \"token\": \"MY_USER_TOKEN\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>The user if the token isn't expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Token is not expired",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": {\n            \"pot\": 0,\n            \"role\": \"SA\",\n            \"profile_pic\": \"images/profile-pic/user-default.jpeg\",\n            \"_id\": \"5efca86355769700b779d1e4\",\n            \"email\": \"admin@admin.com\",\n            \"first_name\": \"admin\",\n            \"last_name\": \"admin\",\n            \"deleted\": false,\n            \"deletedAt\": null,\n            \"createdAt\": \"2020-07-01T15:14:43.459Z\",\n            \"updatedAt\": \"2020-07-01T15:14:43.459Z\",\n            \"__v\": 0,\n            \"quizzes\": []\n        }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "message",
            "description": "<p>Error message</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Token is missing in body",
          "content": "HTTPS/1.1 400 Not Found\n{\n    \"success\": false,\n    \"message\": \"You did not sent the token.\"\n}",
          "type": "json"
        },
        {
          "title": "Token is expired",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"The token is expired.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route will verify if a token is expired or not. It will return the user by the token if it's not expired.</p>",
    "version": "0.0.0",
    "filename": "routes/token.js",
    "groupTitle": "Token"
  },
  {
    "type": "post",
    "url": "/users/create",
    "title": "Create new user",
    "name": "CreateUser",
    "group": "Users",
    "permission": [
      {
        "name": "SA (SuperAdmin), A (Admin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "create",
            "description": "<p>The user create object data.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.first_name",
            "description": "<p>The user first_name field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.last_name",
            "description": "<p>The user last_name field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.email",
            "description": "<p>The user email field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.password",
            "description": "<p>The user password field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create._client",
            "description": "<p>The client ID, it is only required for type roles [&quot;A&quot;, &quot;C&quot;].</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "create.pot",
            "defaultValue": "0",
            "description": "<p>The user wallet point field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "create.role",
            "defaultValue": "C",
            "description": "<p>The user role, it's a enumeration [&quot;SA&quot;, &quot;A&quot;, &quot;C&quot;].</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": true,
            "field": "create.profile_pic",
            "defaultValue": "images/profile-pic/user-default.jpeg",
            "description": "<p>The user profile picture field.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "create.profile_pic.uri",
            "description": "<p>The image data, only base64.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "User create request",
          "content": "{\n    \"create\": {\n        \"first_name\": \"Test\",\n        \"last_name\": \"Test\",\n        \"email\": \"test@test.com\",\n        \"password\": \"azerty\",\n        \"_client\": \"5e3dd5186f19e40659ced1a5\",\n        \"role\": \"A\",\n        \"profile_pic\": {\n            \"uri\": \"data:image/jpeg;base64,\"\n        }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>The user ID of the user who has just been created.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "User Created",
          "content": "HTTPS/1.1 201 OK\n{\n    \"success\": true,\n    \"user_id\": \"5e3dd2d51d9f190609e8258c\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "errors",
            "description": "<p>Errors object, handleAPIError() manage Mongodb errors.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Required value",
          "content": "HTTPS/1.1 400 Not Found\n{\n    \"success\": false,\n    \"errors\": {\n        \"password\": {\n            \"path\": \"password\",\n            \"kind\": \"required\"\n        },\n        \"email\": {\n            \"path\": \"email\",\n            \"kind\": \"required\"\n        }\n    }\n}",
          "type": "json"
        },
        {
          "title": "Create object null",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Create object is null.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to create a new user from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "Users"
  },
  {
    "type": "delete",
    "url": "/users/:id/delete",
    "title": "Delete user",
    "name": "DeleteUser",
    "group": "Users",
    "permission": [
      {
        "name": "SA (SuperAdmin), A (Admin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The user ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "User deleted",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"User is deleted.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "User doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"User doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "User ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"The user id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to delete a user from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "Users"
  },
  {
    "type": "get",
    "url": "/users/list",
    "title": "Get users list",
    "name": "GetListUsers",
    "group": "Users",
    "permission": [
      {
        "name": "SA (SuperAdmin), A (Admin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "selector",
            "description": "<p>The user selector object.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "100",
            "description": "<p>The object capture limit.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "offset",
            "defaultValue": "0",
            "description": "<p>The number of objects to ignore.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "sort",
            "defaultValue": "{\"createdAt\":-1}",
            "description": "<p>Sort result by specific field, 1 = ASC, -1 = DESC.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "isDeleted",
            "defaultValue": "false",
            "description": "<p>Returns only objects that have been not deleted.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Users list request",
          "content": "{\n    \"selector\": {\n       \"role\": \"C\",\n       \"_client\": \"5e3dd5186f19e40659ced1a5\"\n    },\n    \"sort\": {\n       \"last_name\": 1\n    },\n    \"limit\": 1000,\n    \"isDeleted\": false\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>The users list data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Users list data",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": [\n        {\n            \"first_name\": \"Test_1\",\n            \"last_name\": \"Test_1\",\n            \"email\": \"test_1@test.com\",\n            \"password\": \"azerty\",\n            \"_client\": \"5e3dd5186f19e40659ced1a5\",\n            \"role\": \"A\",\n            \"pot\": 0,\n            \"profile_pic\": \"images/profile-pic/user-default.jpeg\"\n        },\n        {\n            \"first_name\": \"Test_2\",\n            \"last_name\": \"Test_2\",\n            \"email\": \"test_2@test.com\",\n            \"password\": \"azerty\",\n            \"_client\": \"5e3dd5186f19e40659ced1a5\",\n            \"role\": \"A\",\n            \"pot\": 0,\n            \"profile_pic\": \"images/profile-pic/user-default.jpeg\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Users list empty",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Users list is empty.\"\n}",
          "type": "json"
        },
        {
          "title": "Selector object null.",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"Selector object is null.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to retrieve a users list from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "Users"
  },
  {
    "type": "get",
    "url": "/users/:id",
    "title": "Get user",
    "name": "GetUser",
    "group": "Users",
    "permission": [
      {
        "name": "SA (SuperAdmin), A (Admin), C (Customer)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The user ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>The user data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "User Data",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": {\n        \"first_name\": \"Test\",\n        \"last_name\": \"Test\",\n        \"email\": \"test@test.com\",\n        \"password\": \"azerty\",\n        \"_client\": \"5e3dd5186f19e40659ced1a5\",\n        \"role\": \"A\",\n        \"pot\": 0,\n        \"profile_pic\": \"images/profile-pic/user-default.jpeg\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "User doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n    \"success\": false,\n    \"message\": \"User doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "User ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n    \"success\": false,\n    \"message\": \"User id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to retrieve a user from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "Users"
  },
  {
    "type": "patch",
    "url": "/users/:id/restore",
    "title": "Restore user",
    "name": "RestoreUser",
    "group": "Users",
    "permission": [
      {
        "name": "SA (SuperAdmin), A (Admin)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The user ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "User restored",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"User is restored.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "User doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"User doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "User ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"The user id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to restore a user from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "Users"
  },
  {
    "type": "put",
    "url": "/users/:id/update",
    "title": "Update user",
    "name": "UpdateUser",
    "group": "Users",
    "permission": [
      {
        "name": "SA (SuperAdmin), A (Admin), C (Customer)"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The user ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "update",
            "description": "<p>The user update object data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "User update request",
          "content": "{\n    \"update\": {\n        \"email\": \"test_3@test.com\",\n        \"password\": \"azertyuiop\",\n         \"profile_pic\": {\n             \"uri\": \"data:image/jpeg;base64,\"\n         }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Success message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "User updated",
          "content": "HTTPS/1.1 200 OK\n{\n    \"success\": true,\n    \"message\": \"User is updated.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Request status.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Error message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "User doesn't exist",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"User doesn't exist.\"\n}",
          "type": "json"
        },
        {
          "title": "Update object null",
          "content": "HTTPS/1.1 404 Not Found\n{\n  \"success\": false,\n  \"message\": \"Update object is null.\"\n}",
          "type": "json"
        },
        {
          "title": "User ID not valid",
          "content": "HTTPS/1.1 400 Not Found\n{\n  \"success\": false,\n  \"message\": \"The user id is not valid.\"\n}",
          "type": "json"
        }
      ]
    },
    "description": "<p>This route allows you to update a user from the database.</p>",
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "Users"
  }
] });
