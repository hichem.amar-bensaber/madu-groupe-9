const mongoose = require("mongoose");
const request = require("request");
const sharp = require("sharp");
const fs = require("fs-extra");
const randomString = require("randomstring");

/**
 * Update data of a mongoose document
 *
 * @param Data actual data of the document
 * @param Update update datas
 * @returns {*}
 */
const Update_Data = (Data, Update) => {
    for(let key in Update) {
        if(key in Data) {
            if(typeof(Update[key]) === "array" || typeof(Update[key]) === "object") {
                Data[key] = Update_Data(Data[key], Update[key]);
            } else {
                if (key in Data) Data[key] = Update[key];
            }
        }
    }
    return Data;
};

/**
 * Format collection to name to singular
 *
 * @param pluralCollectionName
 * @returns {string}
 */
const formatSingular = pluralCollectionName => pluralCollectionName.substring(0, pluralCollectionName.length - 1);

/**
 * Format collection name to capitalize (first letter)
 *
 * @param pluralCollectionName
 * @returns {string}
 */
const formatCapitalize = pluralCollectionName => pluralCollectionName.charAt(0).toUpperCase() + pluralCollectionName.slice(1);

/**
 * Get an object with singular & plurial of a collection name
 *
 * @param collectionName
 * @returns {{plural: string, singular: string}}
 */
const formatName = collectionName => {
    return {
        "singular": formatSingular(formatCapitalize(collectionName)),
        "plural": formatCapitalize(collectionName)
    }
};

/**
 * Check if an email is valid
 *
 * @param email
 * @returns {boolean}
 */
const checkEmail = (email) => {
    const regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return !!email.match(regex);
};

/**
 * Clean users password in data array.
 *
 * @param Data
 * @returns {Object}
 */
const CleanPublicData = (Data) => {
    if(!Array.isArray(Data)) {
        if("user" in Data) Data.user["password"] = undefined;
        else Data["password"] = undefined;
    }
    else {
        for(let i = 0; i < Data.length; i++) {
            Data[i] = CleanPublicData(Data[i]);
        }
    }
    return Data;
};

/**
 * Handle the error coming from the API,
 * Return all the errors.
 *
 * @param res
 * @param error
 * @returns {*}
 */
const handleAPIError = (res, error) => {
    console.log(error);
    let status = 500;
    let APIErrors = {};

    // Handle types of errors
    if (error instanceof mongoose.Error.ValidationError) {
        for (const [field, fieldError] of Object.entries(error.errors)) {
            APIErrors[field] = {
                path: fieldError.path,
                value: fieldError.value,
                kind: fieldError.properties.kind ? fieldError.properties.kind : fieldError.kind
            };
        }
        status = 400;
    } 
    else APIErrors = error;

    // Return errors
    return res.status(status).send({
        success: false,
        errors: APIErrors
    });
};

/**
 * Get latitude and longitude with address thanks to gouv API geolocation
 *
 * @param Query
 * @returns {latitude, longitude}
 */
async function Get_Coords(Query) {
    return new Promise(((resolve, reject) => {
        console.log('Get Coords : ' + Query);
        const Request = `https://api-adresse.data.gouv.fr/search/?q=${encodeURIComponent(Query)}&limit=1`;
        request(Request, {
            json: true
        }, (err, response, body) => {
            if (err) reject(err);
            if (body.features[0].geometry.coordinates) {
                return resolve({
                    latitude: body.features[0].geometry.coordinates[1],
                    longitude: body.features[0].geometry.coordinates[0]
                });
            }
            console.error('Get_Coords return null value with : ' + Query);
            resolve(null);
        })
    }));
}

/**
 * Get address with latitude and longitude thanks to gouv API geolocation
 *
 * @param latitude
 * @param longitude
 * @returns {city, address, zip_code}
 */
async function Get_Address(latitude, longitude) {
    return new Promise(((resolve, reject) => {
        console.log('Get Coords : ' + Query);
        const Request = `https://api-adresse.data.gouv.fr/reverse/?lon=${longitude}&lat=${latitude}&limit=1`;
        request(Request, {
            json: true
        }, (err, response, body) => {
            if (err) reject(err);
            if (body.features[0].properties) {
                return resolve({
                    city: body.features[0].properties.city,
                    address: body.features[0].properties.name,
                    zip_code: body.features[0].properties.postcode
                });
            }
            console.error(`Get_Address return null value with latitude : ${latitude} and longitude : ${longitude}`);
            resolve(null);
        })
    }));
}

/**
 * Check if params is null or empty.
 *
 * @param params
 * @returns {Boolean}
 */
function isNullorEmpty(params) {
    if(params === null || !params) return true;
    if(typeof (params) == "string") {
        if(params.length === 0 || params.trim() === "") return true;
    }
    else {
        if(typeof(params) === "undefined" || params.length === 0) return true;
    }
    return false;
}

/**
 * 
 * Write image file with base64 data.
 * 
 * @param Image
 * @param Path
 * @returns {Boolean}
*/

async function WriteImage(Image, Path) {
    let imageBuffer = Buffer.from(Image.uri.split(";base64,").pop(), "base64");
    sharp(imageBuffer).png({quality: 100}).toFile(Path + Image.name, function(error) {
        if(error) throw error;
        return true;
    });
}

/**
 * 
 * Delete directory with path.
 * 
*/

async function Delete_Directory(Directory_Path) {
    fs.remove(Directory_Path, function(error) {
        if(error) throw error;
        return true;
    });
}

/**
 * 
 * Create directory with path if is not exist.
 * 
 * @param Path
 * @returns {VoidFunction}
*/

function Create_Directory(Path) {
    if(!fs.existsSync(Path)) {
        fs.mkdirSync(Path, 0o755, function(Directory_Error){
            if (Directory_Error) throw Directory_Error;
            return true
        });
    }
    return false;
}

/**
 * 
 * Generate id with length argument.
 *
 * @param length
 * @returns {String}
*/

function Create_ID(length) { // Create 'unique' random string.
    return randomString.generate({
        length: length,
        charset: "alphanumeric",
        capitalization: "lowercase"
    });
}

module.exports = {
    Update_Data,
    formatSingular,
    formatCapitalize,
    formatName,
    checkEmail,
    CleanPublicData,
    handleAPIError,
    Get_Coords,
    Get_Address,
    isNullorEmpty,
    WriteImage,
    Delete_Directory,
    Create_Directory,
    Create_ID
};