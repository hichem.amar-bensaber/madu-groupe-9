const helpers = require("../utils/helpers");
const mongoose = require("mongoose");
const uniqueMongoose = require("mongoose-unique-validator");
const softDelete = require("../utils/soft-delete");
const Schema = mongoose.Schema;

const Client_Schema = new Schema({
    name: { type: String, required: true, unique: true },
    localisation: {
        city: { type: String, required: true },
        address: { type: String, required: true },
        zip_code: { type: String, required: true }
    },
    coords: {
        longitude: { type: Number, trim: true },
        latitude: { type: Number, trim: true }
    },
    phone: { type: String, required: true, trim: true },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        validate: {
            validator: helpers.checkEmail,
            message: props => `${props.value} is not a valid email!`,
            kind: 'not valid email'
        }
    },
    profile_pic: {type: String, default: "images/profile-pic/user-default.jpeg"}
});

// Get coords by MapQuest API (Longitude, Latitude) with city, address, zip code
Client_Schema.pre("save", async function (next) {
    let client = this;

    if(!client.isModified("localisation")) return next();

    if(client.localisation.city && client.localisation.address && client.localisation.zip_code && !!client.coords) {
        let Query = client.localisation.city + ", " + client.localisation.address + ", " + client.localisation.zip_code;
        let API_Result = await helpers.Get_Coords(Query);
        if(API_Result) client.coords = API_Result;
    }
    next();
});
  
Client_Schema.plugin(uniqueMongoose);
Client_Schema.plugin(softDelete);
Client_Schema.set("timestamps", true);

module.exports = mongoose.model("clients", Client_Schema);