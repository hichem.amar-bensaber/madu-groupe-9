const helpers = require("../utils/helpers");
const mongoose = require("mongoose");
const softDelete = require("../utils/soft-delete.js");
const uniqueMongoose = require("mongoose-unique-validator");
const Schema = mongoose.Schema;

const Store_Schema = new Schema({
  name: { type: String, required: true, unique: true },
  description: { type: String, required: true },
  email: {
    type: String,
    required: true,
    unique: true,
    trim: true,
    validate: {
      validator: helpers.checkEmail,
      message: (props) => `${props.value} is not a valid email!`,
      kind: "not valid email",
    },
  },
  phone: { type: String, required: true, trim: true },
  web_site: { type: String, trim: true },
  store_type: {
    type: String,
    enum: ["Boutique", "Restaurant", "Activité"],
    required: true,
  },
  store_speciality: { type: String, required: true },
  store_filters: {
    price_average: { type: String, required: true },
    bio: { type: Boolean, default: false },
    gluten_free: { type: Boolean, default: false },
    vegan: { type: Boolean, default: false },
    veggie: { type: Boolean, default: false },
    terrace: { type: Boolean, default: false },
    take_away: { type: Boolean, default: false },
    handicap: [
      {
        type: { type: String },
        _id: false,
      },
    ],
  },
  localisation: {
    city: { type: String, required: true },
    address: { type: String, required: true },
    zip_code: { type: String, required: true },
  },
  coords: {
    longitude: { type: Number, trim: true },
    latitude: { type: Number, trim: true },
  },
  status: { type: String, enum: ["PENDING", "ON", "OFF"], default: "PENDING" },
  open_hours: {
    mon: { type: String, required: true },
    tue: { type: String, required: true },
    wed: { type: String, required: true },
    thu: { type: String, required: true },
    fri: { type: String, required: true },
    sat: { type: String, required: true },
    sun: { type: String, required: true }
  },
  images: [
    {
      uri: { type: String },
      _id: false,
    },
  ],
  greenscore: { type: Number, default: 0 },
  greenscore_description: { type: String, required: false },
  _proposed_by_user: { type: Schema.Types.ObjectId, ref: "users" },
});

// Get coords by MapQuest API (Longitude, Latitude) with city, address, zip code
Store_Schema.pre("save", async function(next) {
  let store = this;

  if (!store.isModified("localisation")) return next();
  if (
    store.localisation.city &&
    store.localisation.address &&
    store.localisation.zip_code &&
    !!store.coords
  ) {
    let Query =
      store.localisation.city +
      ", " +
      store.localisation.address +
      ", " +
      store.localisation.zip_code;
    let API_Result = await helpers.Get_Coords(Query);
    if (API_Result) store.coords = API_Result;
  }
  next();
});

Store_Schema.plugin(softDelete);
Store_Schema.plugin(uniqueMongoose);
Store_Schema.set("timestamps", true);

module.exports = mongoose.model("stores", Store_Schema);
