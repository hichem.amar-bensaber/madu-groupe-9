const mongoose = require("mongoose");
const uniqueMongoose = require("mongoose-unique-validator");
const softDelete = require("../utils/soft-delete");
const Schema = mongoose.Schema;

const Challenge_Schema = new Schema({
    name: { type: String, required: true, unique: true },
    description: { type: String, required: true },
    type: { type: String, required: true },
    points: { type: Number, required: true },
    duration: { type: Number, required: true },
});
  
Challenge_Schema.plugin(uniqueMongoose);
Challenge_Schema.plugin(softDelete);
Challenge_Schema.set("timestamps", true);

module.exports = mongoose.model("challenges", Challenge_Schema);