const helpers = require("../utils/helpers");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const uniqueMongoose = require("mongoose-unique-validator");
const softDelete = require("../utils/soft-delete.js");
const Schema = mongoose.Schema;

const User_Schema = new Schema({
    first_name: { type: String, required: true, trim: true },
    last_name: { type: String, required: true, trim: true },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        validate: {
            validator: helpers.checkEmail,
            message: props => `${props.value} is not a valid email!`,
            kind: "not valid email"
        }
    },
    password: {type: String, required: true, trim: true},
    _client: {
        type: Schema.Types.ObjectId,
        ref: "clients",
        required: function () {
            return this.role !== "SA";
        }
    },
    quizzes: [
        {
            id: { type: Schema.Types.ObjectId },
            success: { type: Boolean, required: true },
            _id: false
        }
    ],
    type: {type: String, default: "Novice"},
    xp: {type: Number, default: 0},
    role: {type: String, enum: ["SA", "A", "C"], default: "C", trim: true},
    profile_pic: {type: String, default: "images/profile-pic/user-default.jpeg"}
});

// Automatic convert clear password to bcrypt hash on save.
User_Schema.pre("save", async function(next) {
    let user = this;
    let userError = null;

    if(user.isModified("xp")) {
        const types = [
            "Novice",
            "Aventurier",
            "Master"
        ];
        const typesPotMax = [
            0,
            300,
            1000
        ];

        typesPotMax.forEach((typePotMax, index) => {
            if(user.xp < typePotMax) return;
            user.type = types[index];
        });
    }

    if (user.isModified("password")) {
        // generate a salt
        // bcrypt.genSalt(10, function(err, salt) {
        //     if(err) userError = err;
        //     // hash the password using our new salt
        //     bcrypt.hash(user.password, salt, function(err, hash) {
        //         // override the cleartext password with the hashed one
        //         user.password = hash;
        //     });
        // });
        let salt = await bcrypt.genSalt(10);
        let hash = await bcrypt.hash(user.password, salt);
        if(hash) {
            user.password = hash;
        }
    }

    if(userError) return next(userError);
    return next();
});

// Compare the clear password with the crypted password
User_Schema.methods.ValidPassword = async function(clearPassword) {
    let user = this;
    return await bcrypt.compare(clearPassword, user.password);
};

User_Schema.plugin(uniqueMongoose);
User_Schema.plugin(softDelete);
User_Schema.set("timestamps", true);

module.exports = mongoose.model("users", User_Schema);