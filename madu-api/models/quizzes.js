const mongoose = require("mongoose");
const softDelete = require("../utils/soft-delete");
const Schema = mongoose.Schema;

const Quiz_Schema = new Schema({
    name: { type: String, required: true },
    list: [
        {
            question: { type: String, required: true },
            points: { type: Number, required: true, default: 50 },
            answers: [
                {
                    answer: { type: String, required: true },
                    is_answer: { type: Boolean, default: false },
                    bad_answer_text: { type: String },
                    _id: false
                }
            ]
        }
    ]
});

Quiz_Schema.plugin(softDelete);
Quiz_Schema.set("timestamps", true);

module.exports = mongoose.model("quizzes", Quiz_Schema);