const mongoose = require("mongoose");
const softDelete = require("../utils/soft-delete");
const Schema = mongoose.Schema;

const Reward_Schema = new Schema({
    novice: { type: String, required: true },
    adventurer: { type: String, required: true },
    master: { type: String, required: true }
});

Reward_Schema.plugin(softDelete);

module.exports = mongoose.model("rewards", Reward_Schema);