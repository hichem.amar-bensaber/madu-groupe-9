const jwt = require("jsonwebtoken");
const { isNullorEmpty, handleAPIError, CleanPublicData } = require("../utils/helpers.js");
const User = require('../models/users.js');
const jwt_decode = require('jwt-decode');

/**
 * Verify if token have expired or not
 *
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
exports.tokenVerify = async (req, res) => {
    console.log("API Request : Verify Token");
    const {token} = req.body;
    if (isNullorEmpty(token)) return res.status(400).send({success: false, message: "You did not sent the token."});
    const isExpired = jwt.verify(token, process.env.TOKEN_KEY, (err, decoded) => {
        if(err) return true;
        return false;
    });
    if (isExpired) {
        return res.status(404).send({
            success: false,
            message: "The token is expired."
        });
    }
    else {
        const decodedToken = jwt_decode(token);
        if (!decodedToken.id) {
            return res.status(404).send({
                success: false,
                message: "There was an error retrieving the user."
            });
        } else {
            User.findById(decodedToken.id, function(err, _User) {
                if (err) return handleAPIError(res, err);
                if (Array.isArray(_User)) _User = _User[0];
                return res.status(200).send({
                    success: true,
                    data: CleanPublicData(_User)
                });
            }).isDeleted(false).populate("_client");
        }
    }
};