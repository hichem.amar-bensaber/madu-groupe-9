const User = require("../models/users.js");
const jwt = require("jsonwebtoken");
const { handleAPIError } = require("../utils/helpers.js");
const { isNullorEmpty } = require("../utils/helpers.js");

exports.login = async (req, res) => {
    console.log("API Request : Login");
    const {email, password} = req.body;
    if(isNullorEmpty(email) || isNullorEmpty(password)) return res.status(400).send({success: false, message: "Login data is null."});
    User.findOne({"email": email}, async function(err, _User) {
        if (err) return handleAPIError(res, err);
        if (Array.isArray(_User)) _User = _User[0];
        if(isNullorEmpty(_User) || !await _User.ValidPassword(password)) return res.status(400).send({success: false, message: "Incorrect email or password."});
        const payload = {
            id: _User._id
        };
        jwt.sign(payload, process.env.TOKEN_KEY, {expiresIn: "2 days"}, function(err, Token) {
            if (err) return handleAPIError(res, err);
            if(Token) return res.status(200).json({success: true, data: _User, token: Token});
        });
    }).isDeleted(false).populate("_client");
};

exports.SetAPIAccess = (AccessList=["SA", "A", "C"]) => {
    return function(req, res, next) {
        req.body.authorization_list = AccessList;
        next();
    }
};