const Client = require('../models/clients.js');
const { isNullorEmpty, handleAPIError } = require("../utils/helpers.js");

/**
 * Check if email suffix exists in database
 *
 * @param req
 * @param res
 * @returns {Promise<*>}
 */
exports.checkEmailSuffix = async (req, res) => {
    console.log("API Request : Verify Email Suffix ");
    const {email_suffix} = req.body;
    if (isNullorEmpty(email_suffix)) return res.status(400).send({success: false, message: "You did not sent the email suffix to verify."});

    const regex = `([^@\\s,;]+)@(${email_suffix.replace('.', '\\.')})`;
    Client.findOne({ "email" : { $regex: regex, $options: 'i' } },
        function (err, client) {
            if (err) return handleAPIError(res, err);
            if (!client) {
                return res.status(404).send({
                    success: false,
                    message: `The email suffix ${email_suffix} doesn\'t exist.`,
                });
            }
            return res
                .status(200)
                .send({
                    success: true,
                    data: client
                });
        });
};