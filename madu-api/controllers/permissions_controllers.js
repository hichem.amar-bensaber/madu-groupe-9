const permissions = require('../utils/api-permissions');
const { formatName } = require('../utils/helpers');

/**
 * Get schema name (plural & singular)
 *
 * @param Schema
 * @returns {{plural: string, singular: string}}
 */
const schemaName = Schema => {
    const name = formatName(Schema.collection.collectionName);
    console.log("API Permissions : " + name['plural'].toLowerCase());

    return name;
};

/**
 * Get permission
 *
 * @param Schema
 * @param req
 * @param typeOfRequest
 * @returns {*}
 */
const getPermission = (Schema, req, typeOfRequest) => permissions[schemaName(Schema)['plural'].toLowerCase()][req.user.role][typeOfRequest];

/**
 * Create ban object for selector
 *
 * @param bans
 * @returns {{[p: string]: {$nin: *}}}
 */
const createBanObject = bans => {
    let ban;

    for (const [key, value] of Object.entries(bans)) {
        ban = {...ban, [key]: {$nin: value}};
    }

    return ban;
};

/**
 * Handle permissions rights
 *
 * @param req
 * @param permission
 * @param isUpdate
 * @returns {{}}
 */
const handlePermission = (req, permission, isUpdate = false) => {
    if (typeof permission['equal'] !== 'undefined' && typeof req.user[permission['equal']] !== 'undefined') {
        req.body.selector = {...req.body.selector, [permission['equal']]: req.user[permission['equal']]}
    }
    if (!isUpdate && typeof permission['ban'] !== 'undefined') {
        req.body.selector = {...req.body.selector, ...createBanObject(permission['ban'])}
    }
    if (isUpdate && typeof permission['ban'] !== 'undefined') {
        for (const [key, value] of Object.entries(permission['ban'])) {
            if (req.body.update.hasOwnProperty(key)) {
                if (value === '*') {
                    delete req.body.update[key];
                } else {
                    let cantUpdatesValues = [];

                    if (!Array.isArray(value)) {
                        cantUpdatesValues.push(value);
                    } else {
                        cantUpdatesValues = value;
                    }

                    cantUpdatesValues.map(cantUpdate => {
                        if (req.body.update[key] === cantUpdate) delete req.body.update[key];
                    });
                }
            }
        }
    }

    return req.body.selector;
};

/**
 * List content of database filtering with permissions
 *
 * @param Schema
 * @returns {function(...[*]=)}
 */
exports.list = Schema => {
    return async (req, res, next) => {
        let permission = getPermission(Schema, req, 'list');

        if (typeof permission === 'undefined') return res.status(404).send({success: false, message: `${schemaName(Schema)['plural']} list is empty.`});
        req.body.selector = handlePermission(req, permission);

        return next();
    }
};

/**
 * Get a document from the database filtering with permissions
 *
 * @param Schema
 * @returns {function(...[*]=)}
 */
exports.get = Schema => {
    return async (req, res, next) => {
        let permission = getPermission(Schema, req, 'get');

        if (typeof permission === 'undefined') return res.status(404).send({success: false, message: `${schemaName(Schema)['singular']} doesn't exist.`});
        req.body.selector = handlePermission(req, permission);

        return next();
    }
};

/**
 * Update a document from the database filtering with permissions
 *
 * @param Schema
 * @returns {function(...[*]=)}
 */
exports.update = Schema => {
    return async (req, res, next) => {
        let permission = getPermission(Schema, req, 'update');

        if (typeof permission === 'undefined') return res.status(404).send({success: false, message: `${schemaName(Schema)['singular']} doesn't exist.`});
        req.body.selector = handlePermission(req, permission, true);

        return next();
    }
};

/**
 * Delete a document from the database filtering with permissions
 *
 * @param Schema
 * @returns {function(...[*]=)}
 */
exports.delete = Schema => {
    return async (req, res, next) => {
        let permission = getPermission(Schema, req, 'delete');

        if (typeof permission === 'undefined') return res.status(404).send({success: false, message: `${schemaName(Schema)['singular']} doesn't exist.`});
        req.body.selector = handlePermission(req, permission);

        return next();
    }
};

/**
 * Restore a document from the database filtering with permissions
 *
 * @param Schema
 * @returns {function(...[*]=)}
 */
exports.restore = Schema => {
    return async (req, res, next) => {
        let permission = getPermission(Schema, req, 'restore');

        if (typeof permission === 'undefined') return res.status(404).send({success: false, message: `${schemaName(Schema)['singular']} doesn't exist.`});
        req.body.selector = handlePermission(req, permission);

        return next();
    }
};