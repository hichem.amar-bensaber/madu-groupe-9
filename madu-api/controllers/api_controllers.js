const mongoose = require("mongoose");
const {
  Update_Data,
  formatName,
  handleAPIError,
  CleanPublicData,
  WriteImage,
  Create_Directory,
  Create_ID,
  Delete_Directory,
} = require("../utils/helpers");

/**
 * Create a document from Schema
 * See the API Documentation for further informations
 *
 * @param Schema
 * @returns {function(...[*]=)}
 */
exports.create = (Schema) => {
  const Schema_Name = formatName(Schema.collection.collectionName);

  return async (req, res) => {
    console.log(`API Request : Create new ${Schema_Name["singular"]}.`);
    if (!req.body.create)
      return res
        .status(400)
        .send({ success: false, message: "Create object is null." });

    // Create profile-pic system
    if (
      (Schema_Name["singular"] === "User" ||
        Schema_Name["singular"] === "Client") &&
      req.body.create.profile_pic &&
      req.body.create.profile_pic.uri
    ) {
      try {
        let Image = req.body.create.profile_pic;
        Image.name = Create_ID(10) + ".png";
        let _ID = Create_ID(32);
        Create_Directory("./public/images/profile-pic/" + _ID);
        if (WriteImage(Image, "./public/images/profile-pic/" + _ID + "/")) {
          req.body.create.profile_pic =
            "images/profile-pic/" + _ID + "/" + Image.name;
        } else delete req.body.create.profile_pic;
      } catch (error) {
        return handleAPIError(res, error);
      }
    }

    // Create store images system
    if (
      Schema_Name["singular"] === "Store" &&
      req.body.create.images &&
      req.body.create.images.length > 0
    ) {
      try {
        let _ID = Create_ID(32);
        for (let i = 0; i < req.body.create.images.length; i++) {
          req.body.create.images[i].name = Create_ID(10) + ".png";
          Create_Directory("./public/images/stores/" + _ID);
          if (
            WriteImage(
              req.body.create.images[i],
              "./public/images/stores/" + _ID + "/"
            )
          ) {
            req.body.create.images[i].uri =
              "images/stores/" + _ID + "/" + req.body.create.images[i].name;
            delete req.body.create.images[i].name;
          }
        }
      } catch (error) {
        return handleAPIError(res, error);
      }
    }

    Schema.create(req.body.create, (err, Data) => {
      if (err) return handleAPIError(res, err);
      return res.status(201).send({ success: true, _id: Data._id });
    });
  };
};

/**
 * Get a document from Schema
 * See the API Documentation for further informations
 *
 * @param Schema
 * @returns {function(...[*]=)}
 */
exports.get = (Schema) => {
  const Schema_Name = formatName(Schema.collection.collectionName);

  return async (req, res) => {
    console.log(`API Request : Get ${Schema_Name["singular"]}.`);
    if (!req.params.id || !mongoose.Types.ObjectId.isValid(req.params.id))
      return res.status(400).send({
        success: false,
        message: `${Schema_Name["singular"]} id is not valid.`,
      });

    let selector = {
      _id: req.params.id,
    };

    if (req.body.selector) {
      selector = { ...selector };
    }

    Schema.findOne(selector, (err, Data) => {
      if (err) return handleAPIError(res, err);
      if (!Data)
        return res.status(404).send({
          success: false,
          message: `${Schema_Name["singular"]} doesn't exist.`,
        });

      return res
        .status(200)
        .send({ success: true, data: CleanPublicData(Data) });
    });
  };
};

/**
 * List all documents from Schema
 * See the API Documentation for further informations
 *
 * @param Schema
 * @returns {function(...[*]=)}
 */
exports.list = (Schema) => {
  const Schema_Name = formatName(Schema.collection.collectionName);

  return async (req, res) => {
    console.log(`API Request : Get ${Schema_Name["plural"]} list.`);
    let selector = req.body.selector ? req.body.selector : {};
    if (!selector)
      return res
        .status(400)
        .send({ success: false, message: "Selector object is null." });

    let limit = req.body.limit ? req.body.limit : 50,
      offset = req.body.offset ? req.body.offset : 0,
      sort = req.body.sort ? req.body.sort : { createdAt: -1 },
      isDeleted = req.body.isDeleted ? req.body.isDeleted : false;

    if (Schema_Name["singular"] === "Store") limit = req.body.limit ? req.body.limit : 25;
    Schema.find(selector, (err, List_Data) => {
      if (err) return handleAPIError(res, err);
      if (List_Data.length === 0)
        return res.status(404).send({
          success: false,
          message: `${Schema_Name["plural"]} list is empty.`,
        });

      return res
        .status(200)
        .send({ success: true, data: CleanPublicData(List_Data) });
    })
      .sort(sort)
      .isDeleted(isDeleted)
      .skip(offset)
      .limit(limit);
  };
};

/**
 * Update a document from Schema
 * See the API Documentation for further informations
 *
 * @param Schema
 * @returns {function(...[*]=)}
 */
exports.update = (Schema) => {
  const Schema_Name = formatName(Schema.collection.collectionName);

  return async (req, res) => {
    console.log(`API Request : Update ${Schema_Name["singular"]}.`);
    if (!req.params.id || !mongoose.Types.ObjectId.isValid(req.params.id))
      return res.status(400).send({
        success: false,
        message: `${Schema_Name["singular"]} id is not valid.`,
      });
    if (!req.body.update || Object.entries(req.body.update).length === 0)
      return res
        .status(400)
        .send({ success: false, message: "Update object is null." });

    let selector = {
      _id: req.params.id,
    };

    if (req.body.selector) {
      selector = { ...selector, ...req.body.selector };
    }

    Schema.findOne(selector, (err, Data) => {
      if (err) return handleAPIError(res, err);
      if (!Data)
        return res.status(404).send({
          success: false,
          message: `${Schema_Name["singular"]} doesn't exist.`,
        });

      // Update profile-pic system
      if (
        (Schema_Name["singular"] === "User" ||
          Schema_Name["singular"] === "Client") &&
        req.body.update.profile_pic &&
        req.body.update.profile_pic.uri
      ) {
        try {
          let Image = req.body.update.profile_pic;
          Image.name = Create_ID(10) + ".png";
          let _ID = Create_ID(32);
          Create_Directory("./public/images/profile-pic/" + _ID);
          if (WriteImage(Image, "./public/images/profile-pic/" + _ID + "/")) {
            if (Data["profile_pic"] !== "images/profile-pic/user-default.jpeg")
              Delete_Directory("./public/" + Data["profile_pic"].slice(0, -14));
              req.body.update.profile_pic = "images/profile-pic/" + _ID + "/" + Image.name;
          }
        } catch (error) {
          return handleAPIError(res, error);
        }
      }

      // Update store images system (Replace all old images with new images)
      if (
        Schema_Name["singular"] === "Store" &&
        req.body.update.images &&
        req.body.update.images.length > 0
      ) {
        try {
          let _ID = Create_ID(32);
          let _Images = [];
          for (let i = 0; i < req.body.update.images.length; i++) {
            if (req.body.update.images[i].uri.includes("images/stores")) {
              _Images.push({
                uri: req.body.update.images[i].uri.slice(
                  req.body.update.images[i].uri.indexOf("images")
                ),
              });
            }
            if (
              req.body.update.images[i].uri.includes("images/stores") === false
            ) {
              req.body.update.images[i].name = Create_ID(10) + ".png";
              Create_Directory("./public/images/stores/" + _ID);
              if (
                WriteImage(
                  req.body.update.images[i],
                  "./public/images/stores/" + _ID + "/"
                )
              ) {
                _Images.push({
                  uri:
                    "images/stores/" +
                    _ID +
                    "/" +
                    req.body.update.images[i].name,
                });
              }
            }
          }
          Data["images"] = _Images;
          delete req.body.update.images;
        } catch (error) {
          return handleAPIError(res, error);
        }
      }

      if(Object.keys(req.body.update).length > 0) {
        Data = Update_Data(Data, req.body.update);
        Data.save((err) => {
          if (err) return handleAPIError(res, err);
          return res.status(200).send({ success: true, data: Data });
        });
      }
    })
  };
};

/**
 * Soft delete a document from Schema
 * See the API Documentation for further informations
 *
 * @param Schema
 * @returns {function(...[*]=)}
 */
exports.delete = (Schema) => {
  const Schema_Name = formatName(Schema.collection.collectionName);

  return async (req, res) => {
    console.log(`API Request : Delete ${Schema_Name["singular"]}.`);
    if (!req.params.id || !mongoose.Types.ObjectId.isValid(req.params.id))
      return res.status(400).send({
        success: false,
        message: `${Schema_Name["singular"]} id is not valid.`,
      });

    let selector = {
      _id: req.params.id,
    };

    if (req.body.selector) {
      selector = { ...selector, ...req.body.selector };
    }

    Schema.findOne(selector, (err, Data) => {
      if (err) return handleAPIError(res, err);
      if (!Data)
        return res.status(404).send({
          success: false,
          message: `${Schema_Name["singular"]} doesn't exist.`,
        });

      Data.softdelete((err) => {
        if (err) return handleAPIError(res, err);
        return res.status(200).send({ success: true });
      });
    });
  };
};

/**
 * Restore a soft deleted document from schema
 * See the API Documentation for further informations
 *
 * @param Schema
 * @returns {function(...[*]=)}
 */
exports.restore = (Schema) => {
  const Schema_Name = formatName(Schema.collection.collectionName);

  return async (req, res) => {
    console.log(`API Request : Restore ${Schema_Name["singular"]}`);
    if (!req.params.id || !mongoose.Types.ObjectId.isValid(req.params.id))
      return res.status(400).send({
        success: false,
        message: `${Schema_Name["singular"]} id is not valid.`,
      });

    let selector = {
      _id: req.params.id,
    };

    if (req.body.selector) {
      selector = { ...selector, ...req.body.selector };
    }

    Schema.findOne(selector, (err, Data) => {
      if (err) return handleAPIError(res, err);
      if (!Data)
        return res.status(404).send({
          success: false,
          message: `${Schema_Name["singular"]} doesn't exist.`,
        });

      Data.restore((err) => {
        if (err) return handleAPIError(res, err);
        return res.status(200).send({ success: true });
      });
    });
  };
};
