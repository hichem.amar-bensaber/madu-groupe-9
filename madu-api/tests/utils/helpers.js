const mongoose = require("mongoose");
const fakeObjectIDMongoose = new mongoose.Types.ObjectId();

/**
 * Some invalid emails
 *
 * @type {string[]}
 */
const invalidEmails = [
  "plainaddress",
  "#@%^%#$@#$@#.com",
  "@example.com",
  "Joe Smith <email@example.com>",
  "email.example.com",
  "email@example@example.com",
  "email@example.com (Joe Smith)",
  "email@example",
  "email@example..com",
];

/**
 * Some test passwords
 *
 * @type {string[]}
 */
const somePasswords = [
  "123456",
  "123456789",
  "qwerty",
  "password",
  "12345",
  "mon super mot de passe",
];

/**
 * Some fake datas to create an user
 *
 * @type {{password: string, role: {tag: string, slug: string}, last_name: string, first_name: string, email: string}}
 */
const userData = {
  first_name: "John",
  last_name: "Doe",
  email: "johndoe@email.com",
  password: "johndoe",
  role: "C",
  _client: fakeObjectIDMongoose,
};

const userData2 = {
  first_name: "Toto",
  last_name: "Tata",
  email: "toto@email.com",
  password: "toto",
  role: "SA",
};

const tokenUserData = {
  first_name: "Token",
  last_name: "User",
  email: "tokenUser@email.com",
  password: "tokenuser",
  role: "SA",
};

/**
 * Some fake datas to create a client
 *
 * @type {{phone: string, localisation: {address: string, city: string, zip_code: string}, name: string, email: string}}
 */
const clientData = {
  name: "Hetic",
  localisation: {
    city: "Montreuil",
    address: "134 rue Saint-Denis",
    zip_code: "93100",
  },
  coords: {
    latitude: 4.24434,
    longitude: 1.4353,
  },
  phone: "0343453",
  email: "hetic@hetic.net",
};

const clientData2 = {
  name: "Epitech",
  localisation: {
    city: "Le Kremlin-Bicêtre",
    address: "24 Rue Pasteur",
    zip_code: "94270",
  },
  coords: {
    latitude: 4.24434,
    longitude: 1.4353,
  },
  phone: "0343453",
  email: "epitech@gmail.com",
};

/**
 * Some fake datas to create a store
 *
 * @type {{images: [{path: string, name: string}, {path: string, name: string}, {path: string, name: string}], localisation: {address: string, city: string, zip_code: string}, name: string, store_type: string, coords: {latitude: string, longitude: string}, status: {id: number, slug: string}}}
 */
const storeData = {
  name: "Mon super magasin bio",
  store_type: "Boutique",
  localisation: {
    city: "Paris 10",
    address: "rue de la paix",
    zip_code: "75010",
  },
  coords: {
    latitude: 4.24434,
    longitude: 1.4353,
  },
  status: "PENDING",
  images: [
    {
      name: "Ma super image 01",
      path: "/images/ma-super-image-01.jpg",
    },
    {
      name: "Ma super image 02",
      path: "/images/ma-super-image-02.jpg",
    },
    {
      name: "Ma super image 02",
      path: "/images/ma-super-image-03.jpg",
    },
  ],
  tags: [
    {
      name: "Bio",
    },
    {
      name: "Vegan",
    },
  ],
};

const storeData2 = {
  name: "Mon super magasin bio 2",
  store_type: "Restaurant",
  localisation: {
    city: "Paris 10",
    address: "rue de la paix",
    zip_code: "75010",
  },
  coords: {
    latitude: 4.24434,
    longitude: 1.4353,
  },
  status: "PENDING",
  images: [
    {
      name: "Ma super image 01",
      path: "/images/ma-super-image-01.jpg",
    },
    {
      name: "Ma super image 02",
      path: "/images/ma-super-image-02.jpg",
    },
    {
      name: "Ma super image 02",
      path: "/images/ma-super-image-03.jpg",
    },
  ],
  tags: [
    {
      name: "Bio",
    },
    {
      name: "Vegan",
    },
  ],
};

module.exports = {
  invalidEmails,
  somePasswords,
  userData,
  userData2,
  tokenUserData,
  clientData,
  clientData2,
  storeData,
  storeData2,
};
