const app = require('../../server');
const mongoose = require('mongoose');
const dbHandler = require('../utils/db-handler');
const supertest = require('supertest');
const request = supertest(app);

const User = require('../../models/users');
const Client = require('../../models/clients');
const { userData, userData2, clientData, clientData2, tokenUserData } = require('../utils/helpers');

const baseRouteURL = '/users';
const fakeObjectIDMongoose = new mongoose.Types.ObjectId();

let token;

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => await dbHandler.connect());

/**
 * Get token before every test
 */
beforeEach(async () => {
    const tokenUser = new User(tokenUserData);
    const savedTokenUser = await tokenUser.save();
    const tokenRes = await request.post('/login').send({email: savedTokenUser.email, password: tokenUserData.password});
    token = tokenRes.body.token;
});

/**
 * Clear all test data after every test.
 */
afterEach(async () => await dbHandler.clearDatabase());

/**
 * Remove and close the db and server.
 */
afterAll(async () => await dbHandler.closeDatabase());

// TODO : FINIR LES TESTS D'AUTORISATION
describe('API Users', () => {

    // GET API endpoints for users
    describe(`GET ${baseRouteURL}`, () => {

        describe(`GET ${baseRouteURL}/list`, () => {

            // Fail if there is no token in the request
            it(`GET ${baseRouteURL}/list => should failed if there is no token provided`, async done => {
                const res = await request.get(`${baseRouteURL}/list`);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`GET ${baseRouteURL}/list => should failed if the token user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.get(`${baseRouteURL}/list`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if the token user is a customer
            it(`GET ${baseRouteURL}/list => should failed if the token user is a customer`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.role = 'C';
                    tokenUser._client = fakeObjectIDMongoose;
                    tokenUser.save();
                });

                const res = await request.get(`${baseRouteURL}/list`)
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(401);
                expect(res.body).toBe({});

                done();
            });

            // It should get all users should return the 3 same users created before in the order of createdAt
            it(`GET ${baseRouteURL}/list => should return all users, test if it retrieve users with rights datas & sorted by createdAt`, async done => {
                // Create 2 users
                const firstUser = new User(userData);
                const secondUser = new User(userData2);
                const savedFirstUser = await firstUser.save();
                const savedSecondUser = await secondUser.save();

                const res = await request.get(`${baseRouteURL}/list`)
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(3);

                for (let property in userData2) {
                    if (userData2.hasOwnProperty(property)) {
                        expect(JSON.stringify(Object.values(res.body.data)[0][property])).toBe(JSON.stringify(savedSecondUser[property]));
                    }
                }
                for (let property in userData) {
                    if (userData.hasOwnProperty(property)) {
                        expect(JSON.stringify(Object.values(res.body.data)[1][property])).toBe(JSON.stringify(savedFirstUser[property]));
                    }
                }
                for (let property in tokenUserData) {
                    if (tokenUserData.hasOwnProperty(property) && property !== 'password') {
                        expect(JSON.stringify(Object.values(res.body.data)[2][property])).toBe(JSON.stringify(tokenUserData[property]));
                    } else if (tokenUserData.hasOwnProperty(property) && property === 'password') {
                        expect(JSON.stringify(Object.values(res.body.data)[2][property])).not.toBe(JSON.stringify(tokenUserData[property]));
                    }
                }

                done();
            });

            // Should return all users associated with the client
            it(`GET ${baseRouteURL}/list => should return all users associated with the client`, async done => {
                // Create 2 clients
                const firstClient = new Client(clientData);
                const savedFirstClient = await firstClient.save();
                const secondClient = new Client(clientData2);
                const savedSecondClient = await secondClient.save();

                // Create 5 customer users for first client
                for (let i = 0 ; i < 5 ; i++) {
                    const userDataWithFirstClient = Object.assign({}, userData);
                    userDataWithFirstClient.email = 'customer' + userData.email + i;
                    userDataWithFirstClient.role = 'C';
                    userDataWithFirstClient._client = savedFirstClient._id;
                    const newUser = new User(userDataWithFirstClient);
                    await newUser.save();
                }
                // Create 5 admin users for first client
                for (let i = 0 ; i < 5 ; i++) {
                    const userDataWithFirstClient = Object.assign({}, userData);
                    userDataWithFirstClient.email = 'admin' + userData.email + i;
                    userDataWithFirstClient.role = 'A';
                    userDataWithFirstClient._client = savedFirstClient._id;
                    const newUser = new User(userDataWithFirstClient);
                    await newUser.save();
                }

                // Create 5 customer users for second client
                for (let i = 0 ; i < 5 ; i++) {
                    const userDataWithSecondClient = Object.assign({}, userData2);
                    userDataWithSecondClient.email = 'customer' + userData2.email + i;
                    userDataWithSecondClient.role = 'C';
                    userDataWithSecondClient._client = savedSecondClient._id;
                    const newUser = new User(userDataWithSecondClient);
                    await newUser.save();
                }
                // Create 5 admin users for second client
                for (let i = 0 ; i < 5 ; i++) {
                    const userDataWithSecondClient = Object.assign({}, userData2);
                    userDataWithSecondClient.email = 'admin' + userData2.email + i;
                    userDataWithSecondClient.role = 'A';
                    userDataWithSecondClient._client = savedSecondClient._id;
                    const newUser = new User(userDataWithSecondClient);
                    await newUser.save();
                }

                // Token user admin of the first client (should only retrieve users associated with first client)
                const adminTokenUserWithFirstClientData = Object.assign({}, userData);
                adminTokenUserWithFirstClientData.role = 'A';
                adminTokenUserWithFirstClientData._client = savedFirstClient._id;
                const adminTokenUserWithFirstClient = new User(adminTokenUserWithFirstClientData);
                const savedAdminTokenUserWithFirstClient = await adminTokenUserWithFirstClient.save();

                const adminTokenUserWithFirstClientRes = await request.post('/login').send({email: savedAdminTokenUserWithFirstClient.email, password: userData.password});
                adminTokenUserWithFirstClientToken = adminTokenUserWithFirstClientRes.body.token;

                const res = await request.get(`${baseRouteURL}/list`)
                    .set("Authorization", "Bearer " + adminTokenUserWithFirstClientToken);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(10);

                // Check we get the rights datas
                for (let i = 4 ; i >= 0 ; i--) {
                    expect(res.body.data[i]['email']).toBe('admin' + userData.email + i);
                }
                for (let i = 4 ; i >= 0 ; i--) {
                    expect(res.body.data[i]['email']).toBe('customer' + userData.email + i);
                }

                done();
            });

            // It should get all users that are not soft deleted
            it(`GET ${baseRouteURL}/list => should return all non soft deleted users`, async done => {
                // Create 2 users
                const firstUser = new User(userData);
                const secondUser = new User(userData2);
                const savedFirstUser = await firstUser.save();
                const savedSecondUser = await secondUser.save();

                await savedFirstUser.softdelete();

                const res = await request.get(`${baseRouteURL}/list`)
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(2);

                for (let property in userData2) {
                    if (userData2.hasOwnProperty(property) && property !== 'password') {
                        expect(JSON.stringify(res.body.data[0][property])).toBe(JSON.stringify(userData2[property]));
                    }
                }
                for (let property in tokenUserData) {
                    if (tokenUserData.hasOwnProperty(property) && property !== 'password') {
                        expect(JSON.stringify(res.body.data[1][property])).toBe(JSON.stringify(tokenUserData[property]));
                    }
                }

                done();
            });

            // It should get all soft deleted users
            it(`GET ${baseRouteURL}/list => should return all soft deleted users`, async done => {
                // Create 2 users
                const firstUser = new User(userData);
                const secondUser = new User(userData2);
                const savedFirstUser = await firstUser.save();
                const savedSecondUser = await secondUser.save();

                await savedFirstUser.softdelete();
                await savedSecondUser.softdelete();

                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        isDeleted: true,
                        selector: {
                            deleted: true
                        }
                    })
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(2);

                for (let property in userData2) {
                    if (userData2.hasOwnProperty(property) && property !== 'password') {
                        expect(JSON.stringify(res.body.data[0][property])).toBe(JSON.stringify(userData2[property]));
                    }
                }
                for (let property in userData) {
                    if (userData.hasOwnProperty(property) && property !== 'password') {
                        expect(JSON.stringify(res.body.data[1][property])).toBe(JSON.stringify(userData[property]));
                    }
                }

                done();
            });

            // It should return empty datas if selector isn't in the database
            it(`GET ${baseRouteURL}/list => should return empty data`, async done => {
                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        selector: {
                            email: 'test@gmail.com'
                        }
                    })
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(404);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Users list is empty.");

                done();
            });

            // Should failed if selector target an user not in the token user company
            it(`GET ${baseRouteURL}/list => should failed if selector target an user not in the token user company`, async done => {
                // Create a client
                const client = new Client(clientData);
                const savedClient = await client.save();

                // Create an user associated with this client
                const userWithClient = new User(Object.assign({}, {...userData, ...{role: 'C', _client: savedClient._id}}));
                const savedUserWithClient = await userWithClient.save();

                // Create a token user with a fake client in
                const tokenUserWithFakeClientData = Object.assign({}, {...userData2, ...{role: 'A', _client: fakeObjectIDMongoose}});
                const tokenUserWithFakeClient = new User(tokenUserWithFakeClientData);
                const savedTokenUserWithFakeClient = await tokenUserWithFakeClient.save();

                const tokenUserWithFakeClientRes = await request.post('/login').send({email: savedTokenUserWithFakeClient.email, password: userData2.password});
                const tokenUserWithFakeClientToken = tokenUserWithFakeClientRes.body.token;

                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        selector: {
                            email: [userData.email]
                        }
                    })
                    .set("Authorization", "Bearer " + tokenUserWithFakeClientToken);

                expect(res.status).toBe(404);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Users list is null.");

                done();
            });

            // Selectors should works
            it(`GET ${baseRouteURL}/list => selectors should works`, async done => {
                // Create 2 users
                const firstUser = new User(userData);
                const secondUser = new User(userData2);
                const savedFirstUser = await firstUser.save();
                const savedSecondUser = await secondUser.save();

                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        selector: {
                            email: [userData.email, userData2.email]
                        }
                    })
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(2);
                expect(res.body.data[0].email).toBe(userData2.email);
                expect(res.body.data[1].email).toBe(userData.email);

                done();
            });

            // Selectors should works only on users associated with the token user company
            it(`GET ${baseRouteURL}/list => selectors should works only on users associated with the token user company`, async done => {
                // Create 1 client
                const client = new Client(clientData);
                const savedClient = await client.save();

                // Create 2 users
                const firstUser = new User(Object.assign({}, {...userData, ...{role: 'C', _client: savedClient._id}}));
                const secondUser = new User(Object.assign({}, {...userData2, ...{role: 'C', _client: fakeObjectIDMongoose}}));
                const savedFirstUser = await firstUser.save();
                const savedSecondUser = await secondUser.save();

                // Token user associated with the client
                const tokenUserWithClient = new User(Object.assign({}, {...userData, ...{email: 'newemail@gmail.com', role: 'A', _client: savedClient._id}}));
                const savedTokenUserWithClient = await tokenUserWithClient.save();

                const tokenUserWithClientRes = await request.post('/login').send({email: savedTokenUserWithClient.email, password: userData.password});
                const tokenUserWithClientToken = tokenUserWithClientRes.body.token;

                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        selector: {
                            role: 'C'
                        }
                    })
                    .set("Authorization", "Bearer " + tokenUserWithClientToken);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(1);
                expect(res.body.data[0].email).toBe(userData.email);

                done();
            });

            // Limit should works only on users associated with the token user company
            it(`GET ${baseRouteURL}/list => should limit results to 5 for users associated with the token user company`, async done => {
                // Create 1 client
                const client = new Client(clientData);
                const savedClient = await client.save();

                // Create a token user associated with client
                const tokenUserWithClient = new User(Object.assign({}, {...userData, ...{email: 'newemail@gmail.com', role: 'A', _client: savedClient._id}}));
                const savedTokenUserWithClient = await tokenUserWithClient.save();

                const tokenUserWithClientRes = await request.post('/login').send({email: savedTokenUserWithClient.email, password: userData.password});
                const tokenUserWithClientToken = tokenUserWithClientRes.body.token;

                // Create 10 users associated with client
                for (let i = 0 ; i < 10 ; i++) {
                    let data = Object.assign({}, userData);
                    data.role = 'C';
                    data._client = savedClient._id;
                    data.first_name = data.first_name + i;
                    data.email = data.email + i;
                    await new User(data).save();
                }

                // Create 10 users associated with a fake client
                for (let i = 0 ; i < 10 ; i++) {
                    let data = Object.assign({}, userData2);
                    data.role = 'C';
                    data._client = fakeObjectIDMongoose;
                    data.first_name = data.first_name + i;
                    data.email = data.email + i;
                    await new User(data).save();
                }

                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        limit: 5
                    })
                    .set("Authorization", "Bearer " + tokenUserWithClientToken);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(5);
                expect(res.body.data[0].first_name).toBe(userData.first_name + 9);
                expect(res.body.data[1].first_name).toBe(userData.first_name + 8);
                expect(res.body.data[2].first_name).toBe(userData.first_name + 7);
                expect(res.body.data[3].first_name).toBe(userData.first_name + 6);
                expect(res.body.data[4].first_name).toBe(userData.first_name + 5);

                done();
            });

            // Limit should works
            it(`GET ${baseRouteURL}/list => should limit results to 5`, async done => {
                // Create 10 users
                for (let i = 0 ; i < 10 ; i++) {
                    let data = Object.assign({}, userData);
                    data.email = data.email + i;
                    data.first_name = data.first_name + i;
                    await new User(data).save();
                }

                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        limit: 5
                    })
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(5);
                expect(res.body.data[0].first_name).toBe(userData.first_name + 9);
                expect(res.body.data[1].first_name).toBe(userData.first_name + 8);
                expect(res.body.data[2].first_name).toBe(userData.first_name + 7);
                expect(res.body.data[3].first_name).toBe(userData.first_name + 6);
                expect(res.body.data[4].first_name).toBe(userData.first_name + 5);

                done();
            });

            // Offset should works only on users associated with the token user company
            it(`GET ${baseRouteURL}/list => should limit results to 5 but offset at 5 for users associated with the token user company`, async done => {
                // Create 1 client
                const client = new Client(clientData);
                const savedClient = await client.save();

                // Create a token user associated with client
                const tokenUserWithClient = new User(Object.assign({}, {...userData, ...{email: 'newemail@gmail.com', role: 'A', _client: savedClient._id}}));
                const savedTokenUserWithClient = await tokenUserWithClient.save();

                const tokenUserWithClientRes = await request.post('/login').send({email: savedTokenUserWithClient.email, password: userData.password});
                const tokenUserWithClientToken = tokenUserWithClientRes.body.token;

                // Create 10 users associated with client
                for (let i = 0 ; i < 10 ; i++) {
                    let data = Object.assign({}, userData);
                    data.role = 'C';
                    data._client = savedClient._id;
                    data.first_name = data.first_name + i;
                    data.email = data.email + i;
                    await new User(data).save();
                }

                // Create 10 users associated with a fake client
                for (let i = 0 ; i < 10 ; i++) {
                    let data = Object.assign({}, userData2);
                    data.role = 'C';
                    data._client = fakeObjectIDMongoose;
                    data.first_name = data.first_name + i;
                    data.email = data.email + i;
                    await new User(data).save();
                }

                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        limit: 5,
                        offset: 5
                    })
                    .set("Authorization", "Bearer " + tokenUserWithClientToken);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(5);
                expect(res.body.data[0].first_name).toBe(userData.first_name + 4);
                expect(res.body.data[1].first_name).toBe(userData.first_name + 3);
                expect(res.body.data[2].first_name).toBe(userData.first_name + 2);
                expect(res.body.data[3].first_name).toBe(userData.first_name + 1);
                expect(res.body.data[4].first_name).toBe(userData.first_name + 0);

                done();
            });

            // Offset should works
            it(`GET ${baseRouteURL}/list => should limit results to 5 but offset at 5`, async done => {
                // Create 10 users
                for (let i = 0 ; i < 10 ; i++) {
                    let data = Object.assign({}, userData);
                    data.email = data.email + i;
                    data.first_name = data.first_name + i;
                    await new User(data).save();
                }

                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        limit: 5,
                        offset: 5
                    })
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(5);
                expect(res.body.data[0].first_name).toBe(userData.first_name + 4);
                expect(res.body.data[1].first_name).toBe(userData.first_name + 3);
                expect(res.body.data[2].first_name).toBe(userData.first_name + 2);
                expect(res.body.data[3].first_name).toBe(userData.first_name + 1);
                expect(res.body.data[4].first_name).toBe(userData.first_name + 0);

                done();
            });

        });

        describe(`GET ${baseRouteURL}/:id`, () => {

            // Fail if there is no token in the request
            it(`GET ${baseRouteURL}/:id => should failed if there is no token provided`, async done => {
                const res = await request.get(`${baseRouteURL}/noTokenRoute`);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`GET ${baseRouteURL}/:id => should failed if the token user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.get(`${baseRouteURL}/softDeletedTokenUser`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if the token user is a customer
            it(`GET ${baseRouteURL}/:id => should failed if the token user is a customer`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.role = 'C';
                    tokenUser._client = fakeObjectIDMongoose;
                    tokenUser.save();
                });

                const res = await request.get(`${baseRouteURL}/${fakeObjectIDMongoose}`)
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(401);
                expect(res.body).toBe({});

                done();
            });

            // Fail if the token user is an admin but try to retrieve an user from another company
            it(`GET ${baseRouteURL}/:id => should failed if the token user is an admin, but try to retrieve an user from another company`, async done => {
                // Create client & user associated to
                const savedClient = await new Client(clientData).save();
                const savedUser = await new User(Object.assign({}, {...userData, ...{role: 'C', _client: savedClient._id}})).save();

                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.role = 'A';
                    tokenUser._client = fakeObjectIDMongoose;
                    tokenUser.save();
                });

                const res = await request.get(`${baseRouteURL}/${savedUser._id}`)
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(404);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("User doesn't exist.");

                done();
            });

            // Don't retrieve an user if it doesn't exists
            it(`GET ${baseRouteURL}/:id => should failed to retrieve a single user`, async done => {
                const res = await request.get(`${baseRouteURL}/${fakeObjectIDMongoose}`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(404);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("User doesn't exist.");

                done();
            });

            // Error if id isn't valid
            it(`GET ${baseRouteURL}/:id => should failed if id is not valid`, async done => {
                const res = await request.get(`${baseRouteURL}/toto`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe('User id is not valid.');

                done();
            });

            // Test token user admin can successfully retrieve user from his company
            it(`GET ${baseRouteURL}/:id => should successfully retrieve user from company of the token user admin`, async done => {
                // Create client & user associated to
                const savedClient = await new Client(clientData).save();
                const savedUser = await new User(Object.assign({}, {...userData, ...{role: 'C', _client: savedClient._id}})).save();

                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.role = 'A';
                    tokenUser._client = savedClient._id;
                    tokenUser.save();
                });

                const res = await request.get(`${baseRouteURL}/${savedUser._id}`)
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);

                await User.findById(savedUser._id, function (err, doc_user) {
                    doc_user = JSON.parse(JSON.stringify(doc_user));

                    expect(res.body.data).toMatchObject(doc_user);
                });

                done();
            });

            // Test it retrieve the user with the id we created
            it(`GET ${baseRouteURL}/:id => should return a single user with his id`, async done => {
                const user = new User(userData);
                const savedUser = await user.save();

                const res = await request.get(`${baseRouteURL}/${savedUser._id}`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);

                await User.findById(savedUser._id, function (err, doc_user) {
                    doc_user = JSON.parse(JSON.stringify(doc_user));

                    expect(res.body.data).toMatchObject(doc_user);
                });

                done();
            });

        });

    });

    // POST API endpoints for users
    describe(`POST ${baseRouteURL}`, () => {

        describe(`POST ${baseRouteURL}/create`, () => {

            // Fail if there is no token provided
            it(`POST ${baseRouteURL}/create => should failed if there is no token provided`, async done => {
                const res = await request.post(`${baseRouteURL}/create`);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`POST ${baseRouteURL}/create => should failed if the user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.post(`${baseRouteURL}/create`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if the token user is a customer
            it(`POST ${baseRouteURL}/create => should failed if the token user is a customer`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.role = 'C';
                    tokenUser._client = fakeObjectIDMongoose;
                    tokenUser.save();
                });

                const res = await request.post(`${baseRouteURL}/create`)
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(401);
                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if there is nothing sent to request
            it(`POST ${baseRouteURL}/create => should failed if there is nothing sent to request`, async done => {
                const res = await request.post(`${baseRouteURL}/create`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Create object is null.");

               done();
            });

            // Fail if there is nothing sent to post
            it(`POST ${baseRouteURL}/create => should failed if there is an error in the parameter`, async done => {
                const res = await request.post(`${baseRouteURL}/create`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        create: {}
                    });

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.errors).toEqual(expect.any(Object));

                done();
            });

            // Create an user should fail if there is already an user with the same email
            it(`POST ${baseRouteURL}/create => should fail creating an user with an already existing email`, async done => {
                const res = await request.post(`${baseRouteURL}/create`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        create: tokenUserData
                    });

                // Check return request
                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.errors).toEqual(expect.any(Object));
                expect(res.body.errors.email).toBeDefined();

                done();
            });

            // Should failed if the client isn't referenced and the user is a customer
            it(`POST ${baseRouteURL}/create => should fail if client isn't referenced for a customer`, async done => {
                let userDataWithoutClient = Object.assign({}, userData);
                userDataWithoutClient._client = null;

                const res = await request.post(`${baseRouteURL}/create`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        create: userDataWithoutClient
                    });

                // Check return request
                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.errors).toEqual(expect.any(Object));
                expect(res.body.errors._client).toBeDefined();

                done();
            });

            // Should failed if the client isn't referenced and the user is an admin
            it(`POST ${baseRouteURL}/create => should fail if client isn't referenced for an admin`, async done => {
                let userDataWithoutClient = Object.assign({}, userData);
                userDataWithoutClient.role = 'A';
                userDataWithoutClient._client = null;

                const res = await request.post(`${baseRouteURL}/create`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        create: userDataWithoutClient
                    });

                // Check return request
                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.errors).toEqual(expect.any(Object));
                expect(res.body.errors._client).toBeDefined();

                done();
            });

            // Should failed if the token user is an admin and try to create a super admin
            it(`POST ${baseRouteURL}/create => should failed if the token user is an admin and try to create a super admin`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.role = 'A';
                    tokenUser._client = fakeObjectIDMongoose;
                    tokenUser.save();
                });

                const res = await request.post(`${baseRouteURL}/create`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        create: userData2
                    });

                // Check return request
                expect(res.status).toBe(401);
                expect(res.body).toMatchObject({});

                // Make sure super admin didn't get created
                await User.findOne({email: userData2.email}, function (err, notCreatedSuperAdmin) {
                    expect(notCreatedSuperAdmin).toBe(null);
                });

                done();
            });

            // Should create an user with the associated client from the token user admin
            it(`POST ${baseRouteURL}/create => should create an user & should associate the user to the token user admin client`, async done => {
                // Create client
                const savedClient = await new Client(clientData).save();

                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.role = 'A';
                    tokenUser._client = savedClient._id;
                    tokenUser.save();
                });

                const res = await request.post(`${baseRouteURL}/create`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        create: userData
                    });

                // Check return request
                expect(res.status).toBe(201);
                expect(res.body.success).toBe(true);
                expect(res.body._id).toBeDefined();

                // Check if user has been created
                await User.findOne({email: userData.email}, function (err, user) {
                    expect(JSON.stringify(user._id)).toBe(JSON.stringify(res.body._id));
                    expect(JSON.stringify(user._client)).toBe(JSON.stringify(savedClient._id));
                });

                done();
            });

            // Create an user
            it(`POST ${baseRouteURL}/create => should create an user`, async done => {
                const res = await request.post(`${baseRouteURL}/create`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        create: userData
                    });

                // Check return request
                expect(res.status).toBe(201);
                expect(res.body.success).toBe(true);
                expect(res.body._id).toBeDefined();

                // Check if user has been created
                await User.findOne({email: userData.email}, function (err, user) {
                    expect(JSON.stringify(user._id)).toBe(JSON.stringify(res.body._id));
                });

                done();
            });

        });

    });

    // PUT API endpoints for users
    describe(`PUT ${baseRouteURL}`, () => {

        describe(`PUT ${baseRouteURL}/:id/update`, () => {

            // Fail if there is no token provided
            it(`PUT ${baseRouteURL}/:id/update => should failed if there is no token provided`, async done => {
                const res = await request.put(`${baseRouteURL}/11/update`);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`PUT ${baseRouteURL}/:id/update => should failed if the user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.put(`${baseRouteURL}/11/update`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if the id is not valid
            it(`PUT ${baseRouteURL}/:id/update => should failed if the user id is not valid`, async done => {
                const res = await request.put(`${baseRouteURL}/111/update`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("User id is not valid.");

                done();
            });

            // Fail if there is nothing sent to the request
            it(`PUT ${baseRouteURL}/:id/update => should failed if there is nothing send to the request`, async done => {
                const res = await request.put(`${baseRouteURL}/${fakeObjectIDMongoose}/update`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Update object is null.");

                done();
            });

            // Should failed if there is no update value sent
            it(`PUT ${baseRouteURL}/:id/update => should failed if there is no update datas`, async done => {
                const tokenUser = await User.findOne({email: tokenUserData.email});

                const res = await request.put(`${baseRouteURL}/${tokenUser._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {}
                    });

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Update object is null.");

                done();
            });

            // Fail if the user doesn't exist
            it(`PUT ${baseRouteURL}/:id/update => should failed if the user doesn't exist`, async done => {
                const res = await request.put(`${baseRouteURL}/${fakeObjectIDMongoose}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            first_name: 'test'
                        }
                    });

                expect(res.status).toBe(404);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("User doesn't exist.");

                done();
            });

            // Fail if the email sent is already taken
            it(`PUT ${baseRouteURL}/:id/update => should failed if the user email update is already taken`, async done => {
                const user = new User(userData);
                const savedUser = await user.save();

                const tokenUser = await User.findOne({email: tokenUserData.email});

                const res = await request.put(`${baseRouteURL}/${tokenUser._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            email: savedUser.email
                        }
                    });

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.errors).toBeDefined();
                expect(res.body.errors.email).toEqual(expect.any(Object));
                expect(res.body.errors.email.kind).toBe('unique');

                done();
            });

            // Fail if there is missing required data
            it(`PUT ${baseRouteURL}/:id/update => should failed if there is missing required data`, async done => {
                const tokenUser = await User.findOne({email: tokenUserData.email});

                const res = await request.put(`${baseRouteURL}/${tokenUser._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            first_name: ''
                        }
                    });

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.errors).toBeDefined();
                expect(res.body.errors.first_name).toEqual(expect.any(Object));
                expect(res.body.errors.first_name.kind).toBe('required');

                const tokenUser2 = await User.findOne({email: tokenUserData.email});
                expect(tokenUser2.first_name).toBe(tokenUser.first_name);

                done();
            });

            // Should failed if client isn't referenced for a customer
            it(`PUT ${baseRouteURL}/:id/update => should failed if client isn't referenced for a customer`, async done => {
                const user = await new User(userData2).save();

                const res = await request.put(`${baseRouteURL}/${user._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            role: 'C',
                            first_name: 'test',
                        }
                    });

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.errors._client).toBeDefined();

                const user2 = await User.findOne({email: userData2.email});
                expect(user2.first_name).toBe(userData2.first_name);
                expect(JSON.stringify(user2._client)).toBe(JSON.stringify(userData2._client));

                done();
            });

            // Should failed if client isn't referenced for an admin
            it(`PUT ${baseRouteURL}/:id/update => should failed if client isn't referenced for an admin`, async done => {
                const user = await new User(userData2).save();

                const res = await request.put(`${baseRouteURL}/${user._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            role: 'A',
                            first_name: 'test',
                            _client: null
                        }
                    });

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.errors._client).toBeDefined();

                const user2 = await User.findOne({email: userData2.email});
                expect(user2.first_name).toBe(userData2.first_name);
                expect(JSON.stringify(user2._client)).toBe(JSON.stringify(userData2._client));

                done();
            });

            // Password should not be crypted again on update (if we don't change the password)
            it(`PUT ${baseRouteURL}/:id/update => password should not be crypted again on update (if we don't change the password)`, async done => {
                const tokenUser = await User.findOne({email: tokenUserData.email});

                const res = await request.put(`${baseRouteURL}/${tokenUser._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            first_name: 'Test Test Test'
                        }
                    });

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(res.body._id).toBe(JSON.parse(JSON.stringify(tokenUser._id)));

                const tokenUser2 = await User.findOne({email: tokenUserData.email});
                expect(tokenUser2.password).toBe(tokenUser.password);

                done();
            });

            // Test if the password is crypted after update
            it(`PUT ${baseRouteURL}/:id/update => should update the user with the data sent`, async done => {
                const tokenUser = await User.findOne({email: tokenUserData.email});

                const res = await request.put(`${baseRouteURL}/${tokenUser._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            password: 'my_new_password'
                        }
                    });

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(res.body._id).toBe(JSON.parse(JSON.stringify(tokenUser._id)));

                const tokenUser2 = await User.findOne({email: tokenUserData.email});
                expect(tokenUser2.password).not.toBe('my_new_password');

                done();
            });

            // Test if the password check works with updated data
            it(`PUT ${baseRouteURL}/:id/update => password compare should work with the new updated password`, async done => {
                const tokenUser = await User.findOne({email: tokenUserData.email});

                const res = await request.put(`${baseRouteURL}/${tokenUser._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            password: 'my_new_password'
                        }
                    });

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(res.body._id).toBe(JSON.parse(JSON.stringify(tokenUser._id)));

                const tokenUser2 = await User.findOne({email: tokenUserData.email});
                expect(await tokenUser2.ValidPassword('my_new_password')).toBe(true);

                done();
            });

            // Test if the password check fails with updated data, using the password before update
            it(`PUT ${baseRouteURL}/:id/update => password compare should fail using the password before update`, async done => {
                const tokenUser = await User.findOne({email: tokenUserData.email});

                const res = await request.put(`${baseRouteURL}/${tokenUser._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            password: 'my_new_password'
                        }
                    });

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(res.body._id).toBe(JSON.parse(JSON.stringify(tokenUser._id)));

                const tokenUser2 = await User.findOne({email: tokenUserData.email});
                expect(await tokenUser2.ValidPassword(tokenUserData.password)).toBe(false);

                done();
            });

            /* TODO: Faire les tests : */
            // Should not be able to access others users as a token user customer
            // Should not be able to update his own role as a token user customer
            // Should not be able to update his own client reference as a token user customer

            // Should not be able to access users of others companies as a token user admin
            // Should not be able to access others admins of own company as a token user admin
            // Should not be able to update own role to "SA" (Super admin) as a token user admin
            // Should not be able to update client reference of user from this token user company
            // Should not be able to update role to "SA" (Super admin) of user from this token user company

            // Update the actual token user customer
            // Update user associated with token user admin company

            // Update the user with the data sent
            it(`PUT ${baseRouteURL}/:id/update => should update the user with the data sent`, async done => {
                const user = new User(userData);
                const savedUser = await user.save();

                const res = await request.put(`${baseRouteURL}/${savedUser._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: userData2
                    });

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(res.body._id).toBe(JSON.parse(JSON.stringify(savedUser._id)));

                const savedUserUpdated = await User.findById(savedUser._id);

                for (let property in userData2) {
                    if (userData2.hasOwnProperty(property) && property !== 'password') {
                        expect(savedUserUpdated[property]).toBe(userData2[property]);
                    }
                }
                expect(savedUserUpdated).not.toMatchObject(savedUser);

                done();
            });

            // Check that "createdAt" field don't change & that "updatedAt" change
            it(`PUT ${baseRouteURL}/:id/update => should not change "createdAt" field. But change "updatedAt" field`, async done => {
                const tokenUser = await User.findOne({email: tokenUserData.email});

                const res = await request.put(`${baseRouteURL}/${tokenUser._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            first_name: 'My new first name'
                        }
                    });

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(res.body._id).toBe(JSON.parse(JSON.stringify(tokenUser._id)));

                const tokenUser2 = await User.findOne({email: tokenUserData.email});
                expect(JSON.stringify(tokenUser.createdAt)).toBe(JSON.stringify(tokenUser2.createdAt));
                expect(JSON.stringify(tokenUser.updatedAt)).not.toBe(JSON.stringify(tokenUser2.updatedAt));

                done();
            });

        });

    });

    // PATCH API endpoints for users
    describe(`PATCH ${baseRouteURL}`, () => {

        describe(`PATCH ${baseRouteURL}/:id/restore`, () => {

            // Fail if there is no token provided
            it(`PATCH ${baseRouteURL}/:id/restore => should failed if there is no token provided`, async done => {
                const res = await request.patch(`${baseRouteURL}/11/restore`);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`PATCH ${baseRouteURL}/:id/restore => should failed if the token user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.patch(`${baseRouteURL}/11/restore`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if the token user is a customer
            it(`PATCH ${baseRouteURL}/:id/restore => should failed if the token user is a customer`, async done => {
                // Create a user
                const user = new User(userData);
                const savedUser = await user.save();
                savedUser.softdelete();

                const customerUserTokenData = Object.assign({}, userData2);
                customerUserTokenData.role = 'C';
                customerUserTokenData._client = fakeObjectIDMongoose;

                const customerTokenUser = new User(customerUserTokenData);
                const savedCustomerTokenUser = await customerTokenUser.save();

                const customerTokenUserRequest = await request.post('/login').send({email: savedCustomerTokenUser.email, password: userData2.password});
                const customerToken = customerTokenUserRequest.body.token;

                const res = await request.patch(`${baseRouteURL}/${savedUser._id}/restore`)
                    .set("Authorization", "Bearer " + customerToken);

                expect(res.status).toBe(401);
                expect(res.body).toMatchObject({});

                // Make sure the user soft deleted isn't restored
                await User.findById(savedUser._id, function (err, doc_user) {
                   expect(doc_user.deleted).toBe(true);
                   expect(doc_user.deletedAt).toBeInstanceOf(Date);
                });

                done();
            });

            // Fail if the token user is an admin & the user he wants to restore isn't in his company
            it(`PATCH ${baseRouteURL}/:id/restore => should failed if the token user is an admin & the user he wants to restore isn't in his company`, async done => {
                // Create 2 clients
                const firstClient = new Client(clientData);
                const savedFirstClient = await firstClient.save();
                const secondClient = new Client(clientData2);
                const savedSecondClient = await secondClient.save();

                // Create a customer user associated with first client & soft delete it
                const customerUserWithFirstClientData = Object.assign({}, userData);
                customerUserWithFirstClientData.role = 'C';
                customerUserWithFirstClientData._client = savedFirstClient._id;

                const customerUserWithFirstClient = new User(customerUserWithFirstClientData);
                const savedCustomerUserWithFirstClient = await customerUserWithFirstClient.save();
                savedCustomerUserWithFirstClient.softdelete();

                // Create an admin user associated with second client
                const adminUserWithSecondClientData = Object.assign({}, userData2);
                adminUserWithSecondClientData.role = 'A';
                adminUserWithSecondClientData._client = savedSecondClient._id;

                const adminUserWithSecondClient = new User(adminUserWithSecondClientData);
                const savedAdminUserWithSecondClient = await adminUserWithSecondClient.save();

                // Log in the admin user associated with second client : try to restore the customer user associated with first client
                const adminUserWithSecondClientRequestToken = await request.post('/login').send({email: savedAdminUserWithSecondClient.email, password: userData2.password});
                const adminUserWithSecondClientToken = adminUserWithSecondClientRequestToken.body.token;

                const res = await request.patch(`${baseRouteURL}/${savedCustomerUserWithFirstClient._id}/restore`)
                    .set("Authorization", "Bearer " + adminUserWithSecondClientToken);

                expect(res.status).toBe(401);
                expect(res.body).toMatchObject({});

                // Make sure the customer user associated with first client soft deleted isn't restored
                await User.findById(savedCustomerUserWithFirstClient._id, function (err, doc_user) {
                    expect(doc_user.deleted).toBe(true);
                    expect(doc_user.deletedAt).toBeInstanceOf(Date);
                });

                done();
            });

            // Fail if the token user is an admin & the user he want to restore is also an admin of this company
            it(`PATCH ${baseRouteURL}/:id/restore => should failed if the token user is an admin & the user he wants to restore is also an admin of this company`, async done => {
                // Create 1 client
                const client = new Client(clientData);
                const savedClient = await client.save();

                // Create an admin user associated with client & soft delete it
                const firstAdminUserWithClientData = Object.assign({}, userData);
                firstAdminUserWithClientData.role = 'A';
                firstAdminUserWithClientData._client = savedClient._id;

                const firstAdminUserWithClient = new User(firstAdminUserWithClientData);
                const savedFirstAdminUserWithClient = await firstAdminUserWithClient.save();
                savedFirstAdminUserWithClient.softdelete();

                // Create an admin user associated with client, but don't soft delete it
                const secondAdminUserWithClientData = Object.assign({}, userData2);
                secondAdminUserWithClientData.role = 'A';
                secondAdminUserWithClientData._client = savedClient._id;

                const secondAdminUserWithClient = new User(secondAdminUserWithClientData);
                const savedSecondAdminUserWithClient = await secondAdminUserWithClient.save();

                // Log in the second admin user associated with client : try to restore the first admin user associated with client
                const secondAdminUserWithClientRequestToken = await request.post('/login').send({email: savedSecondAdminUserWithClient.email, password: userData2.password});
                const secondAdminUserWithClientToken = secondAdminUserWithClientRequestToken.body.token;

                const res = await request.patch(`${baseRouteURL}/${savedFirstAdminUserWithClient._id}/restore`)
                    .set("Authorization", "Bearer " + secondAdminUserWithClientToken);

                expect(res.status).toBe(401);
                expect(res.body).toMatchObject({});

                // Make sure the admin user associated with client soft deleted isn't restored
                await User.findById(savedFirstAdminUserWithClient._id, function (err, doc_user) {
                    expect(doc_user.deleted).toBe(true);
                    expect(doc_user.deletedAt).toBeInstanceOf(Date);
                });

                done();
            });

            // Fail if the id is not valid
            it(`PATCH ${baseRouteURL}/:id/restore => should failed if the user id is not valid`, async done => {
                const res = await request.patch(`${baseRouteURL}/111/restore`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("User id is not valid.");

                done();
            });

            // Fail if the user doesn't exist
            it(`PATCH ${baseRouteURL}/:id/restore => should failed if the user doesn't exist`, async done => {
                const res = await request.patch(`${baseRouteURL}/${fakeObjectIDMongoose}/restore`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(404);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("User doesn't exist.");

                done();
            });

            // Should restore the customer user soft deleted if the token user is an admin of this company
            it(`PATCH ${baseRouteURL}/:id/restore => Should restore the customer user soft deleted if the token user is an admin of this company`, async done => {
                // Create 1 client
                const client = new Client(clientData);
                const savedClient = await client.save();

                // Create a customer user associated with first client & soft delete it
                const customerUserWithClientData = Object.assign({}, userData);
                customerUserWithClientData.role = 'C';
                customerUserWithClientData._client = savedClient._id;

                const customerUserWithClient = new User(customerUserWithClientData);
                const savedCustomerUserWithClient = await customerUserWithClient.save();
                savedCustomerUserWithClient.softdelete();

                // Create an admin user associated with this client
                const adminUserWithClientData = Object.assign({}, userData2);
                adminUserWithClientData.role = 'A';
                adminUserWithClientData._client = savedClient._id;

                const adminUserWithClient = new User(adminUserWithClientData);
                const savedAdminUserWithClient = await adminUserWithClient.save();

                // Log in the admin user associated with client : try to restore the customer user associated with client
                const adminUserWithClientRequestToken = await request.post('/login').send({email: savedAdminUserWithClient.email, password: userData2.password});
                const adminUserWithClientToken = adminUserWithClientRequestToken.body.token;

                const res = await request.patch(`${baseRouteURL}/${savedCustomerUserWithClient._id}/restore`)
                    .set("Authorization", "Bearer " + adminUserWithClientToken);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);

                // Make sure the customer user associated with first client soft deleted isn't restored
                await User.findById(savedCustomerUserWithClient._id, function (err, doc_user) {
                    expect(doc_user.deleted).toBe(false);
                    expect(doc_user.deletedAt).not.toBeInstanceOf(Date);
                    expect(doc_user.deletedAt).toBe(null);
                });

                done();
            });

            // Should restore the user soft deleted
            it(`PATCH ${baseRouteURL}/:id/restore => should restore the soft deleted user`, async done => {
                const user = new User(userData);
                const savedUser = await user.save();

                savedUser.softdelete();

                const res = await request.patch(`${baseRouteURL}/${savedUser._id}/restore`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);

                // Check that user is successfully restored
                await User.findById(savedUser._id, function (err, doc_user) {
                    expect(doc_user.deleted).toBe(false);
                    expect(doc_user.deletedAt).not.toBeInstanceOf(Date);
                    expect(doc_user.deletedAt).toBe(null);
                });

                done();
            });

        });

    });

    // DELETE API endpoints for users
    describe(`DELETE ${baseRouteURL}`, () => {

        describe(`DELETE ${baseRouteURL}/:id/delete`, () => {

            // Fail if there is no token provided
            it(`DELETE ${baseRouteURL}/:id/delete => should failed if there is no token provided`, async done => {
                const res = await request.delete(`${baseRouteURL}/111/delete`);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`DELETE ${baseRouteURL}/:id/delete => should failed if the token user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.delete(`${baseRouteURL}/111/delete`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if the token user is a customer
            it(`DELETE ${baseRouteURL}/:id/delete => should failed if the token user is a customer`, async done => {
                // Create a user
                const user = new User(userData);
                const savedUser = await user.save();

                const customerUserTokenData = Object.assign({}, userData2);
                customerUserTokenData.role = 'C';
                customerUserTokenData._client = fakeObjectIDMongoose;

                const customerTokenUser = new User(customerUserTokenData);
                const savedCustomerTokenUser = await customerTokenUser.save();

                const customerTokenUserRequest = await request.post('/login').send({email: savedCustomerTokenUser.email, password: userData2.password});
                const customerToken = customerTokenUserRequest.body.token;

                const res = await request.delete(`${baseRouteURL}/${savedUser._id}/delete`)
                    .set("Authorization", "Bearer " + customerToken);

                expect(res.status).toBe(401);
                expect(res.body).toMatchObject({});

                // Make sure the user isn't deleted
                await User.findById(savedUser._id, function (err, doc_user) {
                    expect(doc_user.deleted).toBe(false);
                    expect(doc_user.deletedAt).toBe(null);
                });

                done();
            });

            // Fail if the token user is an admin & the user he wants to delete isn't in his company
            it(`DELETE ${baseRouteURL}/:id/delete => should failed if the token user is an admin & the user he wants to delete isn't in his company`, async done => {
                // Create 2 clients
                const firstClient = new Client(clientData);
                const savedFirstClient = await firstClient.save();
                const secondClient = new Client(clientData2);
                const savedSecondClient = await secondClient.save();

                // Create a customer user associated with first client
                const customerUserWithFirstClientData = Object.assign({}, userData);
                customerUserWithFirstClientData.role = 'C';
                customerUserWithFirstClientData._client = savedFirstClient._id;

                const customerUserWithFirstClient = new User(customerUserWithFirstClientData);
                const savedCustomerUserWithFirstClient = await customerUserWithFirstClient.save();

                // Create an admin user associated with second client
                const adminUserWithSecondClientData = Object.assign({}, userData2);
                adminUserWithSecondClientData.role = 'A';
                adminUserWithSecondClientData._client = savedSecondClient._id;

                const adminUserWithSecondClient = new User(adminUserWithSecondClientData);
                const savedAdminUserWithSecondClient = await adminUserWithSecondClient.save();

                // Log in the admin user associated with second client : try to delete the customer user associated with first client
                const adminUserWithSecondClientRequestToken = await request.post('/login').send({email: savedAdminUserWithSecondClient.email, password: userData2.password});
                const adminUserWithSecondClientToken = adminUserWithSecondClientRequestToken.body.token;

                const res = await request.delete(`${baseRouteURL}/${savedCustomerUserWithFirstClient._id}/delete`)
                    .set("Authorization", "Bearer " + adminUserWithSecondClientToken);

                expect(res.status).toBe(401);
                expect(res.body).toMatchObject({});

                // Make sure the customer user associated with first client isn't deleted
                await User.findById(savedCustomerUserWithFirstClient._id, function (err, doc_user) {
                    expect(doc_user.deleted).toBe(false);
                    expect(doc_user.deletedAt).toBe(null);
                });

                done();
            });

            // Fail if the token user is an admin & the user he want to delete is also an admin of this company
            it(`DELETE ${baseRouteURL}/:id/delete => should failed if the token user is an admin & the user he wants to delete is also an admin of this company`, async done => {
                // Create 1 client
                const client = new Client(clientData);
                const savedClient = await client.save();

                // Create an admin user associated with client
                const firstAdminUserWithClientData = Object.assign({}, userData);
                firstAdminUserWithClientData.role = 'A';
                firstAdminUserWithClientData._client = savedClient._id;

                const firstAdminUserWithClient = new User(firstAdminUserWithClientData);
                const savedFirstAdminUserWithClient = await firstAdminUserWithClient.save();

                // Create an admin user associated with client
                const secondAdminUserWithClientData = Object.assign({}, userData2);
                secondAdminUserWithClientData.role = 'A';
                secondAdminUserWithClientData._client = savedClient._id;

                const secondAdminUserWithClient = new User(secondAdminUserWithClientData);
                const savedSecondAdminUserWithClient = await secondAdminUserWithClient.save();

                // Log in the second admin user associated with client : try to delete the first admin user associated with client
                const secondAdminUserWithClientRequestToken = await request.post('/login').send({email: savedSecondAdminUserWithClient.email, password: userData2.password});
                const secondAdminUserWithClientToken = secondAdminUserWithClientRequestToken.body.token;

                const res = await request.delete(`${baseRouteURL}/${savedFirstAdminUserWithClient._id}/delete`)
                    .set("Authorization", "Bearer " + secondAdminUserWithClientToken);

                expect(res.status).toBe(401);
                expect(res.body).toMatchObject({});

                // Make sure the admin user associated with client isn't deleted
                await User.findById(savedFirstAdminUserWithClient._id, function (err, doc_user) {
                    expect(doc_user.deleted).toBe(false);
                    expect(doc_user.deletedAt).toBe(null);
                });

                done();
            });

            // Fail if the user id is not valid
            it(`DELETE ${baseRouteURL}/:id/delete => should failed if the user id is not valid`, async done => {
                const res = await request.delete(`${baseRouteURL}/12/delete`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("User id is not valid.");

                done();
            });

            // Fail if the user doesn't exist
            it(`DELETE ${baseRouteURL}/:id/delete => should failed if the user id is not valid`, async done => {
                const res = await request.delete(`${baseRouteURL}/${fakeObjectIDMongoose}/delete`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(404);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("User doesn't exist.");

                done();
            });

            // Should soft delete the customer user if the token user is an admin of this company
            it(`DELETE ${baseRouteURL}/:id/delete => should soft delete the customer user if the token user is an admin of this company`, async done => {
                // Create 1 client
                const client = new Client(clientData);
                const savedClient = await client.save();

                // Create a customer user associated with first client
                const customerUserWithClientData = Object.assign({}, userData);
                customerUserWithClientData.role = 'C';
                customerUserWithClientData._client = savedClient._id;

                const customerUserWithClient = new User(customerUserWithClientData);
                const savedCustomerUserWithClient = await customerUserWithClient.save();

                // Create an admin user associated with this client
                const adminUserWithClientData = Object.assign({}, userData2);
                adminUserWithClientData.role = 'A';
                adminUserWithClientData._client = savedClient._id;

                const adminUserWithClient = new User(adminUserWithClientData);
                const savedAdminUserWithClient = await adminUserWithClient.save();

                // Log in the admin user associated with client : try to delete the customer user associated with client
                const adminUserWithClientRequestToken = await request.post('/login').send({email: savedAdminUserWithClient.email, password: userData2.password});
                const adminUserWithClientToken = adminUserWithClientRequestToken.body.token;

                const res = await request.delete(`${baseRouteURL}/${savedCustomerUserWithClient._id}/delete`)
                    .set("Authorization", "Bearer " + adminUserWithClientToken);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);

                // Make sure the customer user associated with first client is deleted
                await User.findById(savedCustomerUserWithClient._id, function (err, doc_user) {
                    expect(doc_user.deleted).toBe(true);
                    expect(doc_user.deletedAt).toBeInstanceOf(Date);
                });

                done();
            });

            // Should soft delete the user
            it(`DELETE ${baseRouteURL}/:id/delete => should soft delete the user`, async done => {
                const firstUser = new User(userData);
                const savedUser = await firstUser.save();

                // Check that user is successfully created before delete
                await User.findById(savedUser._id, function (err, doc_user) {
                    expect(JSON.stringify(doc_user._id)).toBe(JSON.stringify(savedUser._id));
                });

                const res = await request.delete(`${baseRouteURL}/${savedUser._id}/delete`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);

                // Check that user is successfully soft deleted
                await User.findById(savedUser._id, function (err, doc_user) {
                    expect(doc_user.deleted).toBe(true);
                    expect(doc_user.deletedAt).toBeInstanceOf(Date);
                });

                done();
            });

        });

    });

});