const app = require('../../server');
const dbHandler = require('../utils/db-handler');
const supertest = require('supertest');
const request = supertest(app);

const User = require('../../models/users');
const { userData } = require('../utils/helpers');

/**
 * Create an user before each tests
 */
beforeEach(async () => {
    const user = new User(userData);
    const savedUser = await user.save();
});

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => await dbHandler.connect());

/**
 * Clear all test data after every test.
 */
afterEach(async () => await dbHandler.clearDatabase());

/**
 * Remove and close the db and server.
 */
afterAll(async () => await dbHandler.closeDatabase());

describe('API Login', () => {

    // Should failed if email is null
    it('POST /login => should failed if email is null', async done => {
        const res = await request.post('/login').send({email: null, password: 'password'});

        expect(res.status).toBe(400);
        expect(res.body.success).toBe(false);
        expect(res.body.message).toBe("Login data is null.");

        done();
    });

    // Should failed if password is null
    it('POST /login => should failed if password is null', async done => {
        const res = await request.post('/login').send({email: 'email', password: null});

        expect(res.status).toBe(400);
        expect(res.body.success).toBe(false);
        expect(res.body.message).toBe("Login data is null.");

        done();
    });

    // Should failed if email is incorrect
    it('POST /login => should failed if email is incorrect', async done => {
        const res = await request.post('/login').send({email: 'incorrectemail', password: 'password'});

        expect(res.status).toBe(400);
        expect(res.body.success).toBe(false);
        expect(res.body.message).toBe("Incorrect email or password.");

        done();
    });

    // Should failed if email is correct but password isn't
    it('POST /login => should failed if email is correct but password isn\'t', async done => {
        const res = await request.post('/login').send({email: userData.email, password: 'password'});

        expect(res.status).toBe(400);
        expect(res.body.success).toBe(false);
        expect(res.body.message).toBe("Incorrect email or password.");

        done();
    });

    // Should failed if password is empty
    it('POST /login => should failed if password is empty', async done => {
        const res = await request.post('/login').send({email: userData.email, password: '  '});

        expect(res.status).toBe(400);
        expect(res.body.success).toBe(false);
        expect(res.body.message).toBe("Incorrect email or password.");

        done();
    });

    // Should failed if password is empty
    it('POST /login => should failed if password is empty', async done => {
        const res = await request.post('/login').send({email: userData.email, password: '  '});

        expect(res.status).toBe(400);
        expect(res.body.success).toBe(false);
        expect(res.body.message).toBe("Incorrect email or password.");

        done();
    });

    // Should failed if the user requested is soft deleted
    it('POST /login => should failed if the user is soft deleted', async done => {
        await User.findOne({email: userData.email}, function (err, _user) {
            _user.softdelete();
        });
        const res = await request.post('/login').send({email: userData.email, password: userData.password});

        expect(res.status).toBe(400);
        expect(res.body.success).toBe(false);
        expect(res.body.message).toBe("Incorrect email or password.");

        done();
    });

    // Should login and return a token
    it('POST /login => should login and return a token', async done => {
        const res = await request.post('/login').send({email: userData.email, password: userData.password});

        expect(res.status).toBe(200);
        expect(res.body.success).toBe(true);
        expect(res.body.token).toBeDefined();

        done();
    });

});