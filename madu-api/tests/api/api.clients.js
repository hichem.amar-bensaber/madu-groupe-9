const app = require('../../server');
const mongoose = require('mongoose');
const dbHandler = require('../utils/db-handler');
const supertest = require('supertest');
const request = supertest(app);

const User = require('../../models/users');
const Client = require('../../models/clients');
const { clientData, clientData2, userData, userData2, tokenUserData } = require('../utils/helpers');

const baseRouteURL = '/clients';
const fakeObjectIDMongoose = new mongoose.Types.ObjectId();

let token;

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => await dbHandler.connect());

/**
 * Get token before every test
 */
beforeEach(async () => {
    const tokenUser = new User(tokenUserData);
    const savedTokenUser = await tokenUser.save();
    const tokenRes = await request.post('/login').send({email: savedTokenUser.email, password: tokenUserData.password});
    token = tokenRes.body.token;
});

/**
 * Clear all test data after every test.
 */
afterEach(async () => await dbHandler.clearDatabase());

/**
 * Remove and close the db and server.
 */
afterAll(async () => await dbHandler.closeDatabase());

// TODO : FINIR LES TESTS D'AUTORISATION
describe('API Clients', () => {

    // GET API endpoints for clients
    describe(`GET ${baseRouteURL}`, () => {

        describe(`GET ${baseRouteURL}/list`, () => {

            // Fail if there is no token in the request
            it(`GET ${baseRouteURL}/list => should failed if there is no token provided`, async done => {
                const res = await request.get(`${baseRouteURL}/list`);

                expect(res.status).toBe(401);

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`GET ${baseRouteURL}/list => should failed if the user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.get(`${baseRouteURL}/list`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // It should get all clients should return the 2 same clients created before in the order of createdAt
            it(`GET ${baseRouteURL}/list => should return all clients, test if it retrieve clients with rights datas & sorted by createdAt`, async done => {
                // Create 2 clients
                const firstClient = new Client(clientData);
                const secondClient = new Client(clientData2);
                const savedFirstClient = await firstClient.save();
                const savedSecondClient = await secondClient.save();

                const res = await request.get(`${baseRouteURL}/list`)
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(2);

                for (let property in clientData2) {
                    if (clientData2.hasOwnProperty(property)) {
                        expect(JSON.stringify(Object.values(res.body.data)[0][property])).toBe(JSON.stringify(savedSecondClient[property]));
                    }
                }
                for (let property in clientData) {
                    if (clientData.hasOwnProperty(property)) {
                        expect(JSON.stringify(Object.values(res.body.data)[1][property])).toBe(JSON.stringify(savedFirstClient[property]));
                    }
                }

                done();
            });

            // It should get all clients that are not soft deleted
            it(`GET ${baseRouteURL}/list => should return all non soft deleted clients`, async done => {
                // Create 2 clients
                const firstClient = new Client(clientData);
                const secondClient = new Client(clientData2);
                const savedFirstClient = await firstClient.save();
                const savedSecondClient = await secondClient.save();

                await savedFirstClient.softdelete();

                const res = await request.get(`${baseRouteURL}/list`)
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(1) ;

                for (let property in clientData2) {
                    if (clientData2.hasOwnProperty(property)) {
                        expect(JSON.stringify(res.body.data[0][property])).toBe(JSON.stringify(clientData2[property]));
                    }
                }

                done();
            });

            // It should get all soft deleted clients
            it(`GET ${baseRouteURL}/list => should return all soft deleted clients`, async done => {
                // Create 2 clients
                const firstClient = new Client(clientData);
                const secondClient = new Client(clientData2);
                const savedFirstClient = await firstClient.save();
                const savedSecondClient = await secondClient.save();

                await savedFirstClient.softdelete();
                await savedSecondClient.softdelete();

                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        isDeleted: true,
                        selector: {
                            deleted: true
                        }
                    })
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(2);

                for (let property in clientData2) {
                    if (clientData2.hasOwnProperty(property)) {
                        expect(JSON.stringify(res.body.data[0][property])).toBe(JSON.stringify(clientData2[property]));
                    }
                }
                for (let property in clientData) {
                    if (clientData.hasOwnProperty(property)) {
                        expect(JSON.stringify(res.body.data[1][property])).toBe(JSON.stringify(clientData[property]));
                    }
                }

                done();
            });

            // Selectors should return empty data
            it(`GET ${baseRouteURL}/list => should return empty data`, async done => {
                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        selector: {
                            email: 'test@gmail.com'
                        }
                    })
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(404);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Clients list is empty.");

                done();
            });

            // Selectors should works
            it(`GET ${baseRouteURL}/list => selectors should works`, async done => {
                // Create 2 clients
                const firstClient = new Client(clientData);
                const secondClient = new Client(clientData2);
                const savedFirstClient = await firstClient.save();
                const savedSecondClient = await secondClient.save();

                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        selector: {
                            email: [clientData.email, clientData2.email]
                        }
                    })
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(2);
                expect(res.body.data[0].email).toBe(clientData2.email);
                expect(res.body.data[1].email).toBe(clientData.email);

                done();
            });

            // Limit should works
            it(`GET ${baseRouteURL}/list => should limit results to 5`, async done => {
                // Create 10 clients
                for (let i = 0 ; i < 10 ; i++) {
                    let data = Object.assign({}, clientData);
                    data.email = data.email + i;
                    await new Client(data).save();
                }

                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        limit: 5
                    })
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(5);
                expect(res.body.data[0].email).toBe(clientData.email + 9);
                expect(res.body.data[1].email).toBe(clientData.email + 8);
                expect(res.body.data[2].email).toBe(clientData.email + 7);
                expect(res.body.data[3].email).toBe(clientData.email + 6);
                expect(res.body.data[4].email).toBe(clientData.email + 5);

                done();
            });

            // Offset should works
            it(`GET ${baseRouteURL}/list => should limit results to 5 but offset at 5`, async done => {
                // Create 10 clients
                for (let i = 0 ; i < 10 ; i++) {
                    let data = Object.assign({}, clientData);
                    data.email = data.email + i;
                    await new Client(data).save();
                }

                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        limit: 5,
                        offset: 5
                    })
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(5);
                expect(res.body.data[0].email).toBe(clientData.email + 4);
                expect(res.body.data[1].email).toBe(clientData.email + 3);
                expect(res.body.data[2].email).toBe(clientData.email + 2);
                expect(res.body.data[3].email).toBe(clientData.email + 1);
                expect(res.body.data[4].email).toBe(clientData.email + 0);

                done();
            });

        });

        describe(`GET ${baseRouteURL}/:id`, () => {

            // Fail if there is no token in the request
            it(`GET ${baseRouteURL}/:id => should failed if there is no token provided`, async done => {
                const res = await request.get(`${baseRouteURL}/noTokenRoute`);

                expect(res.status).toBe(401);

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`GET ${baseRouteURL}/:id => should failed if the user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.get(`${baseRouteURL}/softDeletedTokenUser`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // Don't retrieve an client if it doesn't exists
            it(`GET ${baseRouteURL}/:id => should failed to retrieve a single client`, async done => {
                const res = await request.get(`${baseRouteURL}/${fakeObjectIDMongoose}`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(404);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Client doesn't exist.");

                done();
            });

            // Error if id isn't valid
            it(`GET ${baseRouteURL}/:id => should failed if id is not valid`, async done => {
                const res = await request.get(`${baseRouteURL}/toto`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe('Client id is not valid.');

                done();
            });

            // Test it retrieve the client with the id we created
            it(`GET ${baseRouteURL}/:id => should return a single client with his id`, async done => {
                const client = new Client(clientData);
                const savedClient = await client.save();

                const res = await request.get(`${baseRouteURL}/${savedClient._id}`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);

                await Client.findById(savedClient._id, function (err, doc_client) {
                    doc_client = JSON.parse(JSON.stringify(doc_client));

                    expect(res.body.data).toMatchObject(doc_client);
                });

                done();
            });

        });

    });

    // POST API endpoints for clients
    describe(`POST ${baseRouteURL}`, () => {

        describe(`POST ${baseRouteURL}/create`, () => {

            // Fail if there is no token provided
            it(`POST ${baseRouteURL}/create => should failed if there is no token provided`, async done => {
                const res = await request.post(`${baseRouteURL}/create`);

                expect(res.status).toBe(401);

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`POST ${baseRouteURL}/create => should failed if the user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.post(`${baseRouteURL}/create`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if there is nothing sent to request
            it(`POST ${baseRouteURL}/create => should failed if there is nothing sent to request`, async done => {
                const res = await request.post(`${baseRouteURL}/create`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Create object is null.");

                done();
            });

            // Fail if there is nothing sent to post
            it(`POST ${baseRouteURL}/create => should failed if there is an error in the parameter`, async done => {
                const res = await request.post(`${baseRouteURL}/create`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        create: {}
                    });

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.errors).toEqual(expect.any(Object));

                done();
            });

            // Create a client should fail if there is already a client with the same email
            it(`POST ${baseRouteURL}/create => should fail creating a client with an already existing email`, async done => {
                const client = new Client(clientData);
                await client.save();

                const res = await request.post(`${baseRouteURL}/create`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        create: clientData
                    });

                // Check return request
                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.errors).toEqual(expect.any(Object));
                expect(res.body.errors.email).toBeDefined();

                done();
            });

            // Create a client
            it(`POST ${baseRouteURL}/create => should create a client`, async done => {
                const res = await request.post(`${baseRouteURL}/create`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        create: clientData
                    });

                // Check return request
                expect(res.status).toBe(201);
                expect(res.body.success).toBe(true);
                expect(res.body._id).toBeDefined();

                // Check if client has been created
                await Client.findOne({email: clientData.email}, function (err, client) {
                    expect(JSON.stringify(client._id)).toBe(JSON.stringify(res.body._id));
                });

                done();
            });

        });

    });

    // PUT API endpoints for clients
    describe(`PUT ${baseRouteURL}`, () => {

        describe(`PUT ${baseRouteURL}/:id/update`, () => {

            // Fail if there is no token provided
            it(`PUT ${baseRouteURL}/:id/update => should failed if there is no token provided`, async done => {
                const res = await request.put(`${baseRouteURL}/11/update`);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`PUT ${baseRouteURL}/:id/update => should failed if the user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.put(`${baseRouteURL}/11/update`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if the id is not valid
            it(`PUT ${baseRouteURL}/:id/update => should failed if the user id is not valid`, async done => {
                const res = await request.put(`${baseRouteURL}/111/update`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Client id is not valid.");

                done();
            });

            // Fail if there is nothing sent to the request
            it(`PUT ${baseRouteURL}/:id/update => should failed if there is nothing send to the request`, async done => {
                const res = await request.put(`${baseRouteURL}/${fakeObjectIDMongoose}/update`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Update object is null.");

                done();
            });

            // Should failed if there is no update value sent
            it(`PUT ${baseRouteURL}/:id/update => should failed if there is no update datas`, async done => {
                const tokenUser = await User.findOne({email: tokenUserData.email});

                const res = await request.put(`${baseRouteURL}/${tokenUser._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {}
                    });

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Update object is null.");

                done();
            });

            // Fail if the client doesn't exist
            it(`PUT ${baseRouteURL}/:id/update => should failed if the client doesn't exist`, async done => {
                const res = await request.put(`${baseRouteURL}/${fakeObjectIDMongoose}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            email: 'test'
                        }
                    });

                expect(res.status).toBe(404);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Client doesn't exist.");

                done();
            });

            // Fail if the email sent is already taken
            it(`PUT ${baseRouteURL}/:id/update => should failed if the client email update is already taken`, async done => {
                const client = new Client(clientData);
                const savedClient = await client.save();

                const client2 = new Client(clientData2);
                const savedClient2 = await client2.save();

                const res = await request.put(`${baseRouteURL}/${savedClient2._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            email: clientData.email
                        }
                    });

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.errors).toBeDefined();
                expect(res.body.errors.email).toEqual(expect.any(Object));
                expect(res.body.errors.email.kind).toBe('unique');

                done();
            });

            // Fail if there is missing required data
            it(`PUT ${baseRouteURL}/:id/update => should failed if there is missing required data`, async done => {
                const client = new Client(clientData);
                const savedClient = await client.save();

                const res = await request.put(`${baseRouteURL}/${savedClient._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            name: ''
                        }
                    });

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.errors).toBeDefined();
                expect(res.body.errors.name).toEqual(expect.any(Object));
                expect(res.body.errors.name.kind).toBe('required');

                const clientNotUpdated = await Client.findOne({email: savedClient.email});
                expect(clientNotUpdated.name).toBe(clientData.name);

                done();
            });

            // Fail if update data is missing in an nested object
            it(`PUT ${baseRouteURL}/:id/update => should failed if there is missing required data in an nested object`, async done => {
                const client = new Client(clientData);
                const savedClient = await client.save();

                const res = await request.put(`${baseRouteURL}/${savedClient._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            localisation: {
                                address: 'My new address',
                                zip_code: ''
                            }
                        }
                    });
                
                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.errors).toBeDefined();
                expect(res.body.errors['localisation.zip_code']).toBeDefined();
                expect(res.body.errors['localisation.zip_code'].kind).toBe('required');

                // Be sure that address didn't changed
                const clientNotUpdated = await Client.findOne({email: savedClient.email});
                expect(clientNotUpdated.localisation.address).toBe(clientData.localisation.address);

                done();
            });

            // Update the client with only one data in nested object should works
            it(`PUT ${baseRouteURL}/:id/update => should update client with one data in nested object`, async done => {
                const client = new Client(clientData);
                const savedClient = await client.save();

                const res = await request.put(`${baseRouteURL}/${savedClient._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            localisation: {
                                address: 'My new address'
                            }
                        }
                    });

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(res.body._id).toBe(JSON.parse(JSON.stringify(savedClient._id)));

                // Be sure that address did changed
                const clientUpdated = await Client.findOne({email: savedClient.email});
                expect(clientUpdated.localisation.address).toBe('My new address');

                done();
            });

            // Update the client with the data sent
            it(`PUT ${baseRouteURL}/:id/update => should update the client with the data sent`, async done => {
                const client = new Client(clientData);
                const savedClient = await client.save();

                const res = await request.put(`${baseRouteURL}/${savedClient._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: clientData2
                    });

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(res.body._id).toBe(JSON.parse(JSON.stringify(savedClient._id)));

                const savedClientUpdated = await Client.findById(savedClient._id);

                for (let property in clientData2) {
                    if (clientData2.hasOwnProperty(property)) {
                        expect(JSON.stringify(savedClientUpdated[property])).toStrictEqual(JSON.stringify(clientData2[property]));
                    }
                }
                expect(savedClientUpdated).not.toMatchObject(savedClient);

                done();
            });

            // Check that "createdAt" field don't change & that "updatedAt" change
            it(`PUT ${baseRouteURL}/:id/update => should not change "createdAt" field. But change "updatedAt" field`, async done => {
                const client = new Client(clientData);
                const savedClient = await client.save();

                const res = await request.put(`${baseRouteURL}/${savedClient._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            name: 'My new name'
                        }
                    });

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(res.body._id).toBe(JSON.parse(JSON.stringify(savedClient._id)));

                const updatedClient = await Client.findOne({email: clientData.email});
                expect(JSON.stringify(updatedClient.createdAt)).toBe(JSON.stringify(savedClient.createdAt));
                expect(JSON.stringify(updatedClient.updatedAt)).not.toBe(JSON.stringify(savedClient.updatedAt));

                done();
            });

        });

    });

    // PATCH API endpoints for clients
    describe(`PATCH ${baseRouteURL}`, () => {

        describe(`PATCH ${baseRouteURL}/:id/restore`, () => {

            // Fail if there is no token provided
            it(`PATCH ${baseRouteURL}/:id/restore => should failed if there is no token provided`, async done => {
                const res = await request.patch(`${baseRouteURL}/11/restore`);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`PATCH ${baseRouteURL}/:id/restore => should failed if the user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.patch(`${baseRouteURL}/11/restore`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if the id is not valid
            it(`PATCH ${baseRouteURL}/:id/restore => should failed if the user id is not valid`, async done => {
                const res = await request.patch(`${baseRouteURL}/111/restore`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Client id is not valid.");

                done();
            });

            // Fail if the client doesn't exist
            it(`PATCH ${baseRouteURL}/:id/restore => should failed if the client doesn't exist`, async done => {
                const res = await request.patch(`${baseRouteURL}/${fakeObjectIDMongoose}/restore`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(404);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Client doesn't exist.");

                done();
            });

            // Should restore the client soft deleted
            it(`PATCH ${baseRouteURL}/:id/restore => should restore the soft deleted client`, async done => {
                const client = new Client(clientData);
                const savedClient = await client.save();

                savedClient.softdelete();

                const res = await request.patch(`${baseRouteURL}/${savedClient._id}/restore`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);

                // Check that user is successfully restored
                await Client.findById(savedClient._id, function (err, doc_client) {
                    expect(doc_client.deleted).toBe(false);
                    expect(doc_client.deletedAt).not.toBeInstanceOf(Date);
                    expect(doc_client.deletedAt).toBe(null);
                });

                done();
            });

        });

    });

    // DELETE API endpoints for clients
    describe(`DELETE ${baseRouteURL}`, () => {

        describe(`DELETE ${baseRouteURL}/:id/delete`, () => {

            // Fail if there is no token provided
            it(`DELETE ${baseRouteURL}/:id/delete => should failed if there is no token provided`, async done => {
                const res = await request.delete(`${baseRouteURL}/111/delete`);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`DELETE ${baseRouteURL}/:id/delete => should failed if the user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.delete(`${baseRouteURL}/111/delete`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if the client id is not valid
            it(`DELETE ${baseRouteURL}/:id/delete => should failed if the client id is not valid`, async done => {
                const res = await request.delete(`${baseRouteURL}/12/delete`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Client id is not valid.");

                done();
            });

            // Fail if the client doesn't exist
            it(`DELETE ${baseRouteURL}/:id/delete => should failed if the client id is not valid`, async done => {
                const res = await request.delete(`${baseRouteURL}/${fakeObjectIDMongoose}/delete`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(404);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Client doesn't exist.");

                done();
            });

            // Should soft delete the client
            it(`DELETE ${baseRouteURL}/:id/delete => should soft delete the client`, async done => {
                const client = new Client(clientData);
                const savedClient = await client.save();

                // Check that client is successfully created before delete
                await Client.findById(savedClient._id, function (err, doc_client) {
                    expect(JSON.stringify(doc_client._id)).toBe(JSON.stringify(savedClient._id));
                });

                const res = await request.delete(`${baseRouteURL}/${savedClient._id}/delete`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);

                // Check that client is successfully soft deleted
                await Client.findById(savedClient._id, function (err, doc_client) {
                    expect(doc_client.deleted).toBe(true);
                    expect(doc_client.deletedAt).toBeInstanceOf(Date);
                });

                done();
            });

        });

    });

});