const app = require('../../server');
const mongoose = require('mongoose');
const dbHandler = require('../utils/db-handler');
const supertest = require('supertest');
const request = supertest(app);

const User = require('../../models/users');
const Store = require('../../models/stores');
const { storeData, storeData2, tokenUserData } = require('../utils/helpers');

const baseRouteURL = '/stores';
const fakeObjectIDMongoose = new mongoose.Types.ObjectId();

let token;

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => await dbHandler.connect());

/**
 * Get token before every test
 */
beforeEach(async () => {
    const tokenUser = new User(tokenUserData);
    const savedTokenUser = await tokenUser.save();
    const tokenRes = await request.post('/login').send({email: savedTokenUser.email, password: tokenUserData.password});
    token = tokenRes.body.token;
});

/**
 * Clear all test data after every test.
 */
afterEach(async () => await dbHandler.clearDatabase());

/**
 * Remove and close the db and server.
 */
afterAll(async () => await dbHandler.closeDatabase());

// TODO : FINIR LES TESTS D'AUTORISATION
// TODO : Tester qu'on peut update les tableaux (images par exemple)
describe('API Stores', () => {

    // GET API endpoints for stores
    describe(`GET ${baseRouteURL}`, () => {

        describe(`GET ${baseRouteURL}/list`, () => {

            // Fail if there is no token in the request
            it(`GET ${baseRouteURL}/list => should failed if there is no token provided`, async done => {
                const res = await request.get(`${baseRouteURL}/list`);

                expect(res.status).toBe(401);

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`GET ${baseRouteURL}/list => should failed if the user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.get(`${baseRouteURL}/list`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // It should get all stores should return the 2 same stores created before in the order of createdAt
            it(`GET ${baseRouteURL}/list => should return all stores, test if it retrieve stores with rights datas & sorted by createdAt`, async done => {
                // Create 2 stores
                const firstStore = new Store(storeData);
                const secondStore = new Store(storeData2);
                const savedFirstStore = await firstStore.save();
                const savedSecondStore = await secondStore.save();

                const res = await request.get(`${baseRouteURL}/list`)
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(2);

                for (let property in storeData2) {
                    if (storeData2.hasOwnProperty(property)) {
                        expect(JSON.stringify(Object.values(res.body.data)[0][property])).toBe(JSON.stringify(savedSecondStore[property]));
                    }
                }
                for (let property in storeData) {
                    if (storeData.hasOwnProperty(property)) {
                        expect(JSON.stringify(Object.values(res.body.data)[1][property])).toBe(JSON.stringify(savedFirstStore[property]));
                    }
                }

                done();
            });

            // It should get all stores that are not soft deleted
            it(`GET ${baseRouteURL}/list => should return all non soft deleted stores`, async done => {
                // Create 2 stores
                const firstStore = new Store(storeData);
                const secondStore = new Store(storeData2);
                const savedFirstStore = await firstStore.save();
                const savedSecondStore = await secondStore.save();

                savedFirstStore.softdelete();

                const res = await request.get(`${baseRouteURL}/list`)
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(1) ;

                for (let property in storeData2) {
                    if (storeData2.hasOwnProperty(property)) {
                        expect(JSON.parse(JSON.stringify(res.body.data[0][property]))).toStrictEqual(JSON.parse(JSON.stringify(storeData2[property])));
                    }
                }

                done();
            });

            // It should get all soft deleted stores
            it(`GET ${baseRouteURL}/list => should return all soft deleted stores`, async done => {
                // Create 2 stores
                const firstStore = new Store(storeData);
                const secondStore = new Store(storeData2);
                const savedFirstStore = await firstStore.save();
                const savedSecondStore = await secondStore.save();

                savedFirstStore.softdelete();
                savedSecondStore.softdelete();

                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        isDeleted: true,
                        selector: {
                            deleted: true
                        }
                    })
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(2);

                for (let property in storeData2) {
                    if (storeData2.hasOwnProperty(property)) {
                        expect(JSON.stringify(res.body.data[0][property])).toStrictEqual(JSON.stringify(storeData2[property]));
                    }
                }
                for (let property in storeData) {
                    if (storeData.hasOwnProperty(property)) {
                        expect(JSON.stringify(res.body.data[1][property])).toStrictEqual(JSON.stringify(storeData[property]));
                    }
                }

                done();
            });

            // Selectors should return empty data
            it(`GET ${baseRouteURL}/list => should return empty data`, async done => {
                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        selector: {
                            name: 'test'
                        }
                    })
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(404);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Stores list is empty.");

                done();
            });

            // Selectors should works
            it(`GET ${baseRouteURL}/list => selectors should works`, async done => {
                // Create 2 stores
                const firstStore = new Store(storeData);
                const secondStore = new Store(storeData2);
                const savedFirstStore = await firstStore.save();
                const savedSecondStore = await secondStore.save();

                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        selector: {
                            name: [storeData.name, storeData2.name]
                        }
                    })
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(2);
                expect(res.body.data[0].name).toBe(storeData2.name);
                expect(res.body.data[1].name).toBe(storeData.name);

                done();
            });

            // Limit should works
            it(`GET ${baseRouteURL}/list => should limit results to 5`, async done => {
                // Create 10 stores
                for (let i = 0 ; i < 10 ; i++) {
                    let data = Object.assign({}, storeData);
                    data.name = data.name + i;
                    await new Store(data).save();
                }

                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        limit: 5
                    })
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(5);
                expect(res.body.data[0].name).toBe(storeData.name + 9);
                expect(res.body.data[1].name).toBe(storeData.name + 8);
                expect(res.body.data[2].name).toBe(storeData.name + 7);
                expect(res.body.data[3].name).toBe(storeData.name + 6);
                expect(res.body.data[4].name).toBe(storeData.name + 5);

                done();
            });

            // Offset should works
            it(`GET ${baseRouteURL}/list => should limit results to 5 but offset at 5`, async done => {
                // Create 10 stores
                for (let i = 0 ; i < 10 ; i++) {
                    let data = Object.assign({}, storeData);
                    data.name = data.name + i;
                    await new Store(data).save();
                }

                const res = await request.get(`${baseRouteURL}/list`)
                    .send({
                        limit: 5,
                        offset: 5
                    })
                    .set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(Object.keys(res.body.data).length).toBe(5);
                expect(res.body.data[0].name).toBe(storeData.name + 4);
                expect(res.body.data[1].name).toBe(storeData.name + 3);
                expect(res.body.data[2].name).toBe(storeData.name + 2);
                expect(res.body.data[3].name).toBe(storeData.name + 1);
                expect(res.body.data[4].name).toBe(storeData.name + 0);

                done();
            });

        });

        describe(`GET ${baseRouteURL}/:id`, () => {

            // Fail if there is no token in the request
            it(`GET ${baseRouteURL}/:id => should failed if there is no token provided`, async done => {
                const res = await request.get(`${baseRouteURL}/noTokenRoute`);

                expect(res.status).toBe(401);

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`GET ${baseRouteURL}/:id => should failed if the user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.get(`${baseRouteURL}/softDeletedTokenUser`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // Don't retrieve a store if it doesn't exists
            it(`GET ${baseRouteURL}/:id => should failed to retrieve a single store`, async done => {
                const res = await request.get(`${baseRouteURL}/${fakeObjectIDMongoose}`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(404);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Store doesn't exist.");

                done();
            });

            // Error if id isn't valid
            it(`GET ${baseRouteURL}/:id => should failed if id is not valid`, async done => {
                const res = await request.get(`${baseRouteURL}/toto`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe('Store id is not valid.');

                done();
            });

            // Test it retrieve the store with the id we created
            it(`GET ${baseRouteURL}/:id => should return a single store with his id`, async done => {
                const store = new Store(storeData);
                const savedStore = await store.save();

                const res = await request.get(`${baseRouteURL}/${savedStore._id}`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);

                await Store.findById(savedStore._id, function (err, doc_store) {
                    doc_store = JSON.parse(JSON.stringify(doc_store));

                    expect(res.body.data).toMatchObject(doc_store);
                });

                done();
            });

        });

    });

    // POST API endpoints for stores
    describe(`POST ${baseRouteURL}`, () => {

        describe(`POST ${baseRouteURL}/create`, () => {

            // Fail if there is no token provided
            it(`POST ${baseRouteURL}/create => should failed if there is no token provided`, async done => {
                const res = await request.post(`${baseRouteURL}/create`);

                expect(res.status).toBe(401);

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`POST ${baseRouteURL}/create => should failed if the user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.post(`${baseRouteURL}/create`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if there is nothing sent to request
            it(`POST ${baseRouteURL}/create => should failed if there is nothing sent to request`, async done => {
                const res = await request.post(`${baseRouteURL}/create`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Create object is null.");

                done();
            });

            // Fail if there is nothing sent to post
            it(`POST ${baseRouteURL}/create => should failed if there is an error in the parameter`, async done => {
                const res = await request.post(`${baseRouteURL}/create`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        create: {}
                    });

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.errors).toEqual(expect.any(Object));

                done();
            });

            // Create a store
            it(`POST ${baseRouteURL}/create => should create a store`, async done => {
                const res = await request.post(`${baseRouteURL}/create`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        create: storeData
                    });

                // Check return request
                expect(res.status).toBe(201);
                expect(res.body.success).toBe(true);
                expect(res.body._id).toBeDefined();

                // Check if store has been created
                await Store.findOne({name: storeData.name}, function (err, store) {
                    expect(JSON.stringify(store._id)).toBe(JSON.stringify(res.body._id));
                });

                done();
            });

        });

    });

    // PUT API endpoints for stores
    describe(`PUT ${baseRouteURL}`, () => {

        describe(`PUT ${baseRouteURL}/:id/update`, () => {

            // Fail if there is no token provided
            it(`PUT ${baseRouteURL}/:id/update => should failed if there is no token provided`, async done => {
                const res = await request.put(`${baseRouteURL}/11/update`);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`PUT ${baseRouteURL}/:id/update => should failed if the user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.put(`${baseRouteURL}/11/update`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if the id is not valid
            it(`PUT ${baseRouteURL}/:id/update => should failed if the user id is not valid`, async done => {
                const res = await request.put(`${baseRouteURL}/111/update`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Store id is not valid.");

                done();
            });

            // Fail if there is nothing sent to the request
            it(`PUT ${baseRouteURL}/:id/update => should failed if there is nothing send to the request`, async done => {
                const res = await request.put(`${baseRouteURL}/${fakeObjectIDMongoose}/update`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Update object is null.");

                done();
            });

            // Should failed if there is no update value sent
            it(`PUT ${baseRouteURL}/:id/update => should failed if there is no update datas`, async done => {
                const tokenUser = await User.findOne({email: tokenUserData.email});

                const res = await request.put(`${baseRouteURL}/${tokenUser._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {}
                    });

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Update object is null.");

                done();
            });

            // Fail if the store doesn't exist
            it(`PUT ${baseRouteURL}/:id/update => should failed if the store doesn't exist`, async done => {
                const res = await request.put(`${baseRouteURL}/${fakeObjectIDMongoose}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            email: 'test'
                        }
                    });

                expect(res.status).toBe(404);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Store doesn't exist.");

                done();
            });

            // Fail if there is missing required data
            it(`PUT ${baseRouteURL}/:id/update => should failed if there is missing required data`, async done => {
                const store = new Store(storeData);
                const savedStore = await store.save();

                const res = await request.put(`${baseRouteURL}/${savedStore._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            name: ''
                        }
                    });

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.errors).toBeDefined();
                expect(res.body.errors.name).toEqual(expect.any(Object));
                expect(res.body.errors.name.kind).toBe('required');

                const storeNotUpdated = await Store.findOne({email: savedStore.email});
                expect(storeNotUpdated.name).toBe(storeData.name);

                done();
            });

            // Fail if update data is missing in an nested object
            it(`PUT ${baseRouteURL}/:id/update => should failed if there is missing required data in an nested object`, async done => {
                const store = new Store(storeData);
                const savedStore = await store.save();

                const res = await request.put(`${baseRouteURL}/${savedStore._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            localisation: {
                                address: 'My new address',
                                zip_code: ''
                            }
                        }
                    });

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.errors).toBeDefined();
                expect(res.body.errors['localisation.zip_code']).toBeDefined();
                expect(res.body.errors['localisation.zip_code'].kind).toBe('required');

                // Be sure that address didn't changed
                const storeNotUpdated = await Store.findOne({email: savedStore.email});
                expect(storeNotUpdated.localisation.address).toBe(storeData.localisation.address);

                done();
            });

            // Update the store with only one data in nested object should works
            it(`PUT ${baseRouteURL}/:id/update => should update store with one data in nested object`, async done => {
                const store = new Store(storeData);
                const savedStore = await store.save();

                const res = await request.put(`${baseRouteURL}/${savedStore._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            localisation: {
                                address: 'My new address'
                            }
                        }
                    });

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(res.body._id).toBe(JSON.parse(JSON.stringify(savedStore._id)));

                // Be sure that address did changed
                const storeUpdated = await Store.findOne({name: savedStore.name});
                expect(storeUpdated.localisation.address).toBe('My new address');

                done();
            });

            // Update the store with the data sent
            it(`PUT ${baseRouteURL}/:id/update => should update the store with the data sent`, async done => {
                const store = new Store(storeData);
                const savedStore = await store.save();

                const res = await request.put(`${baseRouteURL}/${savedStore._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: storeData2
                    });

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(res.body._id).toBe(JSON.parse(JSON.stringify(savedStore._id)));

                const savedStoreUpdated = await Store.findById(savedStore._id);

                for (let property in storeData2) {
                    if (storeData2.hasOwnProperty(property)) {
                        expect(JSON.stringify(savedStoreUpdated[property])).toStrictEqual(JSON.stringify(storeData2[property]));
                    }
                }
                expect(savedStoreUpdated).not.toMatchObject(savedStore);

                done();
            });

            // Check that "createdAt" field don't change & that "updatedAt" change
            it(`PUT ${baseRouteURL}/:id/update => should not change "createdAt" field. But change "updatedAt" field`, async done => {
                const store = new Store(storeData);
                const savedStore = await store.save();

                const res = await request.put(`${baseRouteURL}/${savedStore._id}/update`)
                    .set("Authorization", "Bearer " + token)
                    .send({
                        update: {
                            name: 'My new name'
                        }
                    });

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);
                expect(res.body._id).toBe(JSON.parse(JSON.stringify(savedStore._id)));

                await Store.findOne({name: 'My new name'}, function (err, doc_store) {
                    expect(JSON.stringify(doc_store.createdAt)).toBe(JSON.stringify(savedStore.createdAt));
                    expect(JSON.stringify(doc_store.updatedAt)).not.toBe(JSON.stringify(savedStore.updatedAt));
                });

                done();
            });

        });

    });

    // PATCH API endpoints for stores
    describe(`PATCH ${baseRouteURL}`, () => {

        describe(`PATCH ${baseRouteURL}/:id/restore`, () => {

            // Fail if there is no token provided
            it(`PATCH ${baseRouteURL}/:id/restore => should failed if there is no token provided`, async done => {
                const res = await request.patch(`${baseRouteURL}/11/restore`);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`PATCH ${baseRouteURL}/:id/restore => should failed if the user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.patch(`${baseRouteURL}/11/restore`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if the id is not valid
            it(`PATCH ${baseRouteURL}/:id/restore => should failed if the user id is not valid`, async done => {
                const res = await request.patch(`${baseRouteURL}/111/restore`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Store id is not valid.");

                done();
            });

            // Fail if the store doesn't exist
            it(`PATCH ${baseRouteURL}/:id/restore => should failed if the store doesn't exist`, async done => {
                const res = await request.patch(`${baseRouteURL}/${fakeObjectIDMongoose}/restore`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(404);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Store doesn't exist.");

                done();
            });

            // Should restore the store soft deleted
            it(`PATCH ${baseRouteURL}/:id/restore => should restore the soft deleted store`, async done => {
                const store = new Store(storeData);
                const savedStore = await store.save();

                savedStore.softdelete();

                const res = await request.patch(`${baseRouteURL}/${savedStore._id}/restore`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);

                // Check that store is successfully restored
                await Store.findById(savedStore._id, function (err, doc_store) {
                    expect(doc_store.deleted).toBe(false);
                    expect(doc_store.deletedAt).not.toBeInstanceOf(Date);
                    expect(doc_store.deletedAt).toBe(null);
                });

                done();
            });

        });

    });

    // DELETE API endpoints for stores
    describe(`DELETE ${baseRouteURL}`, () => {

        describe(`DELETE ${baseRouteURL}/:id/delete`, () => {

            // Fail if there is no token provided
            it(`DELETE ${baseRouteURL}/:id/delete => should failed if there is no token provided`, async done => {
                const res = await request.delete(`${baseRouteURL}/111/delete`);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if there is token user in the request, but this one is soft deleted
            it(`DELETE ${baseRouteURL}/:id/delete => should failed if the user is soft deleted`, async done => {
                await User.findOne({email: tokenUserData.email}, function (err, tokenUser) {
                    tokenUser.softdelete();
                });
                const res = await request.delete(`${baseRouteURL}/111/delete`)
                    .set("Authorization", "Bearer " + token);

                expect(res.body).toMatchObject({});

                done();
            });

            // Fail if the store id is not valid
            it(`DELETE ${baseRouteURL}/:id/delete => should failed if the store id is not valid`, async done => {
                let res;
                res = await request.delete(`${baseRouteURL}/${null}/delete`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(400);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Store id is not valid.");

                done();
            });

            // Fail if the store doesn't exist
            it(`DELETE ${baseRouteURL}/:id/delete => should failed if the store id is not valid`, async done => {
                const res = await request.delete(`${baseRouteURL}/${fakeObjectIDMongoose}/delete`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(404);
                expect(res.body.success).toBe(false);
                expect(res.body.message).toBe("Store doesn't exist.");

                done();
            });

            // Should soft delete the store
            it(`DELETE ${baseRouteURL}/:id/delete => should soft delete the store`, async done => {
                const store = new Store(storeData);
                const savedStore = await store.save();

                // Check that store is successfully created before delete
                await Store.findById(savedStore._id, function (err, doc_store) {
                    expect(JSON.stringify(doc_store._id)).toBe(JSON.stringify(savedStore._id));
                });

                const res = await request.delete(`${baseRouteURL}/${savedStore._id}/delete`).set("Authorization", "Bearer " + token);

                expect(res.status).toBe(200);
                expect(res.body.success).toBe(true);

                // Check that store is successfully soft deleted
                await Store.findById(savedStore._id, function (err, doc_store) {
                    expect(doc_store.deleted).toBe(true);
                    expect(doc_store.deletedAt).toBeInstanceOf(Date);
                });

                done();
            });

        });

    });

});