const mongoose = require('mongoose');
const dbHandler = require('../utils/db-handler');
const UserModel = require('../../models/users');
const ClientModel = require('../../models/clients');
const { invalidEmails, clientData } = require('../utils/helpers');

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => await dbHandler.connect());

/**
 * Clear all test data after every test.
 */
afterEach(async () => await dbHandler.clearDatabase());

/**
 * Remove and close the db and server.
 */
afterAll(async () => await dbHandler.closeDatabase());

describe('Client Model Test', () => {

    // Test if client is create and saved successfully
    it('create & save client successfully', async () => {
        const validClient = new ClientModel({...clientData,...{_user: new mongoose.Types.ObjectId()}});
        const savedClient = await validClient.save();

        expect(savedClient._id).toBeDefined();
        expect(savedClient.name).toBeDefined();
        expect(savedClient.localisation.city).toBeDefined();
        expect(savedClient.localisation.address).toBeDefined();
        expect(savedClient.localisation.zip_code).toBeDefined();
        expect(savedClient.phone).toBeDefined();
        expect(savedClient.email).toBeDefined();
        expect(savedClient.createdAt).toBeDefined();
        expect(savedClient.createdAt).toBeInstanceOf(Date);
        expect(savedClient.updatedAt).toBeDefined();
        expect(savedClient.updatedAt).toBeInstanceOf(Date);
        expect(savedClient.deleted).toBe(false);
        expect(savedClient.deletedAt).toBe(null);
    });

    // You shouldn't be able to add in any field that isn't defined in the schema
    it('insert client successfully, but the field does not defined in schema should be undefined', async () => {
        const invalidClientData = Object.assign({}, {...clientData, ...{_user: new mongoose.Types.ObjectId()}});
        invalidClientData.johndoe = 'john doe';

        const clientWithInvalidField = new ClientModel(invalidClientData);
        const savedClientWithInvalidField = await clientWithInvalidField.save();

        expect(savedClientWithInvalidField._id).toBeDefined();
        expect(savedClientWithInvalidField.johndoe).toBeUndefined();
    });

    // It should told us the errors in on name field.
    it('create client without required field should failed', async () => {
        const requiredFieldClientData = Object.assign({}, {...clientData, ...{_user: new mongoose.Types.ObjectId()}});
        requiredFieldClientData.name = '';

        const clientWithoutRequiredField = new ClientModel(requiredFieldClientData);
        let err;

        try {
            const savedClientWithoutRequiredField = await clientWithoutRequiredField.save();
            error = savedClientWithoutRequiredField;
        } catch (error) {
            err = error
        }

        expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
        expect(err.errors.name).toBeDefined();
    });

    // It should told us the errors is on name field.
    /* it('create client with empty (only whitespaces) required field should failed', async () => {
        const requiredFieldClientData = Object.assign({}, {...clientData, ...{_user: new mongoose.Types.ObjectId()}});
        requiredFieldClientData.name = '   ';

        const clientWithoutRequiredField = new ClientModel(requiredFieldClientData);
        let err;

        try {
            const savedClientWithoutRequiredField = await clientWithoutRequiredField.save();
            error = savedClientWithoutRequiredField;
        } catch (error) {
            err = error
        }

        expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
        expect(err.errors.name).toBeDefined();
    }); */

    // It should told us that the email failed if isn't valid email
    it('email field should failed', async () => {
        let invalidEmailFieldUserData = Object.assign({}, clientData);
        invalidEmailFieldUserData = {...invalidEmailFieldUserData, ...{_user: new mongoose.Types.ObjectId()}};

        for (let i = 0 ; i < invalidEmails.length ; i++) {
            invalidEmailFieldUserData.email = invalidEmails[i];

            const clientWithInvalidEmailField = new UserModel(invalidEmailFieldUserData);
            let err;

            try {
                const dontSaveClientWithInvalidEmailField = await clientWithInvalidEmailField.save();
                error = dontSaveClientWithInvalidEmailField;
            } catch (error) {
                err = error;
            }

            expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
            expect(err.errors.email).toBeDefined();
        }

    });

    // Create a client with another client with the same email should failed
    it('create client with an already existing email should failed', async () => {
        let clientDataWithUserId = Object.assign({}, clientData);
        let firstClientDataWithUserId = {...clientDataWithUserId, ...{_user: new mongoose.Types.ObjectId()}};
        let secondClientDataWithUserId = {...clientDataWithUserId, ...{_user: new mongoose.Types.ObjectId()}};

        const firstClient = new ClientModel(firstClientDataWithUserId);
        const secondClient = new ClientModel(secondClientDataWithUserId);

        const saveFirstClient = await firstClient.save();
        expect(saveFirstClient).toBeDefined();

        let err;
        try {
            const dontSaveSecondClient = await secondClient.save();
            err = dontSaveSecondClient;
        } catch (error) {
            err = error;
        }

        expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
        expect(err.errors.email).toBeDefined();
    });

});