const mongoose = require("mongoose");
const dbHandler = require("../utils/db-handler");
const UserModel = require("../../models/users");
const ClientModel = require("../../models/clients");
const {
  invalidEmails,
  somePasswords,
  userData,
  clientData,
} = require("../utils/helpers");

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => await dbHandler.connect());

/**
 * Clear all test data after every test.
 */
afterEach(async () => await dbHandler.clearDatabase());

/**
 * Remove and close the db and server.
 */
afterAll(async () => await dbHandler.closeDatabase());

describe("User Model Test", () => {
  // Test if user is create and saved successfully
  it("create & save user successfully", async () => {
    const validUser = new UserModel(userData);
    const savedUser = await validUser.save();

    expect(savedUser._id).toBeDefined();
    expect(savedUser.first_name).toBe(userData.first_name);
    expect(savedUser.last_name).toBe(userData.last_name);
    expect(savedUser.email).toBe(userData.email);
    expect(savedUser.password).toBeDefined();
    expect(savedUser._client).toBeDefined();
    expect(savedUser.role).toBe(userData.role);
    expect(savedUser.createdAt).toBeDefined();
    expect(savedUser.createdAt).toBeInstanceOf(Date);
    expect(savedUser.updatedAt).toBeInstanceOf(Date);
    expect(savedUser.updatedAt).toBeDefined();
    expect(savedUser.deleted).toBe(false);
    expect(savedUser.deletedAt).toBe(null);
  });

  // You shouldn't be able to add in any field that isn't defined in the schema
  it("insert user successfully, but the field does not defined in schema should be undefined", async () => {
    const invalidUserData = Object.assign({}, userData);
    invalidUserData.johndoe = "john doe";

    const userWithInvalidField = new UserModel(invalidUserData);
    const savedUserWithInvalidField = await userWithInvalidField.save();

    expect(savedUserWithInvalidField._id).toBeDefined();
    expect(savedUserWithInvalidField.johndoe).toBeUndefined();
  });

  // It should told us the errors is on first name field.
  it("create user without required field should failed", async () => {
    const requiredFieldUserData = Object.assign({}, userData);
    requiredFieldUserData.first_name = "";

    const userWithoutRequiredField = new UserModel(requiredFieldUserData);
    let err;

    try {
      const savedUserWithoutRequiredField = await userWithoutRequiredField.save();
      error = savedUserWithoutRequiredField;
    } catch (error) {
      err = error;
    }

    expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
    expect(err.errors.first_name).toBeDefined();
  });

  // It should told us the errors is on first name field.
  it("create user with empty (only whitespaces) required field should failed", async () => {
    const requiredFieldUserData = Object.assign({}, userData);
    requiredFieldUserData.first_name = "    ";

    const userWithoutRequiredField = new UserModel(requiredFieldUserData);
    let err;

    try {
      const savedUserWithoutRequiredField = await userWithoutRequiredField.save();
      error = savedUserWithoutRequiredField;
    } catch (error) {
      err = error;
    }

    expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
    expect(err.errors.first_name).toBeDefined();
  });

  // It should told us that the email failed if isn't valid email
  it("email field should failed", async () => {
    const invalidEmailFieldUserData = Object.assign({}, userData);

    for (let i = 0; i < invalidEmails.length; i++) {
      invalidEmailFieldUserData.email = invalidEmails[i];

      const userWithInvalidEmailField = new UserModel(
        invalidEmailFieldUserData
      );
      let err;

      try {
        const dontSaveUserWithInvalidEmailField = await userWithInvalidEmailField.save();
        error = dontSaveUserWithInvalidEmailField;
      } catch (error) {
        err = error;
      }

      expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
      expect(err.errors.email).toBeDefined();
    }
  });

  // It should told us that we need a client references an user if it's a customer user
  it("create user with customer role should failed if client is not referenced", async () => {
    let userDataWithNoClient = Object.assign({}, userData);
    userDataWithNoClient._client = null;
    const customer = new UserModel(userDataWithNoClient);

    let err;
    try {
      const dontSaveCustomer = await customer.save();
      err = dontSaveCustomer;
    } catch (error) {
      err = error;
    }

    expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
    expect(err.errors._client).toBeDefined();
  });

  // It should told us that we need a client references an user if it's an admin user
  it("create user with admin role should failed if client is not referenced", async () => {
    let adminDataWithNoClient = Object.assign({}, userData);
    adminDataWithNoClient.role = "A";
    adminDataWithNoClient._client = null;
    const admin = new UserModel(adminDataWithNoClient);

    let err;
    try {
      const dontSaveAdmin = await admin.save();
      err = dontSaveAdmin;
    } catch (error) {
      err = error;
    }

    expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
    expect(err.errors._client).toBeDefined();
  });

  // Create an user with another user with the same email should failed
  it("create user with an already existing email should failed", async () => {
    const firstUser = new UserModel(userData);
    const secondUser = new UserModel(userData);

    const saveFirstUser = await firstUser.save();
    expect(saveFirstUser).toBeDefined();

    let err;
    try {
      const dontSaveSecondUser = await secondUser.save();
      err = dontSaveSecondUser;
    } catch (error) {
      err = error;
    }

    expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
    expect(err.errors.email).toBeDefined();
  });

  // It should crypt the password
  it("password should be crypted", async () => {
    const userWithCryptedPassword = new UserModel(userData);
    const saveUserWithCryptedPassword = await userWithCryptedPassword.save();

    expect(saveUserWithCryptedPassword.password).not.toBe(userData.password);
  });

  // It should create an user, retrieve it with it email, then should told us that the password is right
  it("create an user & retrieve it via it email & check if password is the same successfully (for all)", async () => {
    for (let i = 0; i < somePasswords.length; i++) {
      const passwordFieldUserDate = Object.assign({}, userData);
      passwordFieldUserDate.password = somePasswords[i];

      const user = new UserModel(passwordFieldUserDate);
      const savedUser = await user.save();

      const findUser = await UserModel.findOne({ email: savedUser.email });

      // Test if we find the user created, and if the password return true
      expect(findUser).toBeDefined();
      expect(await findUser.ValidPassword(somePasswords[i])).toBe(true);

      // Delete this user (avoid duplicate email error for furthers tests)
      await findUser.remove();
    }
  });

  // Create a client, create an user with this client joined to, then retrieve the user with client object in
  it("create client, user & retrieve the client joined to the user", async () => {
    const validClient = new ClientModel(clientData);
    const savedClient = await validClient.save();

    expect(savedClient).toBeDefined();
    expect(savedClient._id).toBeDefined();

    const validUser = new UserModel({
      ...userData,
      ...{ _client: savedClient._id },
    });
    const savedUser = await validUser.save();

    expect(savedUser).toBeDefined();
    expect(savedUser._id).toBeDefined();
    expect(savedUser._client).toBeDefined();
    expect(savedUser._client).toBe(savedClient._id);

    const user = await UserModel.findOne({ _id: savedUser._id }).populate(
      "_client"
    );

    expect(user._client).toBeDefined();
    expect(JSON.stringify(user._client._id)).toBe(
      JSON.stringify(savedClient._id)
    );

    for (let property in clientData) {
      if (property !== "coords") {
        expect(
          JSON.parse(JSON.stringify(user._client[property]))
        ).toStrictEqual(JSON.parse(JSON.stringify(clientData[property])));
      }
    }
  });
});
