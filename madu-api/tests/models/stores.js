const mongoose = require('mongoose');
const dbHandler = require('../utils/db-handler');
const UserModel = require('../../models/users');
const StoreModel = require('../../models/stores');
const { userData, storeData } = require('../utils/helpers');

/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => await dbHandler.connect());

/**
 * Clear all test data after every test.
 */
afterEach(async () => await dbHandler.clearDatabase());

/**
 * Remove and close the db and server.
 */
afterAll(async () => await dbHandler.closeDatabase());

describe('Store Model Test', () => {

    // Test if store is create and saved successfully
    it('create & save store successfully', async () => {
        const validStore = new StoreModel(storeData);
        const savedStore = await validStore.save();

        expect(savedStore._id).toBeDefined();
        expect(savedStore.name).toBeDefined();
        expect(savedStore.store_type).toBeDefined();
        expect(savedStore.localisation.city).toBeDefined();
        expect(savedStore.localisation.address).toBeDefined();
        expect(savedStore.localisation.zip_code).toBeDefined();
        expect(savedStore.status).toBeDefined();
        expect(savedStore.images).toBeDefined();
        expect(savedStore.tags).toBeDefined();
        expect(savedStore.greenscore).toBe(0);
        expect(savedStore._proposed_by_user).toBeUndefined();
        expect(savedStore.createdAt).toBeDefined();
        expect(savedStore.createdAt).toBeInstanceOf(Date);
        expect(savedStore.updatedAt).toBeDefined();
        expect(savedStore.updatedAt).toBeInstanceOf(Date);
        expect(savedStore.deleted).toBe(false);
        expect(savedStore.deletedAt).toBe(null);
    });

    // You shouldn't be able to add in any field that isn't defined in the schema
    it('insert store successfully, but the field does not defined in schema should be undefined', async () => {
        const invalidStoreData = Object.assign({}, storeData);
        invalidStoreData.johndoe = 'john doe';

        const storeWithInvalidField = new StoreModel(invalidStoreData);
        const savedStoreWithInvalidField = await storeWithInvalidField.save();

        expect(savedStoreWithInvalidField._id).toBeDefined();
        expect(savedStoreWithInvalidField.johndoe).toBeUndefined();
    });

    // It should told us the errors in on name field.
    it('create store without required field should failed', async () => {
        const requiredFieldStoreData = Object.assign({}, storeData);
        requiredFieldStoreData.name = '';

        const clientWithoutRequiredField = new StoreModel(requiredFieldStoreData);
        let err;

        try {
            const savedStoreWithoutRequiredField = await clientWithoutRequiredField.save();
            error = savedStoreWithoutRequiredField;
        } catch (error) {
            err = error
        }

        expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
        expect(err.errors.name).toBeDefined();
    });

    // It should told us the errors is on name field.
    it('create store with empty (only whitespaces) required field should failed', async () => {
        const requiredFieldStoreData = Object.assign({}, storeData);
        requiredFieldStoreData.name = '    ';

        const clientWithoutRequiredField = new StoreModel(requiredFieldStoreData);
        let err;

        try {
            const savedStoreWithoutRequiredField = await clientWithoutRequiredField.save();
            error = savedStoreWithoutRequiredField;
        } catch (error) {
            err = error
        }

        expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
        expect(err.errors.name).toBeDefined();
    });

    // It should told us there is an error if we don't fill something in localisation
    it('should error if we don\'t fill something in localisation', async () => {
        const store = new StoreModel(Object.assign({}, {...storeData, ...{localisation: {zip_code: 93100}}}));

        let err;

        try {
            err = await store.save();
        } catch (error) {
            err = error;
        }

        expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
        expect(err.errors['localisation.address']).toBeDefined();
        expect(err.errors['localisation.city']).toBeDefined();
    });

    // It should told us there is an error if we don't fill something in coords
    it('should error if we don\'t fill something in coords', async () => {
        const store = new StoreModel(Object.assign({}, {...storeData, ...{coords: {longitude: 1111.1111}}}));

        let err;

        try {
            err = await store.save();
        } catch (error) {
            err = error;
        }

        expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
        expect(err.errors['coords.latitude']).toBeDefined();
    });

    // Create a store with coords filled in
    it('should create a store with coords filled in', async () => {
        const savedStore = await new StoreModel(Object.assign({}, {...storeData, ...{coords: {latitude: 1111.1111, longitude: 2222.2222}}})).save();

        expect(savedStore).toBeDefined();
        expect(savedStore._id).toBeDefined();
        expect(savedStore.coords.longitude).toBe(2222.2222);
        expect(savedStore.coords.latitude).toBe(1111.1111);
    });

    // should told us there is an error if we forget to fill something in images
    it('should error if we forget something in images', async () => {
        const store = new StoreModel(Object.assign({}, {...storeData, ...{images: [{name: 'test'}]}}));

        let err;

        try {
            err = await store.save();
        } catch (error) {
            err = error;
        }

        expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
        expect(err.errors['images.0.path']).toBeDefined();
    });

    // Create a store without images
    it('should create a store without images filled', async () => {
        let storeDataWithoutImages = Object.assign({}, storeData);
        delete storeDataWithoutImages.images;

        const savedStore = await new StoreModel(storeDataWithoutImages).save();

        expect(savedStore).toBeDefined();
        expect(savedStore._id).toBeDefined();
        expect(JSON.stringify(savedStore.images)).toBe(JSON.stringify([]));
    });

    // Create a store without tags
    it('should create a store without tags filled', async () => {
        let storeDataWithoutTags = Object.assign({}, storeData);
        delete storeDataWithoutTags.tags;

        const savedStore = await new StoreModel(storeDataWithoutTags).save();

        expect(savedStore).toBeDefined();
        expect(savedStore._id).toBeDefined();
        expect(JSON.stringify(savedStore.tags)).toBe(JSON.stringify([]));
    });

    // should told us there is an error if we forget to fill something for open hours
    it('should error if we forget to fill something in open hours', async () => {
        const store = new StoreModel(Object.assign({}, {...storeData, ...{
            open_hours: {
                mon: {
                    morning: {
                        opening: 1200
                    },
                    afternoon: {
                        opening: 1200,
                        closing: 1200
                    }
                }
            }
        }}));

        let err;

        try {
            err = await store.save();
        } catch (error) {
            err = error;
        }

        expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
        expect(err.errors['open_hours.mon.morning.closing']).toBeDefined();
    });

    // should told us there is an error if we send a number < -1
    it('should error if we send a number less than -1', async () => {
        const store = new StoreModel(Object.assign({}, {...storeData, ...{
                open_hours: {
                    mon: {
                        morning: {
                            opening: 1200,
                            closing: -2
                        },
                        afternoon: {
                            opening: 1200,
                            closing: 1200
                        }
                    }
                }
            }}));

        let err;

        try {
            err = await store.save();
        } catch (error) {
            err = error;
        }

        expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
        expect(err.errors['open_hours.mon.morning.closing']).toBeDefined();
    });

    // should to us there is an error if we send a number > 1440
    it('should error if we send a number greater than 1440', async () => {
        const store = new StoreModel(Object.assign({}, {...storeData, ...{
                open_hours: {
                    mon: {
                        morning: {
                            opening: 1200,
                            closing: 1441
                        },
                        afternoon: {
                            opening: 1200,
                            closing: 1200
                        }
                    }
                }
            }}));

        let err;

        try {
            err = await store.save();
        } catch (error) {
            err = error;
        }

        expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
        expect(err.errors['open_hours.mon.morning.closing']).toBeDefined();
    });

    // You shouldn't be able to add an open hours for a key that doesn't exist in the schema
    it('shouldn\'t be able to add an open hours for an undefined key in the schema', async () => {
        const store = new StoreModel(Object.assign({}, {...storeData, ...{
                open_hours: {
                    mon: {
                        morning: {
                            opening: 1200,
                            closing: 1400
                        },
                        afternoon: {
                            opening: 1200,
                            closing: 1200
                        }
                    },
                    random_key_test: {
                        morning: {
                            opening: 1200,
                            closing: 1441
                        },
                        afternoon: {
                            opening: 1200,
                            closing: 1200
                        }
                    }
                }
            }}));

        const savedStore = await store.save();

        expect(savedStore).toBeDefined();
        expect(savedStore.open_hours['mon']).toBeDefined();
        expect(savedStore.open_hours['mon']).toMatchObject({
            morning: {
                opening: 1200,
                closing: 1400
            },
            afternoon: {
                opening: 1200,
                closing: 1200
            }
        });
        expect(savedStore.open_hours['random_key_test']).toBeUndefined();
    });

    // You shouldn't be able to add an open hours for something other than "morning" or "afternoon"
    it('shouldn\'t be able to add an open hours for an undefined key in the schema in morning', async () => {
        const store = new StoreModel(Object.assign({}, {...storeData, ...{
                open_hours: {
                    mon: {
                        morning: {
                            opening: 1200,
                            closing: 1400
                        },
                        afternoon: {
                            opening: 1200,
                            closing: 1200
                        },
                        random_key_test: {
                            opening: 232323,
                            closing: 24332
                        }
                    }
                }
            }}));

        const savedStore = await store.save();

        expect(savedStore).toBeDefined();
        expect(savedStore.open_hours['mon']).toBeDefined();
        expect(savedStore.open_hours['mon']).toMatchObject({
            morning: {
                opening: 1200,
                closing: 1400
            },
            afternoon: {
                opening: 1200,
                closing: 1200
            }
        });
        expect(savedStore.open_hours['mon.random_key_test']).toBeUndefined();
    });

    // Create a store without open hours
    it('should create a store without open hours filled in', async () => {
        const savedStore = await new StoreModel(storeData).save();

        expect(savedStore).toBeDefined();
        expect(savedStore._id).toBeDefined();
        expect(savedStore.open_hours).toBeUndefined();
    });

    // Create a store with partial open hours filled in
    it('should create a store with partial open hours filled in', async () => {
        const savedStore = await new StoreModel({...storeData, ...{
                open_hours: {
                    mon: {
                        morning: {
                            opening: 1200,
                            closing: 1200
                        },
                        afternoon: {
                            opening: 1200,
                            closing: 1200
                        }
                    }
                }
            }}).save();

        expect(savedStore).toBeDefined();
        expect(savedStore._id).toBeDefined();
        expect(savedStore.open_hours).toBeDefined();
        expect(savedStore.open_hours).toMatchObject({
            mon: {
                morning: {
                    opening: 1200,
                    closing: 1200
                },
                afternoon: {
                    opening: 1200,
                    closing: 1200
                }
            }
        });
    });

    // Create a store with full open hours filled in
    it('should create a store with full open hours filled in', async () => {
        const savedStore = await new StoreModel({...storeData, ...{
                open_hours: {
                    mon: {
                        morning: {
                            opening: 1200,
                            closing: 1200
                        },
                        afternoon: {
                            opening: 1200,
                            closing: 1200
                        }
                    },
                    tue: {
                        morning: {
                            opening: 1200,
                            closing: 1200
                        },
                        afternoon: {
                            opening: 1200,
                            closing: 1200
                        }
                    },
                    wed: {
                        morning: {
                            opening: 1200,
                            closing: 1200
                        },
                        afternoon: {
                            opening: 1200,
                            closing: 1200
                        }
                    },
                    thu: {
                        morning: {
                            opening: 1200,
                            closing: 1200
                        },
                        afternoon: {
                            opening: 1200,
                            closing: 1200
                        }
                    },
                    fri: {
                        morning: {
                            opening: 1200,
                            closing: 1200
                        },
                        afternoon: {
                            opening: 1200,
                            closing: 1200
                        }
                    },
                    sat: {
                        morning: {
                            opening: 1200,
                            closing: 1200
                        },
                        afternoon: {
                            opening: 1200,
                            closing: 1200
                        }
                    },
                    sun: {
                        morning: {
                            opening: 1200,
                            closing: 1200
                        },
                        afternoon: {
                            opening: 1200,
                            closing: 1200
                        }
                    }
                }
            }}).save();

        expect(savedStore).toBeDefined();
        expect(savedStore._id).toBeDefined();
        expect(savedStore.open_hours).toBeDefined();
        expect(savedStore.open_hours).toMatchObject({
            mon: {
                morning: {
                    opening: 1200,
                    closing: 1200
                },
                afternoon: {
                    opening: 1200,
                    closing: 1200
                }
            },
            tue: {
                morning: {
                    opening: 1200,
                    closing: 1200
                },
                afternoon: {
                    opening: 1200,
                    closing: 1200
                }
            },
            wed: {
                morning: {
                    opening: 1200,
                    closing: 1200
                },
                afternoon: {
                    opening: 1200,
                    closing: 1200
                }
            },
            thu: {
                morning: {
                    opening: 1200,
                    closing: 1200
                },
                afternoon: {
                    opening: 1200,
                    closing: 1200
                }
            },
            fri: {
                morning: {
                    opening: 1200,
                    closing: 1200
                },
                afternoon: {
                    opening: 1200,
                    closing: 1200
                }
            },
            sat: {
                morning: {
                    opening: 1200,
                    closing: 1200
                },
                afternoon: {
                    opening: 1200,
                    closing: 1200
                }
            },
            sun: {
                morning: {
                    opening: 1200,
                    closing: 1200
                },
                afternoon: {
                    opening: 1200,
                    closing: 1200
                }
            }
        });
    });

    // Create an user then join it to a store
    it('should create & save an user & create & save a store with the user joined to', async () => {
        const validUser = new UserModel(userData);
        const savedUser = await validUser.save();

        expect(savedUser).toBeDefined();
        expect(savedUser._id).toBeDefined();

        const validStore = new StoreModel({...storeData, ...{ _proposed_by_user: savedUser._id }});
        const savedStore = await validStore.save();

        expect(savedStore).toBeDefined();
        expect(savedStore._id).toBeDefined();
        expect(savedStore._proposed_by_user).toBeDefined();
        expect(savedStore._proposed_by_user).toBe(savedUser._id);
    });

    // Create an user, create a store with the user in, then retrieve a store with user object in
    it('create user, store & retrieve the user joined to the store', async () => {
        const validUser = new UserModel(userData);
        const savedUser = await validUser.save();

        expect(savedUser).toBeDefined();
        expect(savedUser._id).toBeDefined();

        const validStore = new StoreModel({...storeData, ...{ _proposed_by_user: savedUser._id }});
        const savedStore = await validStore.save();

        expect(savedStore).toBeDefined();
        expect(savedStore._id).toBeDefined();
        expect(savedStore._proposed_by_user).toBeDefined();
        expect(savedStore._proposed_by_user).toBe(savedUser._id);

        const store = await StoreModel.findOne({ _id: savedStore._id }).populate('_proposed_by_user');

        expect(store._proposed_by_user).toBeDefined();
        expect(JSON.stringify(store._proposed_by_user._id)).toBe(JSON.stringify(savedUser._id));

        for (let property in userData) {
            if (property !== 'password') {
                    expect(JSON.parse(JSON.stringify(store._proposed_by_user[property]))).toStrictEqual(JSON.parse(JSON.stringify(userData[property])));
            }
        }
    });

});