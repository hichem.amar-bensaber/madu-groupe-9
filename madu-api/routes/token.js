const express = require("express");
const router = express.Router();
const Token = require("../controllers/token.js");

/**
 * @api {post} /token/verify Verify if the token is expired
 * @apiName VerifyToken
 * @apiGroup Token
 *
 * @apiParam {String} token                         The token to verify.
 *
 * @apiParamExample {json} Token verify request
 *     {
 *        "token": "MY_USER_TOKEN"
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object} data The user if the token isn't expired
 *
 * @apiSuccessExample Token is not expired
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "data": {
                "pot": 0,
                "role": "SA",
                "profile_pic": "images/profile-pic/user-default.jpeg",
                "_id": "5efca86355769700b779d1e4",
                "email": "admin@admin.com",
                "first_name": "admin",
                "last_name": "admin",
                "deleted": false,
                "deletedAt": null,
                "createdAt": "2020-07-01T15:14:43.459Z",
                "updatedAt": "2020-07-01T15:14:43.459Z",
                "__v": 0,
                "quizzes": []
            }
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {Object} message Error message
 *
 * @apiErrorExample Token is missing in body
 *     HTTPS/1.1 400 Not Found
 *     {
 *         "success": false,
 *         "message": "You did not sent the token."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 *
 * @apiErrorExample Token is expired
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "The token is expired."
 *     }
 *
 * @apiDescription This route will verify if a token is expired or not. It will return the user by the token if it's not expired.
 *
 */
router.post("/verify", Token.tokenVerify);

module.exports = router;