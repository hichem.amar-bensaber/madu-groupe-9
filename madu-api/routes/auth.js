const express = require("express");
const router = express.Router();
const Auth = require("../controllers/auth.js");

/**
 * @api {post} /login Login
 * @apiName Login
 * @apiGroup Login
 * @apiPermission SA (SuperAdmin), A (Admin), C (Customer)
 * 
 * @apiParam {String} email  The user e-mail.
 * @apiParam {String} password  The user password.
 * 
 * @apiParamExample {json} Login
 *     {
 *         "email": "test@test.com",
 *         "password": "azerty"
 *     }
 *
 * @apiSuccess {Boolean} success    Request status.
 * @apiSuccess {String} token       The user token, necessary to access the API levels. You must insert the token in the authorization header of your request example: "Authorization": "Bearer INSERT_YOUR_TOKEN".
 *
 * @apiSuccessExample Success Login
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlM2RkMmQ1MWQ5ZjE5MDYwOWU4MjU4YyIsImlhdCI6MTU4MTkyOTE3NiwiZXhwIjoxNjEzNDg2MTAyfQ.2i-UFUEdZSl9hH5AcECMMjlYioJ7OBR4fPEx-yFhbWI"
 *     }
 *
 * @apiError {Boolean} success  Request status.
 * @apiError {String}  message  Error message.
 * 
 * @apiErrorExample Incorrect email or password
 *     HTTPS/1.1 400 Not Found
 *     {
 *         "success": false,
 *         "message": "Incorrect email or password."
 *     }
 * 
 * @apiError {Boolean} success  Request status.
 * @apiError {String}  message  Error message.
 *
 * @apiErrorExample Login data null
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Login data is null."
 *     }
 * 
 */

router.post("/", Auth.login);

module.exports = router;