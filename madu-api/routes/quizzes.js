const passport = require("passport");
const express = require("express");
const router = express.Router();
const Quiz_Schema = require('../models/quizzes.js');
const API = require("../controllers/api_controllers.js");

/**
 * @api {post} /quizzes/create Create new quiz
 * @apiName CreateQuiz
 * @apiGroup Quizzes
 *
 * @apiParam {Object} create                                    The quiz create object data.
 * @apiParam {String} create.name                               The quiz name
 * @apiParam {Array} create.list                                The quiz questions
 * @apiParam {String} create.list.question                      The quiz question name
 * @apiParam {Number} create.list.points=50                     The quiz question points
 * @apiParam {Array} create.list.answers                        The quiz answers
 * @apiParam {String} create.list.answers.answer                The quiz answer name
 * @apiParam {Boolean} create.list.answers.is_answer=false      Is this answer the correct one or not
 * @apiParam {String} create.list.answers.bad_answer_text                           A text for the user if this answer is the wrong one
 *
 * @apiParamExample {json} Quiz create request
 *     {
 *         "create": {
 *             "name": "La nature",
                "list": [
                    {
                        "points": 50,
                        "answers": [
                            {
                                "is_answer": false,
                                "answer": "1",
                                "bad_answer_text": "blablabla"
                            },
                            {
                                "is_answer": false,
                                "answer": "2",
                                "bad_answer_text": "blablabla"
                            },
                            {
                                "is_answer": true,
                                "answer": "3"
                            }
                        ]
                    }
     *         }
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} _id The quiz ID of the quiz who has just been created.
 *
 * @apiSuccessExample Quiz Created
 *     HTTPS/1.1 201 OK
 *     {
 *         "success": true,
 *         "_id": "5e3dd2d51d9f190609e8258c"
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {Object} errors Errors object, handleAPIError() manage Mongodb errors.
 *
 * @apiErrorExample Required value
 *     HTTPS/1.1 400 Not Found
 *     {
 *         "success": false,
 *         "errors": {
 *             "name": {
 *                 "path": "name",
 *                 "kind": "required"
 *             }
 *         }
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 *
 * @apiErrorExample Create object null
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Create object is null."
 *     }
 *
 * @apiDescription This route allows you to create a new quiz in the database.
 *
 */
router.post("/create", passport.authenticate("jwt", { session: false }), API.create(Quiz_Schema));

/**
 * @api {get} /quizzes/list Get quizzes list
 * @apiName GetListQuizzes
 * @apiGroup Quizzes
 *
 * @apiParam {Object}   selector               The client selector object.
 * @apiParam {Number}   limit=100              The object capture limit.
 * @apiParam {Number}   offset=0               The number of objects to ignore.
 * @apiParam {Object}   sort={"createdAt":-1}  Sort result by specific field, 1 = ASC, -1 = DESC.
 * @apiParam {Boolean}  isDeleted=false        Returns only objects that have been not deleted.
 * @apiParamExample {json} Clients list request
 *     {
 *         "selector": {
 *            "name": "Test"
 *         },
 *         "sort": {
 *            "name": 1
 *         },
 *         "limit": 1000,
 *         "isDeleted": false
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object} data The quizzes list data.
 *
 * @apiSuccessExample Clients list data
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "data":[
        {
            "_id": "5f033bbcc30d07001fd577cb",
            "name": "La nature",
            "list": [
                {
                    "points": 50,
                    "answers": [
                        {
                            "is_answer": false,
                            "answer": "1",
                            "bad_answer_text": "blablabla"
                        },
                        {
                            "is_answer": false,
                            "answer": "2",
                            "bad_answer_text": "blablabla"
                        },
                        {
                            "is_answer": true,
                            "answer": "3"
                        }
                    ],
                    "_id": "5f033bbcc30d07001fd577cc",
                    "question": "Yolo"
                },
                {
                    "points": 50,
                    "answers": [
                        {
                            "is_answer": false,
                            "answer": "1",
                            "bad_answer_text": "blablabla"
                        },
                        {
                            "is_answer": false,
                            "answer": "2",
                            "bad_answer_text": "blablabla"
                        },
                        {
                            "is_answer": true,
                            "answer": "3"
                        }
                    ],
                    "_id": "5f033bbcc30d07001fd577cd",
                    "question": "Yolo 2"
                }
            ],
            "deleted": false,
            "deletedAt": null,
            "createdAt": "2020-07-06T14:57:00.998Z",
            "updatedAt": "2020-07-06T14:57:00.998Z",
            "__v": 0
        }
    ]
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 *
 * @apiErrorExample Quizzes list empty
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Quizzes list is empty."
 *     }
 *
 * @apiErrorExample Quizzes list selector null
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Selector object is null."
 *     }
 *
 * @apiDescription This route allows you to retrieve a quizzes list from the database.
 *
 */
router.get("/list", passport.authenticate("jwt", { session: false }), API.list(Quiz_Schema));

/**
 * @api {get} /quizzes/:id Get Quiz
 * @apiName GetQuiz
 * @apiGroup Quizzes
 *
 * @apiParam {Number} id The quiz ID.
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object} data     The quiz data.
 *
 * @apiSuccessExample Quiz Data
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "data": {
            "_id": "5f033bbcc30d07001fd577cb",
            "name": "La nature",
            "list": [
                {
                    "points": 50,
                    "answers": [
                        {
                            "is_answer": false,
                            "answer": "1",
                            "bad_answer_text": "blablabla"
                        },
                        {
                            "is_answer": false,
                            "answer": "2",
                            "bad_answer_text": "blablabla"
                        },
                        {
                            "is_answer": true,
                            "answer": "3"
                        }
                    ],
                    "_id": "5f033bbcc30d07001fd577cc",
                    "question": "Yolo"
                },
                {
                    "points": 50,
                    "answers": [
                        {
                            "is_answer": false,
                            "answer": "1",
                            "bad_answer_text": "blablabla"
                        },
                        {
                            "is_answer": false,
                            "answer": "2",
                            "bad_answer_text": "blablabla"
                        },
                        {
                            "is_answer": true,
                            "answer": "3"
                        }
                    ],
                    "_id": "5f033bbcc30d07001fd577cd",
                    "question": "Yolo 2"
                }
            ],
            "deleted": false,
            "deletedAt": null,
            "createdAt": "2020-07-06T14:57:00.998Z",
            "updatedAt": "2020-07-06T14:57:00.998Z",
            "__v": 0
        }
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 *
 * @apiErrorExample Quiz doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *         "success": false,
 *         "message": "Quiz doesn't exist."
 *     }
 *
 * @apiErrorExample Quiz ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *         "success": false,
 *         "message": "Quiz id is not valid."
 *     }
 *
 * @apiDescription This route allows you to retrieve a quiz from the database.
 *
 */
router.get("/:id", passport.authenticate("jwt", { session: false }), API.get(Quiz_Schema));

/**
 * @api {put} /quizzes/:id/update Update Quiz
 * @apiName UpdateQuiz
 * @apiGroup Quizzes
 *
 * @apiParam {Number} id        The quiz ID.
 * @apiParam {Object} update    The quiz update object data.
 * @apiParamExample {json}      Quiz update request
 *     {
 *         "update": {
 *             "name": "La nature"
 *         }
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} message  Success message.
 *
 * @apiSuccessExample Quiz updated
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "Quiz is updated."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample Quiz doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Quiz doesn't exist."
 *     }
 *
 * @apiErrorExample Update object null
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Update object is null."
 *     }
 *
 * @apiErrorExample Quiz ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Quiz id is not valid."
 *     }
 *
 * @apiDescription This route allows you to update a quiz from the database.
 *
 */
router.put("/:id/update", passport.authenticate("jwt", { session: false }), API.update(Quiz_Schema));

/**
 * @api {patch} /quizzes/:id/restore Restore quiz
 * @apiName RestoreQuiz
 * @apiGroup Quizzes
 *
 * @apiParam {Number} id   The quiz ID
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} message  Success message.
 *
 * @apiSuccessExample Quiz restored
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "Quiz is restored."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample Quiz doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Quiz doesn't exist."
 *     }
 *
 * @apiErrorExample Quiz ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Quiz id is not valid."
 *     }
 *
 * @apiDescription This route allows you to restore a quiz from the database.
 *
 */
router.patch("/:id/restore", passport.authenticate("jwt", { session: false }), API.restore(Quiz_Schema));

/**
 * @api {delete} /quizzes/:id/delete Delete quiz
 * @apiName DeleteQuiz
 * @apiGroup Quizzes
 *
 * @apiParam {Number} id   The quiz ID
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} message  Success message.
 *
 * @apiSuccessExample Quiz deleted
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "Quiz is deleted."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample Quiz doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Quiz doesn't exist."
 *     }
 *
 * @apiErrorExample Quiz ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Quiz id is not valid."
 *     }
 *
 * @apiDescription This route allows you to delete a quiz from the database.
 *
 */
router.delete("/:id/delete", passport.authenticate("jwt", { session: false }), API.delete(Quiz_Schema));


module.exports = router;