const express = require("express");
const router = express.Router();
const passport = require("passport");
const API = require("../controllers/api_controllers.js");
const Permissions = require("../controllers/permissions_controllers.js");
const Challenges_Schema = require("../models/challenges.js");
const Auth = require("../controllers/auth.js");

/**
 * @api {post} /challenges/create Create new challenge
 * @apiName CreateChallenge
 * @apiGroup Challenges
 * @apiPermission SA (SuperAdmin)
 * 
 * @apiParam {Object} create                    The challenge create object data.
 * @apiParam {String} create.name               The challenge name field.
 * @apiParam {Object} create.description        The challenge description field.
 * @apiParam {String} create.type               The challenge type field.          
 * @apiParam {String} create.points             The challenge points field.             
 * @apiParam {String} create.duration           The challenge duration field.             
 * 
 * @apiParamExample {json} Challenge create request
 *     {
 *         "create": {
 *             "name": "Test",
 *             "description": "Test",
 *             "type": "Test",
 *             "points": 20,
 *             "duration": 1589466386
 *         }
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} _id The challenge ID of the challenge who has just been created.
 *
 * @apiSuccessExample Challenge Created
 *     HTTPS/1.1 201 OK
 *     {
 *         "success": true,
 *         "_id": "5e3dd2d51d9f190609e8258c"
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {Object} errors Errors object, handleAPIError() manage Mongodb errors.
 * 
 * @apiErrorExample Required value
 *     HTTPS/1.1 400 Not Found
 *     {
 *         "success": false,
 *         "errors": {
 *             "type": {
 *                 "path": "type",
 *                 "kind": "required"
 *             }
 *         }
 *     }
 * 
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 *
 * @apiErrorExample Create object null
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Create object is null."
 *     }
 * 
 * @apiDescription This route allows you to create a new challenge from the database.
 * 
 */

router.post("/create", Auth.SetAPIAccess(["SA"]), passport.authenticate("jwt", { session: false }), API.create(Challenges_Schema));

/**
 * @api {get} /challenges/list Get challenges list
 * @apiName GetListChallenges
 * @apiGroup Challenges
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Object}   selector               The challenge selector object.
 * @apiParam {Number}   limit=100              The object capture limit.
 * @apiParam {Number}   offset=0               The number of objects to ignore.
 * @apiParam {Object}   sort={"createdAt":-1}  Sort result by specific field, 1 = ASC, -1 = DESC.
 * @apiParam {Boolean}  isDeleted=false        Returns only objects that have been not deleted.
 * @apiParamExample {json} Challenges list request
 *     {
 *         "selector": {
 *            "name": "Test"
 *         },
 *         "sort": {
 *            "name": 1
 *         },
 *         "limit": 1000,
 *         "isDeleted": false
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object} data The challenges list data.
 *
 * @apiSuccessExample Challenges list data
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "data": [
 *             {
 *                 "name": "Test_1",
 *                 "description": "Test_1",
 *                 "type": "Test_1",
 *                 "points": 20,
 *                 "duration": 1589466386
 *             },
 *             {
 *                 "name": "Test_2",
 *                 "description": "Test_2",
 *                 "type": "Test_2",
 *                 "points": 40,
 *                 "duration": 1589466386
 *             }
 *         ]
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 * 
 * @apiErrorExample Challenges list empty
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Challenges list is empty."
 *     }
 * 
 * @apiErrorExample Challenges list selector null
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Selector object is null."
 *     }
 * 
 * @apiDescription This route allows you to retrieve a challenges list from the database.
 * 
 */

router.get("/list", Auth.SetAPIAccess(), passport.authenticate("jwt", { session: false }), API.list(Challenges_Schema));

/**
 * @api {get} /challenges/:id Get challenge
 * @apiName GetChallenge
 * @apiGroup Challenges
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Number} id The challenge ID.
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object} data     The challenge data.
 *
 * @apiSuccessExample Challenge Data
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "data": {
 *             "name": "Test",
 *             "description": "Test",
 *             "type": "Test",
 *             "points": 20,
 *             "duration": 1589466386
 *         }
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 *
 * @apiErrorExample Challenge doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *         "success": false,
 *         "message": "Challenge doesn't exist."
 *     }
 * 
 * @apiErrorExample Challenge ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *         "success": false,
 *         "message": "Challenge id is not valid."
 *     }
 * 
 * @apiDescription This route allows you to retrieve a challenge from the database.
 * 
 */

router.get("/:id", Auth.SetAPIAccess(), passport.authenticate("jwt", { session: false }), API.get(Challenges_Schema));

/**
 * @api {put} /challenges/:id/update Update challenge
 * @apiName UpdateChallenge
 * @apiGroup Challenges
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Number} id    The challenge ID.
 * @apiParam {Object} update The challenge update object data.
 * @apiParamExample {json} Challenge update request
 *     {
 *         "update": {
 *             "points": 200
 *         }
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} message  Success message.
 *
 * @apiSuccessExample Challenge updated
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "Challenge is updated."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample Challenge doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Challenge doesn't exist."
 *     }
 * 
 * @apiErrorExample Update object null
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Update object is null."
 *     }
 * 
 * @apiErrorExample Challenge ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Challenge id is not valid."
 *     }
 * 
 * @apiDescription This route allows you to update a challenge from the database.
 * 
 */

router.put("/:id/update", Auth.SetAPIAccess(["SA"]), passport.authenticate("jwt", { session: false }), API.update(Challenges_Schema));

/**
 * @api {patch} /challenges/:id/restore Restore challenge
 * @apiName RestoreChallenge
 * @apiGroup Challenges
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Number} id   The challenge ID
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} message  Success message.
 *
 * @apiSuccessExample Challenge restored
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "Challenge is restored."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample Challenge doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Challenge doesn't exist."
 *     }
 * 
 * @apiErrorExample Challenge ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Challenge id is not valid."
 *     }
 * 
 * @apiDescription This route allows you to restore a challenge from the database.
 * 
 */

router.patch("/:id/restore", Auth.SetAPIAccess(["SA"]), passport.authenticate("jwt", { session: false }), API.restore(Challenges_Schema));

/**
 * @api {delete} /challenges/:id/delete Delete challenge
 * @apiName DeleteChallenge
 * @apiGroup Challenges
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Number} id   The challenge ID
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} message  Success message.
 *
 * @apiSuccessExample Challenge deleted
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "Challenge is deleted."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample Challenge doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Challenge doesn't exist."
 *     }
 * 
 * @apiErrorExample Challenge ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Challenge id is not valid."
 *     }
 * 
 * @apiDescription This route allows you to delete a challenge from the database.
 * 
 */

router.delete("/:id/delete", Auth.SetAPIAccess(["SA"]), passport.authenticate("jwt", { session: false }), API.delete(Challenges_Schema));

module.exports = router;