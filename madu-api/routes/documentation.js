const express = require("express");
const router = express.Router();
const path = require("path");

router.get(["/", "/documentation"], function(req, res) {
    res.sendFile(path.join(__dirname, "../documentation", "index.html"));
});

module.exports = router;