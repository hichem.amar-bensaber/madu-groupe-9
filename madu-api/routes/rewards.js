const express = require("express");
const router = express.Router();
const passport = require("passport");
const API = require("../controllers/api_controllers.js");
const Permissions = require("../controllers/permissions_controllers.js");
const Rewards_Schema = require("../models/rewards.js");
const Auth = require("../controllers/auth.js");

/**
 * @api {post} /rewards/create Create new reward
 * @apiName CreateReward
 * @apiGroup Rewards
 * @apiPermission SA (SuperAdmin)
 * 
 * @apiParam {Object} create                      The reward create object data.
 * @apiParam {String} create.novice               The novice reward field.
 * @apiParam {Object} create.adventurer           The adventurer reward field.
 * @apiParam {String} create.master               The master reward field.                      
 * 
 * @apiParamExample {json} Reward create request
 *     {
 *         "create": {
 *             "novice": "Test",
 *             "adventurer": "Test",
 *             "master": "Test"
 *         }
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} _id The reward ID of the reward who has just been created.
 *
 * @apiSuccessExample Reward Created
 *     HTTPS/1.1 201 OK
 *     {
 *         "success": true,
 *         "_id": "5e3dd2d51d9f190609e8258c"
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {Object} errors Errors object, handleAPIError() manage Mongodb errors.
 * 
 * @apiErrorExample Required value
 *     HTTPS/1.1 400 Not Found
 *     {
 *         "success": false,
 *         "errors": {
 *             "master": {
 *                 "path": "master",
 *                 "kind": "required"
 *             }
 *         }
 *     }
 * 
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 *
 * @apiErrorExample Create object null
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Create object is null."
 *     }
 * 
 * @apiDescription This route allows you to create a new reward from the database.
 * 
 */

router.post("/create", Auth.SetAPIAccess(["SA"]), passport.authenticate("jwt", { session: false }), API.create(Rewards_Schema));

/**
 * @api {get} /rewards/list Get rewards list
 * @apiName GetListRewards
 * @apiGroup Rewards
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Object}   selector               The reward selector object.
 * @apiParam {Number}   limit=100              The object capture limit.
 * @apiParam {Number}   offset=0               The number of objects to ignore.
 * @apiParam {Object}   sort={"createdAt":-1}  Sort result by specific field, 1 = ASC, -1 = DESC.
 * @apiParam {Boolean}  isDeleted=false        Returns only objects that have been not deleted.
 * @apiParamExample {json} Rewards list request
 *     {
 *         "selector": {
 *            "master": "Test"
 *         },
 *         "sort": {
 *            "master": 1
 *         },
 *         "limit": 1000,
 *         "isDeleted": false
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object} data The rewards list data.
 *
 * @apiSuccessExample Rewards list data
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "data": [
 *             {
 *                 "novice": "Test_1",
 *                 "adventurer": "Test_1",
 *                 "master": "Test_1"
 *             },
 *             {
 *                 "novice": "Test_2",
 *                 "adventurer": "Test_2",
 *                 "master": "Test_2"
 *             }
 *         ]
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 * 
 * @apiErrorExample Rewards list empty
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Rewards list is empty."
 *     }
 * 
 * @apiErrorExample Rewards list selector null
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Selector object is null."
 *     }
 * 
 * @apiDescription This route allows you to retrieve a rewards list from the database.
 * 
 */

router.get("/list", Auth.SetAPIAccess(), passport.authenticate("jwt", { session: false }), API.list(Rewards_Schema));

/**
 * @api {get} /rewards/:id Get reward
 * @apiName GetReward
 * @apiGroup Rewards
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Number} id The reward ID.
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object} data     The reward data.
 *
 * @apiSuccessExample Reward Data
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "data": {
 *             "novice": "Test",
 *             "adventurer": "Test",
 *             "master": "Test"
 *         }
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 *
 * @apiErrorExample Reward doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *         "success": false,
 *         "message": "Reward doesn't exist."
 *     }
 * 
 * @apiErrorExample Reward ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *         "success": false,
 *         "message": "Reward id is not valid."
 *     }
 * 
 * @apiDescription This route allows you to retrieve a reward from the database.
 * 
 */

router.get("/:id", Auth.SetAPIAccess(), passport.authenticate("jwt", { session: false }), API.get(Rewards_Schema));

/**
 * @api {put} /rewards/:id/update Update reward
 * @apiName UpdateReward
 * @apiGroup Rewards
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Number} id    The reward ID.
 * @apiParam {Object} update The reward update object data.
 * @apiParamExample {json} Reward update request
 *     {
 *         "update": {
 *             "master": "Test"
 *         }
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} message  Success message.
 *
 * @apiSuccessExample Reward updated
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "Reward is updated."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample Reward doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Reward doesn't exist."
 *     }
 * 
 * @apiErrorExample Update object null
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Update object is null."
 *     }
 * 
 * @apiErrorExample Reward ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Reward id is not valid."
 *     }
 * 
 * @apiDescription This route allows you to update a reward from the database.
 * 
 */

router.put("/:id/update", Auth.SetAPIAccess(["SA"]), passport.authenticate("jwt", { session: false }), API.update(Rewards_Schema));

/**
 * @api {patch} /rewards/:id/restore Restore reward
 * @apiName RestoreReward
 * @apiGroup Rewards
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Number} id   The reward ID
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} message  Success message.
 *
 * @apiSuccessExample Reward restored
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "Reward is restored."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample Reward doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Reward doesn't exist."
 *     }
 * 
 * @apiErrorExample Reward ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Reward id is not valid."
 *     }
 * 
 * @apiDescription This route allows you to restore a reward from the database.
 * 
 */

router.patch("/:id/restore", Auth.SetAPIAccess(["SA"]), passport.authenticate("jwt", { session: false }), API.restore(Rewards_Schema));

/**
 * @api {delete} /rewards/:id/delete Delete reward
 * @apiName DeleteReward
 * @apiGroup Rewards
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Number} id   The reward ID
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} message  Success message.
 *
 * @apiSuccessExample Reward deleted
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "Reward is deleted."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample Reward doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Reward doesn't exist."
 *     }
 * 
 * @apiErrorExample Reward ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Reward id is not valid."
 *     }
 * 
 * @apiDescription This route allows you to delete a reward from the database.
 * 
 */

router.delete("/:id/delete", Auth.SetAPIAccess(["SA"]), passport.authenticate("jwt", { session: false }), API.delete(Rewards_Schema));

module.exports = router;