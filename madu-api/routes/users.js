const express = require("express");
const router = express.Router();
const passport = require('passport');
const API = require("../controllers/api_controllers.js");
const Permissions = require("../controllers/permissions_controllers.js");
const Users_Schema = require("../models/users.js");
const Auth = require("../controllers/auth.js");

/**
 * @api {post} /users/create Create new user
 * @apiName CreateUser
 * @apiGroup Users
 * @apiPermission SA (SuperAdmin), A (Admin)
 *
 * @apiParam {Object}   create             The user create object data.
 * @apiParam {String}   create.first_name  The user first_name field.
 * @apiParam {String}   create.last_name   The user last_name field.
 * @apiParam {String}   create.email       The user email field.
 * @apiParam {String}   create.password    The user password field.
 * @apiParam {String}   create._client     The client ID, it is only required for type roles ["A", "C"].
 * @apiParam {Number}   [create.pot=0]     The user wallet point field.
 * @apiParam {String}   [create.role="C"]  The user role, it's a enumeration ["SA", "A", "C"].
 * @apiParam {Object}   [create.profile_pic="images/profile-pic/user-default.jpeg"] The user profile picture field.
 * @apiParam {String}   create.profile_pic.uri The image data, only base64.
 * 
 * @apiParamExample {json} User create request
 *     {
 *         "create": {
 *             "first_name": "Test",
 *             "last_name": "Test",
 *             "email": "test@test.com",
 *             "password": "azerty",
 *             "_client": "5e3dd5186f19e40659ced1a5",
 *             "role": "A",
 *             "profile_pic": {
 *                 "uri": "data:image/jpeg;base64,"
 *             }
 *         }
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} _id      The user ID of the user who has just been created.
 *
 * @apiSuccessExample User Created
 *     HTTPS/1.1 201 OK
 *     {
 *         "success": true,
 *         "user_id": "5e3dd2d51d9f190609e8258c"
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {Object} errors Errors object, handleAPIError() manage Mongodb errors.
 * 
 * @apiErrorExample Required value
 *     HTTPS/1.1 400 Not Found
 *     {
 *         "success": false,
 *         "errors": {
 *             "password": {
 *                 "path": "password",
 *                 "kind": "required"
 *             },
 *             "email": {
 *                 "path": "email",
 *                 "kind": "required"
 *             }
 *         }
 *     }
 * 
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 *
 * @apiErrorExample Create object null
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Create object is null."
 *     }
 * 
 * @apiDescription This route allows you to create a new user from the database.
 * 
 */

//router.post("/create", Auth.SetAPIAccess(["SA", "A"]), passport.authenticate("jwt", { session: false }), API.create(Users_Schema));
router.post("/create", API.create(Users_Schema));

/**
 * @api {get} /users/list Get users list
 * @apiName GetListUsers
 * @apiGroup Users
 * @apiPermission SA (SuperAdmin), A (Admin)
 *
 * @apiParam {Object}   selector               The user selector object.
 * @apiParam {Number}   limit=100              The object capture limit.
 * @apiParam {Number}   offset=0               The number of objects to ignore.
 * @apiParam {Object}   sort={"createdAt":-1}  Sort result by specific field, 1 = ASC, -1 = DESC.
 * @apiParam {Boolean}  isDeleted=false        Returns only objects that have been not deleted.
 * @apiParamExample {json} Users list request
 *     {
 *         "selector": {
 *            "role": "C",
 *            "_client": "5e3dd5186f19e40659ced1a5"
 *         },
 *         "sort": {
 *            "last_name": 1
 *         },
 *         "limit": 1000,
 *         "isDeleted": false
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object} data The users list data.
 *
 * @apiSuccessExample Users list data
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "data": [
 *             {
 *                 "first_name": "Test_1",
 *                 "last_name": "Test_1",
 *                 "email": "test_1@test.com",
 *                 "password": "azerty",
 *                 "_client": "5e3dd5186f19e40659ced1a5",
 *                 "role": "A",
 *                 "pot": 0,
 *                 "profile_pic": "images/profile-pic/user-default.jpeg"
 *             },
 *             {
 *                 "first_name": "Test_2",
 *                 "last_name": "Test_2",
 *                 "email": "test_2@test.com",
 *                 "password": "azerty",
 *                 "_client": "5e3dd5186f19e40659ced1a5",
 *                 "role": "A",
 *                 "pot": 0,
 *                 "profile_pic": "images/profile-pic/user-default.jpeg"
 *             }
 *         ]
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 * 
 * @apiErrorExample Users list empty
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Users list is empty."
 *     }
 * 
 * @apiErrorExample Selector object null.
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Selector object is null."
 *     }
 * 
 * @apiDescription This route allows you to retrieve a users list from the database.
 * 
 */

router.get("/list", Auth.SetAPIAccess(["SA", "A"]), passport.authenticate("jwt", { session: false }), Permissions.list(Users_Schema), API.list(Users_Schema));

/**
 * @api {get} /users/:id Get user
 * @apiName GetUser
 * @apiGroup Users
 * @apiPermission SA (SuperAdmin), A (Admin), C (Customer)
 *
 * @apiParam {Number} id The user ID.
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object} data     The user data.
 *
 * @apiSuccessExample User Data
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "data": {
 *             "first_name": "Test",
 *             "last_name": "Test",
 *             "email": "test@test.com",
 *             "password": "azerty",
 *             "_client": "5e3dd5186f19e40659ced1a5",
 *             "role": "A",
 *             "pot": 0,
 *             "profile_pic": "images/profile-pic/user-default.jpeg"
 *         }
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 *
 * @apiErrorExample User doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *         "success": false,
 *         "message": "User doesn't exist."
 *     }
 * 
 * @apiErrorExample User ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *         "success": false,
 *         "message": "User id is not valid."
 *     }
 * 
 * @apiDescription This route allows you to retrieve a user from the database.
 * 
 */

router.get("/:id", Auth.SetAPIAccess(), passport.authenticate("jwt", { session: false }), Permissions.get(Users_Schema), API.get(Users_Schema));

/**
 * @api {put} /users/:id/update Update user
 * @apiName UpdateUser
 * @apiGroup Users
 * @apiPermission SA (SuperAdmin), A (Admin), C (Customer)
 *
 * @apiParam {Number} id     The user ID.
 * @apiParam {Object} update The user update object data.
 * @apiParamExample {json} User update request
 *     {
 *         "update": {
 *             "email": "test_3@test.com",
 *             "password": "azertyuiop",
 *              "profile_pic": {
 *                  "uri": "data:image/jpeg;base64,"
 *              }
 *         }
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} message  Success message.
 *
 * @apiSuccessExample User updated
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "User is updated."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample User doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "User doesn't exist."
 *     }
 * 
 * @apiErrorExample Update object null
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Update object is null."
 *     }
 * 
 * @apiErrorExample User ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "The user id is not valid."
 *     }
 * 
 * @apiDescription This route allows you to update a user from the database.
 * 
 */

router.put("/:id/update", Auth.SetAPIAccess(), passport.authenticate("jwt", { session: false }), Permissions.update(Users_Schema), API.update(Users_Schema));

/**
 * @api {patch} /users/:id/restore Restore user
 * @apiName RestoreUser
 * @apiGroup Users
 * @apiPermission SA (SuperAdmin), A (Admin)
 *
 * @apiParam {Number} id   The user ID
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String}  message  Success message.
 *
 * @apiSuccessExample User restored
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "User is restored."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample User doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "User doesn't exist."
 *     }
 * 
 * @apiErrorExample User ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "The user id is not valid."
 *     }
 * 
 * @apiDescription This route allows you to restore a user from the database.
 * 
 */

router.patch("/:id/restore", Auth.SetAPIAccess(["SA", "A"]), passport.authenticate("jwt", { session: false }), Permissions.restore(Users_Schema), API.restore(Users_Schema));

/**
 * @api {delete} /users/:id/delete Delete user
 * @apiName DeleteUser
 * @apiGroup Users
 * @apiPermission SA (SuperAdmin), A (Admin)
 *
 * @apiParam {Number} id   The user ID
 *
 * @apiSuccess {Boolean} success    Request status.
 * @apiSuccess {String}  message    Success message.
 *
 * @apiSuccessExample User deleted
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "User is deleted."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample User doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "User doesn't exist."
 *     }
 * 
 * @apiErrorExample User ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "The user id is not valid."
 *     }
 * 
 * @apiDescription This route allows you to delete a user from the database.
 * 
 */
router.delete("/:id/delete", Auth.SetAPIAccess(["SA", "A"]), passport.authenticate("jwt", { session: false }), Permissions.delete(Users_Schema), API.delete(Users_Schema));

module.exports = router;