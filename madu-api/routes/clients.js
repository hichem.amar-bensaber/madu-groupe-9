const express = require("express");
const router = express.Router();
const passport = require("passport");
const API = require("../controllers/api_controllers.js");
const Permissions = require("../controllers/permissions_controllers.js");
const Clients_Schema = require("../models/clients.js");
const Auth = require("../controllers/auth.js");
const Client = require("../controllers/client.js");

/**
 * @api {post} /clients/create Create new client
 * @apiName CreateClient
 * @apiGroup Clients
 * @apiPermission SA (SuperAdmin)
 * 
 * @apiParam {Object} create                        The client create object data.
 * @apiParam {String} create.name                   The client name field.
 * @apiParam {Object} create.localisation           The client localisation field.
 * @apiParam {String} create.localisation.city      The client localisation.city field.          
 * @apiParam {String} create.localisation.address   The client localisation.address field.             
 * @apiParam {String} create.localisation.zip_code  The client localisation.zip_code field.             
 * @apiParam {Object} [create.coords]               The client coords field.
 * @apiParam {Number} [create.coords.longitude]     The client coords.longitude field.   
 * @apiParam {Number} [create.coords.latitude]      The client coords.latitude field.  
 * @apiParam {String} create.phone                  The client phone field.
 * @apiParam {String} create.email                  The client email field.
 * @apiParam {Object} [create.profile_pic="images/profile-pic/user-default.jpeg"] The client profile picture field.
 * @apiParam {String} create.profile_pic.uri The image data, only base64.
 * 
 * @apiParamExample {json} Client create request
 *     {
 *         "create": {
 *             "name": "Test",
 *             "localisation": {
 *                 "city": "Test",
 *                 "address": "Test",
 *                 "zip_code": "00000"
 *             },
 *             "phone": "0666666666",
 *             "email": "test@test.com",
 *             "profile_pic": {
 *                 "uri": "data:image/jpeg;base64,"
 *             }
 *         }
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} _id The client ID of the client who has just been created.
 *
 * @apiSuccessExample Client Created
 *     HTTPS/1.1 201 OK
 *     {
 *         "success": true,
 *         "_id": "5e3dd2d51d9f190609e8258c"
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {Object} errors Errors object, handleAPIError() manage Mongodb errors.
 * 
 * @apiErrorExample Required value
 *     HTTPS/1.1 400 Not Found
 *     {
 *         "success": false,
 *         "errors": {
 *             "email": {
 *                 "path": "email",
 *                 "kind": "required"
 *             }
 *         }
 *     }
 * 
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 *
 * @apiErrorExample Create object null
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Create object is null."
 *     }
 * 
 * @apiDescription This route allows you to create a new client from the database.
 * 
 */
router.post("/create", Auth.SetAPIAccess(["SA"]), passport.authenticate("jwt", { session: false }), API.create(Clients_Schema));

/**
 * @api {get} /clients/list Get clients list
 * @apiName GetListClients
 * @apiGroup Clients
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Object}   selector               The client selector object.
 * @apiParam {Number}   limit=100              The object capture limit.
 * @apiParam {Number}   offset=0               The number of objects to ignore.
 * @apiParam {Object}   sort={"createdAt":-1}  Sort result by specific field, 1 = ASC, -1 = DESC.
 * @apiParam {Boolean}  isDeleted=false        Returns only objects that have been not deleted.
 * @apiParamExample {json} Clients list request
 *     {
 *         "selector": {
 *            "name": "Test"
 *         },
 *         "sort": {
 *            "name": 1
 *         },
 *         "limit": 1000,
 *         "isDeleted": false
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object} data The clients list data.
 *
 * @apiSuccessExample Clients list data
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "data": [
 *             {
 *                 "name": "Test_1",
 *                 "localisation": {
 *                     "city": "Test_1",
 *                     "address": "Test_1",
 *                     "zip_code": "00000"
 *                 },
 *                 "coords": {
 *                     "longitude": 39.3535,
 *                     "latitude": 4.3535
 *                 }
 *                 "phone": "0666666666",
 *                 "email": "test_1@test.com"
 *             },
 *             {
 *                 "name": "Test_2",
 *                 "localisation": {
 *                     "city": "Test_2",
 *                     "address": "Test_2",
 *                     "zip_code": "00000"
 *                 },
 *                 "coords": {
 *                     "longitude": 39.3535,
 *                     "latitude": 4.3535
 *                 }
 *                 "phone": "0666666666",
 *                 "email": "test_2@test.com"
 *             }
 *         ]
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 * 
 * @apiErrorExample Clients list empty
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Clients list is empty."
 *     }
 * 
 * @apiErrorExample Clients list selector null
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Selector object is null."
 *     }
 * 
 * @apiDescription This route allows you to retrieve a clients list from the database.
 * 
 */
router.get("/list", Auth.SetAPIAccess(["SA"]), passport.authenticate("jwt", { session: false }), Permissions.list(Clients_Schema), API.list(Clients_Schema));

/**
 * @api {get} /clients/:id Get client
 * @apiName GetClient
 * @apiGroup Clients
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Number} id The client ID.
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object} data     The client data.
 *
 * @apiSuccessExample Client Data
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "data": {
 *             "name": "Test",
 *             "localisation": {
 *                 "city": "Test",
 *                 "address": "Test",
 *                 "zip_code": "00000"
 *             },
 *             "coords": {
 *                 "longitude": 39.3535,
 *                 "latitude": 4.3535
 *             }
 *             "phone": "0666666666",
 *             "email": "test@test.com"
 *         }
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 *
 * @apiErrorExample Client doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *         "success": false,
 *         "message": "Client doesn't exist."
 *     }
 * 
 * @apiErrorExample Client ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *         "success": false,
 *         "message": "Client id is not valid."
 *     }
 * 
 * @apiDescription This route allows you to retrieve a client from the database.
 * 
 */
router.get("/:id", Auth.SetAPIAccess(), passport.authenticate("jwt", { session: false }), Permissions.get(Clients_Schema), API.get(Clients_Schema));

/**
 * @api {put} /clients/:id/update Update client
 * @apiName UpdateClient
 * @apiGroup Clients
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Number} id        The client ID.
 * @apiParam {Object} update    The client update object data.
 * @apiParamExample {json}      Client update request
 *     {
 *         "update": {
 *             "email": "test_3@test.com"
  *             "profile_pic": {
 *                  "uri": "data:image/jpeg;base64,"
 *              }
 *         }
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} message  Success message.
 *
 * @apiSuccessExample Client updated
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "Client is updated."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample Client doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Client doesn't exist."
 *     }
 * 
 * @apiErrorExample Update object null
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Update object is null."
 *     }
 * 
 * @apiErrorExample Client ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Client id is not valid."
 *     }
 * 
 * @apiDescription This route allows you to update a client from the database.
 * 
 */
router.put("/:id/update", Auth.SetAPIAccess(["SA", "A"]), passport.authenticate("jwt", { session: false }), Permissions.update(Clients_Schema), API.update(Clients_Schema));

/**
 * @api {patch} /clients/:id/restore Restore client
 * @apiName RestoreClient
 * @apiGroup Clients
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Number} id   The client ID
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} message  Success message.
 *
 * @apiSuccessExample Client restored
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "Client is restored."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample Client doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Client doesn't exist."
 *     }
 * 
 * @apiErrorExample Client ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Client id is not valid."
 *     }
 * 
 * @apiDescription This route allows you to restore a client from the database.
 * 
 */
router.patch("/:id/restore", Auth.SetAPIAccess(["SA"]), passport.authenticate("jwt", { session: false }), Permissions.restore(Clients_Schema), API.restore(Clients_Schema));

/**
 * @api {delete} /clients/:id/delete Delete client
 * @apiName DeleteClient
 * @apiGroup Clients
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Number} id   The client ID
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} message  Success message.
 *
 * @apiSuccessExample Client deleted
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "Client is deleted."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample Client doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Client doesn't exist."
 *     }
 * 
 * @apiErrorExample Client ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Client id is not valid."
 *     }
 * 
 * @apiDescription This route allows you to delete a client from the database.
 * 
 */
router.delete("/:id/delete", Auth.SetAPIAccess(["SA"]), passport.authenticate("jwt", { session: false }), Permissions.delete(Clients_Schema), API.delete(Clients_Schema));

/**
 * @api {post} /clients/check-email-suffix Check if email suffix exist or not
 * @apiName CheckEmailSuffix
 * @apiGroup Clients
 *
 * @apiParam {String} email_suffix                  The email suffix to check.
 *
 * @apiParamExample {json} Check email suffix request
 *     {
 *         "email_suffix": "test.com"
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object} data The client retrieved with the email suffix
 *
 * @apiSuccessExample Email suffix exists
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "data": {
 *             "name": "Test",
 *             "localisation": {
 *                 "city": "Test",
 *                 "address": "Test",
 *                 "zip_code": "00000"
 *             },
 *             "coords": {
 *                 "longitude": 39.3535,
 *                 "latitude": 4.3535
 *             }
 *             "phone": "0666666666",
 *             "email": "test@test.com"
 *         }
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 *
 * @apiErrorExample Required value
 *     HTTPS/1.1 400 Not Found
 *     {
 *         "success": false,
 *         "message": "You did not sent the email suffix to verify."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 *
 * @apiErrorExample The email suffix does not exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "The email suffix test.com doesn't exist."
 *     }
 *
 * @apiDescription This route allows you to check if the email suffix already exists in the database or not.
 *
 */
router.post("/check-email-suffix", Client.checkEmailSuffix);

module.exports = router;