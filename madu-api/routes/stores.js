const express = require("express");
const router = express.Router();
const passport = require("passport");
const API = require("../controllers/api_controllers.js");
const Permissions = require("../controllers/permissions_controllers.js");
const Stores_Schema = require("../models/stores.js");
const Auth = require("../controllers/auth.js");

/**
 * @api {post} /stores/create Create new store
 * @apiName CreateStore
 * @apiGroup Stores
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Object} create                        The store create object data.
 * @apiParam {String} create.name                   The store name field.
 * @apiParam {String} create.description            The store description field.
 * @apiParam {String} create.email                  The store email field.
 * @apiParam {String} create.phone                  The store phone field.
 * @apiParam {String} [create.web_site]               The store web_site field.
 * @apiParam {String} create.store_type             The store type field, it's a enumeration ["Boutique", "Restaurant", "Activité"].
 * @apiParam {String} create.store_speciality       The store speciality field ex (House, Fast food, Beauty, Sport, DIY,..).
 * @apiParam {Object} create.store_filters          The store filters object.
 * @apiParam {String} create.store_filters.price_average            The store store_filters.price average field.
 * @apiParam {Boolean} [create.store_filters.bio=false]             The store store_filters.bio free field.
 * @apiParam {Boolean} [create.store_filters.gluten_free=false]     The store store_filters.gluten free field.
 * @apiParam {Boolean} [create.store_filters.vegan=false]           The store store_filters.vegan field.
 * @apiParam {Boolean} [create.store_filters.veggie=false]          The store store_filters.veggie field.
 * @apiParam {Boolean} [create.store_filters.terrace=false]         The store store_filters.terrace field.
 * @apiParam {Boolean} [create.store_filters.take_away=false]       The store store_filters.take_away field.
 * @apiParam {Array}   [create.store_filters.handicap]              The store store_filters.handicap field.
 * @apiParam {String}  [create.store_filters.handicap.type]         The store store_filters.handicap.type field.
 * @apiParam {Object} create.localisation           The store localisation field.
 * @apiParam {String} create.localisation.city      The store localisation.city field.
 * @apiParam {String} create.localisation.address   The store localisation.address field.
 * @apiParam {String} create.localisation.zip_code  The store localisation.zip_code field.
 * @apiParam {Object} [create.coords]               The store coords field.
 * @apiParam {Number} [create.coords.latitude]      The store coords.latitude field.
 * @apiParam {Number} [create.coords.longitude]     The store coords.longitude field.
 * @apiParam {Object} create.open_hours             The store open_hours field.
 * @apiParam {String} create.open_hours.mon         The store open_hours.mon field.
 * @apiParam {String} create.open_hours.tue         The store open_hours.tue field.
 * @apiParam {String} create.open_hours.wed         The store open_hours.wed field.
 * @apiParam {String} create.open_hours.thu         The store open_hours.thu field.
 * @apiParam {String} create.open_hours.fri         The store open_hours.fri field.
 * @apiParam {String} create.open_hours.sat         The store open_hours.sat field.
 * @apiParam {String} create.open_hours.sun         The store open_hours.sun field.
 * @apiParam {String} create.status="PENDING"       The store status field, it's a enumeration ["PENDING", "ON", "OFF"].
 * @apiParam {Array}  create.images                 The store images field.
 * @apiParam {String} create.images.uri             The store images.uri field, only base64.
 * @apiParam {Number} create.greenscore=0           The store greenscore field.
 * @apiParam {String} [create.greenscore_description]               The store greenscore_description field.
 * @apiParam {String} create._proposed_by_user      The store _proposed_by_user field.
 * @apiParamExample {json} Store create request
 *     {
 *         "create": {
 *             "name": "Test",
 *             "description": "Test",
 *             "email": "test@test.com",
 *             "phone": "0164546465",
 *             "web_site": "https://google.com",
 *             "store_type": "Boutique",
 *             "store_speciality": "Maison",
 *             "store_filters": {
 *                 "price_average": "15€",
 *                 "bio": false,
 *                 "vegan": false,
 *                 "veggie": false,
 *                 "terrace": false,
 *                 "take_away": true,
 *                 "handicap": [
 *                     {"type": "Visuel"},
 *                     {"type": "Auditif"},
 *                     {"type": "Moteur"}
 *                 ]
 *             },
 *             "localisation": {
 *                 "city": "Test",
 *                 "address": "Test",
 *                 "zip_code": "00000"
 *             },
 *             "coords": {
 *                 "longitude": 39.3535,
 *                 "latitude": 4.3535
 *             },
 *             "open_hours": {
 *                 "mon": "9AM - 7PM",
 *                 "tue": "9AM - 7PM",
 *                 "wed": "9AM - 7PM",
 *                 "thu": "9AM - 7PM",
 *                 "fri": "9AM - 7PM",
 *                 "sat": "9AM - 7PM",
 *                 "sun": "9AM - 7PM"
 *             },
 *             "status": "PENDING",
 *             "images":  [
 *                 {
 *                     "uri": "data:image/jpeg;base64,"
 *                 }
 *             ],
 *             "greenscore": 0,
 *             "greenscore_description": "Greenscore justification",
 *             "_proposed_by_user": "5e3dd2d51d9f190609e8258c"
 *         }
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String}  _id The store ID of the store who has just been created.
 *
 * @apiSuccessExample Store Created
 *     HTTPS/1.1 201 OK
 *     {
 *         "success": true,
 *         "store_id": "5e3dd2d51d9f190609e8258c"
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {Object} errors Errors object, handleAPIError() manage Mongodb errors.
 *
 * @apiErrorExample Unique value
 *     HTTPS/1.1 400 Not Found
 *     {
 *         "success": false,
 *         "errors": {
 *             "name": {
 *                 "path": "name",
 *                 "kind": "unique"
 *             }
 *         }
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 *
 * @apiErrorExample Create object null
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Create object is null."
 *     }
 *
 * @apiDescription This route allows you to create a new store from the database.
 *
 */

router.post(
  "/create",
  Auth.SetAPIAccess(["SA"]),
  passport.authenticate("jwt", { session: false }),
  API.create(Stores_Schema)
);

/**
 * @api {get} /stores/list Get stores list
 * @apiName GetListStores
 * @apiGroup Stores
 * @apiPermission SA (SuperAdmin), A (Admin), C (Customer)
 *
 * @apiParam {Object}   selector               The store selector object.
 * @apiParam {Number}   limit=25              The object capture limit.
 * @apiParam {Number}   offset=0               The number of objects to ignore.
 * @apiParam {Object}   sort={"createdAt":-1}  Sort result by specific field, 1 = ASC, -1 = DESC.
 * @apiParam {Boolean}  isDeleted=false        Returns only objects that have been not deleted.
 * @apiParamExample {json} Stores list request
 *     {
 *         "selector": {
 *            "store_type": "Boutique"
 *         },
 *         "sort": {
 *            "name": 1
 *         },
 *         "limit": 1000,
 *         "isDeleted": false
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object} data The stores list data.
 *
 * @apiSuccessExample Stores list data
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "data": [
 *             {
 *                 "name": "Test",
 *                 "description": "Test",
 *                 "email": "test@test.com",
 *                 "phone": "0164546465",
 *                 "web_site": "https://google.com",
 *                 "store_type": "Boutique",
 *                 "store_speciality": "Maison",
 *                 "store_filters": {
 *                     "price_average": "15€",
 *                     "bio": false,
 *                     "vegan": false,
 *                     "veggie": false,
 *                     "terrace": false,
 *                     "take_away": true,
 *                     "handicap": [
 *                         {"type": "Visuel"},
 *                         {"type": "Auditif"},
 *                         {"type": "Moteur"}
 *                     ]
 *                 },
 *                 "localisation": {
 *                     "city": "Test",
 *                     "address": "Test",
 *                     "zip_code": "00000"
 *                 },
 *                 "coords": {
 *                     "longitude": 39.3535,
 *                     "latitude": 4.3535
 *                 },
 *                 "open_hours": {
 *                     "mon": "9AM - 7PM",
 *                     "tue": "9AM - 7PM",
 *                     "wed": "9AM - 7PM",
 *                     "thu": "9AM - 7PM",
 *                     "fri": "9AM - 7PM",
 *                     "sat": "9AM - 7PM",
 *                     "sun": "9AM - 7PM",
 *                 },
 *                 "status": "PENDING",
 *                 "images":  [
 *                     {
 *                         "uri": "images/stores/a4otdtpohzn3w8k5oi7gsf6rlo751ara/c2edkk1n35.jpeg"
 *                     },
 *                     {
 *                         "uri": "images/stores/a4otdtpohzn3w8k5oi7gsf6rlo751ara/fsdfsd34df.jpeg"
 *                     }
 *                 ],
 *                 "greenscore": 0,
 *                 "greenscore_description": "Greenscore justification",
 *                 "_proposed_by_user": "5e3dd2d51d9f190609e8258c"
 *             }
 *         ]
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 *
 * @apiErrorExample Stores list empty
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Stores list is empty."
 *     }
 *
 * @apiErrorExample Stores list selector null
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Selector object is null."
 *     }
 *
 *  @apiDescription This route allows you to retrieve a stores list from the database.
 *
 */

router.get(
  "/list",
  Auth.SetAPIAccess(),
  passport.authenticate("jwt", { session: false }),
  Permissions.list(Stores_Schema),
  API.list(Stores_Schema)
);

/**
 * @api {get} /stores/:id Get store
 * @apiName GetStore
 * @apiGroup Stores
 * @apiPermission SA (SuperAdmin), A (Admin), C (Customer)
 *
 * @apiParam {Number} id The store ID.
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {Object} data     The store data.
 *
 * @apiSuccessExample Store Data
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "data": {
 *             "name": "Test",
 *             "description": "Test",
 *             "email": "test@test.com",
 *             "phone": "0164546465",
 *             "web_site": "https://google.com",
 *             "store_type": "Boutique",
 *             "store_speciality": "Maison",
 *             "store_filters": {
 *                 "price_average": "15€",
 *                 "bio": "false",
 *                 "vegan": false,
 *                 "veggie": false,
 *                 "terrace": false,
 *                 "take_away": true,
 *                 "handicap": [
 *                     {"type": "Visuel"},
 *                     {"type": "Auditif"},
 *                     {"type": "Moteur"}
 *                 ]
 *             },
 *             "localisation": {
 *                 "city": "Test",
 *                 "address": "Test",
 *                 "zip_code": "00000"
 *             },
 *             "coords": {
 *                 "longitude": 39.3535,
 *                 "latitude": 4.3535
 *             },
 *             "open_hours": {
 *                 "mon": "9AM - 7PM",
 *                 "tue": "9AM - 7PM",
 *                 "wed": "9AM - 7PM",
 *                 "thu": "9AM - 7PM",
 *                 "fri": "9AM - 7PM",
 *                 "sat": "9AM - 7PM",
 *                 "sun": "9AM - 7PM",
 *             },
 *             "status": "PENDING",
 *             "images":  [
 *                 {
 *                     "uri": "images/stores/a4otdtpohzn3w8k5oi7gsf6rlo751ara/c2edkk1n35.jpeg"
 *                 },
 *                 {
 *                     "uri": "images/stores/a4otdtpohzn3w8k5oi7gsf6rlo751ara/fsdfsd34df.jpeg"
 *                 }
 *             ],
 *             "greenscore": 0,
 *             "greenscore_description": "Greenscore justification",
 *             "_proposed_by_user": "5e3dd2d51d9f190609e8258c"
 *         }
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message Error message.
 *
 * @apiErrorExample Store doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *         "success": false,
 *         "message": "Store doesn't exist."
 *     }
 *
 * @apiErrorExample Store ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *         "success": false,
 *         "message": "Store id is not valid."
 *     }
 *
 * @apiDescription This route allows you to retrieve a store from the database.
 *
 */

router.get(
  "/:id",
  Auth.SetAPIAccess(),
  passport.authenticate("jwt", { session: false }),
  Permissions.get(Stores_Schema),
  API.get(Stores_Schema)
);

/**
 * @api {put} /stores/:id/update Update store
 * @apiName UpdateStore
 * @apiGroup Stores
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Number} id    The store ID.
 * @apiParam {Object} update The store update object data.
 * @apiParam {Object} update.email The store update email field.
 * @apiParam {Array} update.images The store update images, all the old photos are replaced by the new ones.
 * @apiParam {Object} update.images.uri The store update images.uri field, only base64.
 * @apiParamExample {json} Store update request
 *     {
 *         "update": {
 *             "email": "test_3@test.com",
 *             "images": [
 *                 {
 *                     "uri": "data:image/jpeg;base64,"
 *                 },
 *                 {
 *                     "uri": "data:image/jpeg;base64,"
 *                 }
 *             ]
 *         }
 *     }
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} message  Success message.
 *
 * @apiSuccessExample Store updated
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "Store is updated."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample Store doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Store doesn't exist."
 *     }
 *
 * @apiErrorExample Update object null
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Update object is null."
 *     }
 *
 * @apiErrorExample Store ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Store id is not valid."
 *     }
 *
 * @apiDescription This route allows you to update a store from the database.
 *
 */

router.put(
  "/:id/update",
  Auth.SetAPIAccess(["SA"]),
  passport.authenticate("jwt", { session: false }),
  Permissions.update(Stores_Schema),
  API.update(Stores_Schema)
);

/**
 * @api {patch} /stores/:id/restore Restore store
 * @apiName RestoreStore
 * @apiGroup Stores
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Number} id   The store ID
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} message  Success message.
 *
 * @apiSuccessExample Store restored
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "Store is restored."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample Store doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Store doesn't exist."
 *     }
 *
 * @apiErrorExample Store ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Store id is not valid."
 *     }
 *
 * @apiDescription This route allows you to restore a store from the database.
 *
 */

router.patch(
  "/:id/restore",
  Auth.SetAPIAccess(["SA"]),
  passport.authenticate("jwt", { session: false }),
  Permissions.restore(Stores_Schema),
  API.restore(Stores_Schema)
);

/**
 * @api {delete} /stores/:id/delete Delete store
 * @apiName DeleteStore
 * @apiGroup Stores
 * @apiPermission SA (SuperAdmin)
 *
 * @apiParam {Number} id   The store ID
 *
 * @apiSuccess {Boolean} success Request status.
 * @apiSuccess {String} message  Success message.
 *
 * @apiSuccessExample Store deleted
 *     HTTPS/1.1 200 OK
 *     {
 *         "success": true,
 *         "message": "Store is deleted."
 *     }
 *
 * @apiError {Boolean} success Request status.
 * @apiError {String} message  Error message.
 *
 * @apiErrorExample Store doesn't exist
 *     HTTPS/1.1 404 Not Found
 *     {
 *       "success": false,
 *       "message": "Store doesn't exist."
 *     }
 *
 * @apiErrorExample Store ID not valid
 *     HTTPS/1.1 400 Not Found
 *     {
 *       "success": false,
 *       "message": "Store id is not valid."
 *     }
 *
 * @apiDescription This route allows you to delete a store from the database.
 *
 */

router.delete(
  "/:id/delete",
  Auth.SetAPIAccess(["SA"]),
  passport.authenticate("jwt", { session: false }),
  Permissions.delete(Stores_Schema),
  API.delete(Stores_Schema)
);

module.exports = router;
