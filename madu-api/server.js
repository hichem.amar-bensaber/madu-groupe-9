const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const passport = require("passport");
//const compression = require("compression");

console.clear();

app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json({ limit: "10mb" }));
app.use(bodyParser.text({defaultCharset: "utf-8"}));
app.use(passport.initialize());
//app.use(compression());

require("./config/passport.js")(passport);

app.use(express.static("./public"));
app.use(express.static("./documentation"));

const rooterDocumentation = require("./routes/documentation.js");
app.use("/", rooterDocumentation);

const rooterAuth = require("./routes/auth.js");
app.use("/login", rooterAuth);

const rooterUsers = require("./routes/users.js");
app.use("/users", rooterUsers);

const rooterClients = require("./routes/clients.js");
app.use("/clients", rooterClients);

const rooterStores = require("./routes/stores.js");
app.use("/stores", rooterStores);

const rooterChallenges = require("./routes/challenges.js");
app.use("/challenges", rooterChallenges);

const rooterRewards = require("./routes/rewards.js");
app.use("/rewards", rooterRewards);

const rooterToken = require("./routes/token.js");
app.use("/token", rooterToken);

const rooterQuizzes = require("./routes/quizzes.js");
app.use("/quizzes", rooterQuizzes);

module.exports = app;