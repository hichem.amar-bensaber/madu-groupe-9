# API Backend

## Personnes concernées par l'expertise

- Damien
- Théo

### Technologies utilisées

- NodeJS
- Express
- MongoDB

### Schéma de base de données

- resources/madu-api-uml.jpg
![UML Madu API](resources/madu-api-uml.jpg)

### Documentation de l'API

- http://localhost:8080/documentation

### Liste de librairies utilisées

- bcrypt,
- body-parser,
- cors,
- express,
- grunt,
- jsonwebtoken
- mongoose,
- mongoose-unique-validator,
- passport,
- passport-jwt

#### Librairies de développement

- grunt-apidoc,
- jest,
- mongodb-memory-server,
- nodemon,
- supertest

###

## Dossier d'argumentation

### NodeJS

Nous avons utilisés NodeJS pour sa rapidité et sa facilité de maintenabilité/écriture du code.
Il permet également de faire de l'asynchrone qui nous permet de lancer une tâche et d’avancer dans le programme le temps que cette tâche s’exécute.

### MongoDB

Nous avons utilisés MongoDB comme base de données pour sa flexibilité et sa performance lors des requetes.
Il nous permet de faire évoluer la base de données simplement au fur et à mesure que l'on a de nouvelles spécifications au niveau de l'application.
Il est également "modulaire" ce qui nous permet de créer des fonctions pour valider le mot de passe d'un user par exemple.

### Tests

Nous avons réalisés des tests unitaires et fonctionnels pour permettre à l'application d'évoluer convenablement
et éviter de futures possibles régressions lors d'ajout de fonctionnalités ou lors du refactoring de code.