const mongoose = require("mongoose");
const app = require(__dirname + "/server");
const env = require(__dirname + "/config/env.js");

// TODO : REMOVE IN PRODUCTION (OR FIND ANOTHER WAY TO DO IT)
const faker = require("faker");
const User = require("./models/users");
const Client = require("./models/clients");
const Store = require("./models/stores");

const fakeDatabase = false;
const numberOfUsers = 10;
const numberOfClients = 10;
const numberOfStores = 10;

mongoose
  .connect(env.DB_ADDRESS, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  })
  .then(() => console.log("Mongoose connected to '" + env.DB_ADDRESS + "'"))
  .catch((error) => {
    throw error;
  });

app.listen(env.PORT, () => {
  console.log(`App listening on port ${env.PORT}`);
});

// TODO : REMOVE IN PRODUCTION (OR FIND ANOTHER WAY TO DO IT)
// Generate fake data
if (fakeDatabase) {
  for (let i = 0; i < numberOfClients; i++) {
    Client.create(
      {
        name: faker.lorem.sentence(),
        localisation: {
          city: faker.address.city(),
          address: faker.address.streetAddress(true),
          zip_code: faker.address.zipCode(),
        },
        coords: {
          longitude: faker.address.longitude(),
          latitude: faker.address.latitude(),
        },
        phone: faker.phone.phoneNumber(),
        email: faker.internet.email(),
      },
      (err, Data) => {
        let clientID = Data._id;
        for (let j = 0; j < numberOfUsers; j++) {
          User.create(
            {
              first_name: faker.name.firstName(),
              last_name: faker.name.lastName(),
              email: faker.internet.email(),
              password: "admin",
              _client: clientID,
              pot: faker.random.number(),
              role: faker.random.arrayElement(["SA", "A", "C"]),
            },
            (err, Data) => {
              let userID = Data._id;

              for (let l = 0; l < numberOfStores; l++) {
                Store.create({
                  name: faker.lorem.sentence(),
                  store_type: faker.random.arrayElement([
                    "Boutique",
                    "Restaurant",
                    "Activité",
                  ]),
                  store_speciality: faker.lorem.word(),
                  price_average: faker.lorem.word(),
                  veggie_certification: faker.random.arrayElement([
                    "VG",
                    "VGN",
                    "VGNF",
                  ]),
                  gluten_free: faker.random.boolean(),
                  bio: faker.random.arrayElement(["FULL", "PARTIAL", "NO"]),
                  localisation: {
                    city: faker.address.city(),
                    address: faker.address.streetAddress(true),
                    zip_code: faker.address.zipCode(),
                  },
                  coords: {
                    longitude: faker.address.longitude(),
                    latitude: faker.address.latitude(),
                  },
                  status: faker.random.arrayElement(["PENDING", "ON", "OFF"]),
                  open_hours: {
                    mon: faker.lorem.word(),
                    tue: faker.lorem.word(),
                    wed: faker.lorem.word(),
                    thu: faker.lorem.word(),
                    fri: faker.lorem.word(),
                    sat: faker.lorem.word(),
                    sun: faker.lorem.word(),
                  },
                  // images:  [
                  //     {
                  //         name: {type: String, required: true, trim: true},
                  //         path: {type: String, required: true, trim: true},
                  //         _id: false
                  //     }
                  // ],
                  // tags: [
                  //     {
                  //         name: {type: String, required: true},
                  //         _id: false
                  //     }
                  // ],
                  greenscore: faker.random.number(),
                  _proposed_by_user: userID,
                });
              }
            }
          );
        }
      }
    );
  }
}