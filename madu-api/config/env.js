module.exports = {
    PORT: process.env.PORT,
    DB_ADDRESS: `mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}/${process.env.DB_NAME}?authSource=admin`
};
