const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const User = require("../models/users.js");
const { isNullorEmpty } = require("../utils/helpers.js");

const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.TOKEN_KEY;
opts.passReqToCallback = true;

module.exports = function(passport) {
  passport.use(
    new JwtStrategy(opts, (req, jwt_payload, done) => {
      User.findById(jwt_payload.id, function(err, _User) {
        if (err) return done(null, false);
        if (Array.isArray(_User)) _User = _User[0];
        if (!isNullorEmpty(_User)) return done(null, _User);
        return done(null, false);
      }).isDeleted(false);
    })
  );
};
