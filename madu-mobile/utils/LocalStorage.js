import AsyncStorage from "@react-native-community/async-storage";

export const setData = async(key, value) => {
    try {
        await AsyncStorage.setItem(key, value);
        return true;
    }
    catch (error) {
        console.warn(error);
        return false;
    }
}

export const getData = async(key) => {
    try {
        const value = await AsyncStorage.getItem(key);
        if(value) return value;
        return null;
    }
    catch(error) {
        console.warn(error);
        return false;
    }
}

export const deleteData = async(key) => {
    try {
        await AsyncStorage.removeItem(key);
        return true;
    }
    catch(error) {
        console.warn(error);
        return false;
    }
}
