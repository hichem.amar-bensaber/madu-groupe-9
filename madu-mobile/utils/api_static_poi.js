export const getPoi = [
    {
      "_id": "5ebe82241e7c9e047546ac34",
      "store_filters": {
        "gluten_free": true,
        "vegan": true,
        "veggie": false,
        "terrace": true,
        "take_away": true,
        "price_average": "15€",
        "handicap": [
          {
            "type": "Visuell"
          },
          {
            "type": "Auditif"
          },
          {
            "type": "Moteur"
          }
        ]
      },
      "coords": {
        "latitude": 48.851728,
        "longitude": 2.418476
      },
      "status": "PENDING",
      "greenscore": 84,
      "name": "Pizza hut",
      "description": "Meilleur restaurant de pizza !",
      "store_type": "Restaurant",
      "store_speciality": "Pizza",
      "phone": "0120300094",
      "web_site": "https://www.pizzahut.fr",
      "localisation": {
        "city": "Montreuil",
        "address": "25 rue du savoir",
        "zip_code": "93100"
      },
      "open_hours": {
        "mon": "9:00 - 23:00",
        "tue": "Fermé",
        "wed": "9:00 - 03:00",
        "thu": "9:00 - 19:00",
        "fri": "9:00 - 19:00",
        "sat": "9:00 - 19:00",
        "sun": "Fermé"
      },
      "greenscore_description": "Ce greenscore est de 84%, car c’est un lieu éco-responsable et qui prône le vegan, il ne gaspille pas, et pense avant tout à l’environnement !",
      "_proposed_by_user": "5e3dd2d51d9f190609e8258c",
      "images": [
        {
          "uri": "images/stores/a4otdtpohzn3w8k5oi7gsf6rlo751ara/c2edkk1n35.jpeg"
        },
        {
          "uri": "images/stores/a4otdtpohzn3w8k5oi7gsf6rlo751ara/ag1p71uxav.jpeg"
        }
      ],
    },
    {
      "_id": "5ed7940ff20f44075604ba5b",
      "store_filters": {
        "gluten_free": false,
        "vegan": true,
        "veggie": true,
        "terrace": false,
        "take_away": true,
        "price_average": "15€",
        "handicap": [
          {
            "type": "Visuel"
          },
          {
            "type": "Auditif"
          },
          {
            "type": "Moteur"
          }
        ]
      },
      "coords": {
        "latitude": 48.852848,
        "longitude": 2.420711
      },
      "status": "PENDING",
      "greenscore": 90,
      "name": "Pizza dominos",
      "description": "Meilleur restaurant de pizza américaines !",
      "store_type": "Boutique",
      "store_speciality": "Pizza",
      "phone": "0120300094",
      "web_site": "https://www.dominos.fr/",
      "localisation": {
        "city": "Montreuil",
        "address": "25 rue du savoir",
        "zip_code": "93100"
      },
      "open_hours": {
        "mon": "9:00 - 23:00",
        "tue": "9:00 - 00:00",
        "wed": "9:00 - 19:00",
        "thu": "9:00 - 19:00",
        "fri": "9:00 - 19:00",
        "sat": "9:00 - 19:00",
        "sun": "Fermé"
      },
      "greenscore_description": "Ce greenscore est de 90%, car c’est un lieu éco-responsable et qui prône le vegan, il ne gaspille pas, et pense avant tout à l’environnement !",
      "_proposed_by_user": "5e3dd2d51d9f190609e8258c",
      "images": [
        {
          "uri": "images/stores/ols15top04q8mueg07bx1n2jhxqaxu8y/wsz8tgf7nb.png"
        },
        {
          "uri": "images/stores/ols15top04q8mueg07bx1n2jhxqaxu8y/u0wh0ayboz.png"
        }
      ]
    },
    {
      "_id": "5f0241d34d692c3afe071180",
      "store_filters": {
        "gluten_free": false,
        "vegan": false,
        "veggie": false,
        "terrace": false,
        "take_away": true,
        "price_average": "15€",
        "handicap": [
          {
            "type": "Visuel"
          },
          {
            "type": "Auditif"
          },
          {
            "type": "Moteur"
          }
        ]
      },
      "coords": {
        "latitude": 48.85281,
        "longitude": 2.418084
      },
      "status": "PENDING",
      "greenscore": 100,
      "name": "Tacos",
      "description": "Meilleur restaurant de tacos !",
      "store_type": "activité",
      "store_speciality": "Tacos",
      "phone": "0120300094",
      "web_site": "https://o-tacos.com/fr/menu",
      "localisation": {
        "city": "Montreuil",
        "address": "27 rue Armand Carrel",
        "zip_code": "93100"
      },
      "open_hours": {
        "mon": "9:00 - 23:00",
        "tue": "Fermé",
        "wed": "09:00 - 22:00",
        "thu": "9:00 - 19:00",
        "fri": "9:00 - 19:00",
        "sat": "9:00 - 19:00",
        "sun": "Fermé"
      },
      "greenscore_description": "Ce greenscore est de 100%, car c’est un lieu éco-responsable et qui prône le vegan, il ne gaspille pas, et pense avant tout à l’environnement !",
      "_proposed_by_user": "5e3dd2d51d9f190609e8258c",
      "images": [
        {
          "uri": "images/stores/ols15top04q8mueg07bx1n2jhxqaxu8y/wsz8tgf7nb.png"
        },
        {
          "uri": "images/stores/ols15top04q8mueg07bx1n2jhxqaxu8y/u0wh0ayboz.png"
        }
      ]
    }
]
