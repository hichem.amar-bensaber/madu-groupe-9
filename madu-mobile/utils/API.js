const URL_API_DEV = Platform.OS === "android" ? "10.0.2.2:8080" : "localhost:8080"; // 10.0.2.2 => Fetch localhost emulator android
const URL_LOCAL_API_DEV = Platform.OS === "android" ? "192.168.1.37:8080" : "192.168.1.37:8080"; // 192.168.1.96 => Fetch local network IP API

export const checkUserToken = async (token) => {
  return new Promise((resolve, reject) => {
    fetch("http://" + URL_LOCAL_API_DEV + "/token/verify", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        token: token,
      }),
    })
      .then((res) => res.json())
      .then((json) => {
        if (!json.success) return resolve(false);
        resolve(json);
      })
      .catch((error) => handlingErrors(error, resolve));
  });
};

export const login = async (Email, Password) => {
  return new Promise((resolve, reject) => {
    fetch("http://" + URL_LOCAL_API_DEV + "/login", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: Email,
        password: Password,
      }),
    })
      .then((res) => res.json())
      .then((json) => {
        if (!json.success) return resolve(false);
        resolve(json);
      })
      .catch((error) => handlingErrors(error, resolve));
  });
};

export const checkUserEmail = async (Email) => {
  return new Promise((resolve, reject) => {
    fetch("http://" + URL_LOCAL_API_DEV + "/clients/check-email-suffix", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email_suffix: Email.split("@")[1],
      }),
    })
      .then((res) => res.json())
      .then((json) => {
        resolve(json);
      })
      .catch((error) => handlingErrors(error, resolve));
  });
};

export const register = async (firstName, lastName, Email, Password, ID) => {
  return new Promise((resolve, reject) => {
    fetch("http://" + URL_LOCAL_API_DEV + "/users/create", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        create: {
          first_name: firstName,
          last_name: lastName,
          email: Email,
          password: Password,
          _client: ID,
        },
      }),
    })
      .then((res) => res.json())
      .then((json) => {
        resolve(json);
      })
      .catch((error) => handlingErrors(error, resolve));
  });
};

export const listStores = async (token) => {
  if (!token) return console.log("User token is null !");
  return new Promise((resolve, reject) => {
    fetch("http://" + URL_LOCAL_API_DEV + "/stores/list", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    })
      .then((res) => res.json())
      .then((json) => {
        if (!json.success) resolve(false);
        resolve(json.data);
      })
      .catch((error) => handlingErrors(error, resolve));
  });
};

export const updateUserById = (id, token, data) => {
  if (!token) return console.log("User token is null !");
  return new Promise((resolve, reject) => {
    fetch(`http://${URL_LOCAL_API_DEV}/users/${id}/update`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
      body: JSON.stringify({
        update: data,
      }),
    })
      .then((res) => res.json())
      .then((json) => {
        console.log(json);
        resolve(json);
      })
      .catch((error) => handlingErrors(error, resolve));
  });
};

const handlingErrors = (error, resolve) => {
  console.log("Error fetch " + URL_LOCAL_API_DEV + ", please check if server is still alive !");
  console.warn(error);
  return resolve({
    success: false,
    message: "Error fetch " + URL_LOCAL_API_DEV + ", please check if server is still alive !"
  });
};

export const getQuizzesList = (token) => {
  if (!token) return console.log("User token is null !");
  return new Promise((resolve, reject) => {
    fetch("http://" + URL_LOCAL_API_DEV + "/quizzes/list", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    })
      .then((res) => res.json())
      .then((json) => {
        resolve(json);
      })
      .catch((error) => handlingErrors(error, resolve));
  });
};
