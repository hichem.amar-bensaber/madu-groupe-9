/**
 * @description Check if value is null or empty.
 * @param {*} value 
 */

export const isNullOrEmpty = (value) => {
    if (value === null || !value) return true;
    if (typeof (value) === "string") {
        if (value.length === 0 || value.trim() === "") return true;
    }
    else {
        if (typeof (value) === "undefined" || value.length === 0) return true;
    }
    return false;
};

/**
 * @description Check if email is valid.
 * @param {string} Email 
 */

export const emailValidator = (Email) => {
    if (isNullOrEmpty(Email)) return false;
    let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regex.test(String(Email).toLowerCase())) return true;
    return false;
};

/**
 * @description Check if value is numeric.
 * @param {*} value 
 */

export const isNumeric = (value) => {
    let regex = /^\d+$/s;
    return regex.test(value);
};

/**
 * @description Check if the email suffix is same between new and last email value.
 * @param {String} newEmail 
 * @param {String} lastEmail 
 */

export const checkSuffixEmail = (newEmail, lastEmail) => {
    return (newEmail.split("@")[1] === lastEmail.split("@")[1]);
};

/**
 * 
 * Calculate distance in meter between two coords.
 *
 * @param latitude
 * @param longitude
 * @param latitude_
 * @param longitude_
 * @returns {Number}
*/

export function calculateDistanceCoords(latitude, longitude, latitude_, longitude_) {
    const R = 6371e3; // meters
    const φ1 = latitude * Math.PI / 180;
    const φ2 = latitude_ * Math.PI / 180;
    const Δφ = (latitude_ - latitude) * Math.PI / 180;
    const Δλ = (longitude_ - longitude) * Math.PI / 180;
    const a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
            Math.cos(φ1) * Math.cos(φ2) *
            Math.sin(Δλ/2) * Math.sin(Δλ/2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    let result = R * c;
    return Math.round(result / 10) * 10;
}