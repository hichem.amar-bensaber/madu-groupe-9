import React, { useState, createContext } from "react";

const UserContext = createContext({});

export const UserContextProvider = ({ children, defaultUserToken, defaultUserData, defaultIsAuth }) => {
  const [userToken, setUserToken] = useState((defaultUserToken)? defaultUserToken: null);
  const [userData, setUserData] = useState((defaultUserData)? defaultUserData: null);

  return (
    <UserContext.Provider value={{ userToken, setUserToken, userData, setUserData }}>
      {children}
    </UserContext.Provider>
  );
};

export const UserContextConsumer = ChildComponent => props => (
  <UserContext.Consumer>
    {context => <ChildComponent {...props} context={context} />}
  </UserContext.Consumer>
);
