import React, { useState, createContext } from "react";

const FilterContext = createContext(null);
const store_type = {
  restaurants: {
    types: [
      "Classique",
      "Fast food",
      "Gastronomie",
      "Tacos",
      "Pâtisserie",
      "Pizza",
    ],
    filters: [
      "Junkfood",
      "Afro",
      "Asiatique",
      "Indienne",
      "Italienne",
      "Mexicaine",
      "Orientale",
      "Traditionnelle",
    ],
  },
  shopping: {
    types: ["Alimentaire", "Hygiène / beauté", "Mode", "Maison"],
    filters: [],
  },
  hobbies: {
    types: [
      "Dégustation",
      "Bien-être",
      "Atelier",
      "Sport",
      "Service quotidien",
      "Visite",
    ],
    filters: ["Sans Gluten", "Vegan", "Veggie"],
  },
};

export const FilterContextProvider = ({ children }) => {
  const [option, setOption] = useState({
    restaurants: {
      types: [""],
      filters: [""],
    },
    shopping: {
      types: [""],
      filters: [""],
    },
    hobbies: {
      types: [""],
      filters: [""],
    },
  });
  const [text, setText] = useState("");
  return (
    <FilterContext.Provider
      value={{ store_type, option, setOption, text, setText }}
    >
      {children}
    </FilterContext.Provider>
  );
};

export const FilterContextConsumer = (ChildComponent) => (props) => (
  <FilterContext.Consumer>
    {(ctx) => {
      return (
        <ChildComponent
          {...props}
          store_type={store_type}
          contextOption={[ctx.option, ctx.setOption]}
          contextText={[ctx.text, ctx.setText]}
        />
      );
    }}
  </FilterContext.Consumer>
);
