import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import * as React from "react";

import MapNavigation from "../navigation/MapNavigation";
import ChallengeNavigation from "../navigation/ChallengeNavigation";
import ListNavigation from "../navigation/ListNavigation";
import ProfilNavigation from "../navigation/ProfilNavigation";

import MapIcon from "../components/IconsBottomTabBar/MapIcon";
import ListIcon from "../components/IconsBottomTabBar/ListIcon";
import ChallengesIcon from "../components/IconsBottomTabBar/ChallengesIcon";
import ProfileIcon from "../components/IconsBottomTabBar/ProfileIcon";

import { FilterContextProvider } from "../context/FilterContext";

const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = "Map";
const MapNavigationWithFilters = (props) => (
  <FilterContextProvider>
    <MapNavigation {...props} />
  </FilterContextProvider>
);

const AppNavigation = ({ navigation, route }) => {
  let RouteIndex = route.state ? route.state.index : 0;

  return (
    <BottomTab.Navigator
      initialRouteName={INITIAL_ROUTE_NAME}
      headerMode="none"
      animationEnabled="true"
      swipeEnabled="true"
      tabBarPosition="bottom"
      tabBarOptions={{
        activeTintColor: "#9c74ff",
        inactiveTintColor: "#798090",
        keyboardHidesTabBar: false,
        labelStyle: {
          fontSize: 14,
        },
        tabStyle: {
          height: 65,
          paddingBottom: 20,
          backgroundColor: "#fff",
        },
        style: {
          height: 70,
          paddingTop: 15,
          paddingLeft: 15,
          paddingRight: 15,
          paddingBottom: 15,
          borderWidth: 0,
        },
      }}
    >
      <BottomTab.Screen
        name="Map"
        component={MapNavigationWithFilters}
        options={{
          title: "Carte",
          tabBarIcon: () => (
            <MapIcon selected={RouteIndex === 0 ? true : false} />
          ),
        }}
      />
      <BottomTab.Screen
        name="List"
        component={ListNavigation}
        options={{
          title: "Liste",
          tabBarIcon: () => (
            <ListIcon selected={RouteIndex === 1 ? true : false} />
          ),
        }}
      />
      <BottomTab.Screen
        name="Challenges"
        component={ChallengeNavigation}
        options={{
          title: "Défis",
          tabBarIcon: () => (
            <ChallengesIcon selected={RouteIndex === 2 ? true : false} notification={0}/>
          ),
        }}
      />
      <BottomTab.Screen
        name="Profile"
        component={ProfilNavigation}
        options={{
          title: "Profil",
          tabBarIcon: () => (
            <ProfileIcon selected={RouteIndex === 3 ? true : false} />
          ),
        }}
      />
    </BottomTab.Navigator>
  );
};

export default AppNavigation;
