import { createStackNavigator } from "@react-navigation/stack";
import React from "react";

import ListScreen from "../screens/ListScreen/ListScreen";
import PoiScreen from "../screens/PoiScreen/PoiScreen";

const Stack = createStackNavigator();
const INITIAL_ROUTE_NAME = "Main";

export default function MapNavigation({ navigation, route }) {
  return (
    <Stack.Navigator
      initialRouteName={INITIAL_ROUTE_NAME}
      headerMode="none"
      gesturesEnabled="true"
    >
      <Stack.Screen name="Main" component={ListScreen} />
      <Stack.Screen name="POI" mode="modal" component={PoiScreen} />
    </Stack.Navigator>
  );
}
