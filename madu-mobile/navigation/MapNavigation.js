import { createStackNavigator } from "@react-navigation/stack";
import React, { useState, useEffect } from "react";
import { View } from "react-native";
import MapScreen from "../screens/MapScreen";
import PoiScreen from "../screens/PoiScreen/PoiScreen";
import FilterScreen from "../screens/FilterScreen/FilterScreen";
import FilterDetail from "../screens/FilterDetail/FilterDetail";
import Input from "../components/Input/Input";

const Stack = createStackNavigator();
const INITIAL_ROUTE_NAME = "Main";

export default function MapNavigation(props) {
  return (
    <Stack.Navigator
      initialRouteName={INITIAL_ROUTE_NAME}
      headerMode="none"
      gesturesEnabled="true"
    >
      <Stack.Screen name="Main" component={MapScreen} />
      <Stack.Screen name="Filter" mode="modal" component={FilterScreen} />
      <Stack.Screen
        name="FilterRestaurants"
        mode="modal"
        component={FilterDetail}
      />
      <Stack.Screen
        name="FilterHobbies"
        mode="modal"
        component={FilterDetail}
      />
      <Stack.Screen
        name="FilterShopping"
        mode="modal"
        component={FilterDetail}
      />
      <Stack.Screen name="POI" mode="modal" component={PoiScreen} />
    </Stack.Navigator>
  );
}
