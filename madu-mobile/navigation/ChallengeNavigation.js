import { createStackNavigator } from "@react-navigation/stack";
import React from "react";

import ChallengesScreen from "../screens/ChallengesScreen/ChallengesScreen";
import ModalChallenge from "../screens/ModalChallenge/ModalChallenge";
import ModalQuizz from "../screens/ModalQuizz/ModalQuizz";
import RewardScreen from "../screens/RewardScreen/RewardScreen";
const Stack = createStackNavigator();
const INITIAL_ROUTE_NAME = "Main";

export default function MapNavigation({ navigation, route }) {
  return (
    <Stack.Navigator
      initialRouteName={INITIAL_ROUTE_NAME}
      headerMode="none"
      gesturesEnabled="true"
    >
      <Stack.Screen name="Main" component={ChallengesScreen} />
      <Stack.Screen name="Challenge" mode="modal" component={ModalChallenge} />
      <Stack.Screen name="Reward" mode="modal" component={RewardScreen} />
      <Stack.Screen name="Quizz" mode="modal" component={ModalQuizz} />
    </Stack.Navigator>
  );
}
