import { createStackNavigator } from "@react-navigation/stack";
import React from "react";

import LoginScreen from "../screens/LoginScreen/LoginScreen";
import CheckScreen from "../screens/LoginScreen/CheckScreen";
import RegisterScreen from "../screens/LoginScreen/RegisterScreen";

const Stack = createStackNavigator();
const INITIAL_ROUTE_NAME = "Main";

export default function LoginNavigation({ navigation, route }) {
  return (
    <Stack.Navigator
      initialRouteName={INITIAL_ROUTE_NAME}
      headerMode="none"
      animationEnabled="true"
      swipeEnabled="false"
      gesturesEnabled="false"
    >
      <Stack.Screen name="Main" component={LoginScreen} />
      <Stack.Screen name="Check" mode="modal" component={CheckScreen} />
      <Stack.Screen name="Register" mode="modal" component={RegisterScreen} />
    </Stack.Navigator>
  );
}
