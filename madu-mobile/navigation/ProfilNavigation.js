import { createStackNavigator } from "@react-navigation/stack";
import React from "react";

import ProfilScreen from "../screens/ProfileScreen";
import RewardScreen from "../screens/RewardScreen/RewardScreen";
import ModifyUserScreen from "../screens/ProfileScreen/ModifyUserScreen";

const Stack = createStackNavigator();
const INITIAL_ROUTE_NAME = "Main";

export default function MapNavigation({ navigation, route }) {
  return (
    <Stack.Navigator
      initialRouteName={INITIAL_ROUTE_NAME}
      headerMode="none"
      gesturesEnabled="true"
    >
      <Stack.Screen name="Main" component={ProfilScreen} />
      <Stack.Screen name="Reward" mode="modal" component={RewardScreen} />
      <Stack.Screen name="Modify" mode="modal" component={ModifyUserScreen} />
    </Stack.Navigator>
  );
}
