import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    Root: {
      path: 'root',
      screens: {
        Map: 'Map',
        List: 'List',
        Challenges: 'Challenges',
        Profile: 'Profile',
      },
    },
  },
};
