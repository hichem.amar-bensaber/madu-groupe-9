import React, { useState, useEffect } from "react";
import * as SplashScreen from "expo-splash-screen";
import { setData, getData, deleteData } from "../utils/LocalStorage";
import { Platform } from "react-native";

import { checkUserToken } from "../utils/API";

const useCachedResources = () => {

  const [isLoadingComplete, setLoadingComplete] = useState(false);
  const [isAuth, setAuth] = useState(false);

  const [userToken, setUserToken] = useState(null);
  const [userData, setUserData] = useState(null);
  

  // Load any resources or data that we need prior to rendering the app
  useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        // Show Splashscreen
        SplashScreen.preventAutoHideAsync();

        // Load user token, auto authentification system.
        let token = await getData("userToken");
        if(token) {
          console.log("Token found in device : " + token);

          let fetchAPI = await checkUserToken(token);
          if(fetchAPI.success) {
            console.log("Token is valid, redirection to app..");
            setUserToken(token);
            setUserData(fetchAPI.data);
            await setData("userData", JSON.stringify(fetchAPI.data));
            setAuth(true);
          }
          else {
            await deleteData("userToken");
            await deleteData("userData");
            console.log("Token is not valid, redirection to login..");
            setAuth(false);
          }
        }
        else {
          console.log("Token not found, redirection to login..");
          setAuth(false);
        }
      }
      catch(error) {
        console.warn(error);
      }
      finally {
        setLoadingComplete(true);
        SplashScreen.hideAsync();
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  return [isLoadingComplete, isAuth, userToken, userData];
}

export default useCachedResources;