import React from "react";
import { ScrollView } from "react-native-gesture-handler";

import { View, Text, TouchableOpacity } from "react-native";
import Title from "../../components/Title/Title";
import styled from "./filterdetails.scss";
import FilterHeader from "../../components/FilterHeader/FilterHeader";
import FilterButton from "../../components/FilterButton/FilterButton";
const title = {
  restaurants: "Restaurants",
  shopping: "Boutiques",
  hobbies: "Services / Loisirs",
};
const store_type = {
  restaurants: {
    types: [
      "Classique",
      "Fast food",
      "Gastronomie",
      "Tacos",
      "Pâtisserie",
      "Pizza",
    ],
    filters: [
      "Junkfood",
      "Afro",
      "Asiatique",
      "Indienne",
      "Italienne",
      "Mexicaine",
      "Orientale",
      "Traditionnelle",
    ],
  },
  shopping: {
    types: ["Alimentaire", "Hygiène / beauté", "Mode", "Maison"],
    filters: [],
  },
  hobbies: {
    types: [
      "Dégustation",
      "Bien-être",
      "Atelier",
      "Sport",
      "Service quotidien",
      "Visite",
    ],
    filters: ["Sans Gluten", "Vegan", "Veggie"],
  },
};
const FilterDetail = ({ navigation, route, contextOption }) => {
  const { option, type } = route.params;

  const types = store_type[type].types;
  const filters = store_type[type].filters;

  const optionListTypes = option[type].types;
  const optionListFilters = option[type].filters;
  const currentTitle = title[type];

  const isInTypes = (element) => optionListTypes.find((el) => element === el);
  const isInFilters = (element) =>
    optionListFilters.find((el) => element === el);
  const OptionWithNewData = {
    ...option,
    [type]: {
      types: optionListTypes,
      filters: optionListFilters,
    },
  };
  const clearTypes = () => {
    navigation.setParams({
      option: {
        ...option,
        [type]: {
          types: [],
          filters: optionListFilters,
        },
      },
      type,
    });
  };
  const clearFilters = () => {
    navigation.setParams({
      option: {
        ...option,
        [type]: {
          types: optionListTypes,
          filters: [],
        },
      },
      type,
    });
  };

  const setType = (element) => {
    let types = [];
    if (isInTypes(element)) {
      types = optionListTypes.filter((el) => el !== element);
    } else {
      types = [...optionListTypes, element];
    }
    navigation.setParams(
      {
        option: {
          ...option,
          [type]: {
            types,
            filters: optionListFilters,
          },
        },
      },
      type
    );
  };

  const setFilter = (element) => {
    let filters = [];
    if (isInFilters(element)) {
      filters = optionListFilters.filter((el) => el !== element);
    } else {
      filters = [...optionListFilters, element];
    }
    navigation.setParams({
      option: {
        ...option,
        [type]: {
          types: optionListTypes,
          filters,
        },
      },
      type,
    });
  };
  const TypesList = (element, key) => (
    <FilterButton
      key={key}
      onPress={() => setType(element)}
      active={isInTypes(element)}
      style={styled["filterdetails-section-list-item"]}
    >
      {element}
    </FilterButton>
  );
  const FiltersList = (element, key) => (
    <FilterButton
      key={key}
      onPress={() => setFilter(element)}
      active={isInFilters(element)}
      style={styled["filterdetails-section-list-item"]}
    >
      {element}
    </FilterButton>
  );

  return (
    <View>
      <ScrollView style={styled["filterdetails"]}>
        <FilterHeader
          onSave={() =>
            navigation.navigate("Main", { option: { ...OptionWithNewData } })
          }
          onBack={() => navigation.navigate("Filter", { ...OptionWithNewData })}
        />
        <Title>{currentTitle}</Title>
        <View style={styled["filterdetails-section"]}>
          <View style={styled["filterdetails-section-top"]}>
            <Title size="small">Type</Title>
            <TouchableOpacity onPress={clearTypes}>
              <Title size="small">Effacer tous les filters</Title>
            </TouchableOpacity>
          </View>
          <View style={styled["filterdetails-section-list"]}>
            {types.map(TypesList)}
            <View style={styled["filterdetails-section-list-item"]}></View>
          </View>
        </View>
        {filters.length !== 0 && (
          <View style={styled["filterdetails-section"]}>
            <View style={styled["filterdetails-section-top"]}>
              <Title size="small">Filtres</Title>
              <TouchableOpacity onPress={clearFilters}>
                <Title size="small">Effacer tous les filters</Title>
              </TouchableOpacity>
            </View>
            <View style={styled["filterdetails-section-list"]}>
              {filters.map(FiltersList)}
              <View style={styled["filterdetails-section-list-item"]}></View>
            </View>
          </View>
        )}
      </ScrollView>
    </View>
  );
};

export default FilterDetail;
