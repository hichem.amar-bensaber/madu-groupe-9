import React, { useState } from "react";
import { View, TouchableOpacity, Text } from "react-native";
import styled from "./modalquizz.scss";
import Title from "../../components/Title/Title";
import CloseIcon from "../../components/CloseIcon";
import { ScrollView } from "react-native-gesture-handler";
import ButtonQuizz from "../../components/ButtonQuizz/ButtonQuizz";
import { UserContextConsumer } from "../../context/UserContext";

const ModalQuizz = ({ navigation, route, context }) => {
  const [select, setSelect] = useState({ key: null, text: null, value: null });
  const { key, quizz, answers, question, name, points } = route.params;
  const isOver = question.trim() !== "";

  const { userData, setUserData } = context;

  const responseButton = answers.find((quizz) => quizz.is_answer);

  const onValidate = () => {
    const newQuestion = select.text;

    const newQuizz = quizz.map((el, id) => {
      if (id === key) {
        return {
          ...el,
          list: [{ answers, points, question: newQuestion }],
        };
      }
      return { ...el };
    });

    if (newQuestion === responseButton.answer) {
      const xp = userData.xp + points;
      setUserData({ ...userData, xp });
    }
    navigation.setParams({
      quizz: newQuizz,
      question: newQuestion,
    });
  };
  const next = () => {
    const nextIndex = key + 1;
    navigation.navigate("Quizz", {
      key: nextIndex,
      quizz,
      answers: quizz[nextIndex].answers,
      question: quizz[nextIndex].question,
    });
  };

  const disabledNext = key === quizz.length - 1;

  const quizzSelect = answers.map((quizz, key) => {
    if (key === select.key) {
      return {
        ...quizz,
        selected: true,
      };
    } else {
      return {
        ...quizz,
        selected: false,
      };
    }
  });

  const the_bad_response = answers.find((quizz) => quizz.answer)
    .bad_answer_text;
  const validateDisabled = typeof select.text !== "string";

  return (
    <View style={styled["modalquizz"]}>
      <TouchableOpacity
        style={styled["modalquizz-back"]}
        onPress={() =>
          navigation.navigate("Main", {
            quizz,
          })
        }
      >
        <CloseIcon />
      </TouchableOpacity>
      <ScrollView>
        <View style={styled["modalquizz-header"]}>
          <Title color="pink">Quizz</Title>
          <Text style={styled["modalquizz-header-pts"]}>{points} pts</Text>
        </View>
        <Text style={styled["modalquizz-question"]}>{name}</Text>

        {!isOver ? (
          quizzSelect.map((quizz, key) => (
            <ButtonQuizz
              key={key}
              selected={quizz.selected}
              onPress={() =>
                setSelect({
                  text: quizz.answer,
                  value: quizz.is_answer,
                  key,
                })
              }
              style={{ marginBottom: 16 }}
            >
              {quizz.answer}
            </ButtonQuizz>
          ))
        ) : (
          <View>
            <ButtonQuizz
              style={{ marginBottom: 16 }}
              color="correct"
              selected={true}
              onPress={() => null}
            >
              {responseButton.answer}
            </ButtonQuizz>
            {question === responseButton.answer ? (
              <Text style={styled["modalquizz-correct"]}>Bonne Réponse</Text>
            ) : (
              <View>
                <Text style={styled["modalquizz-incorrect"]}>
                  Mauvaise Réponse
                </Text>
                <Text>{the_bad_response}</Text>
              </View>
            )}
          </View>
        )}
      </ScrollView>
      {!isOver ? (
        <ButtonQuizz
          color="validate"
          disabled={validateDisabled}
          onPress={onValidate}
          style={styled["modalquizz-validate"]}
        >
          Valider
        </ButtonQuizz>
      ) : (
        <ButtonQuizz
          color="validate"
          disabled={disabledNext}
          onPress={next}
          style={styled["modalquizz-validate"]}
        >
          Quizz Suivante
        </ButtonQuizz>
      )}
    </View>
  );
};

export default UserContextConsumer(ModalQuizz);
