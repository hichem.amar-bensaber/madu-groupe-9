import React, { useState } from "react";
import {
  Platform,
  StyleSheet,
  View,
} from "react-native";
import { Updates } from "expo";
import { ScrollView } from "react-native-gesture-handler";
import LayoutProfil from "../components/LayoutProfil/LayoutProfil";
import ProfilNameLvl from "../components/ProfilNameLvL/ProfilNameLvl";
import ImagePicker from "../components/ImagePicker/ImagePicker";
import ItemSpace from "../components/ItemSpacer/ItemSpacer";
import { UserContextConsumer } from "../context/UserContext";
import { deleteData } from "../utils/LocalStorage";

const ProfileScreen = ({ navigation, route, context }) => {

  const { _id, first_name, last_name, email, type, profile_pic } = context.userData;

  const userToken = context.userToken;

  const URL_API_DEV = Platform.OS === "android" ? "http://10.0.2.2:8080" : "http://127.0.0.1:8080";
  const URL_LOCAL_API_DEV = Platform.OS === "android" ? "192.168.1.37:8080" : "192.168.1.37:8080"; // 192.168.1.96 => Fetch local network IP API

  const image = `${URL_LOCAL_API_DEV}/${profile_pic}`;
  const username = `${first_name} ${last_name}`;

  const links = [{ name: "Mes informations", value: "Modify", args: {
    id: _id,
    token: userToken,
    first_name: first_name,
    last_name: last_name,
    email: email
  } }, { name: "Mes récompenses", value: "Reward", args: {} }];

  const Disconnect = async () => {
    await deleteData("userToken");
    await deleteData("userData");
    Updates.reload();
  };

  return (
    <View style={styles.container}>
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
      >
        <LayoutProfil level={(type)? type.toLowerCase(): "Novice"}>
          <View style={styles.user}>
            <ImagePicker
              url={image}
              data={{_id, userToken}}
            />
            <ProfilNameLvl name={username} level={(type)? type.toLowerCase(): "Novice"} />
          </View>
        </LayoutProfil>
        <View>
          {links.map((link, index) => (
            <ItemSpace
              key={index}
              type="button"
              onPress={() => navigation.navigate(link.value, link.args)}
            >
              {link.name}
            </ItemSpace>
          ))}
          <ItemSpace
            color="alert"
            type="button"
            onPress={() => Disconnect()}
            style={{ marginBottom: 52 }}
          >
            Déconnexion
          </ItemSpace>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },

  text: {
    alignContent: "center",
  },
  user: {
    position: "relative",
    width: "100%",
    height: "100%",
    top: 60,
  },
});

export default UserContextConsumer(ProfileScreen);
