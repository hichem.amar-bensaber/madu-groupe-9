import React, { useState, useEffect } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Keyboard
} from "react-native";

import BackArrow from "../../components/BackArrow/BackArrowIcon";
import InputIcons from "../../components/InputIcons/InputIcons";
import LoginButton from "../../components/LoginButton/LoginButton";
import MaduLogo from "../../components/MaduLogo/MaduLogo";

import BottomNotification from "../../components/BottomNotification/BottomNotification";

import { emailValidator, isNullOrEmpty } from "../../utils/helpers";
import { register } from "../../utils/API";

export default function RegisterScreen({ navigation, route }) {

  let textValue = "Vous êtes éligible à l'aventure Madu, veuillez saisir vos informations pour créer votre compte.";
  const ID = (route.params.id)? route.params.id: null;
  const Company = (route.params.company)? route.params.company: null;
  if(Company) textValue = "Votre société " + Company + " est éligible à Madu, veuillez saisir vos informations pour créer votre compte.";

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [Email, setEmail] = useState((route.params.email)? route.params.email: "");
  const [Password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState(null);
  const [errorfirstName, setErrorFirstName] = useState(false);
  const [errorlastName, setErrorLastName] = useState(false);
  const [errorEmail, setErrorEmail] = useState(false);
  const [errorPassword, setErrorPassword] = useState(false);

  const [keyboardVisible, setKeyboardVisible] = useState(false);

  useEffect(() => {
    Keyboard.addListener("keyboardDidShow", () => setKeyboardVisible(true));
    Keyboard.addListener("keyboardDidHide", () => setKeyboardVisible(false));
  }, []);

  const formCheck = async () => {
    let formError = false;
    if(isNullOrEmpty(lastName)) {
      console.log("Empty lastName !");
      setErrorMessage("Veuillez saisir votre nom.");
      setErrorLastName(true);
      formError = true;
    }
    else setErrorLastName(false);

    if(isNullOrEmpty(firstName)) {
      console.log("Empty firstName !");
      setErrorMessage("Veuillez saisir votre prénom.");
      setErrorFirstName(true);
      formError = true;
    }
    else setErrorFirstName(false);

    let _Email = Email.toLowerCase().replace(/\s/g, "");
    if(isNullOrEmpty(Email)) {
      console.log("Empty email !");
      setErrorMessage("Veuillez saisir votre adresse email.");
      setErrorEmail(true);
      formError = true;
    }
    else if(!emailValidator(_Email)) {
      console.log("Email is not valid !");
      setErrorMessage("Veuillez saisir une adresse email valide.");
      setErrorEmail(true);
      formError = true;
    }
    else setErrorEmail(false);

    if(isNullOrEmpty(Password)) {
      console.log("Empty password !");
      setErrorMessage("Veuillez saisir votre mot de passe.");
      setErrorPassword(true);
      formError = true;
    }
    else if(Password.length < 6) {
      console.log("Password is too short < 6 !");
      setErrorMessage("Veuillez saisir un mot de passe sécurisé (minimum : 6 caractères).");
      setErrorPassword(true);
      formError = true;
    }
    else setErrorPassword(false);

    if(!formError) {
      let fetchAPI = await register(firstName, lastName, _Email, Password, ID);
      if(fetchAPI.success) {
        navigation.navigate("Main");
      }
      else {
        if(fetchAPI.errors.email.kind === "unique") {
          setErrorMessage("Cette adresse email est déjà liée à un compte Madu.");
        }
        else console.log(fetchAPI);
      }
    }
  }
  
  return (
    <View style={[styles.container, (keyboardVisible) ? { paddingBottom: 0 } : { paddingBottom: 50 }]}>
      <BackArrow type={"simple"} closeModal={() => navigation.goBack()} style={{ position: "absolute", top: 40, left: 10 }} />
      <View style={styles.imageContainer}>
        <MaduLogo />
      </View>
      <Text style={[styles.text, (keyboardVisible)? { display: "none" }: { display: "flex" }]}>
        { textValue }
      </Text>
      <InputIcons type={"user"} placeholder={"Nom"} defaultValue={lastName} onChange={(value) => setLastName(value)} error={errorlastName}/>
      <InputIcons type={"user"} placeholder={"Prénom"} defaultValue={firstName} onChange={(value) => setFirstName(value)} error={errorfirstName}/>
      <InputIcons type={"email"} placeholder={"Email"} defaultValue={Email} onChange={(value) => setEmail(value)} error={errorEmail}/>
      <InputIcons type={"password"} placeholder={"Mot de passe"} defaultValue={Password} onChange={(value) => setPassword(value)} error={errorPassword}/>
      <LoginButton onPress={() => formCheck()} ButtonColor={"#93A0FF"} text={"Suivant"} textColor={"#FFFFFF"} />
      {(((errorEmail || errorPassword || lastName || firstName) && errorMessage !== null) || errorMessage !== null)? <BottomNotification text={errorMessage}/>: null }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#fff",
    paddingHorizontal: 30
  },
  imageContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    fontSize: 16,
    textAlign: "center",
    marginBottom: 25
  }
});
