import React, { useState, useEffect } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Keyboard
} from "react-native";

import BackArrow from "../../components/BackArrow/BackArrowIcon";
import InputIcons from "../../components/InputIcons/InputIcons";
import LoginButton from "../../components/LoginButton/LoginButton";
import MaduLogo from "../../components/MaduLogo/MaduLogo";

import BottomNotification from "../../components/BottomNotification/BottomNotification";

import { emailValidator, isNullOrEmpty } from "../../utils/helpers";
import { checkUserEmail } from "../../utils/API";

export default function RegisterScreen({ navigation, route }) {

  const [Email, setEmail] = useState("");
  const [errorMessage, setErrorMessage] = useState(null);
  const [errorEmail, setErrorEmail] = useState(false);
  const [keyboardVisible, setKeyboardVisible] = useState(false);

  useEffect(() => {
    Keyboard.addListener("keyboardDidShow", () => setKeyboardVisible(true));
    Keyboard.addListener("keyboardDidHide", () => setKeyboardVisible(false));
  }, []);

  const formCheck = async () => {
    let formError = false;
    let _Email = Email.toLowerCase().replace(/\s/g, "");
    if(isNullOrEmpty(Email)) {
      console.log("Empty email !");
      setErrorMessage("Veuillez saisir votre adresse email.");
      setErrorEmail(true);
      formError = true;
    }
    else if(!emailValidator(_Email)) {
      console.log("Email is not valid !");
      setErrorMessage("Veuillez saisir une adresse email valide.");
      setErrorEmail(true);
      formError = true;
    }
    else setErrorEmail(false);

    if(!formError) {
      let fetchAPI = await checkUserEmail(_Email);
      if(fetchAPI.success && fetchAPI.data && fetchAPI.data.name && fetchAPI.data._id) {
        navigation.navigate("Register", { company: fetchAPI.data.name, email: (_Email)? _Email: null, id: fetchAPI.data._id });
      }
      else {
        console.log(fetchAPI.message);
        setErrorMessage("Cette adresse email n'est pas éligible à l'aventure Madu.");
      }
    }
  }

  return (
    <View style={[styles.container, (keyboardVisible) ? { paddingBottom: 0 } : { paddingBottom: 100 }]}>
      <BackArrow type={"simple"} closeModal={() => navigation.goBack()} style={{ position: "absolute", top: 40, left: 10 }}/>
      <View style={styles.imageContainer}>
        <MaduLogo />
      </View>
      <Text style={[styles.text, (keyboardVisible)? { display: "none" }: { display: "flex" }]}>
        Veuillez saisir votre email d'entreprise, nous vérifions si vous êtes éligible à l'aventure Madu.
      </Text>
      <InputIcons type={"email"} placeholder={"Email"} defaultValue={Email} onChange={(value) => setEmail(value)} error={errorEmail} />
      <LoginButton onPress={() => formCheck()} ButtonColor={"#93A0FF"} text={"Suivant"} textColor={"#FFFFFF"} />
      {((errorEmail  && errorMessage !== null) || errorMessage !== null)? <BottomNotification text={errorMessage}/>: null }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#fff",
    paddingHorizontal: 30
  },
  imageContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    fontSize: 16,
    textAlign: "center",
    marginBottom: 25
  }
});
