import React, { useState, useEffect } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Keyboard
} from "react-native";

import { UserContextConsumer } from "../../context/UserContext";

import InputIcons from "../../components/InputIcons/InputIcons";
import LoginButton from "../../components/LoginButton/LoginButton";
import MaduLogo from "../../components/MaduLogo/MaduLogo";
import BottomNotification from "../../components/BottomNotification/BottomNotification";

import { emailValidator, isNullOrEmpty } from "../../utils/helpers";
import { login } from "../../utils/API";
import { setData } from "../../utils/LocalStorage";

const LoginScreen = ({ navigation, route, context }) => {

  const [Email, setEmail] = useState("admin");
  const [Password, setPassword] = useState("pass");
  const [errorMessage, setErrorMessage] = useState(null);
  const [errorEmail, setErrorEmail] = useState(false);
  const [errorPassword, setErrorPassword] = useState(false);
  const [keyboardVisible, setKeyboardVisible] = useState(false);

  const { setUserToken, setUserData } = context;

  useEffect(() => {
    Keyboard.addListener("keyboardDidShow", () => setKeyboardVisible(true));
    Keyboard.addListener("keyboardDidHide", () => setKeyboardVisible(false));
  }, []);

  const formCheck = async () => {
    let formError = false;
    let _Email = Email.toLowerCase().replace(/\s/g, "");
    if(isNullOrEmpty(Email)) {
      console.log("Empty email !");
      setErrorMessage("Veuillez saisir votre adresse email.");
      setErrorEmail(true);
      formError = true;
    }
    else if(!emailValidator(_Email)) {
      console.log("Email is not valid !");
      setErrorMessage("Veuillez saisir une adresse email valide.");
      setErrorEmail(true);
      formError = true;
    }
    else setErrorEmail(false);

    if(isNullOrEmpty(Password)) {
      console.log("Empty password !");
      setErrorMessage("Veuillez saisir votre mot de passe.");
      setErrorPassword(true);
      formError = true;
    }
    else setErrorPassword(false);

    // TODO PRDUCTION DELETE : CHEAT CODE MISSING PASSWORD
    if(Email === "admin" && Password === "pass") {
      return navigation.navigate("App");
    }

    if(!formError) {
      let fetchAPI = await login(_Email, Password);
      if(fetchAPI.success) {
        console.log("Successful authentification, redirection to app..");
        setUserToken(fetchAPI.token);
        setUserData(fetchAPI.data);
        await setData("userToken", fetchAPI.token);
        await setData("userData", JSON.stringify(fetchAPI.data));
        navigation.navigate("App");
      }
      else {
        console.log(fetchAPI);
        setErrorMessage("Adresse email / mot de passe incorrect.");
      }
    }
  }

  return (
    <View style={[styles.container, (keyboardVisible) ? { paddingBottom: 0 } : { paddingBottom: 50 }]}>
      <View style={styles.imageContainer}>
        <MaduLogo />
      </View>
      <Text style={[styles.text, (keyboardVisible)? { display: "none" }: { display: "flex" }]}>
        Veuillez saisir vos identifiants ou créer un compte pour commencer l'aventure Madu.
      </Text>
      <InputIcons type={"email"} placeholder={"Email"} defaultValue={Email} onChange={(value) => setEmail(value)} error={errorEmail} />
      <InputIcons type={"password"} placeholder={"Mot de passe"} defaultValue={Password} onChange={(value) => setPassword(value)} error={errorPassword} />
      <LoginButton onPress={() => formCheck()} text={"Se connecter"} textColor={"#FFFFFF"} />
      <LoginButton onPress={() => navigation.navigate("Check")} text={"S'inscrire"} textColor={"#455154"} type={"transparent"} />
      {(((errorEmail || errorPassword) && errorMessage !== null) || errorMessage !== null)? <BottomNotification text={errorMessage}/>: null }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#fff",
    paddingHorizontal: 30
  },
  imageContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    fontSize: 16,
    textAlign: "center",
    marginBottom: 25
  }
});

export default UserContextConsumer(LoginScreen);