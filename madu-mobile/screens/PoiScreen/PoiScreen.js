import React, { useState } from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import styled from "./poiscreen.scss";

import BackArrowIcon from '../../components/BackArrow/BackArrowIcon';
import PoiInfos from '../../components/PoiInfos/PoiInfos';
import GreenScore from '../../components/GreenScore/GreenScore';
import BadgeTag from '../../components/BadgeTag/BadgeTag';
import BadgeAccess from '../../components/BadgeAccess/BadgeAccess';
import IconArrow from '../../components/IconArrow/IconArrow';
import PoiLink from '../../components/PoiLlink/PoiLink';

import { calculateDistanceCoords } from "../../utils/helpers";

export default function PoiScreen({ navigation, route }) {
  const { dataGetPoi, userData } = route.params;
  const [showModal, setShowModal] = useState(false);

  const onPress = () => setShowModal(!showModal);

  const checkDay = (actual_day) => {
    switch (actual_day) {
      case 1:
        return dataGetPoi.open_hours.mon
      case 2:
        return dataGetPoi.open_hours.tue
      case 3:
        return dataGetPoi.open_hours.wed
      case 4:
        return dataGetPoi.open_hours.thu
      case 5:
        return dataGetPoi.open_hours.fri
      case 6:
        return dataGetPoi.open_hours.sat
      case 7:
        return dataGetPoi.open_hours.sun
      default:
        return "Aucun horaires trouvés"
    }
  }

  // const _splitPhone = (number) => {
  //   let phone = number;
  //   let newNumberPhone = phone.replace(/\\D/g, "") ;
  //   let resultPhone = `${newNumberPhone.substring(0, 2)}` + `${newNumberPhone.substring(2, 4)}` + `${newNumberPhone.substring(4, 6)}` + `${newNumberPhone.substring(6, 7)}` + `${newNumberPhone.substring(7, 8)}` + `${newNumberPhone.substring(8, 9)}` + `${newNumberPhone.substring(9, 10)}`;
  //   return resultPhone;
  // }
  // console.log(_splitPhone("0149309243"));
  
  return (
    <View style={styled["poiscreen-container"]}>
      <ScrollView>
        <View>
          <ScrollView horizontal>
            {/* {
            dataGetPoi.images.map((el, i ) => 
              <Image source={{uri: `http://localhost:8080/${el.uri}`}} alt="picture of poi" key={i} style={ styled["poiscreen-container-image"] } />
            )
          } */}
            <Image
              source={require("../../assets/images/PoiScreen/poiscreen-picture.jpg")}
              alt="picture of poi"
              style={styled["poiscreen-container-image"]}
            />
            <Image
              source={require("../../assets/images/PoiScreen/poiscreen-picture-02.jpg")}
              alt="picture of poi"
              style={styled["poiscreen-container-image"]}
            />
            <Image
              source={require("../../assets/images/PoiScreen/poiscreen-picture-03.jpg")}
              alt="picture of poi"
              style={styled["poiscreen-container-image"]}
            />
            <Image
              source={require("../../assets/images/PoiScreen/poiscreen-picture-04.jpg")}
              alt="picture of poi"
              style={styled["poiscreen-container-image"]}
            />
          </ScrollView>
        </View>
        <View>
          <View style={styled["poiscreen-about"]}>
            <TouchableOpacity style={styled["poiscreen-about_greenscore"]}>
              <GreenScore
                dataGreenScore={dataGetPoi.greenscore}
                dataGreenScoreDescription={dataGetPoi.greenscore_description}
              />
            </TouchableOpacity>
            <Text style={styled["poiscreen-about_title"]}>
              {dataGetPoi.name}
            </Text>
            <Text style={styled["poiscreen-about_distance"]}>
              {
                calculateDistanceCoords(
                  48.8517832,
                  2.4203658,
                  dataGetPoi.coords.latitude,
                  dataGetPoi.coords.longitude)
              }m
            </Text>

            <View style={styled["poiscreen-about_tag"]}>
              <BadgeTag tag={dataGetPoi.store_filters} />
            </View>

            <Text style={styled["poiscreen-about_title2"]}>Description :</Text>
            <Text style={styled["poiscreen-about_description"]}>
              {dataGetPoi.description}
            </Text>
          </View>

          <View style={styled["poiscreen-about_list"]}>
            <PoiInfos
              src={require("../../assets/images/PoiScreen/poiscreen-picto-access.png")}
            >
              <BadgeAccess dataHandicap={dataGetPoi.store_filters.handicap} />
            </PoiInfos>
          </View>
          <View style={styled["poiscreen-about_list"]}>
            <PoiInfos>
              <Text> {dataGetPoi.localisation.address}, </Text>
              <Text> {dataGetPoi.localisation.zip_code + ' ' + dataGetPoi.localisation.city} </Text>
            </PoiInfos>
          </View>

          <TouchableOpacity style={styled["poiscreen-about_list"]} onPress={onPress}>
              <PoiInfos 
                src={require('../../assets/images/PoiScreen/poiscreen-picto-time.png')}
              >
                <Text> {checkDay( new Date().getDay() ) } </Text>
                {
                  showModal ? (
                    <>
                    <View style={{ marginTop: 15, marginLeft: 15, flexDirection: 'row', flex: 1 }}>
                      <Text style={{ fontWeight: '500' }}> Lundi: </Text>
                      <Text> {dataGetPoi.open_hours.mon} </Text>
                    </View>
                    <View style={{ marginTop: 15, marginLeft: 15, flexDirection: 'row', flex: 1 }}>
                      <Text style={{ fontWeight: '500' }}> Mardi: </Text>
                      <Text> {dataGetPoi.open_hours.tue} </Text>
                    </View>
                    <View style={{ marginTop: 15, marginLeft: 15, flexDirection: 'row', flex: 1 }}>
                      <Text style={{ fontWeight: '500' }}> Mercredi: </Text>
                      <Text> {dataGetPoi.open_hours.wed} </Text>
                    </View>
                    <View style={{ marginTop: 15, marginLeft: 15, flexDirection: 'row', flex: 1 }}>
                      <Text style={{ fontWeight: '500' }}> Jeudi: </Text>
                      <Text> {dataGetPoi.open_hours.thu} </Text>
                    </View>
                    <View style={{ marginTop: 15, marginLeft: 15, flexDirection: 'row', flex: 1 }}>
                      <Text style={{ fontWeight: '500' }}> Vendredi: </Text>
                      <Text> {dataGetPoi.open_hours.fri} </Text>
                    </View>
                    <View style={{ marginTop: 15, marginLeft: 15, flexDirection: 'row', flex: 1 }}>
                      <Text style={{ fontWeight: '500' }}> Samedi: </Text>
                      <Text> {dataGetPoi.open_hours.sat} </Text>
                    </View>
                    <View style={{ marginTop: 15, marginLeft: 15, flexDirection: 'row', flex: 1 }}>
                      <Text style={{ fontWeight: '500', flex: 0.7 }}> Dimanche: </Text>
                      <Text > {dataGetPoi.open_hours.sun} </Text>
                    </View>
                    </>
                  ) : null
                }
              </PoiInfos>
              <View style={styled["poiscreen-about_list_icon"]}>
                <IconArrow />
              </View>
          </TouchableOpacity>
          <View style={styled["poiscreen-about_list"]}>
              <PoiInfos 
                src={require('../../assets/images/PoiScreen/poiscreen-picto-phone.png')}
              >
                <PoiLink href={`tel:+${dataGetPoi.phone.slice(1)}`}> {dataGetPoi.phone.replace(/(\d{3})\D?(\d{4})\D?(\d{4})/,"$1 $2 $3")} </PoiLink>
              </PoiInfos>
          </View>
          <View style={styled["poiscreen-about_list"]}>
            <PoiInfos
              src={require("../../assets/images/PoiScreen/poiscreen-picto-euro.png")}
            >
              <Text>
                {" "}
                {`Prix moyen ${dataGetPoi.store_filters.price_average}`}{" "}
              </Text>
            </PoiInfos>
          </View>
          <View style={styled["poiscreen-about_list"]}>
              <PoiInfos 
                src={require('../../assets/images/PoiScreen/poiscreen-picto-website.png')}>
                <PoiLink href={dataGetPoi.web_site}> {dataGetPoi.web_site} </PoiLink>
              </PoiInfos>
          </View>
        </View>
      </ScrollView>
      <BackArrowIcon closeModal={() => navigation.goBack()} />
    </View>
  );
}
