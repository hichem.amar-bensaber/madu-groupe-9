import React from "react";
import { View, TouchableOpacity, Text } from "react-native";
import CloseIcon from "../../components/CloseIcon";
import styled from "./modalchallenge.scss";
import Title from "../../components/Title/Title";
import { ScrollView } from "react-native-gesture-handler";

const ModalChallenge = ({ navigation, route }) => {
  return (
    <View>
      <ScrollView style={styled["modalchallenge"]}>
        <TouchableOpacity
          style={styled["modalchallenge-back"]}
          onPress={() => navigation.goBack()}
        >
          <CloseIcon />
        </TouchableOpacity>
        <View style={styled["modalchallenge-header"]}>
          <Title color="blue">Défi</Title>
          <Text style={styled["modalchallenge-header-pts"]}>200 pts</Text>
        </View>
        <Text style={styled["modalchallenge-title"]}>
          Changeons de moteur de recherche.
        </Text>
        <Text style={styled["modalchallenge-subtitle"]}>Le saviez-vous ?</Text>
        <Text style={styled["modalchallenge-content"]}>
          Deux requêtes dans Google sont équivalentes à l'énergie nécessaire
          pour faire bouillir l'eau d'une bouilloire. 200 millions sont
          effectuées par jour.
        </Text>
        <Text style={styled["modalchallenge-subtitle"]}>Les solutions.</Text>
        <Text style={{ ...styled["modalchallenge-content"], marginBottom: 64 }}>
          Il existe bon nombre d’alternative à Google.Nous pouvons parler de
          moteur écologique. Alors de quoi s’agit-il ? Comme son nom l’indique
          il est impliqué dans l’écologie.Ses revenus publicitaires servent en
          grande partie à financer des associations écologiques par exemple, et
          à terme cela conduit à des actions favorables pour l’environnement
          comme planter des arbres. En utilisant ce type de moteur de recherche,
          vous participez donc à de bonnes actions à travers le monde. Une
          manière pour l’utilisateur de compenser de son empreinte carbone et
          son impact sur l’environnement.
        </Text>
      </ScrollView>
    </View>
  );
};

export default ModalChallenge;
