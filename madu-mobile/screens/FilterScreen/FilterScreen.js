import React, { useEffect } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import styled from "./filterscreen.scss";
import RightArrow from "../../components/RightArrowIcon";
import BackArrow from "../../components/BackArrow/BackArrowIcon";
import FilterHeader from "../../components/FilterHeader/FilterHeader";

const FilterScreen = ({ navigation, route, contextOption }) => {
  //const [option, setOption] = contextOption;
  const listLinks = [
    { name: "Restaurants", value: "Restaurants" },
    { name: "Boutiques", value: "Shopping" },
    { name: "Services Loisirs", value: "Hobbies" },
  ];

  const option = route.params;
  return (
    <View style={styled["filterscreen"]}>
      <FilterHeader
        onSave={() => navigation.navigate("Filter", route.params.option)}
        onBack={() => navigation.goBack()}
      />
      {listLinks.map((link, key) => {
        const type = link.value.toLowerCase();
        return (
          <TouchableOpacity
            key={key}
            onPress={() =>
              navigation.navigate(`Filter${link.value}`, {
                type,
                option: option,
              })
            }
          >
            <View style={styled["filterscreen-btn"]}>
              <Text>{link.name}</Text>
              <RightArrow color="black" />
            </View>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export default FilterScreen;
