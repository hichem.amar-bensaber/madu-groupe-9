import React, {useState, useEffect} from 'react';
import { SafeAreaView, FlatList, StyleSheet, Text, View } from 'react-native';
import styled from './listscreen.scss';
import PoiList from '../../components/PoiList/PoiList'
import {getPoi} from '../../utils/api_static_poi'
import { listStores } from '../../utils/API';

import PopularIcon from "./PopularIcon";
import GreenScoreIcon from "./GreenScoreIcon";
import ClosestIcon from "./ClosestIcon";
import ToggleIcon from "./ToggleIcon";

import { UserContextConsumer } from "../../context/UserContext";

// import {calculateDistanceCoords} from '../../utils/helpers';

const ListScreen = ({ navigation, route, context }) => {
  const [poiData, setPoiData] = useState([]);
  const { userToken, userData } = context;
  const [poiFilter, setPoiFilter] = useState([
    { filter: "popular", text: "Les plus populaires", "active": true },
    { filter: "greenscore", text: "Greenscore", "active": false },
    { filter: "popular", text: "Autour de moi", "active": false }
  ]);

  useEffect(() => {
    const asyncFunction = async () => {
      let data = await listStores(userToken);
      if(data) {
        console.log("App receive POI data..");
        setPoiData(data);
      }
      else {
        console.log("App not receive POI data, use static data..");
        setPoiData(getPoi);
      }
    }
    asyncFunction();
  }, []);

  const renderIcon = (value) => {
    if(value.filter === "popular") return (<PopularIcon selected={value.active} />);
    else if(value.filter === "closest") return (<ClosestIcon selected={value.active} />);
    else if(value.filter === "greenscore") return (<GreenScoreIcon selected={value.active} />);
  }

  const filterPoi = () => {
    return poiData;
  }

  return (
      <SafeAreaView style={styled["listscreen"]}>
          <FlatList
            data={filterPoi(poiData)}
            renderItem={({ item }) => 
              <PoiList dataGetPoi={item} onClick={()=>navigation.navigate("POI", { dataGetPoi: item, userData: userData})} userData={userData}/>
            }
            keyExtractor={item => item._id}
          />
      </SafeAreaView>
  );
}

const styles = StyleSheet.create({
	filterContainer: {
    position: "relative",
    flex: 1,
    flexDirection: "row",
    width: "100%",
    height: 60,
    paddingHorizontal: 30,
    marginVertical: 25
  },
  iconContainer: {
    position: "absolute",
    top: 0,
    left: 25,
    width: 50,
    height: 50
  },
  text: {
    height: 50,
    fontSize: 16,
    paddingLeft: 30
  },
  toggleContainer: {
    position: "absolute",
    top: 0,
    right: 0,
    width: 50,
    height: 50,
  }
});

export default UserContextConsumer(ListScreen);
