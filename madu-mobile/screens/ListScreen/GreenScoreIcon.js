import React from "react"
import { SvgCss } from "react-native-svg"

export default function GreenScoreIcon({ selected=false }) {

    let xml = null;

    if(selected) {
        xml = `
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M3.77637 10.9574C3.77637 8.21952 5.99589 6 8.7338 6C10.0378 6 11.2241 6.50344 12.1092 7.32656C12.9942 6.50344 14.1806 6 15.4845 6C18.2224 6 20.442 8.21952 20.442 10.9574V18.001H17.1368V10.7498C17.1368 9.83718 16.397 9.09734 15.4844 9.09734C14.5717 9.09734 13.8319 9.83718 13.8319 10.7498V18.001H13.6912H10.5271H10.3863V10.7498C10.3863 9.83718 9.64651 9.09734 8.73387 9.09734C7.82123 9.09734 7.08139 9.83718 7.08139 10.7498V18.001H3.77637V10.9574Z" fill="url(#paint0_radial)"/>
                <defs>
                    <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(18.5553 16.0842) rotate(166.889) scale(10.6544 36.2519)">
                        <stop stop-color="#7A8AFF"/>
                        <stop offset="1" stop-color="#B16BF2"/>
                    </radialGradient>
                </defs>
            </svg>        
        `;
    }
    else {
        xml = `
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path opacity="0.9" fill-rule="evenodd" clip-rule="evenodd" d="M8.2875 6C5.36729 6 3 8.36729 3 11.2875V18H6.52503V10.8592C6.52503 9.88578 7.31412 9.09668 8.28753 9.09668C9.26093 9.09668 10.05 9.88578 10.05 10.8592V18H10.05H13.5749H13.575V11.2875L13.5749 11.2631V10.8592C13.5749 9.88578 14.364 9.09668 15.3374 9.09668C16.3108 9.09668 17.0999 9.88578 17.0999 10.8592V18H20.625V11.2875C20.625 8.36729 18.2577 6 15.3375 6C13.9834 6 12.7482 6.50903 11.8127 7.34619C11.8127 7.34612 11.8126 7.34605 11.8125 7.34598C11.8124 7.34605 11.8124 7.34612 11.8123 7.34618C10.8769 6.50903 9.64163 6 8.2875 6Z" fill="black"/>
            </svg>            
        `;
    }

    return (
        <SvgCss xml={xml}/>
    );
}