import React from "react"
import { SvgCss } from "react-native-svg"

export default function PopularIcon({ selected=false }) {

    let xml = null;

    if(selected) {
        xml = `
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M12 17.27L18.18 21L16.54 13.97L22 9.24L14.81 8.63L12 2L9.19 8.63L2 9.24L7.46 13.97L5.82 21L12 17.27Z" fill="url(#paint0_radial)"/>
                <defs>
                    <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(19.7359 17.9653) rotate(162.919) scale(13.0274 56.331)">
                        <stop stop-color="#7A8AFF"/>
                        <stop offset="1" stop-color="#B16BF2"/>
                    </radialGradient>
                </defs>
            </svg>
        `;
    }
    else {
        xml = `
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M12 17.27L18.18 21L16.54 13.97L22 9.24L14.81 8.63L12 2L9.19 8.63L2 9.24L7.46 13.97L5.82 21L12 17.27Z" fill="black" fill-opacity="0.87"/>
            </svg>        
        `;
    }

    return (
        <SvgCss xml={xml}/>
    );
}