import React from "react"
import { SvgCss } from "react-native-svg"

export default function ClosestIcon({ selected=false }) {

    let xml = null;

    if(selected) {
        xml = `
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect width="24" height="24" fill="url(#paint0_radial)"/>
                <defs>
                    <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(19.7359 17.9653) rotate(162.919) scale(13.0274 56.331)">
                        <stop stop-color="#7A8AFF"/>
                        <stop offset="1" stop-color="#B16BF2"/>
                    </radialGradient>
                </defs>
            </svg>
        `;
    }
    else {
        xml = `
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect width="24" height="24" fill="white"/>
            </svg>               
        `;
    }

    return (
        <SvgCss xml={xml}/>
    );
}