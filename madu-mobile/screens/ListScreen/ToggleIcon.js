import React from "react"
import { SvgCss } from "react-native-svg"

export default function ToggleIcon({ selected=false }) {

    let xml = null;

    if(selected) {
        xml = `
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M12 8L6 14L7.41 15.41L12 10.83L16.59 15.41L18 14L12 8Z" fill="black" fill-opacity="0.87"/>
            </svg>
        `;
    }
    else {
        xml = `
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M12 8L6 14L7.41 15.41L12 10.83L16.59 15.41L18 14L12 8Z" fill="black" fill-opacity="0.87"/>
            </svg>             
        `;
    }

    return (
        <SvgCss xml={xml}/>
    );
}