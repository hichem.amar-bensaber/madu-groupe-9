import React, { useState, useEffect } from "react";
import { View, Text } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import ChallengeLayout from "../../components/ChallengeLayout/ChallengeLayout";
import ProgressText from "../../components/ProgressText/ProgressText";
import LevelingText from "../../components/LevelingText/LevelingText";
import Button from "../../components/Button/Button";
import ChallengeCounter from "../../components/ChallengeCounter/ChallengeCounter";
import styled from "./challengescreen.scss";
import Divider from "../../components/Divider/Divider";
import Title from "../../components/Title/Title";
import ButtonChallenge from "../../components/ButtonChallenge/ButtonChallenge";
import { UserContextConsumer } from "../../context/UserContext";
import { getQuizzesList } from "../../utils/API";
import Loader from "../../components/Loader/Loader";

const ChallengesScreen = ({ navigation, route, context }) => {
  const { userToken } = context;
  const [quizz, setQuizz] = useState([]);
  const { userData } = context;
  useEffect(() => {
    const asyncFunction = async () => {
      const data = await getQuizzesList(userToken);
      if (data) {
        setQuizz(data.data);
      } else {
        setQuizz([]);
      }
    };
    asyncFunction();
  }, []);

  useEffect(() => {
    //const result = route.params
    if (route.params?.quizz) {
      setQuizz(route.params.quizz);
    }
  }, [route.params]);

  if (quizz.length === 0) {
    return <Loader />;
  } else {
    return (
      <View style={styled["challengescreen"]}>
        <ScrollView>
          <ChallengeLayout color="green">
            <View style={styled["challengescreen-header"]}>
              <View>
                <LevelingText
                  level={userData.type ? userData.type.toLowerCase() : "Novice"}
                />
                <ProgressText max={userData.type === "Novice" ? 300 : 1000}>
                  {userData.xp}
                </ProgressText>
              </View>
              <Button onPress={() => navigation.navigate("Reward")}>
                Récompense 🎖️
              </Button>
            </View>
          </ChallengeLayout>
          <View style={styled["challengescreen-list"]}>
            <Title color="blue" style={styled["challengescreen-title"]}>
              Défis
            </Title>
            <ButtonChallenge
              onPress={() => navigation.navigate("Challenge")}
              color="blue"
              title="Défi numéro 1"
              dayLeft="4"
              points="500"
            />
            <Title color="pink" style={styled["challengescreen-title"]}>
              Quizz
            </Title>

            {quizz.length !== 0 &&
              quizz.map((el, key) => {
                const { list } = el;
                const question = list[0].question;
                const answers = list[0].answers;
                const isOver = question.trim() !== "";
                const points = list[0].points;
                const name = list[0].name;

                return (
                  <ButtonChallenge
                    key={key}
                    onPress={() =>
                      navigation.navigate("Quizz", {
                        key,
                        quizz,
                        answers,
                        question,
                        points,
                        name: el.name,
                      })
                    }
                    title={el.name}
                    dayLeft="3"
                    points={points}
                    color="pink"
                    isOver={isOver}
                  />
                );
              })}
          </View>
        </ScrollView>
      </View>
    );
  }
};
export default UserContextConsumer(ChallengesScreen);
