import React, { useState, useEffect } from "react";
import {
	Platform,
	StyleSheet,
	Text,
	View,
	Keyboard
} from "react-native";

import { UserContextConsumer } from "../../context/UserContext";

import BackArrow from "../../components/BackArrow/BackArrowIcon";
import InputIcons from "../../components/InputIcons/InputIcons";
import LoginButton from "../../components/LoginButton/LoginButton";

import BottomNotification from "../../components/BottomNotification/BottomNotification";

import { emailValidator, isNullOrEmpty, checkSuffixEmail } from "../../utils/helpers";
import { updateUserById } from "../../utils/API";
import { setData } from "../../utils/LocalStorage";

const ModifyUserScreen = ({ navigation, route, context }) => {

	const [firstName, setFirstName] = useState(route.params.first_name);
	const [lastName, setLastName] = useState(route.params.last_name);
	const [Email, setEmail] = useState(route.params.email);
	const [Password, setPassword] = useState("");
	const [errorMessage, setErrorMessage] = useState(null);
	const [errorfirstName, setErrorFirstName] = useState(false);
	const [errorlastName, setErrorLastName] = useState(false);
	const [errorEmail, setErrorEmail] = useState(false);
	const [errorPassword, setErrorPassword] = useState(false);

	const [keyboardVisible, setKeyboardVisible] = useState(false);

	const { userData, setUserData } = context;

	useEffect(() => {
		Keyboard.addListener("keyboardDidShow", () => setKeyboardVisible(true));
		Keyboard.addListener("keyboardDidHide", () => setKeyboardVisible(false));
	}, []);

	const formCheck = async () => {
		let formError = false;
		let data = {};

		if(isNullOrEmpty(lastName)) {
			console.log("Empty lastName !");
			setErrorMessage("Veuillez saisir votre nom.");
			setErrorLastName(true);
			formError = true;
		}
		else {
			if(lastName !== route.params.last_name) data.last_name = lastName;
			setErrorLastName(false);
		}

		if(isNullOrEmpty(firstName)) {
			console.log("Empty firstName !");
			setErrorMessage("Veuillez saisir votre prénom.");
			setErrorFirstName(true);
			formError = true;
		}
		else {
			if(firstName !== route.params.first_name) data.first_name = firstName;
			setErrorFirstName(false);
		}

		if(isNullOrEmpty(Email)) {
			console.log("Empty email !");
			setErrorMessage("Veuillez saisir votre adresse email.");
			setErrorEmail(true);
			formError = true;
		}
		else if (!emailValidator(Email)) {
			console.log("Email is not valid !");
			setErrorMessage("Veuillez saisir une adresse email valide.");
			setErrorEmail(true);
			formError = true;
		}
		else if(!checkSuffixEmail(Email, route.params.email)) {
			console.log("Email suffix is not valid !");
			setErrorMessage("Veuillez saisir une adresse email éligible à Madu.");
			setErrorEmail(true);
			formError = true;
    }
		else {
			if(Email !== route.params.email) data.email = Email;
			setErrorEmail(false);
		}

		if(!isNullOrEmpty(Password)) {
			if(Password.length < 6) {
				console.log("Password is too short < 6 !");
				setErrorMessage("Veuillez saisir un mot de passe sécurisé (minimum : 6 caractères).");
				setErrorPassword(true);
				formError = true;
			}
			else {
				data.password = Password;
				setErrorPassword(false);
			}
		}

		if (!formError && Object.keys(data).length > 0) {
			let fetchAPI = await updateUserById(route.params.id, route.params.token, data);
			if(fetchAPI.success) {
				let _data = { ...userData };
				for(const key of Object.keys(data)) {
					_data[key] = data[key];
				}
				await setData("userData", JSON.stringify(_data));
				setUserData(_data);
				navigation.goBack();
			}
			else {
				if(fetchAPI.errors.email.kind === "unique") {
					setErrorMessage("Cette adresse email est déjà liée à un compte Madu.");
				}
				else console.log(fetchAPI);
			}
		}
	}

	return (
		<View style={[styles.container, (keyboardVisible) ? { paddingBottom: 0 } : { paddingBottom: 50 }]}>
			<View style={styles.headerContainer}>
				<BackArrow type={"simple"} closeModal={() => navigation.goBack()} style={{ position: "absolute", top: -6, left: -10 }} />
				<Text style={styles.text}>Mes informations</Text>
			</View>
			<InputIcons type={"user"} placeholder={"Nom"} defaultValue={lastName} onChange={(value) => setLastName(value)} error={errorlastName} />
			<InputIcons type={"user"} placeholder={"Prénom"} defaultValue={firstName} onChange={(value) => setFirstName(value)} error={errorfirstName} />
			<InputIcons type={"email"} placeholder={"Email"} defaultValue={Email} onChange={(value) => setEmail(value)} error={errorEmail} />
			<InputIcons type={"password"} placeholder={"Mot de passe"} defaultValue={Password} onChange={(value) => setPassword(value)} error={errorPassword} />
			<LoginButton onPress={() => formCheck()} ButtonColor={"#93A0FF"} text={"Modifier mes informations"} textColor={"#FFFFFF"} />
			{(((errorEmail || errorPassword || lastName || firstName) && errorMessage !== null) || errorMessage !== null) ? <BottomNotification text={errorMessage} /> : null}
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: "flex-start",
		alignItems: "center",
		backgroundColor: "#fff",
		paddingHorizontal: 30
	},
	headerContainer: {
		width: "100%",
		height: 30,
		marginVertical: 30
	},
	text: {
		width: "100%",
		height: 30,
		fontSize: 24,
		fontWeight: "bold",
		lineHeight: 28,
		color: "#1C1C1C",
		textAlign: "left",
		paddingLeft: 40
	}
});

export default UserContextConsumer(ModifyUserScreen);