import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import ProgressText from "../../components/ProgressText/ProgressText";
import styled from "./rewardscreen.scss";
import Leaf from "../../components/Leaf";
import Title from "../../components/Title/Title";
import ProgressBar from "../../components/ProgressBar/ProgressBar";
import RewardInfo from "../../components/RewardInfo/RewardInfo";
import CloseIcon from "../../components/CloseIcon";
import { UserContextConsumer } from "../../context/UserContext";
const RewardScreen = ({ navigation, route, context }) => {
  const levels = [
    { label: "novice", max: 300 },
    { label: "adventurer", max: 500 },
    { label: "master", max: 1000 },
  ];
  const { userData } = context;
  const rewards = {
    novice: " 1 bon d'achat",
    adventurer: "2 dvd amour gloire et beauté",
    master: "1 télévision 4K",
  };
  const currentLevel = "novice";
  const points = userData.xp;
  const levelData = levels.find((level) => level.label === currentLevel);

  return (
    <View style={styled["rewardscreen"]}>
      <ScrollView>
        <View style={styled["rewardscreen-header"]}>
          <Title>Mon avancée</Title>
          <TouchableOpacity
            style={styled["rewardscreen-header-back"]}
            onPress={() => navigation.goBack()}
          >
            <CloseIcon />
          </TouchableOpacity>
        </View>

        <View style={styled["rewardscreen-score-flex"]}>
          <Leaf style={styled["rewardscreen-score-leaf"]} green />
          <ProgressText
            max={levelData.max}
            color="green"
            suffix=""
            style={styled["rewardscreen-score-txt"]}
          >
            {points}
          </ProgressText>
        </View>

        <View style={styled["rewardscreen-information"]}>
          <View style={styled["rewardscreen-information-header"]}>
            <Title size="small">Récompense:</Title>
            <Text style={styled["rewardscreen-information-header-day"]}>
              22 jours restants
            </Text>
          </View>
          <View style={styled["rewardscreen-information-progresses"]}>
            {levels.map((level, key) => {
              const isCurrentlvl = level.label === currentLevel;
              const score = isCurrentlvl ? (points / level.max) * 100 : 0;
              return (
                <View key={key}>
                  <ProgressBar
                    style={styled["rewardscreen-information-progresses-item"]}
                    value={score}
                    level={level.label}
                  />
                  {isCurrentlvl && (
                    <RewardInfo points={level.max - points}>
                      {rewards[level.label]}
                    </RewardInfo>
                  )}
                </View>
              );
            })}
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default UserContextConsumer(RewardScreen);
