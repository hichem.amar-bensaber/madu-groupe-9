import React, { useState, useEffect } from "react";
import { Platform, StyleSheet, View } from "react-native";

import { ScrollView } from "react-native-gesture-handler";
import Input from "../components/Input/Input";
import Maps from "../components/Maps/Maps";
import Loader from "../components/Loader/Loader";

import { getPoi } from "../utils/api_static_poi";
import { listStores } from "../utils/API";

import { UserContextConsumer } from "../context/UserContext";
const option_init = {
  restaurants: {
    types: [],
    filters: [],
  },
  shopping: {
    types: [],
    filters: [],
  },
  hobbies: {
    types: [],
    filters: [],
  },
};
const MapScreen = ({ navigation, route, context }) => {
  const [text, setText] = useState("");
  const [option, setOption] = useState(option_init);

  const [inBlur, setInBlur] = useState(false);
  const [poiData, setPoiData] = useState([]);

  const userToken = context.userToken;

  const FilterData = () => {
    const optionSpecialities = Object.values(option)
      .map((el) => el.types)
      .flat();
    const optionTypes = Object.values(option)
      .map((el) => el.filters)
      .flat();
    const specialitiesSelected = [...optionSpecialities, ...optionTypes];
    const data =
      poiData.length !== 0 && specialitiesSelected.length !== 0
        ? poiData.filter((el) =>
            specialitiesSelected.find((a) => a === el.store_speciality)
          )
        : poiData;
    return data;
  };

  useEffect(() => {
    if (typeof route.params === "object" && "option" in route.params) {
      setOption(route.params.option);
    }
  }, [route.params]);

  useEffect(() => {
    const asyncFunction = async () => {
      let data = await listStores(userToken);
      if (data) {
        console.log("App receive POI data..");
        setPoiData(data);
      } else {
        console.log("App not receive POI data, use static data..");
        setPoiData(getPoi);
      }
    };
    asyncFunction();
  }, []);

  if (poiData.length === 0) {
    return <Loader />;
  } else {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container}>
          <Input
            text={text}
            onChangeText={setText}
            onSwap={() => navigation.navigate("Filter", option)}
            onBlur={() => console.log("on blur")}
            absolute={true}
            data={poiData}
            contextData={context}
          />
          <Maps navigation={navigation} data={FilterData()} context={context} />
        </ScrollView>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    // position: 'relative'
  },
  text: {
    alignContent: "center",
  },
});

export default UserContextConsumer(MapScreen);
