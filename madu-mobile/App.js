import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import * as React from "react";
import { Platform, StatusBar, StyleSheet, View } from "react-native";

import useCachedResources from "./hooks/useCachedResources";
import AppNavigation from "./navigation/AppNavigation";
import LoginNavigation from "./navigation/LoginNavigation";
import LinkingConfiguration from "./navigation/LinkingConfiguration";
import Header from "./components/Header/Header";

import { UserContextProvider } from "./context/UserContext"; 

import {
  useFonts,
  RobotoCondensed_300Light,
  RobotoCondensed_300Light_Italic,
  RobotoCondensed_400Regular,
  RobotoCondensed_400Regular_Italic,
  RobotoCondensed_700Bold,
  RobotoCondensed_700Bold_Italic,
} from "@expo-google-fonts/roboto-condensed";

const Stack = createStackNavigator();

export default function App({ navigation, route }) {

  const [isLoadingComplete, isAuth, userToken, userData] = useCachedResources();

  let [fontsLoaded] = useFonts({
    RobotoCondensed_300Light,
    RobotoCondensed_400Regular,
    RobotoCondensed_700Bold
  });

  if(isLoadingComplete && fontsLoaded) {
    return (
      <View style={styles.container}>
        {Platform.OS === "ios" && <StatusBar barStyle="dark-content"/>}
        <UserContextProvider defaultUserToken={userToken} defaultUserData={userData} >
          <NavigationContainer linking={LinkingConfiguration}>
            <Stack.Navigator>
              {!isAuth? <Stack.Screen name="Login" component={LoginNavigation} options={{ header: () => null }}/>: null }
              <Stack.Screen
                name="App"
                component={AppNavigation}
                options={{ header: () => <Header xp={(userData && userData.pot)? userData.pot: 0} /> }}
              />
            </Stack.Navigator>
          </NavigationContainer>
        </UserContextProvider>
      </View>
    );
  }
  else return null;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
});