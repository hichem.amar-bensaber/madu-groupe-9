import React from "react";
import { View, Text } from "react-native";
import Leaf from "../Leaf";
import styled from "./greenscoreheader.scss";

const GreenScoreHeader = ({ children, style = {} }) => {
  return (
    <View style={{ ...styled.greenscoreheader, ...style }}>
      <Leaf />
      <Text style={styled["greenscoreheader-text"]}>{children}</Text>
    </View>
  );
};
export default GreenScoreHeader;
