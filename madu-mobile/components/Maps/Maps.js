import React, { useState, useEffect } from "react";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import { StyleSheet, View, Dimensions } from "react-native";
import PoiPreview from "../PoiPreview/PoiPreview";
import mapStyle from "./mapStyle.json";

import FoodMarker from "../MapMarkers/FoodMarker";
import HobbiesMarker from "../MapMarkers/HobbiesMarker";
import ShopMarker from "../MapMarkers/ShopMarker";
import ClientMarker from "../MapMarkers/ClientMarker";

const Maps = ({ navigation, data, context }) => {
	console.log(context.userData)
	const defaultDelta = 0.0100;
	let initialRegion = null;

	if (context.userData && context.userData._client) { // Client coords
		initialRegion = {
			latitude: context.userData._client.coords.latitude,
			longitude: context.userData._client.coords.longitude,
			latitudeDelta: defaultDelta,
			longitudeDelta: defaultDelta
		};
	}
	else if (data.length > 0) { // First store coords
		initialRegion = {
			latitude: data[0].coords.latitude,
			longitude: data[0].coords.longitude,
			latitudeDelta: defaultDelta,
			longitudeDelta: defaultDelta
		};
	}
	else { // defaultRegion => Hétic coords.
		initialRegion = {
			latitude: 48.8517832,
			longitude: 2.4203658,
			latitudeDelta: defaultDelta,
			longitudeDelta: defaultDelta
		};
	}
	const [region, setRegion] = useState(initialRegion);
	const [listPOI, setlistPOI] = useState(data);
	const [activePOI, setActivePOI] = useState(null);

	useEffect(() => {
	  setlistPOI(data);
	}, [data]);

	const renderMarker = (value, selected) => {
		if (value.store_type.toLowerCase() === "boutique") return (<ShopMarker selected={selected} />);
		else if (value.store_type.toLowerCase() === "restaurant") return (<FoodMarker selected={selected} />);
		else if (value.store_type.toLowerCase() === "activité") return (<HobbiesMarker selected={selected} />);
		else return null;
	}

	const getPoiIndex = (poiId) => {
		for (let i = 0; i < listPOI.length; i++) {
			if (listPOI[i]["_id"] === poiId) return i;
		}
	}

	const moveToMarker = (poiData) => {
		if(!poiData) return null;
		setRegion({
			latitude: poiData.coords.latitude,
			longitude: poiData.coords.longitude,
			latitudeDelta: defaultDelta,
			longitudeDelta: defaultDelta
		});
		setActivePOI(getPoiIndex(poiData["_id"]));
	}

	const renderPreview = (index) => {
		if (index === null) return null;
		return (
			<PoiPreview
				onClick={() => navigation.navigate("POI", { dataGetPoi: listPOI[index], userData: context.userData })}
				key={listPOI[index].id}
				data={listPOI[index]}
			/>
		);
	}

	return (
		<View style={styles.container}>
			<MapView style={styles.map}
				initialRegion={region}
				customMapStyle={mapStyle}
				provider={PROVIDER_GOOGLE}
				onPress={() => setActivePOI(null)}>
				<Marker.Animated
					coordinate={{
						latitude: 48.8517832,
						longitude: 2.4203658
					}}
					style={{ width: 50, height: 60 }}
					onPress={() => setActivePOI(null)}
				>
					<ClientMarker />
				</Marker.Animated>
				{listPOI.length > 0 && listPOI.map((value, i) => {
					console.log("Create new marker (" + i + ") " + value.name);
					let selected = (activePOI === i) ? true : false;
					return (
						<Marker.Animated
							coordinate={{
								latitude: value.coords.latitude,
								longitude: value.coords.longitude
							}}
							key={i}
							onPress={() => setActivePOI(i)}
							style={{ width: 50, height: 60 }}
						>
							{renderMarker(value, selected)}
						</Marker.Animated>
					);
				})}
			</MapView>
			{renderPreview(activePOI)}
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff",
		alignItems: "center",
		justifyContent: "center"
	},
	map: {
		width: Dimensions.get("window").width,
		height: Dimensions.get("window").height - 90 - 70
	}
});

export default Maps;