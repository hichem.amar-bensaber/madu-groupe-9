import React from 'react';
import { View, Text } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import styled from './poilisttitle.scss';

const PoiListTitle = ({storeType, title}) => {
    return (
        <LinearGradient
            start={{x: 0, y: 0}} 
            end={{x: 1, y: 0}}
            colors={['#B16BF2', '#7A8AFF']}
            style={[styled["poilisttile"], { alignItems: 'center', borderRadius: 5, flex: 1 }]}
            >
            <View style={styled["poilisttile_title"]}>
                <Text style={styled["poilisttile_title"]}> {title} </Text>
            </View>
        </LinearGradient>
    )
}

export default PoiListTitle;