import React from 'react';
import styled from "./poilist.scss";
import { Text, View, Image, TouchableOpacity } from 'react-native';

import GreenScore from '../GreenScore/GreenScore';
import PoiListTitle from '../PoiListTitle/PoiListTitle';
import PoiLink from '../PoiLlink/PoiLink';

import { calculateDistanceCoords } from "../../utils/helpers";

const PoiPreview = ({
    onClick,
    dataGetPoi,
    userData
}) => {
    const checkDay = (actual_day) => {

    console.log(dataGetPoi);

    switch (actual_day) {
        case 1:
        return dataGetPoi.open_hours.mon
        case 2:
        return dataGetPoi.open_hours.tue
        case 3:
        return dataGetPoi.open_hours.wed
        case 4:
        return dataGetPoi.open_hours.thu
        case 5:
        return dataGetPoi.open_hours.fri
        case 6:
        return dataGetPoi.open_hours.sat
        case 7:
        return dataGetPoi.open_hours.sun
        default:
        return "Aucun horaires trouvés"
    }
    }
    return (
        <TouchableOpacity onPress={() => onClick()} style={styled["poilist"]}>
            <View style={styled["poilist-image"]}>
                <Image
                    style={styled["poilist-image_background"]}
                    resizeMode="cover"
                    source={require('../../assets/images/PoiScreen/poiscreen-picture.jpg')}
                />
                {/* <Image
                    style={styled["poilist-image_background"]}
                    resizeMode="cover"
                    source={{uri: `http://localhost:8080/${dataGetPoi.images[0].uri}`}}
                /> */}
            </View>
            <View style={styled["poilist-informations"]}>
                <View style={styled["poilist-informations_greenscore"]}>
                    <GreenScore 
                        dataGreenScore={dataGetPoi.greenscore}
                        dataGreenScoreDescription={dataGetPoi.greenscore_description}
                    />   
                </View>
                <View style={styled["poilist-informations_poiListTitle"]}>
                    <PoiListTitle 
                        storeType={dataGetPoi.store_type}
                        title={dataGetPoi.name}
                    />
                </View>
                <Text style={styled["poilist-informations_text"]}>
                {
                    calculateDistanceCoords(
                    48.8517832,
                    2.4203658,
                    dataGetPoi.coords.latitude,
                    dataGetPoi.coords.longitude)
                }m
                </Text>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingVertical: 14 }}>
                    <Text> {checkDay( new Date().getDay() ) } </Text>
                </View>
                <PoiLink href={`tel:+${dataGetPoi.phone.slice(1)}`}> {dataGetPoi.phone} </PoiLink>
            </View>
        </TouchableOpacity>
    );
}

export default PoiPreview;