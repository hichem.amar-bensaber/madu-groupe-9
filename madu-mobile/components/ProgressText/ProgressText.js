import React from "react";
import { View, Text } from "react-native";
import PropTypes from "prop-types";
import styled from "./progresstext.scss";

const colorStyle = {
  default: {
    color: "#798090",
    fontWeight: "normal",
  },
  green: {
    color: "#27AE60",
    fontWeight: "bold",
  },
};
const ProgressText = ({ children, max, style, suffix, color }) => {
  return (
    <View style={{ ...styled["progresstext"], ...style }}>
      <Text style={{ ...styled["progresstext-main"], ...colorStyle[color] }}>
        {children} / {max}
      </Text>
      <Text style={{ ...colorStyle[color] }}>{suffix}</Text>
    </View>
  );
};
ProgressText.defaultProps = {
  suffix: "pts",
  color: "default",
  style: {},
};
ProgressText.propTypes = {
  color: PropTypes.oneOf(["default", "green"]),
  style: PropTypes.object.isRequired,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  max: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  suffix: PropTypes.string,
};
export default ProgressText;
