import React from "react";
import { StyleSheet, View, ActivityIndicator, Dimensions } from "react-native";


const Loader = () => {
  return (
    <View style={styles.container}>
      <ActivityIndicator size="large" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    elevation: 10
  }
});

export default Loader;