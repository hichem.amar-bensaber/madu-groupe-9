import React from 'react';
import { View } from 'react-native';
import Svg, { Path, SvgCss, Rect } from 'react-native-svg'
import styled from "./iconaccess.scss";

const MoteurIcon = ({checkAccess}) => {
    let xmlGradient = `
        <svg width="37" height="36" viewBox="0 0 37 36" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="0.717529" y="0.5" width="35" height="35" rx="3.5" stroke="url(#paint0_radial)"/>
        <path d="M15.0002 12.59H14.5002V13.09V20.25V20.75H15.0002H16.0002H16.2713L16.4192 20.5229C17.3486 19.0963 18.7835 18.1123 20.2885 17.6189C21.7998 17.1234 23.325 17.1399 24.4329 17.635C25.8747 18.3049 26.5364 19.6383 26.5029 21.067C26.4691 22.5092 25.7217 24.0199 24.3591 24.9465L24.3581 24.9472C23.2622 25.6961 20.9566 26.4487 18.1613 26.4988C15.4609 26.5472 12.3595 25.9381 9.52678 24.0902C9.53243 24.0344 9.53894 23.9712 9.54633 23.9009C9.57922 23.5883 9.62962 23.1362 9.69946 22.5806C9.83921 21.469 10.0565 19.9454 10.3666 18.2962C10.677 16.6456 11.0789 14.8771 11.5863 13.2732C12.0522 11.8005 12.5975 10.498 13.2231 9.55448L17.3517 10.3802L16.6365 12.59H15.0002Z" stroke="url(#paint1_radial)"/>
        <defs>
        <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(32.1421 30.25) rotate(162.077) scale(23.5584 106.239)">
        <stop stop-color="#7A8AFF"/>
        <stop offset="1" stop-color="#B16BF2"/>
        </radialGradient>
        <radialGradient id="paint1_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(24.9659 24.1262) rotate(162.079) scale(11.7816 53.1243)">
        <stop stop-color="#7A8AFF"/>
        <stop offset="1" stop-color="#B16BF2"/>
        </radialGradient>
        </defs>
        </svg>
    `

    const checkHandicap = (handicap) => {
        return checkAccess.some(data => data.type === handicap);
    }

    return (
        <View style={styled["iconaccess"]}>
        { checkHandicap("Moteur") ? (
            <SvgCss xml={xmlGradient} />
        ) : (
            <Svg width="37" height="36" viewBox="0 0 37 36" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Rect x="0.717529" y="0.5" width="35" height="35" rx="3.5" stroke="#DDDDDD"/>
            <Path d="M15.0002 12.59H14.5002V13.09V20.25V20.75H15.0002H16.0002H16.2713L16.4192 20.5229C17.3486 19.0963 18.7835 18.1123 20.2885 17.6189C21.7998 17.1234 23.325 17.1399 24.4329 17.635C25.8747 18.3049 26.5364 19.6383 26.5029 21.067C26.4691 22.5092 25.7217 24.0199 24.3591 24.9465L24.3581 24.9472C23.2622 25.6961 20.9566 26.4487 18.1613 26.4988C15.4609 26.5472 12.3595 25.9381 9.52678 24.0902C9.53243 24.0344 9.53894 23.9712 9.54633 23.9009C9.57922 23.5883 9.62962 23.1362 9.69946 22.5806C9.83921 21.469 10.0565 19.9454 10.3666 18.2962C10.677 16.6456 11.0789 14.8771 11.5863 13.2732C12.0522 11.8005 12.5975 10.498 13.2231 9.55448L17.3517 10.3802L16.6365 12.59H15.0002Z" stroke="#DDDDDD"/>
            </Svg>
            )
        }
        </View>
    )
}

export default MoteurIcon;