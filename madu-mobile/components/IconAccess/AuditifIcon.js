import React from 'react';
import { View } from 'react-native';
import Svg, { Path, SvgCss, Rect } from 'react-native-svg'
import styled from "./iconaccess.scss";

const AuditifIcon = ({checkAccess}) => {
    let xmlGradient = `
    <svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
    <rect x="0.5" y="0.5" width="35" height="35" rx="3.5" stroke="url(#paint0_radial)"/>
    <path d="M21.7369 21.5044C23.8936 20.0391 25.5 17.7816 25.5 15C25.5 13.0109 24.7098 11.1032 23.3033 9.6967C21.8968 8.29018 19.9891 7.5 18 7.5C16.0109 7.5 14.1032 8.29018 12.6967 9.6967C11.2902 11.1032 10.5 13.0109 10.5 15V24.683C10.5 26.7577 12.1753 28.5 14.25 28.5C16.3247 28.5 17.3827 27.2841 17.873 26.625C18.5653 25.6964 19.8281 22.8019 21.7369 21.5044Z" stroke="url(#paint1_radial)" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M13.5 20.25V14.625C13.5 12.3562 15.525 10.5 18 10.5C20.475 10.5 22.5 12.3562 22.5 14.625" stroke="url(#paint2_radial)" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M13.5 17.2041C14.6719 16.3604 17.2416 16.501 17.2416 16.501C18.4603 16.501 19.1714 17.88 18.4603 18.8729C18.4603 18.8729 16.7325 20.8603 16.4981 21.751" stroke="url(#paint3_radial)" stroke-linecap="round" stroke-linejoin="round"/>
    <defs>
    <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(31.9245 30.25) rotate(162.077) scale(23.5584 106.239)">
    <stop stop-color="#7A8AFF"/>
    <stop offset="1" stop-color="#B16BF2"/>
    </radialGradient>
    <radialGradient id="paint1_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(23.8019 25.1458) rotate(155.638) scale(10.2525 59.3339)">
    <stop stop-color="#7A8AFF"/>
    <stop offset="1" stop-color="#B16BF2"/>
    </radialGradient>
    <radialGradient id="paint2_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(21.4811 18.6927) rotate(160.69) scale(5.93783 28.5393)">
    <stop stop-color="#7A8AFF"/>
    <stop offset="1" stop-color="#B16BF2"/>
    </radialGradient>
    <radialGradient id="paint3_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(18.1497 20.911) rotate(162.027) scale(3.43219 15.5147)">
    <stop stop-color="#7A8AFF"/>
    <stop offset="1" stop-color="#B16BF2"/>
    </radialGradient>
    </defs>
    </svg>
    `

    const checkHandicap = (handicap) => {
        return checkAccess.some(data => data.type === handicap);
    }

    return (
        <View style={styled["iconaccess"]}>
        { checkHandicap("Auditif") ? (
            <SvgCss xml={xmlGradient} />
        ) : (
            <Svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Rect x="0.5" y="0.5" width="35" height="35" rx="3.5" stroke="#DDDDDD"/>
            <Path d="M21.7369 21.5044C23.8936 20.0391 25.5 17.7816 25.5 15C25.5 13.0109 24.7098 11.1032 23.3033 9.6967C21.8968 8.29018 19.9891 7.5 18 7.5C16.0109 7.5 14.1032 8.29018 12.6967 9.6967C11.2902 11.1032 10.5 13.0109 10.5 15V24.683C10.5 26.7577 12.1753 28.5 14.25 28.5C16.3247 28.5 17.3827 27.2841 17.873 26.625C18.5653 25.6964 19.8281 22.8019 21.7369 21.5044Z" stroke="#DDDDDD" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M13.5 20.25V14.625C13.5 12.3562 15.525 10.5 18 10.5C20.475 10.5 22.5 12.3562 22.5 14.625" stroke="#DDDDDD" stroke-linecap="round" stroke-linejoin="round"/>
            <Path d="M13.5 17.2041C14.6719 16.3604 17.2416 16.501 17.2416 16.501C18.4603 16.501 19.1714 17.88 18.4603 18.8729C18.4603 18.8729 16.7325 20.8603 16.4981 21.751" stroke="#DDDDDD" stroke-linecap="round" stroke-linejoin="round"/>
            </Svg>
            )
        }
        </View>
    )
}

export default AuditifIcon;