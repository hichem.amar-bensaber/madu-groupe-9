import React from 'react';
import { View } from 'react-native';
import Svg, { Path, SvgCss, Rect } from 'react-native-svg'
import styled from "./iconaccess.scss";

const EyeIcon = ({checkAccess}) => {
    let xmlGradient = `
        <svg width="37" height="37" viewBox="0 0 37 36" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="1.32617" y="0.5" width="35" height="35" rx="3.5" stroke="url(#paint0_radial)"/>
        <path d="M19 24C12.9028 24 10.1458 18.4909 10.0398 18.2182C9.98675 18.0545 9.98675 17.8909 10.0398 17.7273C10.1458 17.5091 12.9028 12 19 12C25.0972 12 27.8542 17.5091 27.9602 17.7818C28.0133 17.9455 28.0133 18.1091 27.9602 18.2727C27.8542 18.4909 25.0972 24 19 24ZM11.1001 18C11.6834 19.0364 14.2283 22.9091 19 22.9091C23.7717 22.9091 26.2636 19.0364 26.8999 18C26.3166 16.9636 23.7717 13.0909 19 13.0909C14.2283 13.0909 11.7364 16.9636 11.1001 18Z" fill="url(#paint1_radial)"/>
        <path d="M19 22.5C16.5 22.5 14.5 20.5 14.5 18C14.5 15.5 16.5 13.5 19 13.5C21.5 13.5 23.5 15.5 23.5 18C23.5 20.5 21.5 22.5 19 22.5ZM19 14.5C17.05 14.5 15.5 16.05 15.5 18C15.5 19.95 17.05 21.5 19 21.5C20.95 21.5 22.5 19.95 22.5 18C22.5 16.05 20.95 14.5 19 14.5Z" fill="url(#paint2_radial)"/>
        <path d="M19 21C17.32 21 16 19.68 16 18C16 17.64 16.24 17.4 16.6 17.4C16.96 17.4 17.2 17.64 17.2 18C17.2 19.02 17.98 19.8 19 19.8C20.02 19.8 20.8 19.02 20.8 18C20.8 16.98 20.02 16.2 19 16.2C18.64 16.2 18.4 15.96 18.4 15.6C18.4 15.24 18.64 15 19 15C20.68 15 22 16.32 22 18C22 19.68 20.68 21 19 21Z" fill="url(#paint3_radial)"/>
        <defs>
        <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(32.7507 30.25) rotate(162.077) scale(23.5584 106.239)">
        <stop stop-color="#7A8AFF"/>
        <stop offset="1" stop-color="#B16BF2"/>
        </radialGradient>
        <radialGradient id="paint1_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(25.9623 22.0833) rotate(167.832) scale(11.4651 36.3829)">
        <stop stop-color="#7A8AFF"/>
        <stop offset="1" stop-color="#B16BF2"/>
        </radialGradient>
        <radialGradient id="paint2_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(22.4811 21.0625) rotate(162.077) scale(5.88961 26.5596)">
        <stop stop-color="#7A8AFF"/>
        <stop offset="1" stop-color="#B16BF2"/>
        </radialGradient>
        <radialGradient id="paint3_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(21.3208 20.0417) rotate(162.077) scale(3.9264 17.7064)">
        <stop stop-color="#7A8AFF"/>
        <stop offset="1" stop-color="#B16BF2"/>
        </radialGradient>
        </defs>
        </svg>
    `

    const checkHandicap = (handicap) => {
        return checkAccess.some(data => data.type === handicap);
    }

    return (
        <View style={styled["iconaccess"]}>
        { checkHandicap("Visuel") ? (
            <SvgCss xml={xmlGradient} />
        ) : (
            <Svg width="37" height="37" viewBox="0 0 37 36" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Rect x="1.32617" y="0.5" width="35" height="35" rx="3.5" stroke="#DDDDDD"/>
            <Path d="M19 24C12.9028 24 10.1458 18.4909 10.0398 18.2182C9.98675 18.0545 9.98675 17.8909 10.0398 17.7273C10.1458 17.5091 12.9028 12 19 12C25.0972 12 27.8542 17.5091 27.9602 17.7818C28.0133 17.9455 28.0133 18.1091 27.9602 18.2727C27.8542 18.4909 25.0972 24 19 24ZM11.1001 18C11.6834 19.0364 14.2283 22.9091 19 22.9091C23.7717 22.9091 26.2636 19.0364 26.8999 18C26.3166 16.9636 23.7717 13.0909 19 13.0909C14.2283 13.0909 11.7364 16.9636 11.1001 18Z" fill="#DDDDDD"/>
            <Path d="M19 22.5C16.5 22.5 14.5 20.5 14.5 18C14.5 15.5 16.5 13.5 19 13.5C21.5 13.5 23.5 15.5 23.5 18C23.5 20.5 21.5 22.5 19 22.5ZM19 14.5C17.05 14.5 15.5 16.05 15.5 18C15.5 19.95 17.05 21.5 19 21.5C20.95 21.5 22.5 19.95 22.5 18C22.5 16.05 20.95 14.5 19 14.5Z" fill="#DDDDDD"/>
            <Path d="M19 21C17.32 21 16 19.68 16 18C16 17.64 16.24 17.4 16.6 17.4C16.96 17.4 17.2 17.64 17.2 18C17.2 19.02 17.98 19.8 19 19.8C20.02 19.8 20.8 19.02 20.8 18C20.8 16.98 20.02 16.2 19 16.2C18.64 16.2 18.4 15.96 18.4 15.6C18.4 15.24 18.64 15 19 15C20.68 15 22 16.32 22 18C22 19.68 20.68 21 19 21Z" fill="#DDDDDD"/>
            </Svg>
            )
        }
        </View>
    )
}

export default EyeIcon;