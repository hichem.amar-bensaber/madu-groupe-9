import React from "react";
import { View } from "react-native";
import Svg, { Path } from "react-native-svg";
export default ({ style = {} }) => (
  <Svg
    width="18"
    height="12"
    viewBox="0 0 18 12"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <Path d="M7 12H11V10H7V12ZM0 0V2H18V0H0ZM3 7H15V5H3V7Z" fill="black" />
  </Svg>
);
