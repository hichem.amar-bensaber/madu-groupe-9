import React from "react";
import { View, Text, StyleSheet } from "react-native";
import styled from "./profilnamelvl.scss";
import PropTypes from "prop-types";

const colorStyle = {
  novice: {
    backgroundColor: "#27AE60",
  },
  aventurier: {
    backgroundColor: "#EC871B",
  },
  master: {
    backgroundColor: "#3746A9",
  },
};
const shadowColorStyle = {
  novice: {
    backgroundColor: "#B0FFD2",
  },
  aventurier: {
    backgroundColor: "#FFB15C",
  },
  master: {
    backgroundColor: "#93A0FF",
  },
};

const styleTransform = StyleSheet.create({
  level: {
    transform: [{ rotate: "-5.91deg" }],
  },
});

const levelText = {
  novice: "Novice",
  aventurier: "Aventurier",
  master: "Maître",
};
const ProfilNameLvl = ({ name, level }) => {
  return (
    <View style={styled["profilnamelvl"]}>
      <View style={styled["profilnamelvl-name"]}>
        <Text
          style={{
            ...styled["profilnamelvl-name-txt"],
            ...colorStyle[level],
          }}
        >
          {name}
        </Text>
        <View
          style={{
            ...styled["profilnamelvl-name-shadow"],
            ...shadowColorStyle[level],
          }}
        ></View>
      </View>
      <View style={styled["profilnamelvl-level"]}>
        <Text
          style={{
            ...styled["profilnamelvl-level-txt"],
            ...styleTransform.level,
          }}
        >
          {levelText[level]}
        </Text>
      </View>
    </View>
  );
};
ProfilNameLvl.defaultProps = {
  level: "novice",
  style: {},
};
ProfilNameLvl.propTypes = {
  level: PropTypes.oneOf(["novice", "aventurier", "master"]),
  name: PropTypes.string.isRequired,
  style: PropTypes.object,
};
export default ProfilNameLvl;
