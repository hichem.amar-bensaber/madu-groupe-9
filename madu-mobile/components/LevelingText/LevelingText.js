import React from "react";
import PropTypes from "prop-types";
import { View, Text } from "react-native";
import styled from "./levelingtext.scss";

const levelStyle = {
  novice: {
    text: "novice",
    color: "#0A9243",
  },
  aventurier: {
    text: "aventurier",
    color: "#EC871B",
  },
  master: {
    text: "maître",
    color: "#5446F4",
  },
};
const LevelingText = ({ level }) => {
  return (
    <View style={styled["levelingtext"]}>
      <Text style={styled["levelingtext-label"]}>Niveau :</Text>
      <Text
        style={{
          ...styled["levelingtext-level"],
          color: levelStyle[level].color,
        }}
      >
        {levelStyle[level].text}
      </Text>
    </View>
  );
};
LevelingText.defaultProps = {
  level: "novice",
};
LevelingText.propTypes = {
  level: PropTypes.oneOf(["novice", "aventurier", "master"]),
  level: PropTypes.string.isRequired,
};
export default LevelingText;
