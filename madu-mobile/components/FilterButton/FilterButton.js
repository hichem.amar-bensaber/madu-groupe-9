import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import styled from "./filterbutton.scss";
import PropTypes from "prop-types";

const FilterButton = ({ children, onPress, active, style }) => {
  const styleActive = !active
    ? {
        borderColor: "#DDDDDD",
        backgroundColor: "white",
      }
    : {
        borderColor: "#FFEFFE",
        backgroundColor: "#FFEFFE",
      };
  const color = !active ? "black" : "#B26DB2";
  return (
    <TouchableOpacity
      style={{
        ...styleActive,
        ...styled["filterbutton-btn"],
        ...style,
      }}
      onPress={onPress}
    >
      <Text style={{ color, ...styled["filterbutton-btn-txt"] }}>
        {children}
      </Text>
    </TouchableOpacity>
  );
};
PropTypes.defaultProps = {
  style: {},
  active: false,
};
PropTypes.propTypes = {
  onPress: PropTypes.func.isRequired,
  style: PropTypes.object,
  active: PropTypes.bool,
};
export default FilterButton;
