import React from 'react'
import Svg, { Path, Circle, SvgCss } from 'react-native-svg'

export default function ProfileIcon({ selected }) {
  if(selected) {
    const xml = `
      <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="12" cy="12.5" r="10" fill="url(#paint0_radial)"/>
        <path fill-rule="evenodd" clip-rule="evenodd" d="M15.6363 10.6818C15.6363 8.66965 14.012 7.04541 11.9999 7.04541C9.98777 7.04541 8.36353 8.66965 8.36353 10.6818C8.36353 12.6939 9.98777 14.3181 11.9999 14.3181C14.012 14.3181 15.6363 12.6939 15.6363 10.6818ZM11.9998 22.5002C9.49976 22.5002 6.62325 21.6624 5.33325 19.7224C5.36325 17.7324 8.66658 15.8335 11.9998 15.8335C15.3333 15.8335 18.6366 17.7324 18.6666 19.7224C17.3766 21.6624 14.4998 22.5002 11.9998 22.5002Z" fill="url(#paint1_radial)"/>
        <defs>
          <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(19.7359 19.3056) rotate(162.077) scale(13.088 59.0214)">
            <stop stop-color="#D2AFF3"/>
            <stop offset="1" stop-color="#BAC6F8"/>
          </radialGradient>
          <radialGradient id="paint1_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(17.1572 20.0317) rotate(159.449) scale(8.86614 44.8838)">
            <stop stop-color="#7A8AFF"/>
            <stop offset="1" stop-color="#B16BF2"/>
          </radialGradient>
        </defs>
      </svg>
    `;

    return (
      <SvgCss xml={xml} />
    );
  }
  else {
    return (
      <Svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <Circle cx="12" cy="12" r="9.5" stroke="#798090" />
        <Path d="M5.5 19C5.5315 16.9531 8.5 15 11.9998 15C15.5 15 18.4685 16.9531 18.5 19" stroke="#798090" />
        <Path d="M11.9999 6.54541C14.012 6.54541 15.6363 8.16965 15.6363 10.1818C15.6363 12.1939 14.012 13.8181 11.9999 13.8181C9.98777 13.8181 8.36353 12.1939 8.36353 10.1818C8.36353 8.16965 9.98777 6.54541 11.9999 6.54541Z" stroke="#798090" />
      </Svg>
    )
  }
}