import React from "react";
import { StyleSheet, View } from "react-native";
import { SvgCss } from "react-native-svg"


const ChallengesIcon = ({ selected, notification=null }) => {

  let xml = null;

  if(notification === 0) notification = null; // Fix error text render !

  if(notification) {
    xml = `
      <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g>
          <circle cx="9" cy="9" r="8" fill="#EB5757" stroke="white" stroke-width="2"/>
          <text x="6.50" y="12.50" font-size="11" fill="white">${ notification }</text>
        </g>
      </svg>
    `;
  }

  let xml_ = null;

  if(selected) {
    xml_ = `
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M12.9998 16.938V19H17.9998V20H6L5.99976 19H10.9998V16.938C9.06645 16.6942 7.28857 15.7533 5.99976 14.2917C4.71094 12.8302 3.99979 10.9486 3.99976 9V3H19.9998V9C19.9997 10.9486 19.2886 12.8302 17.9997 14.2917C16.7109 15.7533 14.9331 16.6942 12.9998 16.938Z" fill="url(#paint0_radial)"/>
        <mask id="path-2-inside-1" fill="white">
          <path d="M1 5L3 5V9H1V5ZM21 5L23 5V9H21V5Z"/>
        </mask>
        <path d="M1 5L3 5V9H1V5ZM21 5L23 5V9H21V5Z" fill="#C4C4C4"/>
        <path d="M1 5L1 -7H-11V5H1ZM3 5H15V-7H3V5ZM3 9V21H15V9H3ZM1 9H-11V21H1V9ZM21 5V-7H9V5H21ZM23 5H35V-7H23V5ZM23 9V21H35V9H23ZM21 9H9V21H21V9ZM1 17H3V-7H1L1 17ZM-9 5V9H15V5H-9ZM3 -3H1V21H3V-3ZM13 9V5H-11V9H13ZM21 17H23V-7H21V17ZM11 5V9H35V5H11ZM23 -3H21V21H23V-3ZM33 9V5H9V9H33Z" fill="url(#paint1_radial)" mask="url(#path-2-inside-1)"/>
        <rect x="6" y="18" width="12" height="2" fill="url(#paint2_radial)"/>
        <defs>
          <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(18.1884 17.2847) rotate(161.034) scale(10.5341 49.8648)">
            <stop stop-color="#7A8AFF"/>
            <stop offset="1" stop-color="#B16BF2"/>
          </radialGradient>
          <radialGradient id="paint1_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(20.5094 8.36111) rotate(176.634) scale(13.7218 12.385)">
            <stop stop-color="#7A8AFF"/>
            <stop offset="1" stop-color="#B16BF2"/>
          </radialGradient>
          <radialGradient id="paint2_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(16.6415 19.6806) rotate(176.914) scale(7.48255 6.1942)">
            <stop stop-color="#7A8AFF"/>
            <stop offset="1" stop-color="#B16BF2"/>
        </radialGradient>
        </defs>
      </svg>
    `;
  }
  else {
    xml_ = `
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M12.9374 16.4419L12.5 16.4971V16.938V19V19.5H11.5V19V16.938V16.4971L11.0626 16.4419C9.25007 16.2134 7.58329 15.3312 6.37502 13.961C5.16674 12.5909 4.50003 10.8268 4.5 9C4.5 9 4.5 8.99999 4.5 8.99999V3.5H19.5V9C19.5 10.8268 18.8333 12.5909 17.625 13.961C16.4167 15.3312 14.7499 16.2134 12.9374 16.4419Z" stroke="#798090"/>
        <mask id="path-2-inside-1" fill="white">
          <path d="M2 5H3V9H2V5ZM21 5H22V9H21V5Z"/>
        </mask>
        <path d="M2 5H3V9H2V5ZM21 5H22V9H21V5Z" fill="#C4C4C4"/>
        <path d="M2 5V3H0V5H2ZM3 5H5V3H3V5ZM3 9V11H5V9H3ZM2 9H0V11H2V9ZM21 5V3H19V5H21ZM22 5H24V3H22V5ZM22 9V11H24V9H22ZM21 9H19V11H21V9ZM2 7H3V3H2V7ZM1 5V9H5V5H1ZM3 7H2V11H3V7ZM4 9V5H0V9H4ZM21 7H22V3H21V7ZM20 5V9H24V5H20ZM22 7H21V11H22V7ZM23 9V5H19V9H23Z" fill="#798090" mask="url(#path-2-inside-1)"/>
        <rect x="6" y="19" width="12" height="1" fill="#798090"/>
      </svg>
    `;
  }

  return (
    <View style={styles.container}>
      <SvgCss xml={xml_}/>
      {notification &&
        <View style={styles.notificationContainer}>
          <SvgCss style={styles.icon} xml={xml}/>
        </View>
      }
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 24,
    height: 24
  },
  notificationContainer: {
    position: "absolute",
    top: -7.50,
    right: -5,
    width: 16,
    height: 16
  },
  icon: {
    width: 16,
    height: 16
  }
});

export default ChallengesIcon;