import React from "react"
import Svg, { Rect, SvgCss } from "react-native-svg"

export default function ListIcon({ selected }) {
  if(selected) {
    const xml = `
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M6 8H2V4H6V8ZM21 8H8V4H21V8ZM2 20H6V16H2V20ZM21 20H8V16H21V20ZM2 14H6V10H2V14ZM21 14H8V10H21V14Z" fill="url(#paint0_radial)"/>
        <defs>
          <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(18.8491 17.4444) rotate(164.764) scale(12.2612 47.8812)">
            <stop stop-color="#7A8AFF"/>
            <stop offset="1" stop-color="#B16BF2"/>
          </radialGradient>
        </defs>
      </svg>
    `;
    
    return(
      <SvgCss xml={xml}/>
    );
  }
  else {
    return(
      <Svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <Rect width="4.21053" height="4" transform="matrix(1 0 0 -1 2 8)" stroke="#798090"/>
        <Rect width="13.6842" height="4" transform="matrix(1 0 0 -1 8.3158 8)" stroke="#798090"/>
        <Rect width="4.21053" height="4" transform="matrix(1 0 0 -1 2 20)" stroke="#798090"/>
        <Rect width="13.6842" height="4" transform="matrix(1 0 0 -1 8.3158 20)" stroke="#798090"/>
        <Rect width="4.21053" height="4" transform="matrix(1 0 0 -1 2 14)" stroke="#798090"/>
        <Rect width="13.6842" height="4" transform="matrix(1 0 0 -1 8.3158 14)" stroke="#798090"/>
      </Svg>
    )
  }
}