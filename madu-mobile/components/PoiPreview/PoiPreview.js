import React from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import Picto from '../_shared/Picto/Picto';
import PoiLink from '../../components/PoiLlink/PoiLink';
import styled from "./poipreview.scss";

const PoiPreview = ({
    onClick,
    data
}) => {
    return (
        <TouchableOpacity onPress={() => onClick()} style={styled["poipreview"]}>
            <View style={styled["poipreview-img"]}>
                <Image
                    source={require('../../assets/images/picturepoi.jpg')}
                />
                {/* <Image
                    // resizeMode="cover"
                    source={{uri: `http://localhost:8080/${data.images[0].uri}`}}
                /> */}
            </View>
            <View style={styled["poipreview-infos"]}>
                <View style={styled["poipreview-infos_title"]}>
                    <Text style={styled["poipreview-infos_title_gs"]}> {data.greenscore}% </Text>
                    <Text style={styled["poipreview-infos_title_name"]}> {data.name} </Text>
                </View>
                <Text style={[styled["poipreview-infos_address"], {marginBottom: 0}]}> {data.localisation.address} </Text>
                <Text style={styled["poipreview-infos_address"]}> {data.localisation.zip_code + ' ' + data.localisation.city} </Text>
                <PoiLink href={`tel:+${data.phone.slice(1)}`} style={{ color: '#2F80ED', fontWeight: 'bold', marginBottom: 10 }}> {data.phone} </PoiLink>
                <View style={styled["poipreview-infos_picto"]}>
                    <Picto 
                        storeType={data.store_type}
                    />
                </View>
            </View>
        </TouchableOpacity>
    );
}

export default PoiPreview;