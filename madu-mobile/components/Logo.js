import React from "react";
import { SvgCss } from "react-native-svg";

export default () => {
  const xml = `
    <svg width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path opacity="0.9" fill-rule="evenodd" clip-rule="evenodd" d="M18.3016 13.25C11.8528 13.25 6.625 18.4778 6.625 24.9266V39.75H14.4093V23.9809C14.4093 21.8313 16.1519 20.0887 18.3015 20.0887C20.4511 20.0887 22.1937 21.8313 22.1937 23.9809V39.75H22.1938H29.9779H29.9781V24.9266L29.9779 24.8506V23.9809C29.9779 21.8313 31.7205 20.0887 33.8701 20.0887C36.0197 20.0887 37.7623 21.8313 37.7623 23.9809V39.75H45.547V24.9266C45.547 18.4778 40.3192 13.25 33.8704 13.25C30.88 13.25 28.1521 14.3742 26.0863 16.223C26.0862 16.2229 26.086 16.2228 26.0859 16.2227C26.0858 16.2227 26.0857 16.2228 26.0856 16.2229C24.0198 14.3742 21.292 13.25 18.3016 13.25Z" fill="url(#paint0_radial)"/>
      <defs>
        <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(41.1407 35.5174) rotate(167.581) scale(24.8151 80.269)">
          <stop stop-color="#7A8AFF"/>
          <stop offset="1" stop-color="#B16BF2"/>
        </radialGradient>
      </defs>
    </svg>
  `;
  return (
    <SvgCss xml={xml} width={50} height={50}/>
  );
}
