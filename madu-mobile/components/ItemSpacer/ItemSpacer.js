import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import styled from "./itemspacer.scss";
import PropTypes from "prop-types";

const colorStyle = {
  default: {
    color: "black",
  },
  alert: {
    color: "#EB5757",
  },
};

const ItemSpacer = ({ children, style, type, color, onPress }) => {
  return (
    <View style={{ ...styled["itemspacer"], ...style }}>
      {type === "text" ? (
        <Text style={{ ...colorStyle[color] }}>{children}</Text>
      ) : (
        <TouchableOpacity onPress={onPress}>
          <Text style={{ ...colorStyle[color] }}>{children}</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

ItemSpacer.defaultProps = {
  type: "text",
  color: "default",
  style: {},
};

ItemSpacer.propTypes = {
  onPress: PropTypes.func,
  type: PropTypes.oneOf(["text", "button"]),
  color: PropTypes.oneOf(["default", "alert"]),
  style: PropTypes.object.isRequired,
};

export default ItemSpacer;
