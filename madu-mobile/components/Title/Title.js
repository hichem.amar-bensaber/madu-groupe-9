import React from "react";
import { View, Text } from "react-native";
import styled from "./title.scss";
import PropTypes from "prop-types";

const sizeStyle = {
  normal: {
    fontSize: 30,
    fontFamily: "RobotoCondensed_700Bold",
  },
  small: {
    fontSize: 16,
    fontFamily: "RobotoCondensed_700Bold",
  },
};

const colorStyle = {
  default: {
    color: "black",
  },
  blue: {
    color: "#406080",
  },
  pink: {
    color: "#B259A4",
  },
};
const Title = ({ children, size, color, style }) => {
  return (
    <View>
      <Text
        style={{
          ...styled["title"],
          ...sizeStyle[size],
          ...colorStyle[color],
          ...style,
        }}
      >
        {children}
      </Text>
    </View>
  );
};

Title.defaultProps = {
  size: "normal",
  color: "default",
  style: {},
};

Title.propTypes = {
  level: PropTypes.oneOf(["default", "blue", "pink"]),
  size: PropTypes.oneOf(["normal", "small"]),
  children: PropTypes.string.isRequired,
  style: PropTypes.object,
};
export default Title;
