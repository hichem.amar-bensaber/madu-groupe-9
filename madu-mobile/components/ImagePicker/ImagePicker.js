import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, StyleSheet, Image, Text } from "react-native";
import * as ExpoImagePicker from "expo-image-picker";
import styled from "./imagepicker.scss";
import Constants from "expo-constants";
import PropTypes from "prop-types";
import { updateUserById } from "../../utils/API";

const ImagePicker = ({ style, url, data }) => {
  const [image, setImage] = useState(url);

  useEffect(() => {
    (async () => {
      if (Constants.platform.ios) {
        const {
          status,
        } = await ExpoImagePicker.requestCameraRollPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ExpoImagePicker.launchImageLibraryAsync({
      mediaTypes: ExpoImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      base64: true,
      aspect: [4, 3],
      quality: 1,
    });

    if(result.cancelled) return console.log(result);

    let updateData = {
      profile_pic: {
        uri: result.base64
      }
    };

    let fetchAPI = await updateUserById(data._id, data.userToken, updateData);
    if(fetchAPI.success) {
      setImage(result.uri);
    }
    else console.log(fetchAPI.message);
  };

  return (
    <View style={{ ...styled["imagepicker"], ...style }}>
      <TouchableOpacity style={styled["imagepicker-btn"]} onPress={pickImage}>
        {image ? (
          <Image
            source={{ uri: image }}
            style={{ minWidth: "100%", height: "100%" }}
          />
        ) : (
          <Text style={styled["imagepicker-btn-txt"]}>+</Text>
        )}
      </TouchableOpacity>
    </View>
  );
};
ImagePicker.defaultProps = {
  style: {},
  url: null,
};

ImagePicker.propTypes = {
  style: PropTypes.object,
  data: PropTypes.object,
  url: PropTypes.string.isRequired,
};

export default ImagePicker;
