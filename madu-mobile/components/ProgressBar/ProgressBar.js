import React from "react";
import { View, Text } from "react-native";
import styled from "./progressbar.scss";
import { LinearGradient } from "expo-linear-gradient";
import PropTypes from "prop-types";

const backgroundStyle = {
  novice: {
    backgroundColor: "#BBEFD1",
  },
  adventurer: {
    backgroundColor: "#ffdfd4",
  },
  master: {
    backgroundColor: "#efdeff",
  },
};
const barStyle = {
  novice: {
    colors: ["#009177", "#27AE60"],
    start: [1, 0],
  },
  adventurer: {
    colors: ["#FF887A", "#FFC299"],
    start: [1, 0],
  },
  master: {
    colors: ["#E27EF3", "#A1B3F8"],
    start: [1, 0],
  },
};

const levelText = {
  novice: "Novice",
  adventurer: "Aventurier",
  master: "Maître",
};
const ProgressBar = ({ level, value, style }) => {
  const actifText =
    value !== 0
      ? { opacity: 1, color: "white" }
      : { color: "black", opacity: 0.3 };

  const curPosition =
    value === 0 ? { display: "none" } : { width: `${value}%` };

  return (
    <View style={{ ...styled["progressbar"], ...style }}>
      <Text style={{ ...styled["progressbar-bar-txt"], ...actifText }}>
        {levelText[level]}
      </Text>
      <LinearGradient
        style={{
          ...styled["progressbar-bar"],
          ...curPosition,
        }}
        {...barStyle[level]}
      ></LinearGradient>
      <View
        style={{
          ...styled["progressbar-background"],
          ...backgroundStyle[level],
        }}
      />
    </View>
  );
};
ProgressBar.defaultProps = {
  level: "novice",
  value: 0,
  style: {},
};

ProgressBar.propTypes = {
  level: PropTypes.oneOf(["novice", "adventurer", "master"]),
  style: PropTypes.object,
  value: PropTypes.number.isRequired,
};
export default ProgressBar;
