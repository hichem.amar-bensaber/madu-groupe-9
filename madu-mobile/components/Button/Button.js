import React from "react";
import { View, TouchableOpacity, Text } from "react-native";
import PropTypes from "prop-types";
import styled from "./button.scss";
const Button = ({ onPress, children }) => {
  return (
    <View>
      <TouchableOpacity onPress={onPress} style={styled["button"]}>
        <Text style={styled["button-text"]}>{children}</Text>
      </TouchableOpacity>
    </View>
  );
};

Button.propTypes = {
  onPress: PropTypes.func.isRequired,
  children: PropTypes.string.isRequired,
};
export default Button;
