import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import Svg, { Path, Circle } from "react-native-svg";

const absoluteStyle = (absolute) => (type) => {
  if (absolute) {
    switch (type) {
      case "back":
        return {
          position: "absolute",
          left: 10,
          top: 25,
        };
      case "simple":
        return {
          position: "absolute",
          left: 10,
          top: 40,
        };
    }
  }
  return {};
};
export default function BackArrowIcon({
  type = "back",
  style = {},
  closeModal,
  absolute = true,
}) {
  const getTypeStyle = absoluteStyle(absolute);
  if (type === "simple") {
    return (
      <TouchableOpacity
        onPress={() => closeModal()}
        style={(style)? style: getTypeStyle(type)}
      >
        <Svg
          width="48"
          height="48"
          viewBox="0 0 48 48"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <Path
            d="M29 20H16.83L22.42 14.41L21 13L13 21L21 29L22.41 27.59L16.83 22H29V20Z"
            fill="black"
          />
        </Svg>
      </TouchableOpacity>
    );
  } else {
    return (
      <TouchableOpacity
        onPress={() => closeModal()}
        style={{ ...getTypeStyle(type), ...style }}
      >
        <Svg
          width="48"
          height="48"
          viewBox="0 0 48 48"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <Circle opacity="0.6" cx="21" cy="21" r="21" fill="#1C1C1C" />
          <Path
            d="M29 20H16.83L22.42 14.41L21 13L13 21L21 29L22.41 27.59L16.83 22H29V20Z"
            fill="white"
          />
        </Svg>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  back: {
    left: 10,
    top: 25,
  },
  simple: {
    left: 10,
    top: 40,
  },
});