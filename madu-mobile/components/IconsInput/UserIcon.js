import React from 'react'
import Svg, { Path, SvgCss } from 'react-native-svg'

export default function UserIcon({ focus, color }) {
    if(focus) {
        const xml = `
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M7.5 6.49951C7.5 8.98051 9.519 10.9995 12 10.9995C14.481 10.9995 16.5 8.98051 16.5 6.49951C16.5 4.01851 14.481 1.99951 12 1.99951C9.519 1.99951 7.5 4.01851 7.5 6.49951ZM20 20.9995H21V19.9995C21 16.1405 17.859 12.9995 14 12.9995H10C6.14 12.9995 3 16.1405 3 19.9995V20.9995H20Z" fill="url(#paint0_radial)"/>
                <path d="M7.5 6.5C7.5 8.981 9.519 11 12 11C14.481 11 16.5 8.981 16.5 6.5C16.5 4.019 14.481 2 12 2C9.519 2 7.5 4.019 7.5 6.5ZM20 21H21V20C21 16.141 17.859 13 14 13H10C6.14 13 3 16.141 3 20V21H20Z" fill="url(#paint1_radial)"/>
                <defs>
                    <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(18.9623 17.9648) rotate(161.149) scale(11.8427 55.7696)">
                        <stop stop-color="#7A8AFF"/>
                        <stop offset="1" stop-color="#B16BF2"/>
                    </radialGradient>
                    <radialGradient id="paint1_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(18.9623 17.9653) rotate(161.149) scale(11.8427 55.7696)">
                        <stop stop-color="#7A8AFF"/>
                        <stop offset="1" stop-color="#B16BF2"/>
                    </radialGradient>
                </defs>
            </svg>
        `;

        return (
            <SvgCss xml={xml}/>
        );
    }
    else {
        return (
            <Svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <Path d="M7.5 6.5C7.5 8.981 9.519 11 12 11C14.481 11 16.5 8.981 16.5 6.5C16.5 4.019 14.481 2 12 2C9.519 2 7.5 4.019 7.5 6.5ZM20 21H21V20C21 16.141 17.859 13 14 13H10C6.14 13 3 16.141 3 20V21H20Z" fill={color}/>
                <Path d="M7.5 6.5C7.5 8.981 9.519 11 12 11C14.481 11 16.5 8.981 16.5 6.5C16.5 4.019 14.481 2 12 2C9.519 2 7.5 4.019 7.5 6.5ZM20 21H21V20C21 16.141 17.859 13 14 13H10C6.14 13 3 16.141 3 20V21H20Z" fill={color}/>
            </Svg>
        );
    }
}