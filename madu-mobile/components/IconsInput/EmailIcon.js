import React from "react";
import Svg, { Path, Defs, RadialGradient, LinearGradient, Stop, SvgCss } from "react-native-svg";

export default function EmailIcon({ focus, color }) {
    if(focus) {
        const xml = `
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M20 4H4C2.9 4 2.01 4.9 2.01 6L2 18C2 19.1 2.9 20 4 20H20C21.1 20 22 19.1 22 18V6C22 4.9 21.1 4 20 4ZM20 8L12 13L4 8V6L12 11L20 6V8Z" fill="url(#paint0_radial)"/>
                <defs>
                    <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(19.7359 17.4444) rotate(165.493) scale(12.863 48.0433)">
                        <stop stop-color="#7A8AFF"/>
                        <stop offset="1" stop-color="#B16BF2"/>
                    </radialGradient>
                </defs>
            </svg>
        `;

        return (
            <SvgCss xml={xml}/>
        );
    }
    else {
        return (
            <Svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <Path d="M20 4H4C2.9 4 2.01 4.9 2.01 6L2 18C2 19.1 2.9 20 4 20H20C21.1 20 22 19.1 22 18V6C22 4.9 21.1 4 20 4ZM20 8L12 13L4 8V6L12 11L20 6V8Z" fill={color} />
            </Svg>
        );
    }
}