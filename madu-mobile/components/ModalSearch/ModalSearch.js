import React, { useState } from 'react';
import { View, Text, SafeAreaView, FlatList, Alert } from 'react-native';
import PoiItem from './PoiItem';

import styled from './modalsearch.scss';


const ModalSearch = ({dataPoiItem, contextDataPoi, clearModal}) => {

    return(
        <View style={styled["modalsearch"]}>
            <Text style={styled["modalsearch_title"]}> Suggestions de points d'intérêts </Text>
            {
                dataPoiItem.map((item, _id) => 
                    <PoiItem onPress={() => clearModal()} dataPoiItemCard={item} dataContext={contextDataPoi} key={item._id}/>
                )
            }
        </View>
    )
}

export default ModalSearch;