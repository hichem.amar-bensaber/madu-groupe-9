import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import styled from './poiitem.scss';
import IconPoiItem from './IconPoiItem';

import { calculateDistanceCoords } from "../../utils/helpers";



const PoiItem = ({onPress, dataPoiItemCard, dataContext}) => {
    return (
        <TouchableOpacity style={styled["poiitem"]} onPress={() => onPress() }>
            <IconPoiItem dataIcon={dataPoiItemCard.store_type} />
            <Text style={styled["poiitem_name"]}> {dataPoiItemCard.name} </Text>
            <View style={styled["poiitem_distance"]}>
                <Text style={styled["poiitem_distance_text"]}> {
                calculateDistanceCoords(
                    dataContext.userData._client.coords.latitude,
                    dataContext.userData._client.coords.longitude,
                    dataPoiItemCard.coords.latitude,
                    dataPoiItemCard.coords.longitude)
                }m </Text>
                {/* <Text style={styled["poiitem_distance_text"]}> 50m </Text> */}
            </View>
        </TouchableOpacity>
    )
}

export default PoiItem;