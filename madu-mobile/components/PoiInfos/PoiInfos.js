import * as React from 'react';
import { StyleSheet, View, Image } from 'react-native';


export default function PoiInfos ({ 
    src = require("../../assets/images/PoiScreen/poiscreen-picto-location.png"), 
    text = "",
    children
}) {
    return (
        <View style={styles.container}>
            <Image source={src} alt="picto of poi"/>
            <View style={styles.content}>
                { children }
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 0.7,
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    content: {
        paddingHorizontal: 20
    }
});