import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import BackArrow from "../BackArrow/BackArrowIcon";
import styled from "./filterheader.scss";
import PropTypes from "prop-types";

const FilterHeader = ({ onSave, onBack, text }) => {
  return (
    <View style={styled["filterheader"]}>
      <BackArrow type="simple" absolute={false} closeModal={onBack} />
      <TouchableOpacity onPress={onSave}>
        <Text style={styled["filterheader-txt"]}>{text}</Text>
      </TouchableOpacity>
    </View>
  );
};
FilterHeader.defaultProps = {
  text: "Enregistrer et quitter",
};
FilterHeader.propTypes = {
  onSave: PropTypes.func.isRequired,
  onBack: PropTypes.func.isRequired,
  text: PropTypes.string,
};
export default FilterHeader;
