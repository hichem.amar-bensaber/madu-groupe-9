import React, { useState, useEffect } from "react";
import { View, TextInput, TouchableOpacity, Text } from "react-native";
import styles from "./input.scss";
import IconBack from "../IconBack/IconBack";
import IconSearch from "../IconSearch/IconSearch";
import OptionIcon from "../OptionIcon";
import CloseIcon from "../CloseIcon";
import PropTypes from "prop-types";
import ModalSearch from "../ModalSearch/ModalSearch";

const Input = ({
  onBlur,
  onFocus,
  text = "",
  onChangeText,
  onSwap,
  absolute = false,
  data,
  contextData
}) => {
  const [showBtnBack, setShowBtnBack] = useState(false);
  const [swap, setSwap] = useState(false);
  const [showModal, setShowModal] = useState(false)

  const onFocusable = () => setShowModal(!showModal);

  useEffect(() => {
    if (text !== "" && !showBtnBack) {
      setShowBtnBack(true);
    }
    if (text === "" && showBtnBack) {
      setShowBtnBack(false);
    }
  }, [text]);

  const onClear = () => {
    setShowBtnBack(false);
    onChangeText("");
    () => onBack();
    setShowModal(false)
  };

  const absoluteStyle = absolute
    ? { position: "absolute", left: "5%", top: 10, zIndex: 99999999 }
    : { position: "relative" };

  return (
    <>
    {showModal ? 
        <ModalSearch dataPoiItem={data} clearModal={onClear} contextDataPoi={contextData} />
      : null
    }

    <View style={{ ...styles["input-container"], ...absoluteStyle }} >
      {showBtnBack && (
        <TouchableOpacity
          style={styles["input-container-btn"]}
          onPress={onClear}
        >
          <IconBack style={styles["input-container-btn-back"]} />
        </TouchableOpacity>
      )}
      {!showBtnBack && (
        <TouchableOpacity style={styles["input-container-btn"]}>
          <IconSearch />
        </TouchableOpacity>
      )}
      <TextInput
        style={styles["input-container-input"]}
        value={text}
        onChangeText={onChangeText}
        onFocus={onFocusable}
        onBlur={onBlur}
        placeholder="Rechercher..."
      />
      <TouchableOpacity
        onPress={onSwap}
        style={styles["input-container-option"]}
      >
        <OptionIcon style={styles["input-container-btn-option"]} />
      </TouchableOpacity>
    </View>
    </>
  );
};

Input.propTypes = {
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onSwap: PropTypes.func,
  absolute: PropTypes.bool,
  text: PropTypes.string.isRequired,
  onChangeText: PropTypes.func.isRequired,
};
export default Input;
