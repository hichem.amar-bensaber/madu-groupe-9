import React from "react";
import SVG, { LinearGradient, Text, Defs, Stop } from "react-native-svg";
import PropTypes from "prop-types";

const TextGradient = ({
  children,
  color,
  viewBox,
  height,
  fontSize,
  fontWeight,
}) => {
  const options = {
    id: "color",
    x1: "200",
    y1: "15",
    x2: "0",
    y2: "15",
    gradientUnits: "userSpaceOnUse",
  };
  return (
    <SVG viewBow={viewBox} width="100%" height="30">
      {color === "blue" ? (
        <Defs>
          <LinearGradient {...options}>
            <Stop stopColor="#7A8AFF" offset="0%" />
            <Stop stopColor="#D71CF8" offset="100%" />
          </LinearGradient>
        </Defs>
      ) : (
        <Defs>
          <LinearGradient {...options}>
            <Stop stopColor="#009177" offset="0%" />
            <Stop stopColor="#27AE60" offset="100%" />
          </LinearGradient>
        </Defs>
      )}
      <Text
        fill="url(#color)"
        x="50%"
        y="20"
        dominantBaseline="middle"
        textAnchor="middle"
        fontSize={fontSize}
        fontWeight={fontWeight}
      >
        {children}
      </Text>
    </SVG>
  );
};

TextGradient.defaultProps = {
  color: "green",
  viewBox: "0 0 200 30",
  height: "30",
  fontSize: "16",
  fontWeight: "bold",
};

TextGradient.propTypes = {
  color: PropTypes.oneOf(["green", "blue"]),
  viewBox: PropTypes.string.isRequired,
  height: PropTypes.string.isRequired,
  fontSize: PropTypes.string.isRequired,
  fontWeight: PropTypes.string.isRequired,
};
export default TextGradient;
