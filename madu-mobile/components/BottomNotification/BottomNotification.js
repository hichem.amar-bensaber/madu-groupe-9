import React, { useRef, useEffect } from "react";
import { StyleSheet, View, Dimensions, Animated, Text } from "react-native";

import IconAlertNotification from "./IconAlertNotification";


const BottomNotification = ({ text }) => {
    const fadeAnim = useRef(new Animated.Value(0)).current
    const transformAnim = useRef(new Animated.Value(-50)).current

    const fadeInAnim = () => {
        Animated.timing(
            fadeAnim,
            {
                toValue: 1,
                duration: 500,
            }
        ).start();
        Animated.timing(
            transformAnim,
            {
                toValue: 0,
                duration: 500,
            }
        ).start();
    }

    const fadeOutAnim = () => {
        Animated.timing(
            fadeAnim,
            {
                toValue: 0,
                duration: 500,
            }
        ).start();
        Animated.timing(
            transformAnim,
            {
                toValue: -75,
                duration: 500,
            }
        ).start();
    }

    useEffect(() => {
        fadeInAnim();
        setTimeout(() => fadeOutAnim(), 10000);
    }, []);

    return (
        <Animated.View style={[styles.container, { opacity: fadeAnim, bottom: transformAnim }]}>
            <View style={styles.iconContainer}>
                <IconAlertNotification/>
            </View>
            <Text style={styles.text}>{ text }</Text>
        </Animated.View>
    );
};

const styles = StyleSheet.create({
    container: {
        position: "absolute",
        left: 0,
        bottom: -75,
        width: Dimensions.get("screen").width - 30,
		height: 50,
		flexDirection: "row",
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 10,
        elevation: 10,
        backgroundColor: "#FFFFFF",
        borderRadius: 4,
        borderWidth: 2,
        borderColor: "#EB5757"
    },
    iconContainer: {
        width: 50,
		height: 50,
		alignItems: "center",
        justifyContent: "center"
    },
    text: {
        flex: 1,
        width: "100%",
        height: 45,
        textAlign: "left",
        textAlignVertical: "center",
        color: "#000000",
        // fontFamily: "Roboto",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: 14,
        lineHeight: 16,
        marginLeft: -5
    }
});

export default BottomNotification;