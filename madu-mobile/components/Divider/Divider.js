import React from "react";
import { View } from "react-native";
import styled from "./divider.scss";
const Divider = () => {
  return <View style={styled["divider"]}></View>;
};

export default Divider;
