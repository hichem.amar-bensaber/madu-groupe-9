import React from "react";
import { View, StyleSheet } from "react-native";
import BackgroundProfil from "../BackgroundProfil";
import styled from "./layoutprofil.scss";
import PropTypes from "prop-types";

const levelColor = {
  novice: "green",
  aventurier: "yellow",
  master: "blue",
};
const LayoutProfil = ({ children, level }) => {
  return (
    <View style={styled["layoutprofil"]}>
      <BackgroundProfil
        style={styled["layoutprofil-bg"]}
        background={levelColor[level]}
      />
      <View style={{ ...styled["layoutprofil-info"] }}>{children}</View>
    </View>
  );
};
LayoutProfil.defaultProps = {
  level: "novice",
};
LayoutProfil.propTypes = {
  level: PropTypes.oneOf(["novice", "aventurier", "master"]),
};
export default LayoutProfil;
