import React from "react";
import { View, TouchableHighlight, Text } from "react-native";
import PropTypes from "prop-types";
import RightArrowIcon from "../RightArrowIcon";
import styled from "./buttonchallenge.scss";

const backgroundStyle = {
  default: {
    backgroundColor: "#f5f5f5",
  },
  blue: {
    backgroundColor: "rgba(64, 96, 128, 0.1)",
  },
  pink: {
    backgroundColor: "rgba(178, 89, 165, 0.1)",
  },
};
const colorStyle = {
  default: {
    color: "#f5f5f5",
  },
  blue: {
    color: "#406080",
  },
  pink: {
    color: "#B259A4",
  },
};
const ButtonChallenge = ({
  title,
  points,
  color,
  dayLeft,
  isOver,
  onPress,
}) => {
  return (
    <View style={{ marginBottom: 10 }}>
      <TouchableHighlight
        activeOpacity={0.6}
        underlayColor="#f5f5f5"
        onPress={onPress}
      >
        {isOver ? (
          <View
            style={{
              ...styled["buttonchallenge"],
              backgroundColor: colorStyle[color].color,
            }}
          >
            <Text numberOfLines={1} style={styled["buttonchallenge-overtext"]}>
              {title}
            </Text>
            <Text style={styled["buttonchallenge-overtext-bold"]}>Terminé</Text>
          </View>
        ) : (
          <View
            style={{ ...styled["buttonchallenge"], ...backgroundStyle[color] }}
          >
            <View>
              <Text
                numberOfLines={1}
                style={{
                  ...styled["buttonchallenge-title"],
                  ...colorStyle[color],
                }}
              >
                {title}
              </Text>
              <Text
                style={{
                  ...styled["buttonchallenge-points"],
                  ...colorStyle[color],
                }}
              >
                {points} pts
              </Text>
            </View>
            <View style={styled["buttonchallenge-aside"]}>
              <Text
                style={{
                  ...styled["buttonchallenge-aside-limit"],
                  ...colorStyle[color],
                }}
              >
                {dayLeft} jours
              </Text>
              <RightArrowIcon />
            </View>
          </View>
        )}
      </TouchableHighlight>
    </View>
  );
};
ButtonChallenge.defaultProps = {
  color: "default",
  isOver: false,
};
ButtonChallenge.propTypes = {
  onPress: PropTypes.func.isRequired,
  isOver: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  title: PropTypes.string.isRequired,
  dayLeft: PropTypes.string.isRequired,
  points: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
export default ButtonChallenge;
