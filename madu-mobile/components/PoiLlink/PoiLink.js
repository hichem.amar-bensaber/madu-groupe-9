import React from 'react';
import { Text } from 'react-native';
import * as Linking from 'expo-linking';

import styled from './poilink.scss';

export default class PoiLink extends React.Component {
    _handlePress = () => {
        Linking.openURL(this.props.href);
        this.props.onPress && this.props.onPress();
    };

    render() {
        return (
        <Text style={styled["poilink-text"]} {...this.props} onPress={()=>this._handlePress()}>
            {this.props.children}
        </Text>
        );
    }
}