import React from "react";
import { SvgCss } from "react-native-svg";

export default () => {
  
  const xml = `
    <svg width="100%" height="100%" fill="none" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid meet">
      <g clip-path="url(#clip0)">
        <rect opacity="0.6" width="100%" height="100%" fill="url(#paint0_radial)"/>
        <path fill-rule="evenodd" clip-rule="evenodd" d="M86.8393 94.9971C95.2851 83.3214 96.5236 67.9221 94.7273 53.6555C93.1987 41.5157 86.0622 31.5044 78.9378 21.5102C78.3895 20.741 77.8413 19.972 77.2958 19.2021C76.3603 17.882 75.4341 16.5479 74.507 15.2126C67.7385 5.4637 60.9246 -4.35075 50.1404 -9.26423C37.0036 -15.2496 21.8715 -16.283 7.96083 -12.3882C-6.31233 -8.392 -18.8204 0.525183 -27.4962 12.4727C-36.152 24.3926 -39.8987 38.9579 -39.9975 53.6555C-40.0969 68.4316 -37.2198 83.6035 -28.0527 95.2399C-19.0608 106.654 -4.95051 112.345 9.155 116.048C22.5999 119.578 36.4355 119.328 49.8517 115.692C63.8456 111.9 78.3746 106.699 86.8393 94.9971ZM406.791 155.476C402.668 140.305 389.017 126.478 373.212 125.521C356.811 124.528 344.605 138.562 334.068 151.073C333.772 151.426 333.473 151.779 333.174 152.134C322.404 164.906 310.194 179.386 314.384 195.374C317.898 208.789 331.689 214.545 345.017 220.108C347.749 221.248 350.462 222.38 353.063 223.569C356.142 224.976 359.414 226.743 362.767 228.555C375.493 235.43 389.378 242.931 398.283 233.712C405.089 226.666 400.679 217.566 395.989 207.891C392.47 200.629 388.793 193.043 389.585 185.759C390.19 180.192 394.544 175.834 398.765 171.608C403.735 166.633 408.522 161.841 406.791 155.476Z" fill="url(#paint1_radial)"/>
      </g>
      <defs>
        <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(332.434 226.035) rotate(166.971) scale(240.299 812.851)">
          <stop stop-color="#009177"/>
          <stop offset="1" stop-color="#27AE60"/>
        </radialGradient>
        <radialGradient id="paint1_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(356.531 197.639) rotate(169.642) scale(283.028 770.985)">
          <stop stop-color="#009177"/>
          <stop offset="1" stop-color="#27AE60"/>
        </radialGradient>
        <clipPath id="clip0">
          <rect width="100%" height="100%" fill="white"/>
        </clipPath>
      </defs>
    </svg>  
  `;

  return (
    <SvgCss xml={xml} />
  );
}
