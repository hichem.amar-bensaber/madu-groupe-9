import React from "react";
import { View } from "react-native";
// import { LinearGradient } from "expo-linear-gradient";
import styled from "./challengelayout.scss";
import PropTypes from "prop-types";

// const colors = {
//   green: {
//     colors: ["#079342", "#27AE60"],
//     location: [0, 1],
//     start: { x: 1, y: 1 },
//     end: { x: 0, y: 0 },
//   },
//   yellow: {
//     colors: ["#FEA84A", "#F88B15", "#FF782D"],
//     location: [0.1669, 0.48, 1],
//     start: { x: 0, y: 0 },
//     end: { x: 1, y: 1 },
//   },
//   blue: {
//     colors: ["#31409F", "#8C9AF9"],
//     start: { x: 1, y: 1 },
//     end: { x: 0, y: 0 },
//     location: [0, 1],
//   },
// };

const ChallengeLayout = ({ children, style }) => {
  return (
    <View style={styled["challengelayout"]}>
      <View style={{ ...styled["challengelayout-container"], ...style }}>
        {children}
      </View>
    </View>
  );
};

ChallengeLayout.propTypes = {
  children: PropTypes.object.isRequired,
  style: PropTypes.object,
};
export default ChallengeLayout;
