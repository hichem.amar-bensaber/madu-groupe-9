import React, { useState } from "react";
import { StyleSheet, View, Text, Image, Modal, TouchableOpacity } from "react-native";
import Svg, { Path, Defs, Stop, RadialGradient } from "react-native-svg";
import { LinearGradient } from 'expo-linear-gradient';

import styled from "./greenscore.scss";

import PropTypes from "prop-types";

const colors = {
    blue: {
        colors: ["#B16BF2", "#7A8AFF"],
        start: { x: 1, y: 1 },
        end: { x: 0, y: 0 },
        location: [0, 1],
        shadowOpacity: 0.60,
        shadowColor: '#000',
        shadowRadius: 1,
        elevation: 8,
    }
}

const styles = StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 22
    },
    shadowBadge: {
        shadowColor: "#AC53FF",
        shadowOffset: {
          width: 0,
          height: 0
        },
        shadowOpacity: 0.50,
        shadowRadius: 5,
        elevation: 2
    },
    modalView: {
      margin: 40,
      backgroundColor: "#fff",
      borderRadius: 3,
      padding: 35,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 0
      },
      shadowOpacity: 0.50,
      shadowRadius: 10,
      elevation: 10
    },
    modalTitle: {
        color: '#000000',
        fontWeight: 'bold',
        marginVertical: 20,
        textAlign: "center",
        fontSize: 16,
    },
    modalText: {
      marginBottom: 80,
      textAlign: "center",
      color: "#666666"
    },
    modalClose: {
      textAlign: "center",
      color: "#666666",
      textTransform: "uppercase"
    }
  });
  
  
  const GreenScore = ({dataGreenScore, dataGreenScoreDescription, color="blue"}) => {
    const [modalVisible, setModalVisible] = useState(false);
    return (
        <>
        <TouchableOpacity onPress={() => {setModalVisible(true)}} style={styles.shadowBadge}>
            <LinearGradient {...colors[color]} style={styled["greenscore-container"]}>
                <View style={styled["greenscore-container_white"]}>
                    <Image
                        source={require('../../assets/images/GreenScore/madu.png')}
                    />
                    <Text style={styled["greenscore-container_number"]}> { dataGreenScore }% </Text>
                </View>
            </LinearGradient>
        </TouchableOpacity>
        <View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                Alert.alert("Modal has been closed.");
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Image
                            source={require('../../assets/images/GreenScore/logo-gs-modal.png')}
                        />
                        <Text style={styles.modalTitle}> Greenscore </Text>
                        <Text style={styles.modalText}> { dataGreenScoreDescription } </Text>
                        <TouchableOpacity onPress={() => {setModalVisible(!modalVisible)}}>
                            <Text style={styles.modalClose}> Fermer </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </View>
        </>

    )
}

    GreenScore.defaultProps = {
        dataGreenScore: 10
    };

    GreenScore.propTypes = {
        dataGreenScore: PropTypes.number,
    }

export default GreenScore;