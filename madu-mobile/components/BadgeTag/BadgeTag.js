import React from "react";
import { View, Text } from "react-native";
import Svg, { Path } from "react-native-svg";
import styled from "./badgetag.scss";

const BadgeTag = ({tag}) => (
    <>
    { tag.terrace && (
        <View style={styled["pictotag-bio"]}>
            <Svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Path d="M11.3808 0.226921C11.2642 -0.0660473 10.9308 -0.0777661 10.7913 0.198796C10.1454 1.46208 8.98917 2.24958 7.66833 2.24958H6.00167C3.79333 2.24958 2.00167 4.2652 2.00167 6.74958C2.00167 6.91364 2.01833 7.07067 2.03292 7.23005C3.36208 6.15895 5.28083 5.24958 8.00167 5.24958C8.185 5.24958 8.335 5.41833 8.335 5.62458C8.335 5.83083 8.185 5.99958 8.00167 5.99958C2.76417 5.99958 0.543334 9.6113 0.0516677 10.9683C-0.0858322 11.3504 0.0766677 11.7863 0.416251 11.9433C0.757918 12.1027 1.14542 11.9175 1.28708 11.5379C1.31833 11.4535 1.7225 10.4152 2.785 9.41442C3.46 10.4433 4.74333 11.4254 6.42875 11.2238C9.69958 10.9566 12.0017 7.65661 12.0017 3.61598C12.0017 2.43942 11.7767 1.22067 11.3808 0.226921Z" fill="white"/>
            </Svg>
        </View>
        ) 
    }
    { tag.veggie && (
        <View style={styled["pictotag-gluttenfree"]}>
        <Text style={ styled["pictotag-gluttenfree_value"]}> GF </Text>
        </View>
        ) 
    }
    { tag.vegan && (
        <View style={ styled["pictotag-vegan"]}>
        <Text style={ styled["pictotag-vegan_value"]}> VGN </Text>
        </View>
        ) 
    }
    </>
);

export default BadgeTag;