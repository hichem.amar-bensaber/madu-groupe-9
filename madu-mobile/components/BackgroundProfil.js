import React from "react";
import { View } from "react-native";
import BackgroundProfilBlue from "./BackgroundProfilBlue";
import BackgroundProfilGreen from "./BackgroundProfilGreen";
import BackgroundProfilYellow from "./BackgroundProfilYellow";
import PropTypes from "prop-types";

const backgroundComponent = {
  yellow: <BackgroundProfilYellow />,
  blue: <BackgroundProfilBlue />,
  green: <BackgroundProfilGreen />,
};
const BackgroundProfil = ({ background, style }) => {
  return <View style={style}>{backgroundComponent[background]}</View>;
};
BackgroundProfil.defaultProps = {
  background: "green",
  style: {},
};
BackgroundProfil.propTypes = {
  background: PropTypes.oneOf(["green", "yellow", "blue"]),
  style: PropTypes.object,
};
export default BackgroundProfil;
