import React, { useState, useEffect } from "react";
import { View, StyleSheet, TextInput, Text } from "react-native";

import EmailIcon from "../IconsInput/EmailIcon";
import UserIcon from "../IconsInput/UserIcon";
import PasswordIcon from "../IconsInput/PasswordIcon";

const InputIcons = ({ type, placeholder, defaultValue = null, onChange, error = false }) => {

	const [focus, setFocus] = useState(false);

	const renderIconType = (type, color) => {
		if (type === "email") return (<EmailIcon focus={(focus && !error)? true: false} color={(color) ? color : null} />);
		else if (type === "password") return (<PasswordIcon focus={(focus && !error)? true: false} color={(color) ? color : null} />);
		else if (type === "user") return (<UserIcon focus={(focus && !error)? true: false} color={(color) ? color : null} />);
		else return (<UserIcon />);
	};

	if(error) {
		return (
			<View style={[styles.container, { borderColor: "#EB5757" }]}>
				<View style={styles.iconContainer}>
					{renderIconType(type, "#EB5757")}
				</View>
				<TextInput
					style={[styles.input, { color: "#EB5757" }]}
					onChangeText={(value) => onChange(value)}
					secureTextEntry={(type === "password") ? true : false}
					defaultValue={defaultValue}
					placeholder={placeholder}
					numberOfLines={1}
					placeholderTextColor={"#EB5757"}
					onFocus={() => setFocus(true)}
					onBlur={() => setFocus(false)}
				/>
			</View>
		);
	}
	else if (focus) {
		return (
			<View style={[styles.container, { borderColor: "#9c74ff" }]}>
				<View style={styles.iconContainer}>
					{renderIconType(type, "#798090")}
				</View>
				<TextInput
					style={[styles.input, { color: "#1C1C1C" }]}
					onChangeText={(value) => onChange(value)}
					secureTextEntry={(type === "password") ? true : false}
					defaultValue={defaultValue}
					placeholder={placeholder}
					numberOfLines={1}
					placeholderTextColor={"#ABABAB"}
					onFocus={() => setFocus(true)}
					onBlur={() => setFocus(false)}
				/>
			</View>
		);
	}
	else {
		return (
			<View style={[styles.container, { borderColor: "#DDDDDD" }]}>
				<View style={styles.iconContainer}>
					{renderIconType(type, "#798090")}
				</View>
				<TextInput
					style={[styles.input, { color: "#1C1C1C" }]}
					onChangeText={(value) => onChange(value)}
					secureTextEntry={(type === "password") ? true : false}
					defaultValue={defaultValue}
					placeholder={placeholder}
					numberOfLines={1}
					placeholderTextColor={"#ABABAB"}
					onFocus={() => setFocus(true)}
					onBlur={() => setFocus(false)}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		width: "100%",
		height: 50,
		flexDirection: "row",
		backgroundColor: "#FFFFFF",
		borderRadius: 8,
		borderWidth: 2,
		marginBottom: 10
	},
	iconContainer: {
		width: 50,
		height: 50,
		alignItems: "center",
		justifyContent: "center"
	},
	input: {
		flex: 1,
		height: 50,
		marginRight: 15
	}
});

export default InputIcons;