import React from "react";
import { View, Text } from "react-native";
import styled from "./header.scss";
import Logo from "../Logo";
import GreenScoreHeader from "../GreenScoreHeader/GreenScoreHeader";

const Header = ({ xp }) => {
  return (
    <View style={styled.header}>
      <View style={styled["header-container"]}>
        <GreenScoreHeader style={styled["header-img"]}>{ xp }</GreenScoreHeader>
        <Logo style={styled["header-img"]}/>
        <Text style={styled["header-img"]}></Text>
      </View>
    </View>
  );
};

export default Header;
