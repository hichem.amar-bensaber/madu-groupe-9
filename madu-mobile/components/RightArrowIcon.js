import React from "react";
import { View } from "react-native";
import Svg, { Path } from "react-native-svg";
export default ({ style = {}, color = "#ABABAB" }) => (
  <Svg
    width="8"
    height="12"
    viewBox="0 0 8 12"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <Path
      fill-rule="evenodd"
      clip-rule="evenodd"
      d="M3.43323e-05 1.41L1.41003 0L7.41003 6L1.41003 12L3.43323e-05 10.59L4.58003 6L3.43323e-05 1.41Z"
      fill={color}
    />
  </Svg>
);
