import React from "react";
import { View } from "react-native";
import styled from "./badgeaccess.scss";
import AuditifIcon from "../IconAccess/AuditifIcon";
import EyeIcon from "../IconAccess/EyeIcon";
import MoteurIcon from "../IconAccess/MoteurIcon";


const BadgeAccess = ({dataHandicap}) => {
    return (
        <View style={styled["badgeaccess"]}>
            <AuditifIcon checkAccess={dataHandicap} />
            <EyeIcon checkAccess={dataHandicap} />
            <MoteurIcon checkAccess={dataHandicap} />
        </View>
    )
}
export default BadgeAccess;