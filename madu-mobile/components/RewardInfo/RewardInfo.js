import React from "react";
import { View, Text } from "react-native";
import styled from "./rewardinfo.scss";
import PropTypes from "prop-types";
import TextGradient from "../TextGradient/TextGradient";
const RewardInfo = ({ points, reward, children, style }) => {
  return (
    <View style={styled["rewardinfo"]}>
      <View style={{ ...styled["rewardinfo-content"], ...style }}>
        <View style={styled["rewardinfo-content-container"]}>
          <Text style={styled["rewardinfo-content-points"]}>
            {points} points{" "}
            <Text style={styled["rewardinfo-content-txt"]}>
              restants avant votre prochaine récompense !
            </Text>
          </Text>
        </View>
      </View>
      <TextGradient color="green">{children}</TextGradient>
    </View>
  );
};
RewardInfo.defaultProps = {
  style: {},
  points: 0,
  reward: "inconnu",
};

RewardInfo.propTypes = {
  reward: PropTypes.string.isRequired,
  points: PropTypes.number.isRequired,
  children: PropTypes.string.isRequired,
};
export default RewardInfo;
