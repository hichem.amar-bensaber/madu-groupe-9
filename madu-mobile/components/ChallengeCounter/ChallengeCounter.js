import React from "react";
import { View, Text } from "react-native";
import styled from "./challengecounter.scss";
import PropTypes from "prop-types";
const ChallengeCounter = ({ children, type }) => {
  return (
    <View style={styled["challengecounter"]}>
      <View style={styled["challengecounter-text"]}>
        <Text style={styled["challengecounter-text-counter"]}>{children}</Text>
        <Text style={styled["challengecounter-text-counter-shadow"]}>
          {children}
        </Text>
      </View>
      <View style={styled["challengecounter-text"]}>
        <Text style={styled["challengecounter-text-type"]}>{type}</Text>
        <Text style={styled["challengecounter-text-type-shadow"]}>{type}</Text>
      </View>
    </View>
  );
};
ChallengeCounter.defaultProps = {
  type: "défi",
};
ChallengeCounter.propTypes = {
  children: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};
export default ChallengeCounter;
