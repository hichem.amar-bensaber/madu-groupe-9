import React from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";
import { LinearGradient } from "expo-linear-gradient";

const LoginButton = ({ onPress, text, textColor, type = null }) => {
  if (type === "transparent") {
    return (
      <TouchableOpacity onPress={() => onPress()} style={[styles.linear, { backgroundColor: "#FFFFFF" }]}>
        <Text style={[styles.text, { color: textColor }]}>{text}</Text>
      </TouchableOpacity>
    );
  }
  else {
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        colors={["#B16BF2", "#7A8AFF"]} style={styles.linear}>
        <TouchableOpacity onPress={() => onPress()} style={styles.button}>
          <Text style={[styles.text, { color: textColor }]}>{text}</Text>
        </TouchableOpacity>
      </LinearGradient>
    );
  }
};

const styles = StyleSheet.create({
  linear: {
    flex: 1,
    width: "100%",
    maxHeight: 50,
    borderRadius: 8,
    marginTop: 20,
    marginBottom: 20
  },
  button: {
    flex: 1,
    width: "100%",
    height: "100%",
    borderRadius: 8
  },
  text: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
    textAlignVertical: "center",
    fontSize: 18,
    fontWeight: "500",
    lineHeight: 22,
    letterSpacing: -0.3
  }
});

export default LoginButton;