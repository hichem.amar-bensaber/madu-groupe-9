import React, { useState } from "react";
import { View, Text, TouchableHighlight } from "react-native";
import styled from "./buttonquizz.scss";
import PropTypes from "prop-types";
import PinkCheckIcon from "../PinkCheckIcon";
const backgroundColor = (color, bool, disabled) => {
  switch (color) {
    case "validate":
      return {
        backgroundColor: !disabled ? "#B259A4" : "#d6d6d6",
        borderColor: !disabled ? "#B259A4" : "#e3e3e3",
      };
    case "correct":
      return {
        backgroundColor: "white",
        borderColor: "#B259A4",
      };
    case "normal":
      return { borderColor: bool ? "#B259A4" : "#FFEFFE" };
  }
};

const textColor = {
  normal: {
    color: "#B259A4",
  },
  correct: {
    color: "#B259A4",
  },
  validate: { color: "white" },
};
const underlayColor = {
  normal: "#FFEFFE",
  correct: "#FFEFFE",
  validate: "#B259A4",
};
const ButtonQuizz = ({
  children,
  onPress,
  style,
  disabled,
  color,
  selected,
}) => {
  const [isUnderlay, setIsUnderlay] = useState(false);

  return (
    <View style={{ ...styled["buttonquizz"], ...style }}>
      <TouchableHighlight
        style={{
          ...styled["buttonquizz-btn"],
          ...backgroundColor(color, selected, disabled),
        }}
        onPress={onPress}
        underlayColor={underlayColor[color]}
        disabled={disabled}
        onShowUnderlay={() => setIsUnderlay(true)}
      >
        <View style={styled["buttonquizz-btn-container"]}>
          <Text style={{ ...styled["buttonquizz-txt"], ...textColor[color] }}>
            {children}
          </Text>
          {color === "correct" && (
            <PinkCheckIcon style={styled["buttonquizz-btn-correct"]} />
          )}
        </View>
      </TouchableHighlight>
    </View>
  );
};
ButtonQuizz.defaultProps = {
  selected: false,
  disabled: false,
  activate: false,
  style: {},
  color: "normal",
};
ButtonQuizz.propTypes = {
  onPress: PropTypes.func.isRequired,
  style: PropTypes.object.isRequired,
  activate: PropTypes.bool,
  selected: PropTypes.bool.isRequired,
  children: PropTypes.string.isRequired,
};
export default ButtonQuizz;
