import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
// webpack reads by default the file "index.js" in the folder
import { Provider } from "react-redux";
import store from './store/index';
import dotEnv from "dotenv";
dotEnv.config();

// we used Middleware thunk wrap the store's dispatch method
// and call it asynchronously


ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
