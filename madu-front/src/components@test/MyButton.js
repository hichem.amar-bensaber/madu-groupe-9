import React, { useState } from "react";
import { Button } from "antd";

export const MyButton = () => {
  const [loading, setLoading] = useState(false);

  const enterLoading = () => {
    setLoading(!loading);
    setTimeout(() => {
      setLoading(loading);
    }, 3000);
  };

  return (
    <div>
      <Button type="primary" loading={loading} onClick={enterLoading}>
        <span>{loading ? "chargement" : "Clique"}</span>
      </Button>
    </div>
  );
};
