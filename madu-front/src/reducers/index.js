import { poiReducer } from "./data";
import { customersReducer } from "./data";
import { userReducer } from "./data";
import { combineReducers } from "redux";

const allReducers = combineReducers({
  customers: customersReducer,
  user: userReducer,
  poi: poiReducer
});

export default allReducers;
