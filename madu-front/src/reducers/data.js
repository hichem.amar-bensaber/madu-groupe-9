import {
  USER_LOGIN_INFOS,
  INSERT_CUSTOMERS,
  EDIT_CUSTOMER_PROPERTY,
  EDIT_CUSTOMERSLIST_PROPERTY,
  DELETE_CUSTOMER,
  INSERT_POI,
} from "../constants/index";

const user = { email: "", password: "" };
export const userReducer = (state = user, action) => {
  switch (action.type) {
    case USER_LOGIN_INFOS: {
      if (action.name === "email") {
        return {
          ...state,
          email: action.email,
        };
      } else if (action.name === "password") {
        return {
          ...state,
          password: action.password,
        };
      }
    }
  }
  return state;
};

// DEFAULT DATA (static)
const customers = [];
const poi = [];

export const customersReducer = (state = customers, action) => {
  switch (action.type) {
    case INSERT_CUSTOMERS:
      state = [...action.data];
      break;
    case DELETE_CUSTOMER:
      const newCustomers = customers.filter(
        (customer) => customer._id != action.id
      );
      state = [...newCustomers];
      break;
    /**
     * Edit customer property
     * @param {Object} action - contain payload
     * @param {string} action.value - value of new customer property
     * @param {string} action.key - id of customers
     * @param {string} action.name - name property
     * @returns {Array} return new customers array with modified customer
     */
    case EDIT_CUSTOMER_PROPERTY:
      const properties = action.name.split(".");

      const editCustomer = state.map((customer) => {
        if (action.key !== customer._id) {
          return customer;
        }

        if (properties.length === 2) {
          return {
            ...customer,
            [properties[0]]: {
              ...customer[properties[0]],
              [properties[1]]: action.value,
            },
          };
        } else {
          return {
            ...customer,
            [properties[0]]: action.value,
          };
        }
      });

      state = [...editCustomer];
      break;

    case EDIT_CUSTOMERSLIST_PROPERTY:
      const editCustomersList = state.slice(0);
      for (const customer of editCustomersList) {
        for (let user of customer.customersList) {
          if (action.key === user.id) {
            user[action.name] = action.value;
          }
        }
      }
      state = [...editCustomersList];
      break;
  }
  return state;
};

export const poiReducer = (state = poi, action) => {
  switch (action.type) {
    case INSERT_POI:
      state = [...action.data];
      break;
  }
  return state;
};
