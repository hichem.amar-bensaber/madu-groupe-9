import React from 'react'
import { Tag } from 'antd';
import './tagName.scss';


const TagName = ({role, color='#7282F7'}) => {
    return (
        <div className="containerTag">
            {
                role === "A" && (
                    <Tag color={color}> Admin </Tag>
                )
            }
            {
                role === "SA" && (
                    <Tag color={color}> Super admin </Tag>
                )
            }
            {
                role === "C" && (
                    <Tag color={color}> Clients </Tag>
                )
            }
        </div>
    )
}

export default TagName;
