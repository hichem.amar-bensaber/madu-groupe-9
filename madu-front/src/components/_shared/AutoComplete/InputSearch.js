import React, { useState } from "react";
import "./inputsearch.scss";
import { Row, Col, AutoComplete, Icon, Input } from "antd";

const { Option, OptGroup } = AutoComplete;

const InputSearch = () => {
  const data = [
    {
      title: "Libraries",
      children: [
        {
          title: "AntDesign",
          count: 10000,
        },
        {
          title: "AntDesign UI",
          count: 10600,
        },
      ],
    },
    {
      title: "Solutions",
      children: [
        {
          title: "AntDesign UI",
          count: 60100,
        },
        {
          title: "AntDesign",
          count: 30010,
        },
        {
          title: "AntDesign",
          count: 30010,
        },
      ],
    },
    {
      title: "Articles",
      children: [
        {
          title: "AntDesign design language",
          count: 100000,
        },
      ],
    },
  ];

  const searchData = data.map((res) => (
    <OptGroup key={res.title} label="label 1">
      {res.children.map((opt) => (
        <Option key={opt.title} value={opt.title}>
          {opt.title}
          <span>{opt.count} clients</span>
        </Option>
      ))}
    </OptGroup>
  ));

  return (
    <Row type="flex" justify="center">
      <Col span={22}>
        <AutoComplete
          className="certain-category-search"
          dropdownClassName="certain-category-search-dropdown"
          dropdownMatchSelectWidth={false}
          dropdownStyle={{ width: 300 }}
          size="large"
          style={{ width: "100%" }}
          dataSource={searchData}
          placeholder="Vous rechercher un client ?"
          optionLabelProp="value"
        >
          <Input
            suffix={<Icon type="search" className="certain-category-icon" />}
          />
        </AutoComplete>
      </Col>
    </Row>
  );
};

export default InputSearch;
