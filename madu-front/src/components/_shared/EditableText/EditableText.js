import React from "react";
import { Input } from "antd";
import "./editabletext.scss";

export const EditableText = ({
  editable = false,
  value,
  style,
  name,
  props,
  onChange,
  onPressEnter = undefined,
  id = undefined
}) => {
  const onValide = (e, ...arg) => {
    if (onPressEnter !== undefined) {
      //TODO
      // MAKE VALIDATE
      // CALL API
      onPressEnter(e, ...arg);
    }
  };

  return (
    <div className="editabletext" style={style}>
      {!editable ? (
        <div className="editabletext_text">{value}</div>
      ) : (
        <Input
          data-index={id}
          value={value}
          name={name}
          onChange={onChange}
          onPressEnter={onValide}
          {...props}
        />
      )}
    </div>
  );
};
