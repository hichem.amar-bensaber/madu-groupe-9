import React from "react";
import { getToken } from "../../../utils/token";
import { Redirect } from "react-router-dom";

const SecureRoute = ({ Component }) => {
  if (getToken()) {
    return <Component />;
  }

  return <Redirect to="/" />;
};

export default SecureRoute;
