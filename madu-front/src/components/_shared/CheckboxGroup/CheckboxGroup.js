import React, { forwardRef } from "react";
import { Checkbox } from "antd";
import "./checkboxgroup.scss";

const CheckboxGroup = (props) => {
  const { items, label } = props;

  return (
    <div>
      <label className="checkboxgroup_label">{label}</label>
      <Checkbox.Group options={items} />
    </div>
  );
};

export default forwardRef(CheckboxGroup);
