import React, { forwardRef } from "react";
import moment from "moment";
import { DatePicker } from "antd";
import locale from "antd/es/date-picker/locale/fr_FR";
import "moment/locale/fr";
import "./datepickergroup.scss";

const disabledDate = (current) => {
  // Can not select days before today and today
  return current && current < moment().endOf("day");
};

const range = (start, end) => {
  const result = [];
  for (let i = start; i < end; i++) {
    result.push(i);
  }
  return result;
};

const DatePickerGroup = (props, ref) => {
  const { label = "Label", size = "large", ...el } = props;
  return (
    <div className="datepickergroup">
      <label>{label}</label>
      <DatePicker
        locale={locale}
        size={size}
        placeholder="Choisir une date"
        format="DD-MM-YYYY HH:mm"
        disabledDate={disabledDate}
        showTime={{ format: "HH:mm" }}
        {...el}
      />
    </div>
  );
};

export default forwardRef(DatePickerGroup);
