import React from "react";
import "./maintitle.scss";

export const MainTitle = ({ text }) => {
  return <h1 className="maintitle">{text}</h1>;
};
