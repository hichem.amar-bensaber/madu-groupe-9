import React from "react";
import { Icon } from "antd";
import "./badgeAccess.scss";

const Hear = () => (
  <svg
    width="11"
    height="11"
    viewBox="0 0 11 11"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M7.21273 7.10617C8.20123 6.43457 8.9375 5.39988 8.9375 4.125C8.9375 3.21332 8.57534 2.33898 7.93068 1.69432C7.28602 1.04966 6.41168 0.6875 5.5 0.6875C4.58832 0.6875 3.71398 1.04966 3.06932 1.69432C2.42466 2.33898 2.0625 3.21332 2.0625 4.125V8.56303C2.0625 9.51393 2.83035 10.3125 3.78125 10.3125C4.73215 10.3125 5.21705 9.7552 5.44178 9.45312C5.7591 9.02752 6.33789 7.70086 7.21273 7.10617Z"
      stroke="#798090"
      strokeWidth="0.625"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M3.4375 6.53125V3.95312C3.4375 2.91328 4.36563 2.0625 5.5 2.0625C6.63437 2.0625 7.5625 2.91328 7.5625 3.95312"
      stroke="#798090"
      strokeWidth="0.625"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M3.4375 5.13489C3.97461 4.74817 5.15238 4.81262 5.15238 4.81262C5.71098 4.81262 6.03689 5.44469 5.71098 5.89973C5.71098 5.89973 4.91906 6.81067 4.81164 7.21887"
      stroke="#798090"
      strokeWidth="0.625"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

const Eye = () => (
  <svg
    width="12"
    height="11"
    viewBox="0 0 12 11"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M6 8.25C2.9514 8.25 1.5729 5.725 1.51988 5.6C1.49337 5.525 1.49337 5.45 1.51988 5.375C1.5729 5.275 2.9514 2.75 6 2.75C9.0486 2.75 10.4271 5.275 10.4801 5.4C10.5066 5.475 10.5066 5.55 10.4801 5.625C10.4271 5.725 9.0486 8.25 6 8.25ZM2.05007 5.5C2.34168 5.975 3.61414 7.75 6 7.75C8.38586 7.75 9.63181 5.975 9.94993 5.5C9.65832 5.025 8.38586 3.25 6 3.25C3.61414 3.25 2.36819 5.025 2.05007 5.5Z"
      fill="#798090"
    />
    <path
      d="M6 7.5625C4.75 7.5625 3.75 6.64583 3.75 5.5C3.75 4.35417 4.75 3.4375 6 3.4375C7.25 3.4375 8.25 4.35417 8.25 5.5C8.25 6.64583 7.25 7.5625 6 7.5625ZM6 3.89583C5.025 3.89583 4.25 4.60625 4.25 5.5C4.25 6.39375 5.025 7.10417 6 7.10417C6.975 7.10417 7.75 6.39375 7.75 5.5C7.75 4.60625 6.975 3.89583 6 3.89583Z"
      fill="#798090"
    />
    <path
      d="M6 6.875C5.16 6.875 4.5 6.27 4.5 5.5C4.5 5.335 4.62 5.225 4.8 5.225C4.98 5.225 5.1 5.335 5.1 5.5C5.1 5.9675 5.49 6.325 6 6.325C6.51 6.325 6.9 5.9675 6.9 5.5C6.9 5.0325 6.51 4.675 6 4.675C5.82 4.675 5.7 4.565 5.7 4.4C5.7 4.235 5.82 4.125 6 4.125C6.84 4.125 7.5 4.73 7.5 5.5C7.5 6.27 6.84 6.875 6 6.875Z"
      fill="#798090"
    />
  </svg>
);
const Mobility = () => (
  <svg
    width="11"
    height="11"
    viewBox="0 0 11 11"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g opacity="0.7">
      <path
        d="M1.375 8.40583C1.375 8.40583 1.83333 3.24958 3.20833 1.375L5.5 1.83333L5.04167 3.24958H4.125V6.53125H4.58333C5.5 5.12417 7.3975 4.61083 8.54333 5.12417C10.0558 5.82542 9.91833 7.93833 8.54333 8.87333C7.44333 9.625 4.125 10.2804 1.375 8.40583Z"
        stroke="#798090"
      />
    </g>
  </svg>
);

const HearIcon = ({ handicapClass }) => (
  <div className={`icon ${handicapClass}`}>
    <Icon component={Hear} />
  </div>
);
const EyeIcon = ({ handicapClass }) => (
  <div className={`icon ${handicapClass}`}>
    <Icon component={Eye} />
  </div>
);
const MobilityIcon = ({ handicapClass }) => (
  <div className={`icon ${handicapClass}`}>
    <Icon component={Mobility} />
  </div>
);

export const BadgeAccess = ({ dataHandicap }) => {
  const checkHandicap = (handicap) => {
    return dataHandicap.some((data) => data.type === handicap)
      ? "active"
      : null;
  };

  return (
    <div>
      <HearIcon handicapClass={checkHandicap("Auditif")} />
      <EyeIcon handicapClass={checkHandicap("Visuel")} />
      <MobilityIcon handicapClass={checkHandicap("Moteur")} />
    </div>
  );
};
