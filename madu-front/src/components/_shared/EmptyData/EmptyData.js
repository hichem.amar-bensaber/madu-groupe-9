import React from "react";
import { Empty, Button } from "antd";
import { useHistory } from "react-router-dom";
import "./emptydata.scss";

export const EmptyData = ({
  image = "https://gw.alipayobjects.com/mdn/miniapp_social/afts/img/A*pevERLJC9v0AAAAAAAAAAABjAQAAAQ/original",
  description,
  imageStyle = { height: 60 },
  to,
  children,
  ...rest
}) => {
  const history = useHistory();
  return (
    <div className="emptydata">
      <Empty image={image} imageStyle={imageStyle} description={description}>
        <Button {...rest} onClick={() => history.push(to)}>
          {children}
        </Button>
      </Empty>
    </div>
  );
};
