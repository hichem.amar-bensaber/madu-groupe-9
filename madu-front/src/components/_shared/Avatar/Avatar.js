import React, { useState, forwardRef } from "react";
import { Upload, message } from "antd";
import { LoadingOutlined, PlusCircleFilled } from "@ant-design/icons";
import "./avatar.scss";

const getBase64 = (img, callback) => {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
};

const beforeUpload = (file) => {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
  if (!isJpgOrPng) {
    message.error("You can only upload JPG/PNG file!");
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error("Image must smaller than 2MB!");
  }
  return isJpgOrPng && isLt2M;
};

const Avatar = ({ name, handleImage }) => {
  const [loading, setLoading] = useState(false);
  const [imageUrl, setImageUrl] = useState(null);
  const handleChange = (info) => {
    if (info.file.status === "uploading") {
      setLoading(true);
      return;
    }
    if (info.file.status === "done") {
      getBase64(info.file.originFileObj, (imageUrl) => {
        setImageUrl(imageUrl);
        handleImage(imageUrl);
        setLoading(false);
      });
    }
  };
  const uploadButton = (
    <div style={{ color: "white" }}>
      {loading ? <LoadingOutlined /> : "PHOTO"}
      <div className="ant-upload-text">
        <PlusCircleFilled />
      </div>
    </div>
  );
  return (
    <div style={{ width: "100%" }}>
      <Upload
        name={name}
        listType="picture-card"
        className="avatar-uploader"
        showUploadList={false}
        action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
        beforeUpload={beforeUpload}
        onChange={handleChange}
      >
        {imageUrl ? (
          <div style={{ color: "white" }}>
            <div className="ant-upload-text">
              <p>{loading ? <LoadingOutlined /> : "MODIFIER"}</p>
              <PlusCircleFilled />
            </div>
            <img
              className={loading ? "hidden" : ""}
              src={imageUrl}
              alt="avatar"
            />{" "}
          </div>
        ) : (
          uploadButton
        )}
      </Upload>
    </div>
  );
};

export default forwardRef(Avatar);
