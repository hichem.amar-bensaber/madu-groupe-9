import "./editablecustomertable.scss";
import React, { useState } from "react";
import { Input, Table, Divider, Form } from "antd";
import { connect } from "react-redux";
import * as actions from "../../../action";

const { Search } = Input;

const CustomerTable = ({ form, data, editCustomersListProperty }) => {
  const { getFieldDecorator } = form;
  const [editableKey, setEditableKey] = useState("");
  const isEditing = record => record.id === editableKey;
  let columns = [
    {
      title: "Nom",
      dataIndex: "username",
      editable: true
    },
    {
      title: "Adresse",
      dataIndex: "address",
      editable: true
    },
    {
      title: "Tél",
      dataIndex: "phone",
      editable: true
    },
    {
      title: "Email",
      dataIndex: "email",
      editable: true
    },
    {
      title: "Action",
      dataIndex: "action",
      render: (text, record, index) => {
        return (
          <span>
            {editableKey !== record.id ? (
              <a onClick={() => setEditableKey(record.id)}>Modifier</a>
            ) : (
              <a onClick={() => setEditableKey("")}>Sauvegarder</a>
            )}
            {/* <Divider type="vertical" />
            <a>Bloquer</a> */}
          </span>
        );
      }
    }
  ];

  columns = columns.map(col => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: record => ({
        record,
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record)
      })
    };
  });

  const EditableCell = props => {
    const { editing, dataIndex, title, record, children, ...restProps } = props;
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item style={{ margin: 0 }}>
            <Input
              data-index={record.id}
              name={dataIndex}
              defaultValue={record[dataIndex]}
              onChange={editCustomersListProperty}
            />
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };
  const components = {
    body: {
      cell: EditableCell
    }
  };
  return (
    <div className="table">
      <label>Liste des utilisateurs</label>
      <Search
        placeholder="Trouver un utilisateur"
        onSearch={value => console.log(value)}
        style={{ width: 417, marginLeft: 16, marginBottom: 16 }}
      />
      <Table
        columns={columns}
        components={components}
        dataSource={data}
        rowClassName="editablecustomertable-row"
      ></Table>
    </div>
  );
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    validateEdit: e => {
      console.log("validate");
    },
    editCustomersListProperty: event =>
      dispatch(actions.editCustomersListProperty(event))
  };
};

const EditableCustomerTable = connect(
  null,
  mapDispatchToProps
)(Form.create()(CustomerTable));
export default EditableCustomerTable;
