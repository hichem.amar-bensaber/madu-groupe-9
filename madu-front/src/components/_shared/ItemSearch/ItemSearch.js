import React from "react";
import "./itemsearch.scss";

const ItemSearch = ({
  img = "https://lh3.googleusercontent.com/proxy/RDVBLJVGT72m8QnbIvj2ZBjSN60t-YeRPtMBmIlo4mZ5NeJxQyTi7RxLMIId3VfsbdUY_SYB1XPYvRHX2tSVrerrf0FE5h_dDweEF6mjCcaxdx3VzEf2GVF-KBz7P2taABB0VJQozAc",
  name = "hetic",
  address = "mon adresse",
  onClick = () => null,
}) => {
  return (
    <div className="itemsearch" onClick={onClick}>
      <div className="itemsearch-img">
        <img src={img} alt="img" />
      </div>
      <h2>{name}</h2>
      <span className="itemsearch-adresse">{address}</span>
    </div>
  );
};

export default ItemSearch;
