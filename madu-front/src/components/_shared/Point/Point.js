import React from "react";
import "./point.scss";

export const Point = ({ color = "black" }) => (
  <div className="point">
    <div className="point__item" style={{ backgroundColor: color }}></div>
  </div>
);
