import React from "react";
import "./defaultbutton.scss";
const DefaultButton = ({
  children,
  onClick = () => alert("click"),
  color = "#798090",
  loading = false,
  ...el
}) => {
  return (
    <button
      className="defaultbutton"
      style={{ backgroundColor: color }}
      onClick={onClick}
      {...el}
    >
      {loading ? "Chargement" : children}
    </button>
  );
};

export default DefaultButton;
