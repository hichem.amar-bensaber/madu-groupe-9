import React, { forwardRef } from "react";
import { Input, InputNumber } from "antd";
import "./inputlabel.scss";

const newInputWithLabel = (props, ref) => {
  const { label = "Label", size = "large", type = "text", ...el } = props;
  return (
    <div className="inputlabel">
      <label className="inputlabel_label">{label}</label>
      {type !== "number" ? (
        <Input size={size} ref={ref} {...el} />
      ) : (
        <InputNumber size={size} ref={ref} {...el} />
      )}
    </div>
  );
};
export const InputLabel = forwardRef(newInputWithLabel);
