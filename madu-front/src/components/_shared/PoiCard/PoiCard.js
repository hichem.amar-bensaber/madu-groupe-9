import React from "react";
import { deletePoiById } from "../../../utils/api_poi";
import { BadgeAccess } from "../BadgeAccess/BadgeAccess";
import GreenScore from "../GreenScore/GreenScore";
import { Modal } from "antd";
import {
  EditOutlined,
  DeleteOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import "./poiCard.scss";
import { REACT_APP_API_URL } from "../../../utils/config";
import { useHistory } from "react-router-dom";

const PoiCard = ({
  id,
  name,
  description,
  phone,
  email,
  web_site,
  store_type,
  store_speciality,
  store_filters,
  localisation,
  open_hours,
  greenscore,
  images,
  onUpdate,
  children,
}) => {
  const history = useHistory();
  const confirmDeletion = () => {
    Modal.confirm({
      title: "Suppression point d'intérêt",
      icon: <ExclamationCircleOutlined />,
      content: `Etes vous sur de vouloir supprimer ${name}`,
      okText: "Oui",
      cancelText: "Non",
      onOk() {
        deletePoiById(id);
        history.push("/back-office/poi");
      },
    });
  };

  return (
    <div className="poiCard">
      {images && images.length === 1 && (
        <div
          className="poiCard-uniqueBackground"
          style={{
            backgroundImage: `url(${REACT_APP_API_URL}/${images[0].uri})`,
          }}
        ></div>
      )}
      {images && images.length > 1 && (
        <div className="poiCard-background">
          <div
            className="poiCard-background-main"
            style={{
              backgroundImage: `url(${REACT_APP_API_URL}/${images[0].uri})`,
            }}
          ></div>
          <div
            className="poiCard-background-secondary"
            style={{
              backgroundImage: `url(${REACT_APP_API_URL}/${images[1].uri})`,
            }}
          >
            <div className="poiCard-background-secondary-overlay">
              <span className="poiCard-background-secondary-overlay__number">
                {images.length > 2 ? `+${images.length - 2}` : ""}
              </span>
            </div>
          </div>
        </div>
      )}
      <div className="poiCard-informations">
        <div className="poiCard-informations-content">
          <div className="poiCard-header">
            <GreenScore percent={greenscore} />
            <span className="titleCompany"> {name} </span>
            <div className="poiCard-header__badge">{children}</div>
          </div>
          <p className="stories">{description}</p>
          <p>
            {" "}
            {localisation.address}, {localisation.city}, {localisation.zip_code}{" "}
          </p>
          <p className="phone" href="tel:+33123456789">
            {" "}
            {phone}{" "}
          </p>
          <p href="mailto:Mailpro@gmail.com"> {email} </p>
          <a className="link" href={web_site}>
            {" "}
            {web_site}{" "}
          </a>
        </div>

        <div className="poiCard-informations-content">
          <div className="price">
            <span> Prix moyen </span>
            <div className="price-pastille">
              <span> {store_filters.price_average} € </span>
            </div>
          </div>
          <div className="handicap">
            <span> Accès handicapé </span>
            <div className="handicap-item">
              <BadgeAccess dataHandicap={store_filters.handicap} />
            </div>
          </div>
        </div>
      </div>
      <div className="poiCard-actions">
        <EditOutlined
          style={{
            fontSize: "16px",
            color: "#798090",
            marginRight: "15px",
            cursor: "pointer",
          }}
          onClick={onUpdate}
        />
        <DeleteOutlined
          style={{ fontSize: "16px", color: "#798090", cursor: "pointer" }}
          onClick={confirmDeletion}
        />
      </div>
    </div>
  );
};

export default PoiCard;
