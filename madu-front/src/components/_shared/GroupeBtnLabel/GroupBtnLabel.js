import React, { forwardRef } from "react";
import { Radio } from "antd";
import "./groupbtnlabel.scss";

const GroupBtnLabel = (props, ref) => {
  const { items, label, size = "large", onClick = () => null, ...rest } = props;
  const { Group } = Radio;

  // all items have same size
  const style = {
    width: 100 / items.length + "%",
  };

  const radioButton = (item, key) => (
    <Radio.Button style={style} key={key} value={item}>
      <span className="groupbtnlabel_item">{item}</span>
    </Radio.Button>
  );
  return (
    <div>
      <label className="groupbtnlabel_label">{label}</label>
      <Group style={{ width: "100%" }} size={size} {...rest}>
        {items.map(radioButton)}
      </Group>
    </div>
  );
};

export default forwardRef(GroupBtnLabel);
