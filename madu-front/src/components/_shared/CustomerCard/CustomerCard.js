import React, { useState, useEffect } from "react";
import "./customercard.scss";
import { Point } from "../Point/Point";
import EditableCustomerTable from "../EditableCustomerTable/EditableCustomerTable";
import { EditableText } from "../EditableText/EditableText";
import { Card, Popconfirm, Row, Col, Divider } from "antd";
import { Button } from "antd";
import * as actions from "../../../action";
import { connect } from "react-redux";
import { putCustomerById, deleteCustomerById } from "../../../utils/api";
import { useHistory } from "react-router-dom";
import { REACT_APP_API_URL } from "../../../utils/config";

const CustomerCard = ({ data, onChange, deleteCustomer }) => {
  const history = useHistory();
  const cardStyle = {
    minWidth: "100%",
    borderRadius: 4,
    height: 104,
    display: "flex",
    alignItems: "center",
  };

  const [table, setTable] = useState(false);
  const [editable, setEditable] = useState(false);
  const [loading, setLoading] = useState(true);

  const onValidate = async () => {
    if (editable) {
      setTable(true);
      await putCustomerById(data._id, data);
      setTable(false);
    }
    setEditable(!editable);
  };

  const rotate = table ? "rotate" : "";
  const showTable = table ? "active" : "";
  const iconBtn = editable ? "check" : "edit";
  const titleDelete = `Êtes-vous sûr de vouloir bloquer l'accès à ${data.name} ?`;

  const onDelet = async () => {
    setLoading(true);
    await deleteCustomerById(data._id);
    deleteCustomer(data._id);
    history.push("/back-office/customers");
    setLoading(false);
  };
  return (
    <div className="customercard">
      <Row type="flex" align="middle">
        <Col span={1}></Col>
        <Col span={22}>
          <Card style={{ ...cardStyle }}>
            <Row type="flex" align="middle">
              <Col span={6}>
                <Row type="flex" align="middle">
                  <Col span={24}>
                    <div className="customercard-img">
                      <div className="customercard-img-container">
                        <img
                          src={`${REACT_APP_API_URL}/${data.profile_pic}`}
                          className="customercard-img-container_main"
                        />
                      </div>
                      <EditableText
                        editable={editable}
                        loading={true}
                        name="name"
                        style={{ fontWeight: "bolder", marginLeft: 8 }}
                        value={data.name}
                        id={data._id}
                        onChange={onChange}
                        onPressEnter={onValidate}
                      />
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col span={14}>
                <div className="customercard-informations">
                  <div style={{ width: "30%" }}>
                    <EditableText
                      editable={editable}
                      name="localisation.address"
                      id={data._id}
                      value={data.localisation.address}
                      onChange={onChange}
                      onPressEnter={onValidate}
                    />
                    <div
                      style={{
                        display: "flex",
                      }}
                    >
                      <EditableText
                        editable={editable}
                        name="localisation.zip_code"
                        id={data._id}
                        value={data.localisation.zip_code}
                        onChange={onChange}
                        onPressEnter={onValidate}
                      />
                      <EditableText
                        editable={editable}
                        style={{ marginLeft: 8 }}
                        name="localisation.city"
                        id={data._id}
                        value={data.localisation.city}
                        onChange={onChange}
                        onPressEnter={onValidate}
                      />
                    </div>
                  </div>
                  <EditableText
                    editable={editable}
                    style={{ width: "30%" }}
                    id={data._id}
                    name="phone"
                    value={data.phone}
                    onChange={onChange}
                    onPressEnter={onValidate}
                  />
                  <EditableText
                    editable={editable}
                    style={{ width: "30%" }}
                    id={data._id}
                    name="email"
                    value={data.email}
                    onChange={onChange}
                    onPressEnter={onValidate}
                  />
                </div>
              </Col>
              <Col span={4}>
                <div className="customercard-btns">
                  <Button
                    shape="circle"
                    icon="down"
                    onClick={() => setTable(!table)}
                    className={rotate}
                  />
                  <Divider type="vertical" />
                  <Button shape="circle" icon={iconBtn} onClick={onValidate} />
                  <Popconfirm
                    placement="topLeft"
                    title={titleDelete}
                    onConfirm={onDelet}
                    okText="Oui"
                    cancelText="Non"
                  >
                    <Button
                      style={{ marginLeft: 8 }}
                      shape="circle"
                      icon="stop"
                    />
                  </Popconfirm>
                </div>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col span={22} offset={1}>
          <div className={`customercard-table ${showTable}`}>
            <EditableCustomerTable data={data.customersList} />
          </div>
        </Col>
      </Row>
    </div>
  );
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    deleteCustomer: (id) => dispatch(actions.deleteCustomer(id)),
  };
};

export default connect(null, mapDispatchToProps)(CustomerCard);
