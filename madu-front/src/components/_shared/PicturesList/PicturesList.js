import React, { useState, useEffect, forwardRef } from "react";
import { Upload, Modal } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { REACT_APP_API_URL } from "../../../utils/config";

const PicturesList = ({ handleImage, poiImageList }) => {
  const [previewState, setPreview] = useState({
    previewVisible: false,
    previewImage: "",
    previewTitle: "",
  });

  const [imageList, setImageList] = useState({
    fileList: [],
  });

  const showIcon = {
    showRemoveIcon: true,
    showPreviewIcon: true,
    showDownloadIcon: false,
  };

  useEffect(() => {
    const poiList =
      poiImageList && poiImageList.length > 0
        ? poiImageList.reduce((acc, currImage, index) => {
            return [
              ...acc,
              {
                ["uid"]: index,
                ["url"]: `${REACT_APP_API_URL}/${currImage.uri}`,
              },
            ];
          }, [])
        : null;

    if (poiList !== null) {
      setImageList({ fileList: poiList });
    }
  }, []);

  const getBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  };

  const handleCancel = () =>
    setPreview({ ...previewState, previewVisible: false });

  const handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }

    setPreview({
      previewImage: file.url || file.preview,
      previewVisible: true,
      previewTitle:
        file.name || file.url.substring(file.url.lastIndexOf("/") + 1),
    });
  };

  const handleChange = ({ fileList }) => {
    setImageList({ fileList });
    handleImage(fileList);
  };

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div className="ant-upload-text">Ajouter</div>
    </div>
  );

  return (
    <div className="clearfix">
      <Upload
        action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
        listType="picture-card"
        fileList={imageList.fileList}
        onPreview={handlePreview}
        onChange={handleChange}
        showUploadList={showIcon}
      >
        {imageList.fileList.length >= 8 ? null : uploadButton}
      </Upload>
      <Modal
        visible={previewState.previewVisible}
        title={previewState.previewTitle}
        footer={null}
        onCancel={handleCancel}
      >
        <img
          alt="image_zoom"
          style={{ width: "100%" }}
          src={previewState.previewImage}
        />
      </Modal>
    </div>
  );
};

export default forwardRef(PicturesList);
