import React from "react";
import { Icon } from "antd";
import "./badge.scss";

const BioSvg = () => (
  <svg
    width="8"
    height="7"
    viewBox="0 0 8 7"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g clip-path="url(#clip0)">
      <path
        d="M6.76801 0.154554C6.7015 0.0060868 6.51146 0.000148129 6.43188 0.140301C6.06368 0.78049 5.40449 1.17957 4.65146 1.17957H3.70128C2.44228 1.17957 1.42083 2.20102 1.42083 3.46002C1.42083 3.54316 1.43033 3.62274 1.43864 3.70351C2.19642 3.16071 3.29032 2.69987 4.8415 2.69987C4.94602 2.69987 5.03154 2.78539 5.03154 2.88991C5.03154 2.99443 4.94602 3.07994 4.8415 3.07994C1.85554 3.07994 0.589411 4.91024 0.309106 5.59794C0.230715 5.79154 0.323359 6.01246 0.516959 6.09204C0.711748 6.17281 0.932667 6.07897 1.01343 5.88656C1.03125 5.8438 1.26167 5.31764 1.86741 4.81047C2.25224 5.33189 2.98388 5.82955 3.94476 5.72741C5.80951 5.592 7.12195 3.91967 7.12195 1.87202C7.12195 1.27578 6.99368 0.658153 6.76801 0.154554Z"
        fill="white"
      />
    </g>
    <defs>
      <clipPath id="clip0">
        <rect
          x="0.28125"
          y="0.0390625"
          width="6.84135"
          height="6.0812"
          fill="white"
        />
      </clipPath>
    </defs>
  </svg>
);

const DefaultIcon = (props) => <Icon component={BioSvg} {...props} />;

export const Badge = ({ tag }) => (
  <>
    {tag.bio && (
      <div
        className="badge"
        style={{ backgroundColor: "#6FCF97", color: "#ffffff" }}
      >
        <DefaultIcon />
      </div>
    )}
    {tag.gluten_free && (
      <div
        className="badge"
        style={{ backgroundColor: "#E9D912", color: "#ffffff" }}
      >
        <span>G</span>
      </div>
    )}
    {tag.vegan && (
      <div
        className="badge"
        style={{ backgroundColor: "#F2994A", color: "#ffffff" }}
      >
        <span>VG</span>
      </div>
    )}
    {tag.veggie && (
      <div
        className="badge"
        style={{ backgroundColor: "#095105", color: "#ffffff" }}
      >
        <span>VE</span>
      </div>
    )}
    {tag.terrace && (
      <div
        className="badge"
        style={{ backgroundColor: "#826315", color: "#ffffff" }}
      >
        <span>T</span>
      </div>
    )}
    {tag.take_away && (
      <div
        className="badge"
        style={{ backgroundColor: "#1FA3BE", color: "#ffffff" }}
      >
        <span>E</span>
      </div>
    )}
  </>
);
