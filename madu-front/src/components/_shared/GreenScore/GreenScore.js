import React from 'react';
import './greenscore.scss';

const GreenScore = ({percent="30"}) => {
    return (
        <div className="greenScore">
            <span> {percent}% </span>
        </div>
    )
}

export default GreenScore
