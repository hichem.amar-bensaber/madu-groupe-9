import React, { useState } from "react";
import { Button } from "antd";
import { Link } from "react-router-dom";
import "./buttonDefault.scss";

const ButtonDefault = (props) => {
  const { color, name, width = "300px", to, ghost = false, onClick } = props;
  return (
    <div>
      <Link to={to}>
        <Button
          style={{
            backgroundColor: color,
            width: width,
            border: "none",
            textTransform: "uppercase",
          }}
          size="large"
          ghost={ghost}
          onClick={onClick}
        >
          {name}
        </Button>
      </Link>
    </div>
  );
};

export default ButtonDefault;
