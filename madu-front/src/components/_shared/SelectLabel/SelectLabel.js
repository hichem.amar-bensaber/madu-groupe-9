import React, { forwardRef } from "react";
import { Select } from "antd";
import "./selectlabel.scss";

const SelectLabel = (props, ref) => {
  const { label = "Label", size = "large", items, children, ...rest } = props;
  const { Option } = Select;

  const option = (item, key) => (
    <Option key={key} value={item}>
      {item}
    </Option>
  );
  return (
    <div>
      <label className="selectlabel_label">{label}</label>
      <Select style={{ width: "100%" }} size={size} ref={ref} {...rest}>
        {items.map(option)}
      </Select>
    </div>
  );
};

export default forwardRef(SelectLabel);
