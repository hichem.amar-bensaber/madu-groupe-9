import React from "react";
import "./popupcontent.scss";

const PopUpContent = ({
  email = "Email",
  phone = "Phone",
  address = "Adresse",
  name = "Name",
  img = "https://lh3.googleusercontent.com/proxy/RDVBLJVGT72m8QnbIvj2ZBjSN60t-YeRPtMBmIlo4mZ5NeJxQyTi7RxLMIId3VfsbdUY_SYB1XPYvRHX2tSVrerrf0FE5h_dDweEF6mjCcaxdx3VzEf2GVF-KBz7P2taABB0VJQozAc",
}) => (
  <div className="popupcontent">
    <div className="popupcontent-img">
      <img src={img} draggable="false" />
    </div>
    <div className="popupcontent-info">
      <h2>{name}</h2>
      <div>
        <b>Email:</b>
        <a> {email}</a>
      </div>
      <div className="popupcontent-info-phone">
        <span>
          <b>Téléphone:</b> {phone}
        </span>
      </div>
      <div>
        <p>
          <b>Adresse: </b>
          {address}
        </p>
      </div>
      <a>En savoir plus</a>
    </div>
  </div>
);

export default PopUpContent;
