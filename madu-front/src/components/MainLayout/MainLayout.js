import React from 'react'
import { Layout } from "antd";
const { Content } = Layout;

export const MainLayout = ({children}) => {
    return (
        <Layout>
            <Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
                {children}
            </Content>
        </Layout>
    )
}
