import React, { useState, useEffect } from "react";
import { MainTitle } from "../../components/_shared/MainTitle/MainTitle";
import { Row, Col, Form, notification } from "antd";
import { InputLabel } from "../../components/_shared/InputLabel/InputLabel";
import DefaultButton from "../../components/_shared/DefaultButton/DefaultButton";
import DatePickerGroup from "../../components/_shared/DatePickerGroup/DatePickerGroup";

const ChallengeAdd = ({
  title = "ajouter un défi",
  form,
  data = null,
  action = null,
}) => {
  const {
    getFieldDecorator,
    getFieldError,
    isFieldTouched,
    setFieldsValue,
  } = form;

  useEffect(() => {
    form.validateFields();
    if (data) {
      setFieldsValue({
        ...data,
      });
    }
  }, []);

  const hasErrors = (fieldsError) => {
    return Object.keys(fieldsError).some((field) => {
      if (fieldsError[field] === undefined) {
        return false;
      }
      return fieldsError[field];
    });
  };
  const [loading, setLoading] = useState(false);
  const nameError = isFieldTouched("name") && getFieldError("name");
  const descriptionError =
    isFieldTouched("description") && getFieldError("description");
  const typeError = isFieldTouched("type") && getFieldError("type");
  const pointsError = isFieldTouched("points") && getFieldError("points");
  const durationError = isFieldTouched("duration") && getFieldError("duration");

  const handleSubmit = async (e) => {
    e.preventDefault();
    form.validateFields(async (err, values) => {
      const duration = values.duration.unix();
      const newData = { ...values, duration };
      setLoading(true);

      try {
        if (action) {
          await action(newData, data);
        }
        notification.info({
          message: `${data ? "Modification" : "Ajout"} effectué${
            data ? "e" : ""
          }`,
          description: `Le défi ${values.name} a été ${
            data ? "modifié" : "créé"
          }.`,
        });
        if (data === null) {
          form.resetFields();
        }
      } catch {
        notification.error({
          message: `Une erreur est survenue`,
          description: `Veuillez recommencer.`,
        });
      } finally {
        setLoading(false);
        form.validateFields();
      }
    });
  };

  return (
    <>
      <Row type="flex" justify="center">
        <Col span={16}>
          <div style={{ marginBottom: 40 }}>
            <MainTitle text={title} />
          </div>
          <Row type="flex" align="bottom" justify="space-between" gutter={24}>
            <Col span={24}>
              <Form.Item
                validateStatus={nameError ? "error" : ""}
                help={nameError || ""}
              >
                {getFieldDecorator("name", {
                  rules: [
                    {
                      required: true,
                      message: "merci d'inquer un intitulé",
                    },
                  ],
                })(
                  <InputLabel
                    name="name"
                    label="Intitulé du défi"
                    placeholder="Intitulé du défi"
                  />
                )}
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                validateStatus={descriptionError ? "error" : ""}
                help={descriptionError || ""}
              >
                {getFieldDecorator("description", {
                  rules: [
                    {
                      required: false,
                      message: "Entrer une description",
                    },
                  ],
                })(
                  <InputLabel
                    name="description"
                    label="Description du défi"
                    placeholder="Description du défi"
                  />
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                validateStatus={typeError ? "error" : ""}
                help={typeError || ""}
              >
                {getFieldDecorator("type", {
                  rules: [
                    {
                      required: true,
                      message: "merci de spécifier le type de défi",
                    },
                  ],
                })(
                  <InputLabel
                    name="type"
                    label="type de défi"
                    placeholder="Type de défi"
                  />
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                validateStatus={pointsError ? "error" : ""}
                help={pointsError || ""}
              >
                {getFieldDecorator("points", {
                  rules: [
                    {
                      required: true,
                      message: "Merci d'indiquer le nombre de points",
                    },
                  ],
                })(
                  <InputLabel
                    type="number"
                    min={0}
                    name="points"
                    label="points à gagner"
                    placeholder="Points à gagner"
                  />
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                validateStatus={durationError ? "error" : ""}
                help={durationError || ""}
              >
                {getFieldDecorator("duration", {
                  rules: [
                    {
                      required: true,
                      message: "Merci de sélectionner la durée du défi",
                    },
                  ],
                })(<DatePickerGroup name="duration" label="durée du défi" />)}
              </Form.Item>
            </Col>
            <Col span={24} style={{ marginTop: 24 }}>
              <DefaultButton
                type="submit"
                disabled={hasErrors(form.getFieldsError()) || loading}
                size="large"
                onClick={handleSubmit}
                loading={loading}
                color="#7282F7"
              >
                {data ? "modifier" : "valider"}
              </DefaultButton>
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  );
};

export default Form.create()(ChallengeAdd);
