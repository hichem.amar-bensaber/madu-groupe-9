import React from "react";
import { Marker } from "react-map-gl";

const ICON = `M20.2,15.7L20.2,15.7c1.1-1.6,1.8-3.6,1.8-5.7c0-5.6-4.5-10-10-10S2,4.5,2,10c0,2,0.6,3.9,1.6,5.4c0,0.1,0.1,0.2,0.2,0.3
  c0,0,0.1,0.1,0.1,0.2c0.2,0.3,0.4,0.6,0.7,0.9c2.6,3.1,7.4,7.6,7.4,7.6s4.8-4.5,7.4-7.5c0.2-0.3,0.5-0.6,0.7-0.9
  C20.1,15.8,20.2,15.8,20.2,15.7z`;

const SIZE = 36;

const PoiIcon = () => (
  <g>
    <path
      d="m32 60c11 0 17-2.643 17-4 0-.945-3.33-2.977-11.293-3.742-2.7 3.094-4.732 5.176-4.993 5.442a1 1 0 0 1 -1.428 0c-.261-.266-2.29-2.348-4.993-5.442-7.963.765-11.293 2.797-11.293 3.742 0 1.357 6 4 17 4z"
      data-original="#000000"
      className=""
      data-old_color="#000000"
      fill="#7282F7"
    />
    <path
      d="m33.105 27h2.895.18l.6-3h-3.375z"
      data-original="#000000"
      data-old_color="#000000"
      fill="#7282F7"
    />
    <path
      d="m31.095 27 .3-3h-2.79l.3 3z"
      data-original="#000000"
      data-old_color="#000000"
      fill="#7282F7"
    />
    <path
      d="m32 39a14.971 14.971 0 0 0 11.971-24h-3.19l-.806 3.223-1.775 8.888a2.493 2.493 0 0 1 -.7 4.889h-.28a3 3 0 1 1 -4.44 0h-3.56a3 3 0 1 1 -4.44 0h-.78a1 1 0 0 1 0-2h13.5a.5.5 0 0 0 0-1h-14.5a1 1 0 0 1 -.98-.8l-2-10a1 1 0 0 1 .98-1.2h17.219l.621-2.485a2 2 0 0 1 1.941-1.515h1.382a14.99 14.99 0 1 0 -10.163 26z"
      data-original="#000000"
      data-old_color="#000000"
      fill="#7282F7"
    />
    <path
      d="m26.895 27-.3-3h-3.375l.6 3z"
      data-original="#000000"
      className=""
      data-old_color="#000000"
      fill="#7282F7"
    />
    <path
      d="m37.78 19h-3.875l-.3 3h3.575z"
      data-original="#000000"
      className=""
      data-old_color="#000000"
      fill="#7282F7"
    />
    <path
      d="m31.895 19h-3.79l.3 3h3.19z"
      data-original="#000000"
      className=""
      data-old_color="#000000"
      fill="#7282F7"
    />
    <path
      d="m32 55.555c3.647-3.855 20-21.681 20-31.555a20 20 0 0 0 -40 0c0 9.878 16.353 27.7 20 31.555zm0-48.555a17 17 0 1 1 -17 17 17.019 17.019 0 0 1 17-17z"
      data-original="#000000"
      className=""
      data-old_color="#000000"
      fill="#7282F7"
    />
    <circle
      cx="20"
      cy="27"
      r="1"
      data-original="#000000"
      className=""
      data-old_color="#000000"
      fill="#7282F7"
    />
    <path
      d="m22.22 19 .6 3h3.575l-.3-3z"
      data-original="#000000"
      className=""
      data-old_color="#000000"
      fill="#7282F7"
    />
    <circle
      cx="35"
      cy="34"
      r="1"
      data-original="#000000"
      className=""
      data-old_color="#000000"
      fill="#7282F7"
    />
  </g>
);
const ClientIcon = () => (
  <g>
    <path
      d="m180 0c-99.25 0-180 80.75-180 180 0 42.5 15.148438 83.78125 42.660156 116.238281l137.339844 164.132813 137.308594-164.089844c27.53125-32.472656 42.691406-73.761719 42.691406-116.28125 0-99.25-80.75-180-180-180zm0 300c-66.171875 0-120-53.828125-120-120s53.828125-120 120-120 120 53.828125 120 120-53.828125 120-120 120zm0 0"
      data-original="#000000"
      className="active-path"
      data-old_color="#000000"
      fill="#7282F7"
    />
    <path
      d="m180 150c-8.269531 0-15 6.730469-15 15s6.730469 15 15 15 15-6.730469 15-15-6.730469-15-15-15zm0 0"
      data-original="#000000"
      className="active-path"
      data-old_color="#000000"
      fill="#7282F7"
    />
    <path
      d="m180 240c-14.839844 0-27.191406 10.828125-29.570312 25 9.261718 3.238281 19.210937 5 29.570312 5s20.308594-1.761719 29.570312-5c-2.378906-14.171875-14.730468-25-29.570312-25zm0 0"
      data-original="#000000"
      className="active-path"
      data-old_color="#000000"
      fill="#7282F7"
    />
    <path
      d="m180 90c-49.628906 0-90 40.371094-90 90 0 28.21875 13.058594 53.449219 33.449219 69.960938 8.261719-23.261719 30.492187-39.960938 56.550781-39.960938-24.808594 0-45-20.191406-45-45s20.191406-45 45-45 45 20.191406 45 45-20.191406 45-45 45c26.058594 0 48.289062 16.699219 56.550781 39.960938 20.390625-16.511719 33.449219-41.742188 33.449219-69.960938 0-49.628906-40.371094-90-90-90zm0 0"
      data-original="#000000"
      className="active-path"
      data-old_color="#000000"
      fill="#7282F7"
    />
    <path
      d="m263.21875 407.660156-83.21875 99.460938-83.230469-99.460938c-23.28125 10.769532-36.769531 26.308594-36.769531 44.339844 0 44.898438 74.460938 60 120 60 45.949219 0 120-15.25 120-60 0-18.03125-13.488281-33.578125-36.78125-44.339844zm0 0"
      data-original="#000000"
      className="active-path"
      data-old_color="#000000"
      fill="#7282F7"
    />
  </g>
);
const SVG = ({ onClick, children, viewBox = "0 0 60 60" }) => (
  <svg
    height={SIZE}
    viewBox={viewBox}
    style={{
      cursor: "pointer",
      fill: "#7282F7",
      stroke: "none",
      transform: `translate(${-SIZE / 2}px,${-SIZE}px)`,
    }}
    onClick={onClick}
  >
    {children}
  </svg>
);

export const Markers = ({ data, onClick }) => {
  return data.map((city, index) => (
    <Marker
      key={`marker-${index}`}
      longitude={city.longitude}
      latitude={city.latitude}
      coordinates=""
    >
      {city.type === "POI" ? (
        <SVG onClick={() => onClick(city)}>
          <PoiIcon />
        </SVG>
      ) : (
        <SVG onClick={() => onClick(city)} viewBox="0 0 550 550">
          <ClientIcon />
        </SVG>
      )}
    </Marker>
  ));
};
