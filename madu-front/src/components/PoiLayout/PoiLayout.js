import React, { useState, useEffect } from "react";
import {
  Row,
  Col,
  Button,
  Checkbox,
  Form,
  Icon,
  Input,
  notification,
} from "antd";
import PicturesList from "../_shared/PicturesList/PicturesList";
import { MainTitle } from "../_shared/MainTitle/MainTitle";
import { InputLabel } from "../_shared/InputLabel/InputLabel";
import SelectLabel from "../_shared/SelectLabel/SelectLabel";
import GroupBtnLabel from "../_shared/GroupeBtnLabel/GroupBtnLabel";
import jwtDecode from "jwt-decode";
import { getToken } from "../../utils/token";

const PoiLayout = ({ data = null, action = null, form }) => {
  const [loading, setLoading] = useState(false);
  const [images, setImages] = useState([]);
  const {
    getFieldDecorator,
    getFieldError,
    isFieldTouched,
    getFieldValue,
    setFieldsValue,
  } = form;

  const { TextArea } = Input;

  const errorText = {
    name: isFieldTouched("name") && getFieldError("name"),
    description: isFieldTouched("description") && getFieldError("description"),
    email: isFieldTouched("email") && getFieldError("email"),
    phone: isFieldTouched("phone") && getFieldError("phone"),
    zip_code: isFieldTouched("zip_code") && getFieldError("zip_code"),
    address: isFieldTouched("address") && getFieldError("address"),
    city: isFieldTouched("city") && getFieldError("city"),
    store_type: isFieldTouched("store_type") && getFieldError("store_type"),
    store_speciality:
      isFieldTouched("store_speciality") && getFieldError("store_speciality"),
    price_average:
      isFieldTouched("price_average") && getFieldError("price_average"),
    mon: isFieldTouched("mon") && getFieldError("mon"),
    tue: isFieldTouched("tue") && getFieldError("tue"),
    wed: isFieldTouched("wed") && getFieldError("wed"),
    thu: isFieldTouched("thu") && getFieldError("thu"),
    fri: isFieldTouched("fri") && getFieldError("fri"),
    sat: isFieldTouched("sat") && getFieldError("sat"),
    sun: isFieldTouched("sun") && getFieldError("sun"),
    greenscore: isFieldTouched("greenscore") && getFieldError("greenscore"),
  };
  const selectData = {
    store_speciality: [
      "Beauté",
      "Epicerie",
      "Opticien",
      "Oriental",
      "Pâtisserie",
      "Pizza",
      "Soin",
      "Yoga",
    ],
    store_type: ["Restaurant", "Boutique", "Activité"],
    openingDay: ["Ouvert", "Fermé"],
    openingTime: Array(24 * 4)
      .fill(0)
      .map((_, i) => {
        return ("0" + ~~(i / 4) + ":0" + 60 * ((i / 4) % 1)).replace(
          /\d(\d\d)/g,
          "$1"
        );
      }),
  };

  useEffect(() => {
    form.validateFields();
    if (data) {
      const open_hours = Object.entries(data.open_hours).reduce(
        (acc, currentDay) => {
          return {
            ...acc,
            ...(currentDay[1] !== "Fermé"
              ? {
                  [currentDay[0]]: "Ouvert",
                  [`${currentDay[0]}Open`]: currentDay[1].substring(
                    0,
                    currentDay[1].indexOf("-") - 1
                  ),
                  [`${currentDay[0]}Close`]: currentDay[1].substring(
                    currentDay[1].indexOf("-") + 2
                  ),
                }
              : { [currentDay[0]]: "Fermé" }),
          };
        },
        {}
      );

      setFieldsValue({
        ...data,
        price_average: data.store_filters.price_average,
        bio: data.store_filters.bio ? "oui" : "non",
        gluten: data.store_filters.gluten_free ? "avec" : "sans",
        vegan: data.store_filters.vegan ? "oui" : "sans",
        veggie: data.store_filters.veggie ? "oui" : "non",
        terrace: data.store_filters.terrace ? "oui" : "non",
        take_away: data.store_filters.take_away ? "oui" : "non",
        handicap: data.store_filters.handicap.reduce((acc, currHand) => {
          return [...acc, currHand.type];
        }, []),
        city: data.localisation.city,
        address: data.localisation.address,
        zip_code: data.localisation.zip_code,
        ...open_hours,
      });
    }
  }, []);

  const hasErrors = (fieldsError) => {
    return Object.keys(fieldsError).some((field) => fieldsError[field]);
  };

  const handleImage = (imageList) => {
    setImages(
      imageList
        .map((image) => image.thumbUrl || image.url)
        .reduce((acc, dataImage) => {
          return [...acc, { ["uri"]: dataImage }];
        }, [])
    );
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    form.validateFields(async (err, values) => {
      const {
        city,
        address,
        zip_code,
        price_average,
        gluten,
        bio,
        vegan,
        veggie,
        terrace,
        take_away,
        handicap,
        mon,
        monOpen,
        monClose,
        tue,
        tueOpen,
        tueClose,
        wed,
        wedOpen,
        wedClose,
        thu,
        thuOpen,
        thuClose,
        fri,
        friOpen,
        friClose,
        sat,
        satOpen,
        satClose,
        sun,
        sunOpen,
        sunClose,
        ...rest
      } = values;

      const mappedHandicap = handicap.reduce((acc, hand) => {
        return [...acc, { ["type"]: hand }];
      }, []);

      const store_filters = {
        price_average,
        gluten: gluten === "avec" ? true : false,
        bio: bio === "oui" ? true : false,
        vegan: vegan === "oui" ? true : false,
        veggie: veggie === "oui" ? true : false,
        terrace: terrace === "oui" ? true : false,
        take_away: take_away === "oui" ? true : false,
        handicap: mappedHandicap,
      };
      const localisation = { city, address, zip_code };

      const open_hours = {
        mon: mon === "Ouvert" ? `${monOpen} - ${monClose}` : "Fermé",
        tue: tue === "Ouvert" ? `${tueOpen} - ${tueClose}` : "Fermé",
        wed: wed === "Ouvert" ? `${wedOpen} - ${wedClose}` : "Fermé",
        thu: thu === "Ouvert" ? `${thuOpen} - ${thuClose}` : "Fermé",
        fri: fri === "Ouvert" ? `${friOpen} - ${friClose}` : "Fermé",
        sat: sat === "Ouvert" ? `${satOpen} - ${satClose}` : "Fermé",
        sun: sun === "Ouvert" ? `${sunOpen} - ${sunClose}` : "Fermé",
      };

      const newData = {
        store_filters,
        localisation,
        open_hours,
        images,
        _proposed_by_user: jwtDecode(getToken()).id,
        status: "ON",
        ...rest,
      };

      setLoading(true);
      try {
        if (action) {
          await action(newData, data);
        }
        notification.info({
          message: `${data ? "Modification" : "Ajout"} effectué${
            data ? "e" : ""
          }`,
          description: `Le point d'intérêt ${values.name} a été ${
            data ? "modifié" : "créé"
          }.`,
        });
        if (data === null) {
          form.resetFields();
        }
      } catch {
        notification.error({
          message: `Une erreur est survenue`,
          description: `Veuillez recommencer.`,
        });
      } finally {
        setLoading(false);
      }
    });
  };

  return (
    <>
      <Row type="flex" justify="center">
        <Col span={16}>
          <div style={{ marginBottom: 40 }}>
            <MainTitle text="Ajouter un point d'intérêt" />
          </div>
          <Row type="flex" justify="space-between" gutter={[24, 0]}>
            <Col span={24}>
              <div
                style={{
                  marginBottom: 10,
                }}
              >
                <label
                  style={{
                    fontSize: 14,
                    color: "#798090",
                    textTransform: "uppercase",
                  }}
                >
                  Ajouter des images
                </label>
              </div>

              <PicturesList
                handleImage={handleImage}
                poiImageList={data ? data.images : null}
              />
            </Col>
            <Col span={24}>
              <Form.Item
                validateStatus={errorText.name ? "error" : ""}
                help={errorText.name || ""}
              >
                {getFieldDecorator("name", {
                  initialValue: "",
                  rules: [
                    {
                      required: true,
                      message: "Merci d'entrer un nom",
                    },
                  ],
                })(
                  <InputLabel
                    name="name"
                    label="nom du point d'intérêt"
                    placeholder="Nom du point d'intérêt"
                  />
                )}
              </Form.Item>
            </Col>
            <Col span={24}>
              <label
                style={{
                  fontSize: "14px",
                  color: "#798090",
                  textTransform: "uppercase",
                }}
              >
                Description
              </label>
              <Form.Item
                style={{
                  marginTop: "15px",
                }}
                validateStatus={errorText.description ? "error" : ""}
                help={errorText.description || ""}
              >
                {getFieldDecorator("description", {
                  initialValue: "",
                  rules: [
                    {
                      required: true,
                      message: "Merci d'entrer une description",
                    },
                  ],
                })(
                  <TextArea
                    style={{ fontSize: "16px" }}
                    placeholder="Description du point d'intérêt"
                    rows={4}
                  />
                )}
              </Form.Item>
            </Col>
            <Col span={16}>
              <Form.Item
                validateStatus={errorText.address ? "error" : ""}
                help={errorText.address || ""}
              >
                {getFieldDecorator("address", {
                  initialValue: "",
                  rules: [
                    {
                      required: true,
                      message: "Merci d'entrer une adresse",
                    },
                  ],
                })(
                  <InputLabel
                    name="address"
                    label="Adresse"
                    placeholder="Adresse"
                  />
                )}
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item
                validateStatus={errorText.zip_code ? "error" : ""}
                help={errorText.zip_code || ""}
              >
                {getFieldDecorator("zip_code", {
                  initialValue: "",
                  rules: [
                    {
                      required: true,
                      message: "Merci d'entrer un code postal",
                    },
                  ],
                })(
                  <InputLabel
                    name="zip_code"
                    label="Code postal"
                    placeholder="Code postal"
                  />
                )}
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                validateStatus={errorText.city ? "error" : ""}
                help={errorText.city || ""}
              >
                {getFieldDecorator("city", {
                  initialValue: "",
                  rules: [
                    {
                      required: true,
                      message: "Merci d'entrer une ville",
                    },
                  ],
                })(<InputLabel label="ville" placeholder="Ville" />)}
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item
                validateStatus={errorText.phone ? "error" : ""}
                help={errorText.phone || ""}
              >
                {getFieldDecorator("phone", {
                  initialValue: "",
                  rules: [
                    {
                      required: true,
                      message: "Merci d'entrer un numéro de téléphone",
                    },
                  ],
                })(
                  <InputLabel
                    name="mobile"
                    label="mobile du point d'intérêt"
                    placeholder="Mobile du point d'intérêt"
                  />
                )}
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item
                validateStatus={errorText.email ? "error" : ""}
                help={errorText.email || ""}
              >
                {getFieldDecorator("email", {
                  initialValue: "",
                  rules: [
                    {
                      required: true,
                      message: "Merci d'entrer un email",
                    },
                  ],
                })(
                  <InputLabel
                    name="email"
                    label="email du point d'intérêt"
                    placeholder="Email du point d'intérêt"
                  />
                )}
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item>
                {getFieldDecorator("web_site", {
                  initialValue: "",
                  rules: [
                    {
                      required: false,
                    },
                  ],
                })(
                  <InputLabel
                    name="website"
                    label="site web du point d'intérêt"
                    placeholder="Site web du point d'intérêt"
                  />
                )}
              </Form.Item>
            </Col>
            <Col span={24}>
              <h4
                style={{
                  fontSize: "14px",
                  color: "#798090",
                  textTransform: "uppercase",
                }}
              >
                Horaires
              </h4>
            </Col>
            <Col span={8}>
              <Form.Item
                validateStatus={errorText.mon ? "error" : ""}
                help={errorText.mon || ""}
              >
                {getFieldDecorator("mon", {
                  initialValue: "Ouvert",
                  rules: [
                    {
                      required: true,
                      message:
                        "Merci d'indiquer si ce jour le point d'intérêt est ouvert ou non",
                    },
                  ],
                })(<SelectLabel label="Lundi" items={selectData.openingDay} />)}
              </Form.Item>
            </Col>
            {getFieldValue("mon") === "Ouvert" ? (
              <>
                <Col span={8}>
                  <Form.Item>
                    {getFieldDecorator("monOpen", {
                      initialValue: "",
                      rules: [
                        {
                          required: false,
                        },
                      ],
                    })(
                      <SelectLabel
                        label="Ouverture"
                        items={selectData.openingTime}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item>
                    {getFieldDecorator("monClose", {
                      initialValue: "",
                      rules: [
                        {
                          required: false,
                        },
                      ],
                    })(
                      <SelectLabel
                        label="Fermeture"
                        items={selectData.openingTime}
                      />
                    )}
                  </Form.Item>
                </Col>
              </>
            ) : (
              <Col span={16}></Col>
            )}
            <Col span={8}>
              <Form.Item
                validateStatus={errorText.tue ? "error" : ""}
                help={errorText.tue || ""}
              >
                {getFieldDecorator("tue", {
                  initialValue: "Ouvert",
                  rules: [
                    {
                      required: true,
                      message:
                        "Merci d'indiquer si ce jour le point d'intérêt est ouvert ou non",
                    },
                  ],
                })(<SelectLabel label="Mardi" items={selectData.openingDay} />)}
              </Form.Item>
            </Col>
            {getFieldValue("tue") === "Ouvert" ? (
              <>
                <Col span={8}>
                  <Form.Item>
                    {getFieldDecorator("tueOpen", {
                      initialValue: "",
                      rules: [
                        {
                          required: false,
                        },
                      ],
                    })(
                      <SelectLabel
                        label="Ouverture"
                        items={selectData.openingTime}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item>
                    {getFieldDecorator("tueClose", {
                      initialValue: "",
                      rules: [
                        {
                          required: false,
                        },
                      ],
                    })(
                      <SelectLabel
                        label="Fermeture"
                        items={selectData.openingTime}
                      />
                    )}
                  </Form.Item>
                </Col>
              </>
            ) : (
              <Col span={16}></Col>
            )}
            <Col span={8}>
              <Form.Item
                validateStatus={errorText.wed ? "error" : ""}
                help={errorText.wed || ""}
              >
                {getFieldDecorator("wed", {
                  initialValue: "Ouvert",
                  rules: [
                    {
                      required: true,
                      message:
                        "Merci d'indiquer si ce jour le point d'intérêt est ouvert ou non",
                    },
                  ],
                })(
                  <SelectLabel label="Mercredi" items={selectData.openingDay} />
                )}
              </Form.Item>
            </Col>
            {getFieldValue("wed") === "Ouvert" ? (
              <>
                <Col span={8}>
                  <Form.Item>
                    {getFieldDecorator("wedOpen", {
                      initialValue: "",
                      rules: [
                        {
                          required: false,
                        },
                      ],
                    })(
                      <SelectLabel
                        label="Ouverture"
                        items={selectData.openingTime}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item>
                    {getFieldDecorator("wedClose", {
                      initialValue: "",
                      rules: [
                        {
                          required: false,
                        },
                      ],
                    })(
                      <SelectLabel
                        label="Fermeture"
                        items={selectData.openingTime}
                      />
                    )}
                  </Form.Item>
                </Col>
              </>
            ) : (
              <Col span={16}></Col>
            )}
            <Col span={8}>
              <Form.Item
                validateStatus={errorText.thu ? "error" : ""}
                help={errorText.thu || ""}
              >
                {getFieldDecorator("thu", {
                  initialValue: "Ouvert",
                  rules: [
                    {
                      required: true,
                      message:
                        "Merci d'indiquer si ce jour le point d'intérêt est ouvert ou non",
                    },
                  ],
                })(<SelectLabel label="Jeudi" items={selectData.openingDay} />)}
              </Form.Item>
            </Col>
            {getFieldValue("thu") === "Ouvert" ? (
              <>
                <Col span={8}>
                  <Form.Item>
                    {getFieldDecorator("thuOpen", {
                      initialValue: "",
                      rules: [
                        {
                          required: false,
                        },
                      ],
                    })(
                      <SelectLabel
                        label="Ouverture"
                        items={selectData.openingTime}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item>
                    {getFieldDecorator("thuClose", {
                      initialValue: "",
                      rules: [
                        {
                          required: false,
                        },
                      ],
                    })(
                      <SelectLabel
                        label="Fermeture"
                        items={selectData.openingTime}
                      />
                    )}
                  </Form.Item>
                </Col>
              </>
            ) : (
              <Col span={16}></Col>
            )}
            <Col span={8}>
              <Form.Item
                validateStatus={errorText.fri ? "error" : ""}
                help={errorText.fri || ""}
              >
                {getFieldDecorator("fri", {
                  initialValue: "Ouvert",
                  rules: [
                    {
                      required: true,
                      message:
                        "Merci d'indiquer si ce jour le point d'intérêt est ouvert ou non",
                    },
                  ],
                })(
                  <SelectLabel label="Vendredi" items={selectData.openingDay} />
                )}
              </Form.Item>
            </Col>
            {getFieldValue("fri") === "Ouvert" ? (
              <>
                <Col span={8}>
                  <Form.Item>
                    {getFieldDecorator("friOpen", {
                      initialValue: "",
                      rules: [
                        {
                          required: false,
                        },
                      ],
                    })(
                      <SelectLabel
                        label="Ouverture"
                        items={selectData.openingTime}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item>
                    {getFieldDecorator("friClose", {
                      initialValue: "",
                      rules: [
                        {
                          required: false,
                        },
                      ],
                    })(
                      <SelectLabel
                        label="Fermeture"
                        items={selectData.openingTime}
                      />
                    )}
                  </Form.Item>
                </Col>
              </>
            ) : (
              <Col span={16}></Col>
            )}
            <Col span={8}>
              <Form.Item
                validateStatus={errorText.sat ? "error" : ""}
                help={errorText.sat || ""}
              >
                {getFieldDecorator("sat", {
                  initialValue: "Ouvert",
                  rules: [
                    {
                      required: true,
                      message:
                        "Merci d'indiquer si ce jour le point d'intérêt est ouvert ou non",
                    },
                  ],
                })(
                  <SelectLabel label="Samedi" items={selectData.openingDay} />
                )}
              </Form.Item>
            </Col>
            {getFieldValue("sat") === "Ouvert" ? (
              <>
                <Col span={8}>
                  <Form.Item>
                    {getFieldDecorator("satOpen", {
                      initialValue: "",
                      rules: [
                        {
                          required: false,
                        },
                      ],
                    })(
                      <SelectLabel
                        label="Ouverture"
                        items={selectData.openingTime}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item>
                    {getFieldDecorator("satClose", {
                      initialValue: "",
                      rules: [
                        {
                          required: false,
                        },
                      ],
                    })(
                      <SelectLabel
                        label="Fermeture"
                        items={selectData.openingTime}
                      />
                    )}
                  </Form.Item>
                </Col>
              </>
            ) : (
              <Col span={16}></Col>
            )}
            <Col span={8}>
              <Form.Item
                validateStatus={errorText.sun ? "error" : ""}
                help={errorText.sun || ""}
              >
                {getFieldDecorator("sun", {
                  initialValue: "Ouvert",
                  rules: [
                    {
                      required: true,
                      message:
                        "Merci d'indiquer si ce jour le point d'intérêt est ouvert ou non",
                    },
                  ],
                })(
                  <SelectLabel label="Dimanche" items={selectData.openingDay} />
                )}
              </Form.Item>
            </Col>
            {getFieldValue("sun") === "Ouvert" ? (
              <>
                <Col span={8}>
                  <Form.Item>
                    {getFieldDecorator("sunOpen", {
                      initialValue: "",
                      rules: [
                        {
                          required: false,
                        },
                      ],
                    })(
                      <SelectLabel
                        label="Ouverture"
                        items={selectData.openingTime}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item>
                    {getFieldDecorator("sunClose", {
                      initialValue: "",
                      rules: [
                        {
                          required: false,
                        },
                      ],
                    })(
                      <SelectLabel
                        label="Fermeture"
                        items={selectData.openingTime}
                      />
                    )}
                  </Form.Item>
                </Col>
              </>
            ) : (
              <Col span={16}></Col>
            )}

            <Col span={12} style={{ marginTop: "10px" }}>
              <label
                style={{
                  fontSize: "14px",
                  color: "#798090",
                  textTransform: "uppercase",
                  paddingTop: "10px",
                  paddingBottom: "10px",
                }}
              >
                Handicap
              </label>
              <Form.Item style={{ marginTop: "15px" }}>
                {getFieldDecorator("handicap", {
                  initialValue: [],
                  rules: [
                    {
                      required: false,
                    },
                  ],
                })(
                  <Checkbox.Group options={["Visuel", "Auditif", "Moteur"]} />
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item>
                {getFieldDecorator("price_average", {
                  initialValue: "",
                  rules: [
                    {
                      required: false,
                    },
                  ],
                })(
                  <InputLabel
                    label="Prix moyen"
                    min={0}
                    placeholder="Prix moyen"
                    type="Number"
                    addonAfter={<Icon type="euro" />}
                  />
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                validateStatus={errorText.store_type ? "error" : ""}
                help={errorText.store_type || ""}
              >
                {getFieldDecorator("store_type", {
                  initialValue: "",
                  rules: [
                    {
                      required: true,
                      message: "Merci d'entrer le type du point d'intérêt",
                    },
                  ],
                })(<SelectLabel label="Type" items={selectData.store_type} />)}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                validateStatus={errorText.store_speciality ? "error" : ""}
                help={errorText.store_speciality || ""}
              >
                {getFieldDecorator("store_speciality", {
                  initialValue: "",
                  rules: [
                    {
                      required: true,
                      message:
                        "Merci d'entrer la spécialité du point d'intérêt",
                    },
                  ],
                })(
                  <SelectLabel
                    label="Spécialité"
                    items={selectData.store_speciality}
                  />
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                validateStatus={errorText.greenscore ? "error" : ""}
                help={errorText.greenscore || ""}
              >
                {getFieldDecorator("greenscore", {
                  initialValue: "",
                  rules: [
                    {
                      required: true,
                      message: "Veuillez founir un greenscore",
                    },
                  ],
                })(
                  <InputLabel
                    label="Greenscore"
                    placeholder="Greenscore"
                    type="Number"
                    min={0}
                    max={100}
                    addonAfter={<Icon type="percentage" />}
                  />
                )}
              </Form.Item>
            </Col>
            <Col
              span={12}
              style={{
                marginTop: "5px",
              }}
            >
              <label
                style={{
                  fontSize: "14px",
                  color: "#798090",
                  textTransform: "uppercase",
                }}
              >
                Description greenscore
              </label>
              <Form.Item
                style={{
                  marginTop: "15px",
                }}
              >
                {getFieldDecorator("greenscore_description", {
                  initialValue: "",
                })(
                  <TextArea
                    style={{ fontSize: "16px" }}
                    placeholder="Description du greenscore"
                    rows={4}
                  />
                )}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item>
                {getFieldDecorator("bio", {
                  initialValue: "",
                  rules: [
                    {
                      required: false,
                    },
                  ],
                })(<GroupBtnLabel label="bio" items={["oui", "non"]} />)}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item>
                {getFieldDecorator("gluten", {
                  initialValue: "",
                  rules: [
                    {
                      required: false,
                    },
                  ],
                })(<GroupBtnLabel label="gluten" items={["avec", "sans"]} />)}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item>
                {getFieldDecorator("vegan", {
                  initialValue: "",
                  rules: [
                    {
                      required: false,
                    },
                  ],
                })(<GroupBtnLabel label="vegan" items={["oui", "non"]} />)}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item>
                {getFieldDecorator("veggie", {
                  initialValue: "",
                  rules: [
                    {
                      required: false,
                    },
                  ],
                })(<GroupBtnLabel label="veggie" items={["oui", "non"]} />)}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item>
                {getFieldDecorator("terrace", {
                  initialValue: "",
                  rules: [
                    {
                      required: false,
                    },
                  ],
                })(<GroupBtnLabel label="terrace" items={["oui", "non"]} />)}
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item>
                {getFieldDecorator("take_away", {
                  initialValue: "",
                  rules: [
                    {
                      required: false,
                    },
                  ],
                })(<GroupBtnLabel label="à emporter" items={["oui", "non"]} />)}
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item>
                <Button
                  htmlType="submit"
                  disabled={hasErrors(form.getFieldsError()) || loading}
                  size="large"
                  block
                  style={{ marginTop: 16 }}
                  onClick={handleSubmit}
                  loading={loading}
                >
                  {data ? "Modifier" : "Valider"}
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  );
};
export default Form.create()(PoiLayout);
