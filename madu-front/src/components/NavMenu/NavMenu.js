import React, { useState, useEffect } from "react";
import { Menu, Icon, Layout } from "antd";
import { Link } from "react-router-dom";
import TagName from "../_shared/tag/TagName";
import jwtDecode from "jwt-decode";
import { getToken } from "../../utils/token";
import { getUser } from "../../utils/api";
import { useHistory } from "react-router-dom";
import logoMadu from "../../assets/img/madu-logo-2.svg";
import "./navMenu.scss";

const NavMenu = () => {
  const history = useHistory();
  const { Sider } = Layout;
  const [data, setData] = useState(null);

  const style = {
    styleItem: {
      fontSize: 18,
      marginBottom: 16,
      marginTop: 16,
      fontWeight: 400,
    },
  };

  const currentRoute = () => {
    localStorage.clear();
  };

  useEffect(() => {
    getUser(jwtDecode(getToken()).id).then((user) => {
      setData(user.data);
      if (user.success === false) {
        history.push("/");
        alert("Seul les admins peuvent accéder au back office");
      }
    });
  }, []);

  return (
    <Sider
      style={{
        height: "100vh",
        position: "relative",
        zIndex: "9999999",
        boxShadow: "3px 0px 8px 2px #ABABAB",
      }}
      disabled={false}
      width="235"
    >
      <div className="containerTopNav">
        <img src={logoMadu} alt="logo of madu" />
        <TagName role={data && data.role} />
      </div>
      {data && data.role === "SA" && (
        <Menu theme="dark" mode="inline">
          <Menu.Item key="1" style={style.styleItem}>
            <Link to="/back-office/customers">
              <span>Clients</span>
            </Link>
            <Link to="/back-office/customers/add">
              <Icon
                type="plus-circle"
                style={{ fontSize: "16px", color: "#ffffff" }}
                theme="outlined"
              />
            </Link>
          </Menu.Item>
          <Menu.Item key="2" style={style.styleItem}>
            <Link to="/back-office/poi">
              <span>Points d'intérêts</span>
            </Link>
            <Link to="/back-office/poi/add">
              <Icon
                type="plus-circle"
                style={{ fontSize: "16px", color: "#ffffff" }}
                theme="outlined"
              />
            </Link>
          </Menu.Item>
          <Menu.Item key="3" style={style.styleItem}>
            <Link to="/back-office/map">
              <span>Maps</span>
            </Link>
          </Menu.Item>
          <Menu.Item key="4" style={style.styleItem}>
            <Link to="/back-office/challenge">
              <span>Défis</span>
            </Link>
            <Link to="/back-office/challenge/add">
              <Icon
                type="plus-circle"
                style={{ fontSize: "16px", color: "#ffffff" }}
                theme="outlined"
              />
            </Link>
          </Menu.Item>
        </Menu>
      )}
      <div className="button-logout">
        <Link to="/" onClick={() => currentRoute()}>
          <span>Déconnexion</span>
        </Link>
      </div>
    </Sider>
  );
};

export default NavMenu;
