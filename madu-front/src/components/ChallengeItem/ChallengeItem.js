import React from "react";
import "./challengeitem.scss";
import { Button, Modal } from "antd";
import { ExclamationCircleOutlined } from "@ant-design/icons";

const ChallengeItem = ({
  color = "#F8BA76",
  points = "20",
  type = "type",
  name = "name",
  description = "description",
  dayleft = "unknow",
  onDelete,
  onUpdate,
}) => {
  const confirmDeletion = () => {
    Modal.confirm({
      title: "Suppression d'un défi",
      icon: <ExclamationCircleOutlined />,
      content: `Etes vous sur de vouloir supprimer ${name}`,
      okText: "Oui",
      cancelText: "Non",
      onOk() {
        onDelete();
      },
    });
  };

  return (
    <div className="challengeitem">
      <div className="challengeitem-container">
        <span className="challengeitem-type" style={{ color }}>
          {type}
        </span>
        <h2 className="challengeitem-title">{name}</h2>
        <p className="challengeitem-content">{description}</p>
        <div className="challengeitem-flex">
          <span className="challengeitem-timer">{dayleft} jours restants</span>
          <Button className="challengeitem-btn-update" onClick={onUpdate}>
            Modifier
          </Button>
        </div>
        <div className="challengeitem-flex">
          <span className="challengeitem-points">+ {points} points</span>
          <Button
            className="challengeitem-btn-delete"
            onClick={confirmDeletion}
          >
            Supprimer
          </Button>
        </div>
      </div>
    </div>
  );
};
export default ChallengeItem;
