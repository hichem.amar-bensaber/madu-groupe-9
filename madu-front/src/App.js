import React, { useState } from "react";
import "./assets/styles/_colors.scss";
import "./assets/styles/_global.scss";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Customers from "./Views/Customers/Customers";
import { Layout } from "antd";
import CustomersAdd from "./Views/CustomersAdd";
import Login from "./Views/Login/Login"; // Start view before the back-office
import Map from "./Views/Map/Map";
import Poi from "./Views/Poi/Poi";
import PoiAdd from "./Views/PoiAdd/PoiAdd";
import PoiUpdate from "./Views/PoiUpdate/PoiUpdate";
import SecureRoute from "./components/_shared/SecureRoute/SecureRoute";
import Challenge from "./Views/Challenge/Challenge";
import ChallengeAdd from "./Views/ChallengeAdd/ChallengeAdd";
import ChallengeUpdate from "./Views/ChallengeUpdate/ChallengeUpdate";
import Awards from "./Views/Awards/Awards";

const App = () => {
  const routes = [
    {
      id: 1,
      path: "/",
      name: "connexion",
      exact: true,
      main: () => <Login />,
    },
    {
      id: 2,
      path: "/back-office/customers",
      name: "customers",
      exact: true,
      main: () => <SecureRoute Component={Customers} />,
    },
    {
      id: 3,
      path: "/back-office/poi",
      name: "poi",
      exact: true,
      main: () => <SecureRoute Component={Poi} />,
    },
    {
      id: 4,
      path: "/back-office/map",
      name: "map",
      exact: true,
      main: () => <SecureRoute Component={Map} />,
    },
    {
      id: 5,
      path: "/back-office/customers/add",
      name: "customers-add",
      exact: true,
      main: () => <SecureRoute Component={CustomersAdd} />,
    },
    {
      id: 6,
      path: "/back-office/poi/add",
      name: "poi-add",
      exact: true,
      main: () => <SecureRoute Component={PoiAdd} />,
    },
    {
      id: 7,
      path: "/back-office/poi/update/:id",
      name: "poi-update",
      exact: true,
      main: () => <SecureRoute Component={PoiUpdate} />,
    },
    {
      id: 8,
      path: "/back-office/challenge",
      name: "challenge",
      exact: true,
      main: () => <SecureRoute Component={Challenge} />,
    },
    {
      id: 9,
      path: "/back-office/challenge/add",
      name: "challenge-add",
      exact: true,
      main: () => <SecureRoute Component={ChallengeAdd} />,
    },
    {
      id: 9,
      path: "/back-office/challenge/update/:id",
      name: "challenge-update",
      exact: true,
      main: () => <SecureRoute Component={ChallengeUpdate} />,
    },
    {
      id: 10,
      path: "/back-office/challenge/awards",
      name: "challenge-awards",
      exact: true,
      main: () => <SecureRoute Component={Awards} />,
    },
  ];
  // const [showNav, setShowNav] = useState(false);

  return (
    <div className="App">
      <Layout style={{ minHeight: "100vh" }}>
        <Router>
          <Switch>
            {routes.map((route, id) => (
              <Route
                key={id}
                path={route.path}
                name={route.name}
                render={route.main}
                exact={route.exact}
              />
            ))}
          </Switch>
        </Router>
      </Layout>
    </div>
  );
};

export default App;
