import {
  USER_LOGIN_INFOS,
  INSERT_CUSTOMERS,
  EDIT_CUSTOMER_PROPERTY,
  EDIT_CUSTOMERSLIST_PROPERTY,
  DELETE_CUSTOMER,
  INSERT_POI,
} from "../constants/index";

// USER
export const handleUserLogin = (content, value) => {
  return {
    type: USER_LOGIN_INFOS,
    [content]: value,
    name: content,
  };
};

// CUSTOMERS
export const addCustomers = (data) => {
  return (dispatch) => {
    dispatch({
      type: INSERT_CUSTOMERS,
      data,
    });
  };
};

export const deleteCustomer = (id) => {
  return (dispatch) => {
    dispatch({
      type: DELETE_CUSTOMER,
      id,
    });
  };
};

export const editCustomerProperty = (event) => {
  const target = event.target;
  const name = target.name;
  const value = target.value;
  const key = target.dataset.index;
  return (dispatch) => {
    dispatch({ type: EDIT_CUSTOMER_PROPERTY, key, name, value });
  };
};

export const editCustomersListProperty = (event) => {
  const target = event.target;
  const name = target.name;
  const value = target.value;
  const key = target.dataset.index;
  return (dispatch) => {
    dispatch({
      type: EDIT_CUSTOMERSLIST_PROPERTY,
      key,
      name,
      value,
    });
  };
};

// POI
export const addPoi = (data) => {
  return (dispatch) => {
    dispatch({
      type: INSERT_POI,
      data,
    });
  };
};
