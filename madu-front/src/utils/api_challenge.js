import { getToken } from "../utils/token";
import { REACT_APP_API_URL } from "./config";

export const addChallenge = (data) => {
  const createData = { create: data };
  return fetch(`${REACT_APP_API_URL}/challenges/create`, {
    method: "POST",
    body: JSON.stringify(createData),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((res) => {
      console.log(res);
      // return res.json();
    })
    .catch((err) => {
      throw new Error(err);
    });
};

export const getChallengeList = (data) => {
  const createDate = { create: data };
  return fetch(`${REACT_APP_API_URL}/challenges/list`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getToken()}`,
    },
  }).then(
    (res) => {
      return res.json();
    },
    (err) => {
      throw new Error(err);
    }
  );
};
export const getChallengeById = (id) => {
  return fetch(`${REACT_APP_API_URL}/challenges/${id}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((res) => {
      return res.json();
    })
    .catch((err) => {
      console.log(err);
    });
};
export const updateChallengeById = (data, { _id }) => {
  const updateData = { update: data };
  return fetch(`${REACT_APP_API_URL}/challenges/${_id}/update`, {
    method: "PUT",
    body: JSON.stringify(updateData),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((res) => {
      res.json().then((res) => {
        console.log(res);
      });
    })
    .catch((err) => {
      throw new Error(err);
    });
};
export const deleteChallengeById = (_id) => {
  return fetch(`${REACT_APP_API_URL}/challenges/${_id}/delete`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((res) => {
      res.json().then((res) => {
        console.log(res);
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
