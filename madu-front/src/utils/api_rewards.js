import { getToken } from "../utils/token";
import { REACT_APP_API_URL } from "./config";

export const addRewards = (data) => {
  const createData = { create: data };
  return fetch(`${REACT_APP_API_URL}/rewards/create`, {
    method: "POST",
    body: JSON.stringify(createData),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((res) => {
      console.log(res);
      // return res.json();
    })
    .catch((err) => {
      throw new Error(err);
    });
};
export const getFirstReward = () => {
  return fetch(`${REACT_APP_API_URL}/rewards/list`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getToken()}`,
    },
  }).then((res) => {
    return res.json().then(
      (res) => {
        if (res.success) {
          return res.data[res.data.length - 1];
        } else {
          return {
            novice: "",
            adventurer: "",
            master: "",
          };
        }
      },
      (err) => {
        throw new Error(err);
      }
    );
  });
};
