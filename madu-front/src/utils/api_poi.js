import { getToken } from "./token";
import { REACT_APP_API_URL } from "./config";

export const getPoi = () => {
  return fetch(`${REACT_APP_API_URL}/stores/list`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getToken()}`,
    },
  }).then((res) => {
    return res.json();
  });
};

export const addPoi = (data) => {
  return fetch(`${REACT_APP_API_URL}/stores/create`, {
    method: "POST",
    body: JSON.stringify({ create: data }),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getToken()}`,
    },
  });
};

export const getPoiById = (id) => {
  return fetch(`${REACT_APP_API_URL}/stores/${id}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((res) => {
      return res.json();
    })
    .catch((err) => {
      console.log(err);
    });
};

export const updatePoiById = (data, { _id }) => {
  const updateData = { update: data };
  return fetch(`${REACT_APP_API_URL}/stores/${_id}/update`, {
    method: "PUT",
    body: JSON.stringify(updateData),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((res) => {
      res.json().then((res) => {
        console.log(res);
      });
    })
    .catch((err) => {
      throw new Error(err);
    });
};

export const deletePoiById = (id) => {
  return fetch(`${REACT_APP_API_URL}/stores/${id}/delete`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  });
};
