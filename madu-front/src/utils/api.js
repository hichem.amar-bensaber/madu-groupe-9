import { getToken } from "../utils/token";
import { REACT_APP_API_URL } from "./config";

export const checkForConnection = (data) => {
  return fetch(`${REACT_APP_API_URL}/login`, {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((res) => {
      return res.json();
    })
    .catch((err) => {
      console.log(err);
    });
};

export const getUser = (id) => {
  return fetch(`${REACT_APP_API_URL}/users/${id}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getToken()}`,
    },
  }).then((res) => {
    return res.json();
  });
};

export const getClients = () => {
  return fetch(`${REACT_APP_API_URL}/clients/list`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getToken()}`,
    },
  }).then((res) => {
    return res.json();
  });
};

export const addClient = (data) => {
  return fetch(`${REACT_APP_API_URL}/clients/create`, {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getToken()}`,
    },
  });
};

export const deleteCustomerById = (id) => {
  fetch(`${REACT_APP_API_URL}/clients/${id}/delete`, {
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${getToken()}`,
    },
  });
};

export const putCustomerById = (id, { localisation, name, email, phone }) => {
  const update = { update: { localisation, name, email, phone } };

  return fetch(`${REACT_APP_API_URL}/clients/${id}/update`, {
    method: "PUT",
    body: JSON.stringify(update),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getToken()}`,
    },
  })
    .then((res) => {
      res.json().then((res) => {
        console.log(res);
      });
    })
    .catch((err) => {
      throw new Error(err);
    });
};
