import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { getPoi } from "../../utils/api_poi";
import * as actions from "../../action/index";
import PoiCard from "../../components/_shared/PoiCard/PoiCard";
import NavMenu from "../../components/NavMenu/NavMenu";
import { Row, Col, Input, Form } from "antd";
import { MainLayout } from "../../components/MainLayout/MainLayout";
import { Badge } from "../../components/_shared/Badge/Badge";

const Poi = ({ poi, addPoi, form }) => {
  const history = useHistory();
  const { getFieldDecorator } = form;
  const [filteredPoi, setFilteredPoi] = useState(null);

  useEffect(() => {
    getPoi()
      .then((res) => {
        addPoi(res.data);
      })
      .catch((e) => {
        addPoi([]);
      });
  }, []);

  const filterPoi = (event) => {
    event.preventDefault();
    setFilteredPoi(
      poi.filter((item) =>
        item.name.toLowerCase().startsWith(event.target.value)
      )
    );
  };

  return (
    <>
      <NavMenu />
      <MainLayout>
        <Row type="flex" justify="center" gutter={[20, 20]}>
          <Col span={20}>
            <Form.Item>
              {getFieldDecorator("poiSearch", { initialValue: "" })(
                <div>
                  <label
                    style={{ fontSize: "15px", textTransform: "uppercase" }}
                  >
                    Rechercher un point d'intérêt
                  </label>
                  <Input
                    placeholder="Recherche..."
                    onChange={filterPoi}
                  ></Input>
                </div>
              )}
            </Form.Item>
          </Col>
          {filteredPoi === null &&
            poi.map(
              (
                {
                  _id,
                  name,
                  description,
                  phone,
                  email,
                  web_site,
                  store_type,
                  store_speciality,
                  store_filters,
                  localisation,
                  open_hours,
                  greenscore,
                  images,
                },
                index
              ) => (
                <Col span={10} key={_id}>
                  <PoiCard
                    key={index}
                    id={_id}
                    name={name}
                    description={description}
                    phone={phone}
                    email={email}
                    web_site={web_site}
                    store_type={store_type}
                    store_speciality={store_speciality}
                    store_filters={store_filters}
                    localisation={localisation}
                    open_hours={open_hours}
                    greenscore={greenscore}
                    images={images}
                    onUpdate={() =>
                      history.push(`/back-office/poi/update/${_id}`)
                    }
                  >
                    <Badge tag={store_filters} />
                  </PoiCard>
                </Col>
              )
            )}
          {filteredPoi !== null &&
            filteredPoi.map(
              (
                {
                  _id,
                  name,
                  description,
                  phone,
                  email,
                  web_site,
                  store_type,
                  store_speciality,
                  store_filters,
                  localisation,
                  open_hours,
                  greenscore,
                  images,
                },
                index
              ) => (
                <Col span={10}>
                  <PoiCard
                    key={index}
                    id={_id}
                    name={name}
                    description={description}
                    phone={phone}
                    email={email}
                    web_site={web_site}
                    store_type={store_type}
                    store_speciality={store_speciality}
                    store_filters={store_filters}
                    localisation={localisation}
                    open_hours={open_hours}
                    greenscore={greenscore}
                    images={images}
                    onUpdate={() =>
                      history.push(`/back-office/poi/update/${_id}`)
                    }
                  >
                    <Badge tag={store_filters} />
                  </PoiCard>
                </Col>
              )
            )}
        </Row>
      </MainLayout>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    poi: state.poi,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addPoi: (data) => dispatch(actions.addPoi(data)),
  };
};

export default Form.create()(connect(mapStateToProps, mapDispatchToProps)(Poi));
