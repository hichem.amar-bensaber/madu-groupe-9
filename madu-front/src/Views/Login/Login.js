import React from "react";
import { useHistory } from "react-router-dom";

// LIBRARY ANT.DESIGN
import { Input, Row, Col } from "antd";

// ASSETS
import logoMadu from "../../assets/img/maduLogo.svg";

// COMPONENTS
import ButtonDefault from "../../components/_shared/Button/ButtonDefault";
import "../Login/login.scss";

// REDUX
import { connect } from "react-redux";
import { handleUserLogin } from "../../action/index";

// API / Token
import { checkForConnection } from "../../utils/api";
import { setToken } from "../../utils/token";

const Login = ({ email, password, handleChange }) => {
  const history = useHistory();

  const getToken = () => {
    return localStorage.getItem("authorization_token");
  };

  const checkLogin = () => {
    checkForConnection({ email, password }).then(data => {
      if (data.success === true) {
        setToken(data.token);
        history.push("/back-office/customers");
      } else {
        alert("Votre nom d'utilisateur ou votre mot de passe n'est pas correct");
      }
    });
  };

  return (
    <div className="login">
      <Row
        type="flex"
        justify="center"
        align="middle"
        style={{ height: "100vh" }}
      >
        <Col span={12} style={{ textAlign: "center" }}>
          <img src={logoMadu} className="login-img" />
          <Row type="flex" justify="center" align="middle">
            <Col>
              <Input.Group size="large">
                <Col
                  span={24}
                  style={{ paddingRight: "0px", textAlign: "left" }}
                >
                  <Input
                    style={{
                      marginBottom: "20px",
                      backgroundColor: "transparent"
                    }}
                    placeholder="Email"
                    name="email"
                    value={email}
                    onChange={handleChange}
                  />
                </Col>
                <Col span={24}>
                  <Input.Password
                    style={{
                      marginBottom: "30px",
                      backgroundColor: "transparent"
                    }}
                    placeholder="Mot de passe"
                    name="password"
                    value={password}
                    onChange={handleChange}
                  />
                </Col>
              </Input.Group>
            </Col>
          </Row>
          <ButtonDefault
            onClick={checkLogin}
            color="#69FFD4"
            name="Connexion"
          />
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    email: state.user.email,
    password: state.user.password
  };
};

const mapDispatchToProps = dispatch => ({
  handleChange: event =>
    dispatch(handleUserLogin(event.target.name, event.target.value))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
