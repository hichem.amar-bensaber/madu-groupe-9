import React, { useState, useEffect } from "react";
import NavMenu from "../../components/NavMenu/NavMenu";
import { MainLayout } from "../../components/MainLayout/MainLayout";
import ChallengeLayout from "../../components/ChallengeLayout/ChallengeLayout";
import moment from "moment";
import { useParams } from "react-router-dom";
import {
  getChallengeById,
  updateChallengeById,
} from "../../utils/api_challenge";

const ChallengeAdd = () => {
  const { id } = useParams();
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getChallengeById(id).then((res) => {
      const duration = moment(res.data.duration * 1000);
      setData({ ...res.data, duration });
      setLoading(false);
    });
  }, []);

  return (
    <>
      <NavMenu />
      <MainLayout>
        {!loading && (
          <ChallengeLayout
            title={"modifier le défi: " + "ceci est un défi"}
            data={data}
            action={updateChallengeById}
          />
        )}
      </MainLayout>
    </>
  );
};

export default ChallengeAdd;
