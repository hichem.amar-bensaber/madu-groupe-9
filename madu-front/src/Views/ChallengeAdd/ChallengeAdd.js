import React from "react";
import NavMenu from "../../components/NavMenu/NavMenu";
import { MainLayout } from "../../components/MainLayout/MainLayout";
import ChallengeLayout from "../../components/ChallengeLayout/ChallengeLayout";
import { addChallenge } from "../../utils/api_challenge";

const ChallengeAdd = () => {
  return (
    <>
      <NavMenu />
      <MainLayout>
        <ChallengeLayout action={addChallenge} />
      </MainLayout>
    </>
  );
};

export default ChallengeAdd;
