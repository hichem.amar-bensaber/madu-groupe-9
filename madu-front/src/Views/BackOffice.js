import React from "react";
import { MainLayout } from "../components/MainLayout/MainLayout";
import Customers from "./Customers";

const BackOffice = () => {
  return (
    <MainLayout>
      <Customers key="2" />
    </MainLayout>
  );
};

export default BackOffice;
