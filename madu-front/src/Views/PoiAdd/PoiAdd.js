import React from "react";
import NavMenu from "../../components/NavMenu/NavMenu";
import { MainLayout } from "../../components/MainLayout/MainLayout";
import PoiLayout from "../../components/PoiLayout/PoiLayout";
import { addPoi } from "../../utils/api_poi";

const PoiAdd = () => {
  return (
    <>
      <NavMenu />
      <MainLayout>
        <PoiLayout action={addPoi} />
      </MainLayout>
    </>
  );
};

export default PoiAdd;
