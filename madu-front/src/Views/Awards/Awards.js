import React, { useState, useEffect } from "react";
import { MainTitle } from "../../components/_shared/MainTitle/MainTitle";
import NavMenu from "../../components/NavMenu/NavMenu";
import { MainLayout } from "../../components/MainLayout/MainLayout";
import { Row, Col, Form, notification } from "antd";
import { InputLabel } from "../../components/_shared/InputLabel/InputLabel";
import DefaultButton from "../../components/_shared/DefaultButton/DefaultButton";
import { addRewards, getFirstReward } from "../../utils/api_rewards";

const Awards = ({ form }) => {
  const {
    getFieldDecorator,
    getFieldsError,
    getFieldError,
    isFieldTouched,
    setFieldsValue,
  } = form;
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState({});

  useEffect(() => {
    getFirstReward().then((res) => {
      setData(res);
      setLoading(false);
      setFieldsValue({
        ...res,
      });
    });
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    form.validateFields(async (err, values) => {
      setLoading(true);
      try {
        await addRewards(values);
        notification.info({
          message: `Modification effectuée`,
          description: `Les récompenses ont été ajoutées.`,
        });
      } catch {
        notification.error({
          message: `Une erreur est survenue`,
          description: `Veuillez recommencer.`,
        });
      } finally {
        setLoading(false);
        form.validateFields();
      }
    });
  };

  return (
    <>
      <NavMenu />
      <MainLayout>
        <Row type="flex" justify="center">
          <Col span={16}>
            <div style={{ marginBottom: 40 }}>
              <MainTitle text="Configurer les awards" />
            </div>
          </Col>
          <Col span={16}>
            <Form.Item>
              {getFieldDecorator("novice", {
                rules: [
                  {
                    required: true,
                    message: "Merci de remplir ce champs",
                  },
                ],
              })(
                <InputLabel
                  name="novice"
                  label="Récompense novice"
                  placeholder="Récompense novice"
                />
              )}
            </Form.Item>
          </Col>
          <Col span={16}>
            <Form.Item>
              {getFieldDecorator("adventurer", {
                rules: [
                  {
                    required: true,
                    message: "Merci de remplir ce champs",
                  },
                ],
              })(
                <InputLabel
                  name="adventurer"
                  label="Récompense Aventurier"
                  placeholder="Récompense Aventurier"
                />
              )}
            </Form.Item>
          </Col>
          <Col span={16}>
            <Form.Item>
              {getFieldDecorator("master", {
                rules: [
                  {
                    required: true,
                    message: "Merci de remplir ce champs",
                  },
                ],
              })(
                <InputLabel
                  name="master"
                  label="Récompense maître"
                  placeholder="Récompense maître"
                />
              )}
            </Form.Item>
          </Col>

          <Col span={16} style={{ marginTop: 40 }}>
            <DefaultButton
              color="#7282F7"
              loading={loading}
              onClick={handleSubmit}
            >
              Valider
            </DefaultButton>
          </Col>
        </Row>
      </MainLayout>
    </>
  );
};

export default Form.create()(Awards);
