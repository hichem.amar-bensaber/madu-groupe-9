import React, { useState, useEffect } from "react";
import { MainLayout } from "../../components/MainLayout/MainLayout";
import NavMenu from "../../components/NavMenu/NavMenu";
import ChallengeItem from "../../components/ChallengeItem/ChallengeItem";
import { Row, Col } from "antd";
import DefaultButton from "../../components/_shared/DefaultButton/DefaultButton";
import { EmptyData } from "../../components/_shared/EmptyData/EmptyData";
import "./challenge.scss";
import { useHistory } from "react-router-dom";
import {
  getChallengeList,
  deleteChallengeById,
} from "../../utils/api_challenge";
import moment from "moment";

const Challenge = () => {
  const history = useHistory();
  const [challenges, setChallenges] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    getChallengeList().then(
      (res) => {
        setChallenges(res.data);
        setLoading(false);
      },
      (err) => {
        setLoading(false);
        console.log(err);
      }
    );
  }, []);

  const daysRemaining = (timestamp) => {
    var eventdate = moment(timestamp * 1000);
    var todaysdate = moment();
    return eventdate.diff(todaysdate, "days");
  };

  const onDelete = async (id) => {
    await deleteChallengeById(id);
    const newchallenges = challenges.filter(
      (challenge) => challenge._id !== id
    );
    setChallenges([...newchallenges]);
  };

  return (
    <>
      <NavMenu />
      <MainLayout>
        <div className="challenge">
          <Row type="flex" gutter={32} justify="space-between">
            {!loading &&
              challenges &&
              challenges.map((challenge, id) => (
                <Col key={id} xs={24} sm={12} lg={8} xl={8}>
                  <ChallengeItem
                    name={challenge.name}
                    type={challenge.type}
                    description={challenge.description}
                    points={challenge.points}
                    dayleft={daysRemaining(challenge.duration)}
                    onUpdate={() =>
                      history.push(
                        `/back-office/challenge/update/${challenge._id}`
                      )
                    }
                    onDelete={() => onDelete(challenge._id)}
                  />
                </Col>
              ))}

            <Col xs={24} sm={12} lg={8} xl={8}></Col>
            <Col xs={24} sm={12} lg={8} xl={8}></Col>
            <Col xs={24} sm={12} lg={8} xl={8}></Col>
          </Row>
          <div style={{ position: "fixed", bottom: "24px", right: "30px" }}>
            <DefaultButton
              onClick={() => history.push("/back-office/challenge/awards")}
            >
              configurer les awards
            </DefaultButton>
          </div>
        </div>
        {!challenges && (
          <div className="challenge center">
            <EmptyData description="Aucun Défi" to="/back-office/challenge/add">
              Ajouter un nouveau défi
            </EmptyData>
          </div>
        )}
      </MainLayout>
    </>
  );
};

export default Challenge;
