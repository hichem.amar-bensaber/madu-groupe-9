import React, { useState, useEffect } from "react";
import { getClients } from "../../utils/api";
import { getPoi } from "../../utils/api_poi";
import NavMenu from "../../components/NavMenu/NavMenu";
import MapGL, { Popup } from "react-map-gl";
import PopUpContent from "../../components/PopUpContent/PopUpContent";
import { Markers } from "../../components/Markers/Markers";
import { Input, Select } from "antd";
import ItemSearch from "../../components/_shared/ItemSearch/ItemSearch";
import "./map.scss";
import { REACT_APP_API_URL } from "../../utils/config";

const Map = () => {
  const { Option } = Select;

  const [popupInfo, setPopupInfo] = useState(null);
  const [filter, setFilter] = useState("Tout");
  const [search, setSearch] = useState("");
  const [cities, setCities] = useState([]);

  useEffect(() => {
    Promise.all([getClients(), getPoi()]).then((values) =>
      setCities(
        values.flatMap((item) => {
          return item.data.reduce((acc, currItem) => {
            return [
              ...acc,
              {
                ["name"]: currItem.name,
                ["email"]: currItem.email,
                ["phone"]: currItem.phone,
                ["longitude"]: currItem.coords.longitude,
                ["latitude"]: currItem.coords.latitude,
                ["address"]: `${currItem.localisation.address}, ${currItem.localisation.city}, ${currItem.localisation.zip_code}`,
                ["type"]: currItem.greenscore ? "POI" : "Clients",
                ["img"]: currItem.greenscore
                  ? `${REACT_APP_API_URL}/${currItem.images[0].uri}`
                  : `${REACT_APP_API_URL}/${currItem.profile_pic}`,
              },
            ];
          }, []);
        })
      )
    );
  }, []);

  const citiesFilter = cities.filter((city) => {
    if (filter === "Tout") {
      return true;
    }
    return filter === city.type ? true : false;
  });

  const searchList = citiesFilter.filter((city) => {
    return city.name.toLowerCase().indexOf(search.toLowerCase()) > -1;
  });

  const API_MAP = process.env.REACT_APP_MAP;
  const [viewport, setViewport] = useState({
    center: [48.864716, 2.349014],
    country: "FR",
    height: "100vh",
    width: "100%",
    latitude: 48.864716,
    longitude: 2.32,
    zoom: 11.5,
  });
  const onClickMarker = (city) => {
    setPopupInfo(city);
    const { latitude, longitude } = city;
    setViewport({
      ...viewport,
      latitude: latitude,
      longitude: longitude,
      transitionDuration: 400,
    });
  };
  const onClickItem = (city) => {
    onClickMarker(city);
    setSearch("");
  };
  const RenderPopup = () => {
    return (
      popupInfo && (
        <Popup
          tipSize={5}
          anchor="top"
          longitude={popupInfo.longitude}
          latitude={popupInfo.latitude}
          closeOnClick={false}
          onClose={() => setPopupInfo(null)}
        >
          <PopUpContent
            email={popupInfo.email}
            phone={popupInfo.phone}
            address={popupInfo.address}
            name={popupInfo.name}
            img={popupInfo.img}
          />
        </Popup>
      )
    );
  };
  return (
    <div>
      <NavMenu />
      <div className="map-inputs">
        <Input.Group compact>
          <Select
            style={{ width: 120 }}
            size="large"
            onChange={setFilter}
            defaultValue={filter}
          >
            <Option value="Tout">Tout</Option>
            <Option value="POI">POI</Option>
            <Option value="Clients">Entreprise</Option>
          </Select>
          <Input
            style={{ width: "70%" }}
            size="large"
            placeholder="Trouver un point d'interet"
            value={search}
            onPressEnter={() => onClickItem(searchList[0])}
            onChange={(e) => setSearch(e.target.value)}
          />
          <div className="map-itemcontainer">
            {search !== "" &&
              searchList.map((item) => (
                <ItemSearch
                  onClick={() => onClickItem(item)}
                  name={item.name}
                  img={item.img}
                  address={item.address}
                />
              ))}
          </div>
        </Input.Group>
      </div>
      <div className="map-container">
        <MapGL
          mapStyle="mapbox://styles/thomasfranja/ck66ykftj17b61isd5kxo0x4h"
          mapboxApiAccessToken={API_MAP}
          {...viewport}
          onViewportChange={(viewport) => {
            setViewport(viewport);
          }}
        >
          <Markers data={citiesFilter} onClick={onClickMarker} />
          {RenderPopup()}
        </MapGL>
      </div>
    </div>
  );
};

export default Map;
