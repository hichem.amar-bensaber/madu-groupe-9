import React, { useState, useEffect } from "react";
import { Row, Col, Input, Button, Form, notification } from "antd";
import { MainLayout } from "../components/MainLayout/MainLayout";
import { MainTitle } from "../components/_shared/MainTitle/MainTitle";
import { InputLabel } from "../components/_shared/InputLabel/InputLabel";
import Avatar from "../components/_shared/Avatar/Avatar";
import NavMenu from "../components/NavMenu/NavMenu";
import { addClient } from "../utils/api";

const CustomersAdd = ({ form }) => {
  const [loading, setLoading] = useState(false);
  const [profileImage, setProfileImage] = useState(null);
  const {
    getFieldDecorator,
    getFieldsError,
    getFieldError,
    isFieldTouched,
  } = form;

  // To disable submit button at the beginning.
  useEffect(() => {
    form.validateFields();
  }, []);

  const hasErrors = (fieldsError) => {
    return Object.keys(fieldsError).some((field) => fieldsError[field]);
  };

  const handleImage = (image) => {
    setProfileImage({ ["uri"]: image });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    form.validateFields(async (err, values) => {
      const { city, address, zip_code, ...rest } = values;
      const localisation = { city, address, zip_code };
      const data = {
        create: { localisation, profile_pic: profileImage, ...rest },
      };
      setLoading(true);
      try {
        await addClient(data);
        notification.info({
          message: `Ajout effectué`,
          description: `Le client ${values.name} a été créé.`,
        });
        form.resetFields();
      } catch {
        notification.error({
          message: `Une erreur est survenue`,
          description: `Veuillez recommencer.`,
        });
      } finally {
        setLoading(false);
      }
    });
  };
  // Only show error after a field is touched.
  const nameError = isFieldTouched("name") && getFieldError("name");
  const zip_codeError = isFieldTouched("zip_code") && getFieldError("zip_code");
  const addressError = isFieldTouched("address") && getFieldError("address");
  const cityError = isFieldTouched("city") && getFieldError("city");
  const phoneError = isFieldTouched("phone") && getFieldError("phone");
  const emailError = isFieldTouched("email") && getFieldError("email");

  return (
    <>
      <NavMenu />
      <MainLayout>
        <Form>
          <Row type="flex" justify="center">
            <Col span={16}>
              <div style={{ marginBottom: 40 }}>
                <MainTitle text="ajouter un client" />
              </div>
              <Row type="flex" justify="space-between" gutter={[24, 0]}>
                <Col span={1}>
                  <Avatar name="icon" handleImage={handleImage} />
                </Col>
                <Col span={20} style={{ marginBottom: 21 }}>
                  <Form.Item
                    validateStatus={nameError ? "error" : ""}
                    help={nameError || ""}
                  >
                    {getFieldDecorator("name", {
                      rules: [
                        {
                          required: true,
                          message: "Merci d'entrer un nom d'utilisateur",
                        },
                      ],
                    })(
                      <InputLabel
                        name="name"
                        label="nom du client"
                        placeholder="Nom du client"
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Form.Item
                    validateStatus={cityError ? "error" : ""}
                    help={cityError || ""}
                  >
                    {getFieldDecorator("city", {
                      rules: [
                        {
                          required: true,
                          message: "Merci d'entrer une Ville",
                        },
                      ],
                    })(
                      <InputLabel
                        name="city"
                        label="ville"
                        placeholder="Ville"
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col span={16}>
                  <Form.Item
                    validateStatus={addressError ? "error" : ""}
                    help={addressError || ""}
                  >
                    {getFieldDecorator("address", {
                      rules: [
                        {
                          required: true,
                          message: "Merci d'entrer une adresse",
                        },
                      ],
                    })(
                      <InputLabel
                        name="address"
                        label="adresse"
                        placeholder="adresse"
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item
                    validateStatus={zip_codeError ? "error" : ""}
                    help={zip_codeError || ""}
                  >
                    {getFieldDecorator("zip_code", {
                      rules: [
                        {
                          required: true,
                          message: "Merci d'entrer un code postal",
                        },
                      ],
                    })(
                      <InputLabel
                        name="zip_code"
                        label="code postal"
                        placeholder="Code Postal"
                        type="number"
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    validateStatus={emailError ? "error" : ""}
                    help={emailError || ""}
                  >
                    {getFieldDecorator("email", {
                      rules: [
                        {
                          required: true,
                          message: "Merci d'entrer un email",
                        },
                      ],
                    })(
                      <InputLabel
                        name="email"
                        label="Email"
                        placeholder="Email"
                        type="email"
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    validateStatus={phoneError ? "error" : ""}
                    help={phoneError || ""}
                  >
                    {getFieldDecorator("phone", {
                      rules: [
                        {
                          required: true,
                          message: "Merci d'entrer un numéro de téléphone",
                        },
                      ],
                    })(
                      <InputLabel
                        name="phone"
                        label="Numéro"
                        placeholder="Numéro"
                        type="number"
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Form.Item>
                    <Button
                      htmlType="submit"
                      disabled={hasErrors(form.getFieldsError()) || loading}
                      size="large"
                      block
                      style={{ marginTop: 16 }}
                      loading={loading}
                      onClick={handleSubmit}
                    >
                      {loading ? "Chargement" : "Valider"}
                    </Button>
                  </Form.Item>
                </Col>
              </Row>
            </Col>
          </Row>
        </Form>
      </MainLayout>
    </>
  );
};

export default Form.create()(CustomersAdd);
