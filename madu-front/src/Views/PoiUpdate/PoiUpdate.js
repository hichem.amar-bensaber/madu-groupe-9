import React, { useState, useEffect } from "react";
import NavMenu from "../../components/NavMenu/NavMenu";
import { MainLayout } from "../../components/MainLayout/MainLayout";
import PoiLayout from "../../components/PoiLayout/PoiLayout";
import { useParams } from "react-router-dom";
import { getPoiById, updatePoiById } from "../../utils/api_poi";
import { DatabaseTwoTone } from "@ant-design/icons";

const PoiUpdate = () => {
  const { id } = useParams();
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getPoiById(id).then((res) => {
      setData({ ...res.data });
      setLoading(false);
    });
  }, []);

  return (
    <>
      <NavMenu />
      <MainLayout>
        {!loading && (
          <PoiLayout
            title={"Modifier le point d'intérêt"}
            data={data}
            action={updatePoiById}
          />
        )}
      </MainLayout>
    </>
  );
};

export default PoiUpdate;
