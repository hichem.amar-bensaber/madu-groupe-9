import React, { useState, useEffect } from "react";
import { MainLayout } from "../../components/MainLayout/MainLayout";
import CustomerCard from "../../components/_shared/CustomerCard/CustomerCard";
import NavMenu from "../../components/NavMenu/NavMenu";
import { connect } from "react-redux";
import { getClients } from "../../utils/api";
import { EmptyData } from "../../components/_shared/EmptyData/EmptyData";
import * as actions from "../../action/index";
import { Spin } from "antd";
import "./customers.scss";

const Customers = ({
  customers,
  editCustomerProperty,
  validateEdit,
  addCustomers,
}) => {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getClients()
      .then((res) => {
        addCustomers(res.data);
        setLoading(false);
      })
      .catch((e) => {
        addCustomers([]);
        setLoading(false);
      });
  }, []);

  const mapCustomerCard = customers.map((customer, key) => (
    <div key={key}>
      <CustomerCard
        data={customer}
        onChange={editCustomerProperty}
        validateEdit={validateEdit}
      />
    </div>
  ));

  return (
    <>
      <NavMenu />
      <MainLayout>
        {loading ? (
          <div className="customers center">
            <Spin size="large" />
          </div>
        ) : !loading && customers.length ? (
          <>{mapCustomerCard}</>
        ) : (
          <div className="customers center">
            <EmptyData
              description="Aucun Client"
              to="/back-office/customers/add"
            >
              Ajouter un nouveau client
            </EmptyData>
          </div>
        )}
      </MainLayout>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    customers: state.customers,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    editCustomerProperty: (event) =>
      dispatch(actions.editCustomerProperty(event)),
    addCustomers: (data) => dispatch(actions.addCustomers(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Customers);
