### Front de Madu

<img src="https://skillvalue.com/jobs/wp-content/uploads/sites/7/2019/04/d%C3%A9veloppeur-front-end-reactjs-redux-mission-freelance-paris.png" width="200">
<img src="https://avatars2.githubusercontent.com/u/26566442?s=460&v=4" width="100">



# ReactJS

Nous avons choisi d'intégrer ReactJS au sein de notre projet simplement parce qu'il nous est déjà familier, pouvoir aussi s'améliorer pour faire évoluer nos compétences.
De plus, avec React il est beaucoup plus facile d'utiliser Redux qu'avec un autre framework.
Par ailleurs, nous voulions intégrer le projet avec de la programmation fonctionnelles, depuis les hooks avec React nous pouvons programmer en fonctionnel et non pas avec des classes contrairement à Angular.
Cela nous permettra également de gérer les tests plus facilement.

## Pourquoi ne pas avoir utilisé le context "Provider", "Consumer" ?

Nous avons opté de ne pas utilisé le context simplement pour pouvoir se challenger sur Redux.



## Redux

Nous avons choisis d'utiliser Redux afin de stocker toutes nos datas de notre projet, et également nos fonctions dans un fichier /reducer/data.js.
Grâce à Redux nous avons pu manipûler, faire un appel à la/les data/datas souhaitée/souhaitées nos datas pour pouvoir intéragir le store(une bibliothèque de tous les états du projets). 
Pouvoir se challenger, découvrir d'avantage le fonctionnement de la librairie.



## Notre librairie UI / Interface graphique

Nous avons opté pour la librairie Ant.design [Ant library](https://ant.design/) car elle correspond et se rapproche au design de nos maquettes créer au préalable par nos UI/UX designer.
Dans la librairie, nous avons pu réutilisé les composants fonctionnels puis définir leurs propriétés respectifs pour les adaptés à nos datas pour le projet. 
Chaque composant est fonctionnel et spécifié par un nom respectif propre pour la bonne compréhension.



## Les Hooks / UseState / UseEffect

Nous utiliserons dans ce projet les Hooks fonctionnels afin de définir en un premier temps l'état initié avant de l'update grâce à son setState passée en paramètre puis l'appeler pour monter le composant avec son état à modifier.


## L'usage de l'API

Dans un premier temps nous avons utilisé la methode fetch() pour faire les requêtes à l'API.
A l'authentification, l'utilisateur à accès au back-office grâce à un token d'authorisation renvoyé par le back et géneré au moment où il se connecte, juste pour une question de sécurité. 
Grâce à ce token il pourra ainsi consummer l'API.

À l'avenir, nous mettrons en place un système de redirection dans le cas où l'utilisateur n'a pas génerée son token d'authorisation.

